package com.lemonet.dreamball.admin.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.MatchDAO;

@Controller
@Secured("ROLE_ADMIN")
public class IndexAdminController {
	@Autowired LeagueDAO ldao;
	@Autowired MatchDAO mdao;

	/**
	 * Admin dashboard
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/admin/index.html")
	public String showDashboard(ModelMap model)  {
		List<League> leagues = ldao.getLeagues();
		model.addAttribute("leagues", leagues);  
//		Round r = leagues.get(0).getFirstUnfinishedRound();
//		model.addAttribute("matches", mdao.getMatches(leagues.get(0), 2013L, r.getNumber(), r.getNumber()));  

		return "admin/index";
	}

/*	@Secured("ROLE_USER")
	@RequestMapping("/admin/{leagueId}/simulate.html")
	public String simulateRound(@PathVariable Long leagueId, ModelMap model)  {
		Simulator simulator = new Simulator();
		RoundDAO rdao = new RoundDAOImpl();		
		LeagueDAO ldao = new LeagueDAOImpl();
		League league = ldao.getLeagueById(leagueId);
			
		// Get next round
		Round round = rdao.getFirstUnfinishedRound(league);
		if (round != null){
			round.setStatus(Round.Status.FINISHED);
			rdao.update(round);
			simulator.simulateRound(round);
			Engine.distributePrizes();
	}
		
		return "redirect:/admin/index.html";
	}*/
}