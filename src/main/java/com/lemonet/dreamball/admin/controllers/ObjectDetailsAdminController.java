package com.lemonet.dreamball.admin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.finance.domain.OperationDAO;

@Controller
@Secured("ROLE_ADMIN")
public class ObjectDetailsAdminController {
	@Autowired OperationDAO odao;
	@Autowired PlayerDAO pdao;
	@Autowired VirtualTeamDAO vtdao;
	@Autowired VirtualLeagueDAO vldao;
	
	/**
	 * Shows details of a given player
	 * 
	 * @param model
	 * @param playerId
	 * @return
	 */
	@RequestMapping("/admin/player/{playerId}/details.html")
	public String showDashboard(ModelMap model, @PathVariable Long playerId)  {
		Player player = null;
		player = pdao.getPlayerById(playerId);
		model.addAttribute("operations", odao.getOperations(player));
		model.addAttribute("player", player);
		model.addAttribute("virtualTeams", vtdao.getTeamsByPlayer(player));
		
		return "admin/player/details";
	}

	/**
	 * Shows details of a given virtual team
	 * 
	 * @param model
	 * @param virtualTeamId
	 * @return
	 */
	@RequestMapping("/admin/vteam/{virtualTeamId}/details.html")
	public String virtualTeamDetails(ModelMap model, @PathVariable Long virtualTeamId)  {
		VirtualTeam vteam = null;
		vteam = vtdao.getVirtualTeamById(virtualTeamId);
		model.addAttribute("vteam", vteam);
		return "admin/vteam/details";
	}

	/**
	 * Shows details of a given virtual team
	 * 
	 * @param model
	 * @param virtualTeamId
	 * @return
	 */
	@RequestMapping("/admin/team/{teamId}/details.html")
	public String teamDetails(ModelMap model, @PathVariable Long teamId)  {
		VirtualTeam vteam = vtdao.getVirtualTeamById(teamId);
		model.addAttribute("vteam", vteam);
		return "admin/vteam/details";
	}

	/**
	 * Shows details of a given virtual team
	 * 
	 * @param model
	 * @param virtualTeamId
	 * @return
	 */
	@RequestMapping("/admin/vleague/{virtualLeagueId}/details.html")
	public String virtualLeagueDetails(ModelMap model, @PathVariable Long virtualLeagueId)  {
		VirtualLeague vleague = vldao.getLeagueById(virtualLeagueId);
		model.addAttribute("vleague", vleague);
		return "admin/vleague/details";
	}
}
