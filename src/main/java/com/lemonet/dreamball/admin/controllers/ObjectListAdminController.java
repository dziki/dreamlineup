package com.lemonet.dreamball.admin.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.realworld.domain.Athlete;
import com.lemonet.dreamball.realworld.domain.AthleteDAO;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.Performance;
import com.lemonet.dreamball.realworld.domain.PerformanceDAO;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;

@Controller
@Secured("ROLE_ADMIN")
public class ObjectListAdminController {
//	@RequestMapping("/admin/list.html")
//	public String showDashboard(ModelMap model)  {
//		return "admin/list";
//	}
	@Autowired PlayerDAO pdao;
	@Autowired VirtualLeagueDAO vldao;
	@Autowired LeagueDAO ldao;
	@Autowired VirtualTeamDAO vtdao;
	@Autowired TeamDAO tdao;
	@Autowired AthleteDAO adao;
	@Autowired PerformanceDAO perdao;

	@RequestMapping("/admin/player/list.html")
	public String listPlayers(ModelMap model)  {
		List<Player> objects = pdao.list();
		model.addAttribute("objects", objects);
		return "admin/player/list";
	}

	@RequestMapping("/admin/vleague/list.html")
	public String listVirtualLeagues(ModelMap model)  {
		
		List<VirtualLeague> objects = vldao.getLeagues();
		model.addAttribute("objects", objects);
		return "admin/vleague/list";
	}

	@RequestMapping("/admin/vteam/list.html")
	public String listVirtualTeams(ModelMap model)  {
		
		List<VirtualTeam> objects = vtdao.getActiveVirtualTeams();
		model.addAttribute("objects", objects);
		return "admin/vteam/list";
	}

	@RequestMapping("/admin/team/list.html")
	public String listTeams(ModelMap model)  {
		List<Team> objects = tdao.getTeams();
		model.addAttribute("objects", objects);
		return "admin/team/list";
	}

	@RequestMapping("/admin/athlete/list.html")
	public String listAthletes(ModelMap model)  {
		League l = ldao.getLeagues().get(0);
		List<Athlete> objects = adao.getAthletsWithPerformance(l);
		model.addAttribute("objects", objects);
		return "admin/athlete/list";
	}

	@RequestMapping("/admin/performance/list.html")
	public String listPerformance(ModelMap model,
			@RequestParam(value="rid") Long rid)  {
		if (rid == null)
			rid = 1L;
		List<Performance> objects = perdao.getByRound(rid);
		model.addAttribute("objects", objects);
		return "admin/performance/list";
	}

	@RequestMapping("/admin/team/{teamId}/athlete/list.html")
	public String listAthlete(ModelMap model, @PathVariable Long teamId)  {
		List<Athlete> objects = adao.getTeamAthletes(teamId);
		model.addAttribute("objects", objects);
		return "admin/athlete/list";
	}

}