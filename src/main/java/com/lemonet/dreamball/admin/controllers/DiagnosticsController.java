package com.lemonet.dreamball.admin.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lemonet.dreamball.dataprovider.AthleteHandler;
import com.lemonet.dreamball.dataprovider.DataGenerator;
import com.lemonet.dreamball.dataprovider.MatchHandler;
import com.lemonet.dreamball.dataprovider.StatisticHandler;
import com.lemonet.dreamball.dataprovider.TeamHandler;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.service.DataGrabberService;
import com.lemonet.dreamball.service.DreamworldService;
import com.lemonet.dreamball.service.LeagueMaintenanceService;
import com.lemonet.notification.domain.Email;
import com.lemonet.notification.service.EmailService;

@Controller
@RequestMapping("/admin/diagnostics")
public class DiagnosticsController {
	@Autowired private MatchDAO mdao;
	@Autowired private LeagueDAO ldao;

	@Autowired MatchHandler matchHandler;
	@Autowired StatisticHandler statisticHandler;
	@Autowired TeamHandler teamHandler;
	@Autowired AthleteHandler athleteHandler;
	
	@Autowired private DataGrabberService dataGrabberService;
	@Autowired private LeagueMaintenanceService leagueMaintenanceService;
	@Autowired private EmailService emailService;
	@Autowired DreamworldService dreamService;
	@Autowired DataGenerator dataGenerator;
	
	@RequestMapping("/load.data.html")
    public String loadData(Model model) throws Exception {
		dreamService.loadData();
		return "redirect:/rules.html";
	}

	@RequestMapping("/load.athletes.html")
    public String loadAthletes(Model model) throws Exception {
		dreamService.loadAthletes();
		return "redirect:/rules.html";
	}

	
	@RequestMapping("/test-error.html")
	public String showDepositForm(ModelMap model) throws Exception  {
		throw new Exception("UNHANDLED EXCEPTION TEST");
	}

	@RequestMapping("/update-matches.html")
	public void updateMatches(ModelMap model) throws Exception  {
		League l = ldao.getLeagueById(Constants.CURRENT_LEAGUE);
		List<Match> matches = mdao.getMatches(l, Constants.CURRENT_SEASON, 1L, 1L);
		for (Match m : matches){
			dataGrabberService.downloadResult(m.getOutsideId());
			leagueMaintenanceService.parseResults();
			break;
		}	
	}

	@RequestMapping("/download-results.html")
	public void downloadResults(ModelMap model) throws Exception  {
		leagueMaintenanceService.updateResults();
	}
	@RequestMapping("/download-result.html")
	public void downloadResults(ModelMap model, @RequestParam(value = "mid") Long mid) throws Exception  {
		leagueMaintenanceService.updateResult(mid);
	}
	@RequestMapping("/finish-leagues.html")
	public void finishLeagues(ModelMap model) throws Exception  {
		dreamService.finishRounds();
	}
	@RequestMapping("/distribute-prizes.html")
	public void distributePrizes(ModelMap model) throws Exception  {
		dreamService.distributePrizes();
	}
	@RequestMapping("/cancel-leagues.html")
	public void cancelLeagues(ModelMap model) throws Exception  {
		dreamService.cancelLeagues();
	}
	@RequestMapping("/reimburse.html")
	public void reimburseMainLeague(ModelMap model) throws Exception  {
		dreamService.reimburseMainLeague();
	}
	@RequestMapping("/generate-emails.html")
	public void sendRoundFinishedEmail(ModelMap model,
			@RequestParam(value = "rid") Long rid) throws Exception  {
		leagueMaintenanceService.generateRoundEmails(rid);
	}
	@RequestMapping("/send-emails.html")
	public void sendQueuedEmails(ModelMap model) throws Exception  {
		leagueMaintenanceService.sendQueuedEmails();
	}
	@RequestMapping("/opdate-all-matches.html")
	public void updateAllGames(ModelMap model) throws Exception  {
		League l = ldao.getLeagueById(Constants.CURRENT_LEAGUE);
		List<Match> matches = mdao.getMatches(l, Constants.CURRENT_SEASON, 1L, 21L);
		
		int counter = 0;
		for (Match m : matches){
			counter++;
			if (counter > 10)
				break;
	        URL url;

	        try {
	            // get URL content

	            String a = "http://dreamlineup.com/diagnostics/download-result.html?mid=" + m.getId();
	            url = new URL(a);
	            URLConnection conn = url.openConnection();

	            // open the stream and put it into BufferedReader
	            BufferedReader br = new BufferedReader(
	                               new InputStreamReader(conn.getInputStream()));

	            String inputLine;
	            while ((inputLine = br.readLine()) != null) {
	                    System.out.println(inputLine);
	            }
	            br.close();

	            System.out.println("Done");

	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

		}
	}

	@RequestMapping("/test-email.html")
	public void testEmail(ModelMap model) throws Exception  {
		VelocityContext context = new VelocityContext();
		context.put("registrationLink","REGISTRATION LINK - Rejestracja Łączeń źdźbło żółwić Órę.");
		String msg = EmailService.fillOutTemplate("velocity/registration_pl.jsp", context);
		String to = "dziki.dziurkacz@gmail.com";
		Email email = new Email(to,"Rejestracja Łączeń źdźbło żółwić Órę.", msg);
		emailService.queue(email);
		emailService.send(email);
	}

	@RequestMapping("/update-dates.html")
	public void updateDates(ModelMap model) throws Exception  {
		leagueMaintenanceService.updateDates();
	}

	@RequestMapping("/copy-teams.html")
	public void copyTeams(ModelMap model) throws Exception  {
		dreamService.resubscribeToMain();
	}
}