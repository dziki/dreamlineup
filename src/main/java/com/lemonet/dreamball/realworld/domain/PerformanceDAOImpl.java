package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lemonet.dreamball.engine.Constants;

@Repository("performanceDAO")
public class PerformanceDAOImpl implements PerformanceDAO {
	@Autowired
	SessionFactory sessionFactory;

	// TODO SHOUDNT BE HERE
	public Long evaluate(Performance performance) {
		Long res = new Long(0);
		if (performance.getTimeOnPitch(90L) > 60)
			res += 2;
		else if (performance.getTimeOnPitch(90L) <= 60)
			res += 1;
		
		int multiplier = 6;
		if (performance.getPosition().equalsIgnoreCase("M"))
			multiplier = 5;
		if (performance.getPosition().equalsIgnoreCase("S"))
			multiplier = 4;
		res += performance.getGoals()*multiplier;
//		res += performance.getAssists()*3;

		if (performance.getPosition().equalsIgnoreCase("G"))
			res += performance.getCleanSheet() * 4;
		if (performance.getPosition().equalsIgnoreCase("D"))
			res += performance.getCleanSheet() * 4;
		if (performance.getPosition().equalsIgnoreCase("M"))
			res += performance.getCleanSheet() * 1;

	//	res += performance.getPenaltiesDefended() * 5;
	//	res += performance.getShotsDefended() / 3;
	//	res += performance.getMissedPenalties() * (-2);

		if (performance.getPosition().equalsIgnoreCase("G") || performance.getPosition().equalsIgnoreCase("D"))
			res += - performance.getGoalsConceded() / 2;
		Long cards = new Long(0);
		cards += performance.getYellowCard() * (1);
		cards += performance.getRedCard() * (3);
		if (cards > 4L)
			cards = 4L;
		res -= cards;
		res += performance.getOwnGoals() * (-2);

		return res;
	}

	@Override
    public void create(Performance performance){
		Logger logger = Logger.getLogger("PARSER");
		Long points = evaluate(performance);
		performance.setPoints(points);
		if (performance.getAthlete() == null){
			logger.error("PARSER: Athlete not found");
			// TODO
			return;
		}
		if (this.getByAthleteMatch(performance.getAthlete(), performance.getMatch()) == null)
			this.update(performance);
		else{
			logger.error("PARSER: Performance already added");
			// TODO
		}
    }

	@Override
    public void update(Performance performance){
    	Session session = sessionFactory.getCurrentSession();
    	session.saveOrUpdate(performance);
    }

	@Override
	public Performance getByAthleteMatch(Athlete a, Match match) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Performance.class)
			.add(Restrictions.eq("athlete", a))
			.add(Restrictions.eq("match", match));
		List<Performance> l = criteria.list();
		if (l.size() == 0)
			return null;
    	return l.get(0);
	}

	@Override
	public List<Performance> getByRound(Long roundId) {
    	Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select p "
				+ "from Performance p "
				+ "where p.match.round = :rid and " +
				"p.match.season = " + Constants.CURRENT_SEASON);
		query.setParameter("rid", roundId);
		@SuppressWarnings("unchecked")
		List<Performance> l = query.list();
		return l;
	}

	@Override
	public void remove(Performance performance) {
    	Session session = sessionFactory.getCurrentSession();
		session.delete(performance);
	}
}
