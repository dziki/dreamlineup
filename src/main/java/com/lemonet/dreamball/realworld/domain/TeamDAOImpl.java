package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("teamDAO")
public class TeamDAOImpl implements TeamDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Team getTeamById(Long id) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Team.class)
			.add(Restrictions.eq("id", id));
    	return (Team) criteria.uniqueResult();
	}

	@Override
    public void create(Team team){
    	Session session = sessionFactory.getCurrentSession();
    	session.save(team);
    }

	@Override
    public void update(Team team){
    	Session session = sessionFactory.getCurrentSession();
    	session.update(team);
    }

	@Override
	public List<Team> getTeams() {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Team.class)
    	.addOrder( Property.forName("name").asc() );
    	@SuppressWarnings("unchecked")
		List<Team> teams = criteria.list();
	    return teams;
	}

	@Override
	public Team getTeamByOutsideId(Long teamOutsideId) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Team.class)
			.add(Restrictions.eq("outsideId", teamOutsideId));
    	return (Team) criteria.uniqueResult();
	}

	@Override
	public List<Team> getTeams(League baseLeague) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Team.class)
			.add(Restrictions.eq("league", baseLeague));
		List<Team> teams = criteria.list();
	    return teams;
	}

}
