package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;

@Repository("roundDAO")
public class RoundDAOImpl implements RoundDAO {
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	MatchDAO mdao;

	@Override
    public void create(Round round){
    	Session session = sessionFactory.getCurrentSession();
    	session.save(round);
    }

	@Override
    public void update(Round round){
		Session session = sessionFactory.getCurrentSession();
    	session.saveOrUpdate(round);
    }

	@Override
	public Round getRound(Long season, League league, Long number) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Round.class)
			.add(Restrictions.eq("season", season))
			.add(Restrictions.eq("league", league))
			.add(Restrictions.eq("number", number));
    	return (Round) criteria.uniqueResult();
	}

	@Override
	public Round getFirstUnfinishedRound(League league) {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select rd " +
    			"from Round rd " +
    			"where rd.status != :status " +
    			"and rd.league = :league " +
    			"order by rd.season asc, rd.number asc");
    	query.setParameter("league", league);
    	query.setParameter("status", Round.FINISHED);
    	query.setMaxResults(1);

    	return (Round) query.uniqueResult();
	}

	@Override
	public Round getFirstUnstartedRound(League league) {
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select rd " +
    			"from Round rd " +
    			"where rd.status = :status " +
    			"and rd.league = :league " +
    			"order by rd.season asc, rd.number asc");
    	query.setParameter("league", league);
    	query.setParameter("status", Round.UNSTARTED);
    	query.setMaxResults(1);

    	return (Round) query.uniqueResult();
	}

	@Override
	public Round getRoundById(Long id) {
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select rd " +
    			"from Round rd " +
    			"where rd.id = :id ");
    	query.setParameter("id", id);
		return (Round)query.uniqueResult();
	}

	@Override
	public List<Round> getRounds(VirtualLeague vleague) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Round.class)
			.add(Restrictions.eq("season", vleague.getSeason()))
			.add(Restrictions.eq("league", vleague.getBaseLeague()))
			.add(Restrictions.eq("number", new Long(vleague.getStartingRound())))
			.addOrder(Order.asc("number"));
    	criteria.setMaxResults(vleague.getDuration());
 
    	@SuppressWarnings("unchecked")
		List<Round> rounds = criteria.list();
    	for (Round r : rounds){
    		r.setMatches(mdao.getMatches(r.getLeague(), r.getSeason(), r.getNumber(), r.getNumber()));
    	}
    	return rounds;
	}
}
