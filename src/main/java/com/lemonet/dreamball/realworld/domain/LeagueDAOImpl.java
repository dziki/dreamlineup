package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository("leagueDAO")
public class LeagueDAOImpl implements LeagueDAO {
	@Autowired
	SessionFactory sessionFactory;

	/**
	 * @uml.property  name="session"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="com.lemonet.dreamball.realworld.domain.League"
	 */
	@Override
	@Transactional
    public List<League> getLeagues(){
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(League.class);
    	@SuppressWarnings("unchecked")
    	List<League> vLeagues = criteria.list();
        return vLeagues;
    }

	@Override
	public void create(League league) {
		this.update(league);
	}

	@Override
	public void update(League league) {
		Session session = sessionFactory.getCurrentSession();
    	session.saveOrUpdate(league);
	}

	@Override
	public League getLeagueById(Long id) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(League.class)
    		.add(Restrictions.eq("id", id));
    	return (League) criteria.uniqueResult();
	}
}