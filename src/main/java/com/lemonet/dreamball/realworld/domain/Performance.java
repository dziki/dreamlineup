package com.lemonet.dreamball.realworld.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

@Entity
@Table(name = "realworld__performance",
		uniqueConstraints = {@UniqueConstraint(columnNames={"athlete_id", "match_id"})})
public class Performance {
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

	/**
	 * @uml.property  name="athlete"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@JoinColumn(name = "athlete_id", nullable = false)
	private Athlete athlete;

	/**
	 * @uml.property  name="match"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@JoinColumn(name = "match_id", nullable = false)
	private Match match;

	/**
	 * @uml.property  name="position"
	 */
	@NotNull
	private String position;
	/**
	 * @uml.property  name="points"
	 */
	@NotNull
	private Long points = new Long(0);
	/**
	 * @uml.property  name="goals"
	 */
	@NotNull
	private Long goals = new Long(0);
	/**
	 * @uml.property  name="minutesPlayed"
	 */
	@NotNull
	private Long minutesPlayed = new Long(0);

	/**
	 * @uml.property  name="startingLinup"
	 */
	@NotNull
	private Long startingLinup = new Long(0);
	/**
	 * @uml.property  name="secondLinup"
	 */
	@NotNull
	private Long secondLinup = new Long(0);

	/**
	 * @uml.property  name="assists"
	 */
	@NotNull
	private Long assists = new Long(0);
	/**
	 * @uml.property  name="withoutLostGoals"
	 */
	@NotNull
	private Long withoutLostGoals = new Long(0);
	/**
	 * @uml.property  name="penaltiesDefended"
	 */
	@NotNull
	private Long penaltiesDefended = new Long(0);
	/**
	 * @uml.property  name="penaltiesReceived"
	 */
	@NotNull
	private Long penaltiesReceived = new Long(0);

	/**
	 * @uml.property  name="yellowCard"
	 */
	@NotNull
	private Long yellowCard = new Long(0);
	/**
	 * @uml.property  name="redCard"
	 */
	@NotNull
	private Long redCard = new Long(0);

	/**
	 * @uml.property  name="lostGoals"
	 */
	@NotNull
	private Long lostGoals = new Long(0);
	/**
	 * @uml.property  name="lostPenalties"
	 */
	@NotNull
	private Long lostPenalties = new Long(0);
	/**
	 * @uml.property  name="ownGoals"
	 */
	@NotNull
	private Long ownGoals = new Long(0);
	/**
	 * @uml.property  name="goalsConceded"
	 */
	@NotNull
	private Long goalsConceded = new Long(0);

	/**
	 * @uml.property  name="causedPenalties"
	 */
	@NotNull
	private Long causedPenalties = new Long(0);
	/**
	 * @uml.property  name="shotsDefended"
	 */
	@NotNull
	private Long shotsDefended = new Long(0);

	@NotNull
	private Long cleanSheet = new Long(0);

	/**
	 * @uml.property  name="missedPenalties"
	 */
	@NotNull
	private Long missedPenalties = new Long(0);

	@NotNull
	private Long timeIn = new Long(0);	// when did he got on the pitch

	/**
	 * @uml.property  name="offTime"
	 */
	@NotNull
	private Long timeOut = new Long(90);	// when did he got out of the pitch


	public Long getGoalsConceded() {
		Long res = 0L;
		
		String goals;
		if (this.getAway() == 0L){
			goals = this.match.getGoalTimesAway();
		}
		else{
			goals = this.match.getGoalTimesHome();
		}
		String[] A = goals.split(",");
		try{
			for (String s : A){
				if (s.length() == 0){
					continue;
				}
				Long time = Long.parseLong(s);
				if (time > 90L)
					time = 90L;
				if (this.timeIn <= time && this.timeOut >= time)
					res += 1L; 
			}
		}
		catch (java.lang.NumberFormatException notANumber){
			
		}
		
		return res;

	}
	public Integer getAway() {
		return match.getAwayTeam().getId().equals(athlete.getTeam().getId()) ? 1 : 0;
	}
	public Long getShotsDefended() {
		return shotsDefended;
	}
	public void setShotsDefended(Long shotsDefended) {
		this.shotsDefended = shotsDefended;
	}

	public Long getCleanSheet() {
		if (this.getTimeOnPitch(90L) < 60L)
			return 0L;
		if (this.getGoalsConceded() > 0L)
			return 0L;
		return 1L;
	}

	public Long getLong() {
		return this.id;
	}
	public void setLong(Long id) {
		this.id = id;
	}
	/**
	 * @return
	 * @uml.property  name="athlete"
	 */
	public Athlete getAthlete() {
		return athlete;
	}
	/**
	 * @param athlete
	 * @uml.property  name="athlete"
	 */
	public void setAthlete(Athlete athlete) {
		this.athlete = athlete;
	}
	/**
	 * @return
	 * @uml.property  name="match"
	 */
	public Match getMatch() {
		return match;
	}
	/**
	 * @param match
	 * @uml.property  name="match"
	 */
	public void setMatch(Match match) {
		this.match = match;
	}
	/**
	 * @return
	 * @uml.property  name="position"
	 */
	public String getPosition() {
		return position;
	}
	/**
	 * @param position
	 * @uml.property  name="position"
	 */
	public void setPosition(String position) {
		this.position = position;
	}
	/**
	 * @return
	 * @uml.property  name="goals"
	 */
	public Long getGoals() {
		return goals;
	}
	/**
	 * @param goals
	 * @uml.property  name="goals"
	 */
	public void setGoals(Long goals) {
		this.goals = goals;
	}
	/**
	 * @return
	 * @uml.property  name="startingLinup"
	 */
	public Long getStartingLinup() {
		return startingLinup;
	}
	/**
	 * @param startingLinup
	 * @uml.property  name="startingLinup"
	 */
	public void setStartingLinup(Long startingLinup) {
		this.startingLinup = startingLinup;
	}
	/**
	 * @return
	 * @uml.property  name="secondLinup"
	 */
	public Long getSecondLinup() {
		return secondLinup;
	}
	/**
	 * @param secondLinup
	 * @uml.property  name="secondLinup"
	 */
	public void setSecondLinup(Long secondLinup) {
		this.secondLinup = secondLinup;
	}
	/**
	 * @return
	 * @uml.property  name="assists"
	 */
	public Long getAssists() {
		return assists;
	}
	/**
	 * @param assists
	 * @uml.property  name="assists"
	 */
	public void setAssists(Long assists) {
		this.assists = assists;
	}
	/**
	 * @return
	 * @uml.property  name="withoutLostGoals"
	 */
	public Long getWithoutLostGoals() {
		return withoutLostGoals;
	}
	/**
	 * @param withoutLostGoals
	 * @uml.property  name="withoutLostGoals"
	 */
	public void setWithoutLostGoals(Long withoutLostGoals) {
		this.withoutLostGoals = withoutLostGoals;
	}
	/**
	 * @return
	 * @uml.property  name="penaltiesDefended"
	 */
	public Long getPenaltiesDefended() {
		return penaltiesDefended;
	}
	/**
	 * @param penaltiesDefended
	 * @uml.property  name="penaltiesDefended"
	 */
	public void setPenaltiesDefended(Long penaltiesDefended) {
		this.penaltiesDefended = penaltiesDefended;
	}
	/**
	 * @return
	 * @uml.property  name="penaltiesReceived"
	 */
	public Long getPenaltiesReceived() {
		return penaltiesReceived;
	}
	/**
	 * @param penaltiesReceived
	 * @uml.property  name="penaltiesReceived"
	 */
	public void setPenaltiesReceived(Long penaltiesReceived) {
		this.penaltiesReceived = penaltiesReceived;
	}
	/**
	 * @return
	 * @uml.property  name="yellowCard"
	 */
	public Long getYellowCard() {
		return yellowCard;
	}
	/**
	 * @param yellowCard
	 * @uml.property  name="yellowCard"
	 */
	public void setYellowCard(Long yellowCard) {
		this.yellowCard = yellowCard;
	}
	/**
	 * @return
	 * @uml.property  name="redCard"
	 */
	public Long getRedCard() {
		return redCard;
	}
	/**
	 * @param redCard
	 * @uml.property  name="redCard"
	 */
	public void setRedCard(Long redCard) {
		this.redCard = redCard;
	}
	/**
	 * @return
	 * @uml.property  name="lostGoals"
	 */
	public Long getLostGoals() {
		return lostGoals;
	}
	/**
	 * @param lostGoals
	 * @uml.property  name="lostGoals"
	 */
	public void setLostGoals(Long lostGoals) {
		this.lostGoals = lostGoals;
	}
	/**
	 * @return
	 * @uml.property  name="lostPenalties"
	 */
	public Long getLostPenalties() {
		return lostPenalties;
	}
	/**
	 * @param lostPenalties
	 * @uml.property  name="lostPenalties"
	 */
	public void setLostPenalties(Long lostPenalties) {
		this.lostPenalties = lostPenalties;
	}
	/**
	 * @return
	 * @uml.property  name="ownGoals"
	 */
	public Long getOwnGoals() {
		return ownGoals;
	}
	/**
	 * @param ownGoals
	 * @uml.property  name="ownGoals"
	 */
	public void setOwnGoals(Long ownGoals) {
		this.ownGoals = ownGoals;
	}
	/**
	 * @return
	 * @uml.property  name="causedPenalties"
	 */
	public Long getCausedPenalties() {
		return causedPenalties;
	}
	/**
	 * @param causedPenalties
	 * @uml.property  name="causedPenalties"
	 */
	public void setCausedPenalties(Long causedPenalties) {
		this.causedPenalties = causedPenalties;
	}
	public void addGoal() {
		this.goals = this.goals + 1;
	}
	public void addAssist() {
		addAssist(1);
	}
	/**
	 * @param minutesPlayed
	 * @uml.property  name="minutesPlayed"
	 */
	public void setMinutesPlayed(Long minutesPlayed) {
		this.minutesPlayed = minutesPlayed;
	}
	/**
	 * @return
	 * @uml.property  name="minutesPlayed"
	 */
	public Long getMinutesPlayed() {
		return minutesPlayed;
	}
	
	/**
	 * @param onTime
	 * @uml.property  name="onTime"
	 */
	public void setTimeIn(Long onTime) {
		this.timeIn = onTime;
	}
	/**
	 * @return
	 * @uml.property  name="onTime"
	 */
	public Long getTimeIn() {
		return timeIn;
	}

	public void setTimeOut(Long offTime) {
		this.timeOut = offTime;
	}
	public Long getTimeOut() {
		return timeOut;
	}

	/**
	 * @uml.property  name="onTime"
	 */
	public void addMissedPenalty() {
		addMissedPenalty(1);
	}
	public void addMissedPenalty(int missedpenalties) {
		this.missedPenalties += missedpenalties;
	}
	/**
	 * @param missedPenalties
	 * @uml.property  name="missedPenalties"
	 */
	public void setMissedPenalties(Long missedPenalties) {
		this.missedPenalties = missedPenalties;
	}
	/**
	 * @return
	 * @uml.property  name="missedPenalties"
	 */
	public Long getMissedPenalties() {
		return missedPenalties;
	}
	public void addOwnGoal() {
		this.ownGoals += 1;
	}
	public Long getTimeOnPitch(Long fullTime) {
		return timeOut - timeIn;
		
	}
	public Long getPoints() {
		return points;
	}
	public void setPoints(Long points) {
		this.points = points;
	}
	public void addAssist(int n) {
		this.assists = this.assists + n;
	}
	public void setAssists(int assists) {
		this.assists = new Long(assists);
	}
}
