package com.lemonet.dreamball.realworld.domain;

import java.util.List;

public interface LeagueDAO {
    public void create(League league);
    public void update(League league);
	public League getLeagueById(Long id);
	public List<League> getLeagues();
}
