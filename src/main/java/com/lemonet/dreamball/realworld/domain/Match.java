package com.lemonet.dreamball.realworld.domain;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.ModelAttribute;

@Entity
@Table(name = "realworld__match")
public class Match {
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

	/**
	 * @uml.property  name="homeTeam"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "home_team_id", nullable = false)
	private Team homeTeam;

	/**
	 * @uml.property  name="awayTeam"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "away_team_id", nullable = false)
	private Team awayTeam;
	
    /**
	 * @uml.property  name="homeScore"
	 */
    private Long homeScore = new Long(0);
    /**
	 * @uml.property  name="awayScore"
	 */
    private Long awayScore = new Long(0);
    /**
	 * @uml.property  name="round"
	 */
//	@ManyToOne
/*	@NotNull
	@JoinColumn(name = "round_id", nullable = false)
    private Round roundInstance;

	public Round getRoundInstance() {
		return roundInstance;
	}
	public void setRoundInstance(Round roundInstance) {
		this.roundInstance = roundInstance;
	}*/

	@NotNull
    private Long round;
	
    private String goalTimesHome;
    private String goalTimesAway;
	
	/**
	 * @uml.property  name="matchTime"
	 */
    private Long matchTime;

	/**
	 * @uml.property  name="league"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "league_id", nullable = false)
	private League league;
	
	/**
	 * @uml.property  name="season"
	 */
	private Long season;

	/**
	 * @uml.property  name="date"
	 */
	private Calendar date;

    private Long outsideId;
	public Long getOutsideId() {
		return outsideId;
	}
	public void setOutsideId(Long outsideId) {
		this.outsideId = outsideId;
	}
	/**
	 * @return
	 * @uml.property  name="homeTeam"
	 */
	public Team getHomeTeam() {
		return homeTeam;
	}
	/**
	 * @param homeTeam
	 * @uml.property  name="homeTeam"
	 */
	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}
	/**
	 * @return
	 * @uml.property  name="awayTeam"
	 */
	public Team getAwayTeam() {
		return awayTeam;
	}
	/**
	 * @param awayTeam
	 * @uml.property  name="awayTeam"
	 */
	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}
	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @param homeScore
	 * @uml.property  name="homeScore"
	 */
	public void setHomeScore(Long homeScore) {
		this.homeScore = homeScore;
	}
	/**
	 * @return
	 * @uml.property  name="homeScore"
	 */
	public Long getHomeScore() {
		return homeScore;
	}
	/**
	 * @param awayScore
	 * @uml.property  name="awayScore"
	 */
	public void setAwayScore(Long awayScore) {
		this.awayScore = awayScore;
	}
	/**
	 * @return
	 * @uml.property  name="awayScore"
	 */
	public Long getAwayScore() {
		return awayScore;
	}
	/**
	 * @param round
	 * @uml.property  name="round"
	 */
	public void setRound(Long round) {
		this.round = round;
	}
	/**
	 * @return
	 * @uml.property  name="round"
	 */
	public Long getRound() {
		return round;
	}
	/**
	 * @param cal
	 * @uml.property  name="date"
	 */
	public void setDate(Calendar cal) {
		this.date = cal;
	}
	/**
	 * @return
	 * @uml.property  name="date"
	 */
	public Calendar getDate() {
		return date;
	}
	/**
	 * @param season
	 * @uml.property  name="season"
	 */
	public void setSeason(Long season) {
		this.season = season;
	}
	/**
	 * @return
	 * @uml.property  name="season"
	 */
	public Long getSeason() {
		return season;
	}
	/**
	 * @param matchTime
	 * @uml.property  name="matchTime"
	 */
	public void setMatchTime(Long matchTime) {
		this.matchTime = matchTime;
	}
	/**
	 * @return
	 * @uml.property  name="matchTime"
	 */
	public Long getMatchTime() {
		return matchTime;
	}
	
	public void addHomeGoal(){
		this.homeScore += 1;
	}
	public void addAwayGoal(){
		this.awayScore += 1;
	}
	/**
	 * @param league
	 * @uml.property  name="league"
	 */
	public void setLeague(League league) {
		this.league = league;
	}
	/**
	 * @return
	 * @uml.property  name="league"
	 */
	public League getLeague() {
		return league;
	}
	
	@ModelAttribute("result")
	public String getResult() {
		if (this.homeScore != null)
			return this.homeScore.toString() + " - " + this.awayScore.toString();
    	return "";
  	}
	public String getGoalTimesHome() {
		return goalTimesHome;
	}
	public void setGoalTimesHome(String goalTimesHome) {
		this.goalTimesHome = goalTimesHome;
	}
	public String getGoalTimesAway() {
		return goalTimesAway;
	}
	public void setGoalTimesAway(String goalTimesAway) {
		this.goalTimesAway = goalTimesAway;
	}
}
