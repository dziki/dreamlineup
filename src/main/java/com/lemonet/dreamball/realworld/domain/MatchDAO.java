package com.lemonet.dreamball.realworld.domain;

import java.util.List;


public interface MatchDAO {
    public void create(Match match);
    public void update(Match match);
	public Match getFirstMatch(League league, Long season, Long round);
	public Match getMatchById(Long id);
	public List<Match> getMatches(League league, Long season, Long startRound, Long finishRound);
	public Match getMatchByOutsideId(Long matchOutsideId);
	public Match findMatch(League league, Long season, Team homeTeam,
			Team awayTeam);
	public List<Match> getFinishedMatches();
	
}
