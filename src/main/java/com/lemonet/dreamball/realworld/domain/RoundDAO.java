package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;


public interface RoundDAO {
    public void create(Round round);
    public void update(Round round);
	public Round getRound(Long season, League league, Long number);
	public Round getFirstUnfinishedRound(League league);
	public Round getFirstUnstartedRound(League league);
	public Round getRoundById(Long id);
	public List<Round> getRounds(VirtualLeague vleague);
}
