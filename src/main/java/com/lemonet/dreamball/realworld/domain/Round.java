package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "realworld__round")
public class Round {
	public static final String UNSTARTED = "US";
	public static final String STARTED = "S";
	public static final String FINISHED = "F";

	
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

	/**
	 * @uml.property  name="league"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@JoinColumn(name = "league_id", nullable = false)
	private League league;
	
//	@OneToMany(mappedBy="round", cascade = CascadeType.ALL, fetch=FetchType.LAZY)  

	@Transient
	private List<Match> matches;

	public List<Match> getMatches(){
		return matches;
	}

	public void setMatches(List<Match> matches) {
		this.matches = matches;
	}

	/**
	 * @uml.property  name="season"
	 */
	@NotNull
	private Long season;
	/**
	 * @uml.property  name="number"
	 */
	@NotNull
	private Long number;
	/**
	 * @uml.property  name="status"
	 */
	private String status;
	
	/**
	 * @return
	 * @uml.property  name="league"
	 */
	public League getLeague() {
		return league;
	}
	/**
	 * @param league
	 * @uml.property  name="league"
	 */
	public void setLeague(League league) {
		this.league = league;
	}
	/**
	 * @return
	 * @uml.property  name="season"
	 */
	public Long getSeason() {
		return season;
	}
	/**
	 * @param season
	 * @uml.property  name="season"
	 */
	public void setSeason(Long season) {
		this.season = season;
	}
	/**
	 * @return
	 * @uml.property  name="number"
	 */
	public Long getNumber() {
		return number;
	}
	/**
	 * @param number
	 * @uml.property  name="number"
	 */
	public void setNumber(Long number) {
		this.number = number;
	}
	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String finished) {
		this.status = finished;
	}
}
