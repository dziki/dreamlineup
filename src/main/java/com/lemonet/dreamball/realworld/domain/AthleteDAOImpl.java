package com.lemonet.dreamball.realworld.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Property;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.proxy.HibernateProxyHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;

@Repository("athleteDAO")
public class AthleteDAOImpl implements AthleteDAO {
	@Autowired SessionFactory sessionFactory;
	
	@Autowired ApplicationContext appContext;

	@Override
    public List<Athlete> getLeagueAthlets(League league){
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Athlete.class);
    	criteria.createAlias("team", "t")
    			//.setFetchMode("virtualLeaguePlayers", FetchMode.JOIN)
				//.createAlias("virtualLeaguePlayers", "virtualLeaguePlayer")
				.add(Restrictions.eq("t.league", league));
    	@SuppressWarnings("unchecked")
    	List<Athlete> vLeagues = criteria.list();
        return vLeagues;
    }

	@Override
    public List<Athlete> getAthletsWithPerformance(League league){
		Session session = sessionFactory.getCurrentSession();
		
		String columns = "";
		
		HibernateProxyHelper h;
		
		ClassMetadata hibernateMetadata = sessionFactory.getClassMetadata(Performance.class);
	     AbstractEntityPersister persister = (AbstractEntityPersister) hibernateMetadata;
	     String[] columnNames = persister.getKeyColumnNames();
	     
	     for (String column : columnNames){
				if (column.equals("id"))
					continue;
				if (column.equals("athlete_id"))
					continue;
				columns += ", sum(p." + column + ")"; 
	     }

    	Query query = session.createQuery("select a" + columns + " " +
    			"from Athlete a " +
    			"left join a.performances as p " +
//    			"where vt.virtualLeague = :virtualLeague " +
    			"group by a.id");

    	@SuppressWarnings("unchecked")
		List<Athlete> athletes = query.list();
        return athletes;
    }

	@Override
    public void create(Athlete athlete){
    	Session session = sessionFactory.getCurrentSession();
    	session.save(athlete);
    }

	@Override
    public void update(Athlete athlete){
    	Session session = sessionFactory.getCurrentSession();
    	session.update(athlete);
    }

	@Override
	public Athlete getAthleteById(Long id) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Athlete.class)
		.add(Restrictions.eq("id", id));
    	return (Athlete) criteria.uniqueResult();
	}

	@Override
	public List<Athlete> list() {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Athlete.class);
    	@SuppressWarnings("unchecked")
    	List<Athlete> vAthletes = criteria.list();
        return vAthletes;
	}

	@Override
	public Athlete getAthleteByOutsideId(Long athleteOutsideId) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Athlete.class)
		.add(Restrictions.eq("outsideId", athleteOutsideId));
    	return (Athlete) criteria.uniqueResult();
	}

	@Override
	public Double getMeanPoints(Athlete athlete) {
    	Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select avg(p.points) " +
    			"from Performance p " +
    			"left join p.athlete as a " +
    			"where a = :a ");
    	query.setParameter("a", athlete);
    	return (Double) query.uniqueResult();
	}

	@Override
	public List<Athlete> getTeamAthletes(Long teamId) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Athlete.class);
    	criteria.add(Restrictions.eq("team.id", teamId));
    	@SuppressWarnings("unchecked")
    	List<Athlete> vLeagues = criteria.list();
        return vLeagues;
	}

	@Override
	public List<Athlete> getLeagueAthletsByPerformance(League baseLeague) {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select a " //, sum(p.points), sum(p.points*p.points) "
    			+ "from Athlete a " +
    			"left join a.performances as p " +
    			"left join a.team as t " +
    			"where t.league = '" + baseLeague.getId() + "'" +
    			"group by a.id " +
    			"order by sum(p.points) desc NULLS LAST");
    	
    	@SuppressWarnings("rawtypes")
//		Iterator res = query.list().iterator();
    	List<Athlete> vAthletes = query.list(); //new ArrayList<Athlete>();
//    	while (res.hasNext()){
//    		Object[] tuple = (Object[])res.next();
//    		Athlete a = (Athlete)tuple[0];
//    		if (tuple[1] != null)
//   			a.setMeanPoints((Double)tuple[1]);
//    		if (tuple[2] != null)
//   			a.setStdevPoints((Double)tuple[2]);
//    		vAthletes.add(a);
//    	}
		return vAthletes;
	}

	@Override
	public void removeOthersFromTeam(Long teamId, Set<Long> set) {
		Session session = sessionFactory.getCurrentSession();
//		INSERT INTO realworld__team(id,name,outsideid,league_id) VALUES (502,'Dummy',1,2);
		String s = "-1";
		for (Long l : set)
			s += ", " + l.toString();
		String sql = "UPDATE realworld__athlete SET team_id = '502' WHERE team_id=" + teamId + " AND " +
				"outsideid NOT IN (" + s + ")";
		Query q = session.createSQLQuery(sql);
		q.executeUpdate();
	}

}
