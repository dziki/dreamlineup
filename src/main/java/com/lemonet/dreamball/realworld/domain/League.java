package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;

@Entity
@Table(name = "realworld__league")
public class League {
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

	@OneToMany(mappedBy="league", cascade = CascadeType.ALL, fetch = FetchType.LAZY)  
	private List<Round> rounds;

	@OneToMany(mappedBy="baseLeague", cascade = CascadeType.ALL, fetch = FetchType.LAZY)  
	private List<VirtualLeague> vLeagues;

	/**
	 * @uml.property  name="division"
	 */
	@NotNull
	private int division;
    /**
	 * @uml.property  name="country"
	 */
	@NotNull
    private String country;
    /**
	 * @uml.property  name="name"
	 */
	@NotNull
    private String name;
    
	/**
	 * @return
	 * @uml.property  name="division"
	 */
	public int getDivision() {
		return division;
	}
	/**
	 * @param division
	 * @uml.property  name="division"
	 */
	public void setDivision(int division) {
		this.division = division;
	}
	/**
	 * @return
	 * @uml.property  name="country"
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country
	 * @uml.property  name="country"
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}
	public List<Round> getRounds() {
		return rounds;
	}

	public List<VirtualLeague> getVirtualLeagues() {
		return vLeagues;
	}
}
