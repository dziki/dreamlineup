package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"athletes","league"})
@Table(name = "realworld__team")
public class Team {
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    /**
	 * @uml.property  name="name"
	 */
    private String name;
    
    private Long outsideId;
	public Long getOutsideId() {
		return outsideId;
	}
	public void setOutsideId(Long outsideId) {
		this.outsideId = outsideId;
	}

    /**
	 * @uml.property  name="league"
	 * @uml.associationEnd  
	 */
    @ManyToOne
	@JoinColumn(name = "league_id", nullable = false)
    private League league;
    
	/**
	 * @uml.property  name="athletes"
	 */
	@OneToMany(mappedBy="team", cascade = CascadeType.ALL, fetch=FetchType.LAZY)  
	private List<Athlete> athletes;

	/**
	 * @return
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return
	 * @uml.property  name="league"
	 */
	public League getLeague() {
		return league;
	}
	/**
	 * @param league
	 * @uml.property  name="league"
	 */
	public void setLeague(League league) {
		this.league = league;
	}
	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public List<Athlete> getAthletes() {
		return athletes;
	}
	public void setAthletes(List<Athlete> athletes) {
		this.athletes = athletes;
	}

	/**
	 * @uml.property  name="dirty"
	 */
	@Transient
	private boolean dirty = false;
	
	/**
	 * @param dirty
	 * @uml.property  name="dirty"
	 */
	public void setDirty(boolean dirty){
		this.dirty = dirty;
	}
	public boolean getDirty(){
		return this.dirty;
	}
}
