package com.lemonet.dreamball.realworld.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.Formula;
import org.springframework.beans.BeanUtils;

@Entity
@JsonIgnoreProperties({"performances"})
@Table(name = "realworld__athlete")
public class Athlete {
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private Long outsideId;

	/**
	 * @uml.property  name="team"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@JoinColumn(name = "team_id", nullable = false)
	private Team team;

	@OneToMany(mappedBy="athlete", cascade = CascadeType.ALL, fetch = FetchType.LAZY)  
	private List<Performance> performances;

	@Transient
	@Formula("(select sum(p.points*p.points) as stdev from realworld__performance p left outer join realworld__athlete a on p.athlete_id=a.id where (a.id=id )")
	double stdevPoints;
	
	public double getStdevPoints() {
		return stdevPoints;
	}
	public void setStdevPoints(double stdevPoints) {
		this.stdevPoints = stdevPoints;
	}
	
	@Transient
	@Formula("(select sum(p.points) as m from realworld__performance p left outer join realworld__athlete a on p.athlete_id=a.id where (a.id=id )")
	double meanPoints;

	public double getMeanPoints() {
		return meanPoints;
	}
	public void setMeanPoints(double meanPoints) {
		this.meanPoints = meanPoints;
	}
	/**
	 * @uml.property  name="firstName"
	 */
	@Column(name = "first_name", length = 255)
	@NotNull
    private String firstName;

	/**
	 * @uml.property  name="lastName"
	 */
	@Column(name = "last_name", length = 255)
	@NotNull
    private String lastName;
    
	/**
	 * @uml.property  name="position"
	 */
	@Column(name = "position", length = 1)
	@NotNull
    private String position;
	
	/**
	 * @uml.property  name="shirtNr"
	 */
	private Long shirtNr;
    
	/**
	 * @uml.property  name="dirty"
	 */
	@Transient
	private boolean dirty = false;
	
	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return
	 * @uml.property  name="firstName"
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName
	 * @uml.property  name="firstName"
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return
	 * @uml.property  name="lastName"
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName
	 * @uml.property  name="lastName"
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @param team
	 * @uml.property  name="team"
	 */
	public void setTeam(Team team) {
		this.team = team;
	}
	/**
	 * @return
	 * @uml.property  name="team"
	 */
	public Team getTeam() {
		return team;
	}
	/**
	 * @param position
	 * @uml.property  name="position"
	 */
	public void setPosition(String position) {
		this.position = position;
	}
	/**
	 * @return
	 * @uml.property  name="position"
	 */
	public String getPosition() {
		return position;
	}
	/**
	 * @param shirtNr
	 * @uml.property  name="shirtNr"
	 */
	public void setShirtNr(Long shirtNr) {
		this.shirtNr = shirtNr;
	}
	/**
	 * @return
	 * @uml.property  name="shirtNr"
	 */
	public Long getShirtNr() {
		return shirtNr;
	}
	public Long getOutsideId() {
		return outsideId;
	}
	public void setOutsideId(Long outsideId) {
		this.outsideId = outsideId;
	}
	
	/**
	 * @param dirty
	 * @uml.property  name="dirty"
	 */
	public void setDirty(boolean dirty){
		this.dirty = dirty;
	}
	public boolean getDirty(){
		return this.dirty;
	}
	public void setPerformances(List<Performance> performances) {
		this.performances = performances;
	}
	public List<Performance> getPerformances() {
		return performances;
	}
	public void copyProperties(Athlete a) {
		this.setShirtNr(a.getShirtNr());
		this.setPosition(a.getPosition());
		this.setTeam(a.getTeam());
	}
}
