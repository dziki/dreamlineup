package com.lemonet.dreamball.realworld.domain;

import java.util.List;

public interface PerformanceDAO {
    public void create(Performance performance);
    public void update(Performance performance);
	public Performance getByAthleteMatch(Athlete a, Match match);
	public List<Performance> getByRound(Long roundId);
	public void remove(Performance performance);
}
