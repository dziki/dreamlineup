package com.lemonet.dreamball.realworld.domain;

import java.util.List;
import java.util.Set;

public interface AthleteDAO {

	public List<Athlete> getLeagueAthlets(League league);

	void update(Athlete athlete);

	void create(Athlete athlete);

	public Athlete getAthleteById(Long id);

	public List<Athlete> list();

	public Athlete getAthleteByOutsideId(Long athleteOutsideId);

	public Double getMeanPoints(Athlete athlete);

	public List<Athlete> getTeamAthletes(Long teamId);

	List<Athlete> getAthletsWithPerformance(League league);

	public List<Athlete> getLeagueAthletsByPerformance(League baseLeague);

	public void removeOthersFromTeam(Long teamId, Set<Long> set);

}
