package com.lemonet.dreamball.realworld.domain;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("matchDAO")
public class MatchDAOImpl implements MatchDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Match getFirstMatch(League league, Long season, Long round) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Match.class)
    		.add(Restrictions.eq("league", league))
    		.add(Restrictions.eq("season", season))
    	    .add(Restrictions.eq("round", round))
    	    .addOrder( Order.asc("date") );

    	try {
    		return (Match) criteria.list().get(0);
    	}
    	catch (IndexOutOfBoundsException e){
    		
    	}
    	return null;
	}	

	@Override
	public Match findMatch(League league, Long season, Team homeTeam,
			Team awayTeam) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Match.class)
    		.add(Restrictions.eq("league", league))
    		.add(Restrictions.eq("season", season))
    		.add(Restrictions.eq("homeTeam", homeTeam))
    		.add(Restrictions.eq("awayTeam", awayTeam))
    	    .addOrder( Order.asc("date") );

    	return (Match) criteria.uniqueResult();
	}
	
	@Override
    public void create(Match match){
    	Session session = sessionFactory.getCurrentSession();
    	session.save(match);
    }

	@Override
    public void update(Match match){
    	Session session = sessionFactory.getCurrentSession();
    	session.save(match);
    }

	@Override
	public Match getMatchById(Long id) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Match.class)
			.add(Restrictions.eq("id", id));
    	return (Match) criteria.uniqueResult();
	}

	@Override
	public List<Match> getMatches(League league, Long season, Long startRound,
			Long finishRound) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Match.class)
    		.add(Restrictions.eq("league", league))
    		.add(Restrictions.eq("season", season))
    	    .add(Restrictions.ge("round", startRound))
    	    .add(Restrictions.le("round", finishRound))
    	    .addOrder( Order.asc("date") );

    	@SuppressWarnings("unchecked")
    	List<Match> res = criteria.list();
    	return res;
	}

	@Override
	public Match getMatchByOutsideId(Long matchOutsideId) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Match.class)
			.add(Restrictions.eq("outsideId", matchOutsideId));
    	return (Match) criteria.uniqueResult();
	}

	@Override
	public List<Match> getFinishedMatches() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, -3);
		
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Match.class)
        	.add(Restrictions.isNull("homeScore"))
    		.add(Restrictions.lt("date", cal))
    	    .addOrder( Order.asc("date") );

    	@SuppressWarnings("unchecked")
    	List<Match> res = criteria.list();
    	return res;
	}
}
