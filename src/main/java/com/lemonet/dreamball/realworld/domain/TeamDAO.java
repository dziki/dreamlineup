package com.lemonet.dreamball.realworld.domain;

import java.util.List;


public interface TeamDAO {
    public Team getTeamById(Long id);

	void update(Team team);

	void create(Team team);

	public List<Team> getTeams();

	public Team getTeamByOutsideId(Long teamOutsideId);

	public List<Team> getTeams(League baseLeague);

}
