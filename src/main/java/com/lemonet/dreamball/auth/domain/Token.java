package com.lemonet.dreamball.auth.domain;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;

import com.lemonet.dreamball.engine.Constants;

@Entity
@Table(name = "auth__token")
public class Token implements Cloneable {
	static Logger logger = Logger.getLogger(Token.class);

	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
	 * @uml.property  name="firstName"
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "player_id", nullable = false)
	private Player player;
	/**
	 * @uml.property  name="lastName"
	 */
	@NotNull
	private String token;
	/**
	 * @uml.property  name="lastName"
	 */
	@NotNull
	private Date date;

	public static class TokenExpiredException extends Exception {
		private static final long serialVersionUID = -1390837964955341990L;
		
	}

	public Token(Player player) {
		super();
		SecureRandom random = new SecureRandom();
		this.setDate(new Date());
		this.player = player;
		this.setToken(new BigInteger(130, random).toString(32));;
	}
	
	public Token() {
		super();
	}
	
	public String getToken() {
		return token;
	}
	private void setToken(String token) {
		this.token = token;
	}

	public Player getPlayer() {
		return player;
		
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isValid() {
		Date now = new Date();
		Date timeout = new Date(now.getTime() - Constants.TOKEN_TIMEOUT);
		return timeout.before(this.getDate());
	}
	
	
	

}
