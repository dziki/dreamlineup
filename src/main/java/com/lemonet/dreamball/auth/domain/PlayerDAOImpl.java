package com.lemonet.dreamball.auth.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.finance.domain.Operation;
import com.lemonet.dreamball.realworld.domain.Round;

@Repository("playerDAO")
public class PlayerDAOImpl implements PlayerDAO {
	@Autowired
	SessionFactory sessionFactory;

	/**
	 * Thrown if a young person tries to sign up
	 * @author kidzik
	 */
	public class TooYoung extends Exception {
		private static final long serialVersionUID = 1L;

		TooYoung(String comment){
			super(comment);
		}
	}

    @Override
	public void create(Player player) throws TooYoung{
    	Session session = sessionFactory.getCurrentSession();
    	session.save(player);
    }
    @Override
	public void update(Player player){
    	Session session = sessionFactory.getCurrentSession();
    	session.save(player);
    }

    /**
     * Loads a player with given username and password
     * 
     * @param username
     * @param password
     * @return
     */
    @Override
    public Player loginPlayer(String username, String password){
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Player.class)
    		.add(Restrictions.eq("email", username))
    		.add(Restrictions.eq("password", password));
		return (Player) criteria.uniqueResult();
    }

    @Override
	@Transactional
	public Player findByEmailOrUsername(String arg) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Player.class)
    		.add(Restrictions.or(
    				Restrictions.eq("email", arg),
    				Restrictions.eq("username", arg)
    		));
		return (Player) criteria.uniqueResult();
	}

	/**
	 * Returns a list of players with additional informations:
	 *  - number of wins
	 *  - number of cashed tournaments
	 *  - sum of wins
	 */
	@Override
	@Transactional
	public List<Player> listWithWins(int limit){
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("" +
    			"select o.player, count(o.prizeNr), count(o.value), sum(o.value) " +
    			"from Operation o " +
    			"where o.typeOp = " + Operation.PRIZE + " " +
    			"group by o.player " +
    			"order by sum(o.value) desc");
    	query.setMaxResults(limit);
    	
    	@SuppressWarnings("unchecked")
		Iterator<Object[]> iter = query.iterate();
		
    	List<Player> players = new ArrayList<Player>();
        while ( iter.hasNext() ) {
            Object[] tuple = iter.next();
            Player p = (Player) tuple[0];
            p.setWins((Long)tuple[1]);
            p.setCashed((Long)tuple[2]);
            p.setTotalWin((Double)tuple[3]);
            players.add(p);
        }    
		return players;
	}

	/**
	 * Lists all players
	 */
	@Override
	public List<Player> list(){
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Player.class);

    	@SuppressWarnings("unchecked")
    	List<Player> res = criteria.list();

    	return res;
	}
	
	@Override
	public Player getPlayerById(Long playerId) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Player.class)
    		.add(Restrictions.eq("id", playerId));
		return (Player) criteria.uniqueResult();
	}
	@Override
	@Transactional
	public Object findByUsernamel(String username) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Player.class)
    		.add(Restrictions.eq("username", username));
		return (Player) criteria.uniqueResult();
	}
	@Override
	public List<Player> getNotActive() {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Player.class)
    			.add(Restrictions.eq("active",false));

    	@SuppressWarnings("unchecked")
    	List<Player> res = criteria.list();

    	return res;
	}
	@Override
	public List<Player> getPlayersFinishedInRound(Round r) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select distinct p "
				+ "from Player p "
				+ "left join p.virtualTeams as vt "
				+ "left join vt.virtualLeague as vl "
				+ "where vl.status = 1 and "
				+ "vl.startingRound + vl.duration -1 = :rnum ");
    	query.setParameter("rnum", r.getNumber());


    	@SuppressWarnings("unchecked")
		List<Player> players = query.list();
   
    	return players;
	}
	@Override
	public List<Player> getPlayersWithResubmit() {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Player.class)
    			.add(Restrictions.eq("resubmit",true));

    	@SuppressWarnings("unchecked")
    	List<Player> res = criteria.list();

    	return res;
	}
	
}