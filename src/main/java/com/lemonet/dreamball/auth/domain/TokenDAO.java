package com.lemonet.dreamball.auth.domain;



/**
 * @author  kidzik
 */
public interface TokenDAO {
    public void create(Token token);
	public Token find(String token);
}
