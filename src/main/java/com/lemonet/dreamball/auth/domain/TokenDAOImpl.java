package com.lemonet.dreamball.auth.domain;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("tokenDAO")
public class TokenDAOImpl implements TokenDAO {
	@Autowired
	SessionFactory sessionFactory;

    @Override
	public void create(Token token) {
    	Session session = sessionFactory.getCurrentSession();
    	session.save(token);
    }
    
	@Override
	public Token find(String token) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Token.class)
    		.add(Restrictions.eq("token", token));
        Token t = (Token) criteria.uniqueResult();
        return t;
	}
}