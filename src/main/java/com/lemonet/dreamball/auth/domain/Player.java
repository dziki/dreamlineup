package com.lemonet.dreamball.auth.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.finance.domain.Operation;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.finance.domain.OperationDAOImpl;

@Entity
@Table(name = "auth__player"
//, uniqueConstraints= @UniqueConstraint(columnNames={"email", "username"} )
)
public class Player implements Cloneable {
	static Logger logger = Logger.getLogger(Player.class);
	
	public Player(String firstName, String lastName, Date birthDate,
			String nationality, String email, String username, String password, String address,
			String houseNr, String city, String country, String zipcode, String documentId,
			String documentType) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.nationality = nationality;
		this.email = email;
		this.username = username;
		this.password = password;
		this.address = address;
		this.houseNr = houseNr;
		this.city = city;
		this.setCountry(country);
		this.zipcode = zipcode;
		this.documentId = documentId;
		this.documentType = documentType;
		this.role = Constants.ROLE_USER;
	}
	
	@OneToMany(mappedBy="player", cascade = CascadeType.ALL, fetch=FetchType.LAZY)  
	private List<VirtualTeam> virtualTeams;
	
	@OneToMany(mappedBy="player", cascade = CascadeType.ALL, fetch=FetchType.LAZY)  
	private List<Operation> operations;
	
	public List<Operation> getOperations() {
		return operations;
	}

	@OneToMany(mappedBy="player", cascade = CascadeType.ALL, fetch=FetchType.LAZY)  
	private List<Token> tokens;
		
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
	 * @uml.property  name="firstName"
	 */
	private String firstName;
	/**
	 * @uml.property  name="lastName"
	 */
	private String lastName;
	
	/**
	 * @uml.property  name="birthDate"
	 */
	private Date birthDate;
	/**
	 * @uml.property  name="nationality"
	 */
	private String nationality;
	/**
	 * @uml.property  name="email"
	 */
	@Column(unique = true)
	@Email
	@NotBlank
	@NotNull
	@Pattern(regexp=".+@.+\\..+")
	@Length(max = 50, min = 2) 
	private String email;
	/**
	 * @uml.property  name="username"
	 */
	@Column(unique = true)
	@Length(max = 50, min = 2) 
	@NotBlank
	@NotNull
	private String username;
	/**
	 * @uml.property  name="password"
	 */
	@NotNull
	private String password;
	/**
	 * @uml.property  name="address"
	 */
	private String address;
	/**
	 * @uml.property  name="houseNr"
	 */
	private String houseNr;
	/**
	 * @uml.property  name="city"
	 */
	private String city;
	/**
	 * @uml.property  name="country"
	 */
	private String country;
	/**
	 * @uml.property  name="zipcode"
	 */
	private String zipcode;
	/**
	 * @uml.property  name="documentId"
	 */
	private String documentId;
	/**
	 * @uml.property  name="documentType"
	 */
	private String documentType;
	/**
	 * True if the account was activated
	 * 
	 * @uml.property  name="active"
	 */
	@NotNull
	private Boolean active = new Boolean(false);

	@NotNull
	private Boolean emailSubscription = new Boolean(true);
	
	@NotNull
	private Boolean resubmit = new Boolean(true);
	
	public Boolean getEmailSubscription() {
		return emailSubscription;
	}
	public void setEmailSubscription(Boolean emailSubscription) {
		this.emailSubscription = emailSubscription;
	}
	public Boolean getResubmit() {
		return resubmit;
	}
	public void setResubmit(Boolean resubmit) {
		this.resubmit = resubmit;
	}
	
	/**
	 * True if the account was temporarly banned
	 * 
	 * @uml.property  name="banned"
	 */
	private Boolean banned;


	/**
	 * Admin or a regular player
	 * 
	 * @uml.property  name="role"
	 */
	private Integer role;
	
	@Transient
	private Long wins;
	
	/**
	 * How many times player won nonzero money
	 */
	@Transient
	private Long cashed;

	/**
	 * How much money did he win
	 */
	@Transient
	private Double totalWin;


	public Integer getRole() {
		return role;
	}
	public void setRole(Integer role) {
		this.role = role;
	}

	public Player(){
	}
	
	public Player(Player command) {
	}

	/**
	 * @return
	 * @uml.property  name="firstName"
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName
	 * @uml.property  name="firstName"
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return
	 * @uml.property  name="lastName"
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName
	 * @uml.property  name="lastName"
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return
	 * @uml.property  name="birthDate"
	 */
	public Date getBirthDate() {
		return birthDate;
	}
	/**
	 * @param birthDate
	 * @uml.property  name="birthDate"
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	/**
	 * @return
	 * @uml.property  name="nationality"
	 */
	public String getNationality() {
		return nationality;
	}
	/**
	 * @param nationality
	 * @uml.property  name="nationality"
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	/**
	 * @return
	 * @uml.property  name="email"
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email
	 * @uml.property  name="email"
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return
	 * @uml.property  name="password"
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password
	 * @uml.property  name="password"
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return
	 * @uml.property  name="address"
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address
	 * @uml.property  name="address"
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return
	 * @uml.property  name="houseNr"
	 */
	public String getHouseNr() {
		return houseNr;
	}
	/**
	 * @param houseNr
	 * @uml.property  name="houseNr"
	 */
	public void setHouseNr(String houseNr) {
		this.houseNr = houseNr;
	}
	/**
	 * @return
	 * @uml.property  name="city"
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city
	 * @uml.property  name="city"
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return
	 * @uml.property  name="zipcode"
	 */
	public String getZipcode() {
		return zipcode;
	}
	/**
	 * @param zipcode
	 * @uml.property  name="zipcode"
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	/**
	 * @return
	 * @uml.property  name="documentId"
	 */
	public String getDocumentId() {
		return documentId;
	}
	/**
	 * @param documentId
	 * @uml.property  name="documentId"
	 */
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	/**
	 * @return
	 * @uml.property  name="documentType"
	 */
	public String getDocumentType() {
		return documentType;
	}
	/**
	 * @param documentType
	 * @uml.property  name="documentType"
	 */
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	/**
	 * @return
	 * @uml.property  name="active"
	 */
	public Boolean isActive() {
		return active;
	}
	/**
	 * @param active
	 * @uml.property  name="active"
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param banned
	 * @uml.property  name="banned"
	 */
	public void setBanned(Boolean banned) {
		this.banned = banned;
	}
	/**
	 * @return
	 * @uml.property  name="banned"
	 */
	public Boolean getBanned() {
		return banned;
	}
	
	@Override
	public String toString(){
		return this.firstName;
	}

	@ModelAttribute("fullName")
	public String getFullName(){
		return this.firstName + " " + this.lastName;
	}


	public void setVirtualTeams(List<VirtualTeam> virtualTeams) {
		this.virtualTeams = virtualTeams;
	}

	
	public List<VirtualTeam> getVirtualTeams() {
		return this.virtualTeams;
	}
	
	public void setWins(Long wins) {
		this.wins = wins;
	}

	public Long getWins() {
		return wins;
	}

	public Long getCashed() {
		return cashed;
	}

	public void setCashed(Long cashed) {
		this.cashed = cashed;
	}

	public void setTotalWin(Double totalWin) {
		this.totalWin = totalWin;
	}

	public Double getTotalWin() {
		return totalWin;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCountry() {
		return country;
	}
	
	public List<Token> getTokens() {
		return tokens;
	}
	public void setTokens(List<Token> tokens) {
		this.tokens = tokens;
	}
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
	public void setCountry(String country) {
		this.country = country;
	}

}
