package com.lemonet.dreamball.auth.domain;

import java.util.List;

import com.lemonet.dreamball.auth.domain.PlayerDAOImpl.TooYoung;
import com.lemonet.dreamball.realworld.domain.Round;

/**
 * @author  kidzik
 */
public interface PlayerDAO {
    public void create(Player player) throws TooYoung;
    public void update(Player player);
	public Player loginPlayer(String username, String password);
	public Player findByEmailOrUsername(String arg);
	public List<Player> list();
	public List<Player> listWithWins(int limit);
	public Player getPlayerById(Long playerId);
	public Object findByUsernamel(String username);
	public List<Player> getNotActive();
	public List<Player> getPlayersFinishedInRound(Round r);
	public List<Player> getPlayersWithResubmit();
}
