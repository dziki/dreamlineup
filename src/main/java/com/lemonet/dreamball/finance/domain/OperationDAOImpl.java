package com.lemonet.dreamball.finance.domain;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;

@Repository("operationDAO")
public class OperationDAOImpl implements OperationDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void save(Operation operation) throws NotEnoughMoneyException {
		if (operation.getValue() + this.getAccountBalance(operation.getPlayer()) < 0)
			throw new NotEnoughMoneyException();
		
    	Session session = sessionFactory.getCurrentSession();
    	session.save(operation);
	}

	@Override
	public void update(Operation operation) throws NotEnoughMoneyException {
    	Session session = sessionFactory.getCurrentSession();
    	session.update(operation);
	}
	
	@Override
	public double getAccountBalance(Player player) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Operation.class)
    		.add(Restrictions.eq("player", player))
    		.add(Restrictions.ge("status", new Long(-1)));

    	criteria.setProjection(Projections.sum("value"));
    	Double res = (Double)criteria.uniqueResult();
    	if (res == null)
    		return 0d;
    	return res;
	}

	@Override
	public double getInPlayBalance(Player player) {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select sum(vl.buyIn)  " +
    			"from VirtualTeam vt " +
    			"left join vt.virtualLeague as vl " +
    			"where vt.player = :player and " +
    			"vl.status = 0");
    	query.setParameter("player", player);
    	@SuppressWarnings("unchecked")
		List<Double> vt = query.list();
		if (vt.size() == 0 || vt.get(0) == null)
			return 0;
		
		VirtualLeague vl = new VirtualLeague();
        return vt.get(0) * vl.getRake();
	}

	@Override
	public List<VirtualTeam> getMainLeagueNotReimbursed() {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select vt " +
    			"from VirtualTeam vt " +
    			"left join vt.virtualLeague as vl " +
    			"left join vl.bonuses as b " +
    			"left join vt.player as p " +
    			"where b.description LIKE '%50PLN EMPiK%' and "
    			+ "(vt.id not in (select o.relatedId from Operation o where o.relatedId is not null))");
    	@SuppressWarnings("unchecked")
		List<VirtualTeam> teams = query.list();
        return teams;
	}

	@Override
	public List<Operation> getOperations(Player player) {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Operation.class)
    		.add(Restrictions.eq("player", player));

    	@SuppressWarnings("unchecked")
    	List<Operation> res = criteria.list();
    	
    	return res;
	} 
	
}
