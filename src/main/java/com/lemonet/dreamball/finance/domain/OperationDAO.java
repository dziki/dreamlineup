package com.lemonet.dreamball.finance.domain;

import java.util.List;

import org.hibernate.Session;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;

public interface OperationDAO {
    public void save(Operation operation) throws NotEnoughMoneyException;
    public void update(Operation operation) throws NotEnoughMoneyException;
	public double getAccountBalance(Player player);
	public List<Operation> getOperations(Player player);
	double getInPlayBalance(Player player);
	List<VirtualTeam> getMainLeagueNotReimbursed();
}
