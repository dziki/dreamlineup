package com.lemonet.dreamball.finance.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.engine.Constants;

@Entity
@Table(name = "finance__operation")
public class Operation {
	static public class NotEnoughMoneyException extends Exception { 
	private static final long serialVersionUID = 1L;
	
	public NotEnoughMoneyException() { 
	    super(); 
	  } 
	}

	public static Long ENTRANCE_BONUS		= 1001L;
	public static Long DEPOSIT				= 1002L;
	public static Long PRIZE				= 1003L;
	public static Long PAYBACK				= 1004L;

	public static Long WITHDRAWAL			= 2001L;
	public static Long BUYIN				= 2002L;
	
	public static Long MAINLEAGUE_BONUS		= 3001L;

	public Operation(){}
	
	public Operation(Double value, Long type, Player player){
		this.value = value;
		this.typeOp = type;
		this.player = player;
	}
	
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	/**
	 * @uml.property  name="player"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "player_id", nullable = false)
	private Player player;
	
	/**
	 * @uml.property  name="date"
	 */
	@NotNull
	private Date date = new Date();
	/**
	 * @uml.property  name="value"
	 */
	@NumberFormat(style = Style.CURRENCY)
	private Double value;
	/**
	 * @uml.property  name="status"
	 */
	@NotNull
	private Long status = 0L;
	/**
	 * @uml.property  name="status"
	 */
	@NotNull
	private Long typeOp;
	
	private Long prizeNr;

	/**
	 * @uml.property  name="orginalValue"
	 */
	@NumberFormat(style = Style.CURRENCY)
	private Double orginalValue;
	/**
	 * @uml.property  name="orginalCurrency"
	 */
	private String orginalCurrency;
	/**
	 * @uml.property  name="exchangeRate"
	 */
	private Double exchangeRate;

	/**
	 * @uml.property  name="relatedClass"
	 */
	private String relatedClass;
	/**
	 * @uml.property  name="relatedId"
	 */
	private Long relatedId;

	/**
	 * @return
	 * @uml.property  name="relatedClass"
	 */
	public String getRelatedClass() {
		return relatedClass;
	}

	/**
	 * @param relatedClass
	 * @uml.property  name="relatedClass"
	 */
	public void setRelatedClass(String relatedClass) {
		this.relatedClass = relatedClass;
	}

	/**
	 * @return
	 * @uml.property  name="relatedId"
	 */
	public Long getRelatedId() {
		return relatedId;
	}

	/**
	 * @param relatedId
	 * @uml.property  name="relatedId"
	 */
	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	/**
	 * @uml.property  name="comments"
	 */
	private String comments;

	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return
	 * @uml.property  name="player"
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @param player
	 * @uml.property  name="player"
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * @return
	 * @uml.property  name="date"
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 * @uml.property  name="date"
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return
	 * @uml.property  name="value"
	 */
	public Double getValue() {
		return value;
	}

	/**
	 * @param value
	 * @uml.property  name="value"
	 */
	public void setValue(Double value) {
		this.value = value;
	}

	/**
	 * @return
	 * @uml.property  name="status"
	 */
	public Long getStatus() {
		return status;
	}

	/**
	 * @param status
	 * @uml.property  name="status"
	 */
	public void setStatus(Long status) {
		this.status = status;
	}

	/**
	 * @return
	 * @uml.property  name="orginalValue"
	 */
	public Double getOrginalValue() {
		return orginalValue;
	}

	/**
	 * @param orginalValue
	 * @uml.property  name="orginalValue"
	 */
	public void setOrginalValue(Double orginalValue) {
		this.orginalValue = orginalValue;
	}

	/**
	 * @return
	 * @uml.property  name="orginalCurrency"
	 */
	public String getOrginalCurrency() {
		return orginalCurrency;
	}

	/**
	 * @param orginalCurrency
	 * @uml.property  name="orginalCurrency"
	 */
	public void setOrginalCurrency(String orginalCurrency) {
		this.orginalCurrency = orginalCurrency;
	}

	/**
	 * @return
	 * @uml.property  name="exchangeRate"
	 */
	public Double getExchangeRate() {
		return exchangeRate;
	}

	/**
	 * @param exchangeRate
	 * @uml.property  name="exchangeRate"
	 */
	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	/**
	 * @return
	 * @uml.property  name="comments"
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 * @uml.property  name="comments"
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	public Long getTypeOp() {
		return typeOp;
	}

	
	@ModelAttribute("typeOpDesc")
	public String getTypeOpDesc() {
		return Constants.getTypeDesc(this.typeOp);
	}

	public void setType(Long type) {
		this.typeOp = type;
	}

	public Long getPrizeNr() {
		return prizeNr;
	}

	public void setPrizeNr(Long prizeNr) {
		this.prizeNr = prizeNr;
	}
}
