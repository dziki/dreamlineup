package com.lemonet.dreamball.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import com.lemonet.dreamball.finance.domain.Operation;

@Component
public class Constants {
	public static ReloadableResourceBundleMessageSource messageSource;  

    @Autowired
    public void setMessageSource(ReloadableResourceBundleMessageSource messageSource){
    	Constants.messageSource = messageSource;
    }    
  
	public static final long TOKEN_TIMEOUT = 15*60*1000;	// 15 minutes
	public static final int DEADLINE_MINUTES = 30;
	public static final int LAST_CHANGE_MINUTES = 5;
	public static final int MAX_TEAM_PLAYERS = 5;
	public static final long MAX_TEAM_ENTRIES = 3;
	public static final int ROLE_USER = 1;
	public static final int ROLE_ADMIN = 2;
	public static final long CURRENT_SEASON = 2014L;
	public static final long CURRENT_LEAGUE = 1L;
	
	public static final double RAKE = 1d;
	
//	public static String PARSER_PATH = "/Users/kidzik/Documents/workspace/dreamlineup/src/main/python/parser/";
//	public static final String PARSER_PATH = "/home/kidzik/workspace/dreamlineup/soccer-parser/";
	public static final String PARSER_PATH = "/var/dreamlineup/soccer-parser/";


	static public String getTypeDesc(Long type)
	{
		if (type==null)
			return "";
		if (type.equals(Operation.ENTRANCE_BONUS))
			return messageSource.getMessage("finance.entrance_bonus", null, "Bonus", null);
		if (type.equals(Operation.WITHDRAWAL))
			return messageSource.getMessage("finance.withdrawal", null, "Withdrawal", null);
		if (type.equals(Operation.DEPOSIT))
			return messageSource.getMessage("finance.deposit", null, "Deposit", null);
		if (type.equals(Operation.BUYIN))
			return messageSource.getMessage("finance.buyin", null, "Buy-in", null);
		if (type.equals(Operation.PAYBACK))
			return messageSource.getMessage("finance.payback", null, "Payback", null);
		if (type.equals(Operation.PRIZE))
			return messageSource.getMessage("finance.prize", null, "Prize", null);
		if (type.equals(Operation.MAINLEAGUE_BONUS))
			return messageSource.getMessage("finance.main_league_reimbursement", null, "Reimbursment", null);
		return "";
	}
}
