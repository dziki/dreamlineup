package com.lemonet.dreamball.dreamworld.domain;

import com.lemonet.dreamball.realworld.domain.Team;


public interface VirtualTeamAthleteDAO {

	void create(VirtualTeamAthlete vta);
	void update(VirtualTeamAthlete vta);
	void delete(VirtualTeamAthlete vta);

	VirtualTeamAthlete getByID(Long vTeamAthleteId);
	Long countPlayersOnPos(VirtualTeam vt, String pos);
	Long getRealTeamFrequency(VirtualTeam vt, Team team);

}
