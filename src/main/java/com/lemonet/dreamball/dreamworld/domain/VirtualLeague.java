package com.lemonet.dreamball.dreamworld.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Type;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.realworld.domain.MatchDAOImpl;

@Entity
@Table(name = "dreamworld__virtualleague")
public class VirtualLeague {
	public static final Long FINISHED = 1L;
	public static final Long READY = 0L;
	public static final Long CANCELED = -1L;
	
	
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
	 * @uml.property  name="season"
	 */
	@NotNull
	private Long season;

	/**
	 * @uml.property  name="baseLeague"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@JoinColumn(name = "base_league_id", nullable = false)
	private League baseLeague;

	/**
	 * @uml.property  name="owner"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@JoinColumn(name = "owner_id", nullable = false)
	private Player owner;

	@OneToMany(mappedBy="virtualLeague", cascade = CascadeType.ALL, fetch=FetchType.LAZY)  
	private List<VirtualLeagueAthlete> virtualLeagueAthletes;

	@OneToMany(mappedBy="virtualLeague", cascade = CascadeType.ALL, fetch=FetchType.LAZY)  
	private List<VirtualLeagueBonus> bonuses;

	/**
	 * @uml.property  name="startingRound"
	 */
	@NotNull
	private int startingRound;
	/**
	 * @uml.property  name="duration"
	 */
	@NotNull
	private int duration;
	/**
	 * @uml.property  name="numOfPlayers"
	 */
	private Long numOfPlayers;
	/**
	 * @uml.property  name="buyIn"
	 */
	@NotNull
	private double buyIn;
	/**
	 * @uml.property  name="budget"
	 */
	@NotNull
	private int budget;

	/**
	 * @uml.property  name="finished"
	 */
	@NotNull
//	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Long status = new Long(0);
	
	/**
	 * @uml.property  name="virtualTeams"
	 */
	@OneToMany(mappedBy="virtualLeague", cascade = CascadeType.ALL)  
	private List<VirtualTeam> virtualTeams;
	
	/**
	 * @uml.property  name="virtualAthletes"
	 */
	@OneToMany(mappedBy="virtualLeague", cascade = CascadeType.ALL)  
	private List<VirtualLeagueAthlete> virtualAthletes;
	
	/**
	 * @uml.property  name="curNumOfPlayers"
	 */
	@Formula("(select count(vlt.id) from dreamworld__virtualteam vlt where vlt.virtual_league_id = id)")
	private int curNumOfPlayers;
	
	public List<VirtualLeagueAthlete> getVirtualLeagueAthletes() {
		return virtualLeagueAthletes;
	}
	public void setVirtualLeagueAthletes(
			List<VirtualLeagueAthlete> virtualLeagueAthletes) {
		this.virtualLeagueAthletes = virtualLeagueAthletes;
	}
	public List<VirtualLeagueBonus> getBonuses() {
		return bonuses;
	}
	public void setBonuses(List<VirtualLeagueBonus> bonuses) {
		this.bonuses = bonuses;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}

	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	@ModelAttribute("title")
	public String getTitle() {
		if (this.getBonuses().size() == 1 && this.getBonuses().get(0).getType() == 1)
			return "Turniej główny";
		return "Liga " + buyIn + " DP";
	}
	/**
	 * @return
	 * @uml.property  name="owner"
	 */
	public Player getOwner() {
		return owner;
	}
	/**
	 * @param owner
	 * @uml.property  name="owner"
	 */
	public void setOwner(Player owner) {
		this.owner = owner;
	}
	/**
	 * @return
	 * @uml.property  name="startingRound"
	 */
	public int getStartingRound() {
		return startingRound;
	}
	/**
	 * @param startingRound
	 * @uml.property  name="startingRound"
	 */
	public void setStartingRound(int startingRound) {
		this.startingRound = startingRound;
	}
	/**
	 * @return
	 * @uml.property  name="duration"
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * @param duration
	 * @uml.property  name="duration"
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	/**
	 * @return
	 * @uml.property  name="numOfPlayers"
	 */
	public Long getNumOfPlayers() {
		return numOfPlayers;
	}
	/**
	 * @param numOfPlayers
	 * @uml.property  name="numOfPlayers"
	 */
	public void setNumOfPlayers(Long numOfPlayers) {
		this.numOfPlayers = numOfPlayers;
	}
	
	@ModelAttribute("rake")
	public double getRake()
	{
		return Constants.RAKE;
	}
	
	@ModelAttribute("buyInPlusRake")
	public double getBuyInPlusRake()
	{
		return getRake() * getBuyIn();
	}
	
	/**
	 * @return
	 * @uml.property  name="buyIn"
	 */
	public double getBuyIn() {
		return buyIn;
	}
	/**
	 * @param buyIn
	 * @uml.property  name="buyIn"
	 */
	public void setBuyIn(double buyIn) {
		this.buyIn = buyIn;
	}
	/**
	 * @param curNumOfPlayers
	 * @uml.property  name="curNumOfPlayers"
	 */
	public void setCurNumOfPlayers(int curNumOfPlayers) {
		this.curNumOfPlayers = curNumOfPlayers;
	}
	/**
	 * @return
	 * @uml.property  name="curNumOfPlayers"
	 */
	public int getCurNumOfPlayers() {
		return curNumOfPlayers;
	}
	/**
	 * @param season
	 * @uml.property  name="season"
	 */
	public void setSeason(Long season) {
		this.season = season;
	}
	/**
	 * @return
	 * @uml.property  name="season"
	 */
	public Long getSeason() {
		return season;
	}
	
	/**
	 * @uml.property  name="firstMatch"
	 * @uml.associationEnd  
	 */
	@Formula("(select m.date from realworld__match m where m.league_id = base_league_id and m.season = season and m.round = startinground order by m.date asc limit 1)")
	private Calendar firstMatchCal;
	
	
	public Calendar getFirstMatch() {
		return firstMatchCal;
	}

	public void setFirstMatch(Calendar firstMatch) {
		this.firstMatchCal = firstMatch;
	}
	
	@ModelAttribute("firstMatchDate")
	public Date getFirstMatchDate(){
		return firstMatchCal.getTime();
	}

	@ModelAttribute("registrationDeadline")
	public Date getRegistrationDeadline(){
		Calendar cal = (Calendar) firstMatchCal.clone();
		cal.add(Calendar.MINUTE, -Constants.DEADLINE_MINUTES);
		return cal.getTime();
	}

	@ModelAttribute("prizePool")
	public double getPrizePool(){
		return this.getBuyIn() * this.getCurNumOfPlayers();
	}
	public void setVirtualTeams(List<VirtualTeam> virtualTeams) {
		this.virtualTeams = virtualTeams;
	}
	public List<VirtualTeam> getVirtualTeams() {
		return virtualTeams;
	}
	/**
	 * @param budget
	 * @uml.property  name="budget"
	 */
	public void setBudget(int budget) {
		this.budget = budget;
	}
	/**
	 * @return
	 * @uml.property  name="budget"
	 */
	public int getBudget() {
		return budget;
	}
	/**
	 * @param baseLeague
	 * @uml.property  name="baseLeague"
	 */
	public void setBaseLeague(League baseLeague) {
		this.baseLeague = baseLeague;
	}
	/**
	 * @return
	 * @uml.property  name="baseLeague"
	 */
	public League getBaseLeague() {
		return baseLeague;
	}
	public void setVirtualAthletes(List<VirtualLeagueAthlete> virtualAthletes) {
		this.virtualAthletes = virtualAthletes;
	}
	public List<VirtualLeagueAthlete> getVirtualAthletes() {
		return virtualAthletes;
	}
	private double round2(double d){
		return Math.round(d*100)/100.0d;
	}
	
	/**
	 * Computation of prizes based on the number of players
	 * 
	 * @return
	 */
	public List<Double> getPrizeDistributionProgressive()
	{
		int np = curNumOfPlayers;
		List<Double> res = new ArrayList<Double>();
		double pool = buyIn*np;
		if (np < 4){
			res.add(round2(pool*1));
		}
		else if (np < 8){
			res.add(round2(pool*0.70));
			res.add(round2(pool*0.30));
		}
		else if (np < 16){
			res.add(round2(pool*0.45));
			res.add(round2(pool*0.25));
			res.add(round2(pool*0.15));
			res.add(round2(pool*0.15));
		}
		else if (np < 32){
			res.add(round2(pool*0.35));
			res.add(round2(pool*0.20));
			res.add(round2(pool*0.10));
			res.add(round2(pool*0.10));
			res.add(round2(pool*0.0625));
			res.add(round2(pool*0.0625));
			res.add(round2(pool*0.0625));
			res.add(round2(pool*0.0625));
		}
		else { //if (numOfPlayers <= 64){
			res.add(round2(pool*0.3));
			res.add(round2(pool*0.18));
			res.add(round2(pool*0.10));
			res.add(round2(pool*0.10));
			res.add(round2(pool*0.04));
			res.add(round2(pool*0.04));
			res.add(round2(pool*0.04));
			res.add(round2(pool*0.04));
			for (int i=0; i<8; i++){
				res.add(round2(pool*0.02));
			}
		}
		while (res.size() < np){
			res.add(0d);
		}
		return res;
	}
	
	/**
	 * Compute the prizes in Double-Or-Nothing tournaments
	 * @return
	 */
	public List<Double> getPrizeDistributionDouble(){
		int np = curNumOfPlayers;
		List<Double> res = new ArrayList<Double>();
		int i;
		for (i=0; i<(np/2);i++)
			res.add(round2(buyIn*2));		
		if (np%2 == 1)
			res.add(round2(buyIn));		
		while (res.size() < np)
			res.add(0d);
		return res;
	}

	/**
	 * Get the distribuion of prizes depending on the tournament type
	 * For the moment only progressive prizes
	 * TODO: Should be moved out from here
	 * @return
	 */
	@ModelAttribute("prizeDistribution")
	public List<Double> getPrizeDistribution(){
		return getPrizeDistributionProgressive();
	}
	public Double getPrize(int pos) {
		List<Double> pdist = getPrizeDistribution();
		if (pdist.size() <= pos)
			return 0d;
		return pdist.get(pos);
	}
	public List<Match> getMatches() {
		MatchDAO mdao = new MatchDAOImpl();
		return mdao.getMatches(this.baseLeague, this.season, new Long(this.startingRound), new Long(this.startingRound + this.duration - 1));
	}

	public boolean isFinished() {
		return status == 1L;
	}
	public void setFinished() {
		this.status = 1L;
	}

	public boolean hasOnlyOnePlayer() {
		Long lastId = this.virtualTeams.get(0).getPlayer().getId();
		
		for (VirtualTeam vt : this.virtualTeams){
			if (lastId != vt.getPlayer().getId()){
				return false;
			}
		}
		return true;
	}
	
	@ModelAttribute("closed")
	public boolean isClosed() {
    	Calendar now = Calendar.getInstance();
    	Calendar deadline = Calendar.getInstance();
    	deadline.setTime(this.getRegistrationDeadline());
    	return now.after(deadline);
  	}

	@ModelAttribute("started")
	public boolean isStarted() {
    	Calendar now = Calendar.getInstance();
    	Calendar deadline = Calendar.getInstance();
    	deadline.setTime(this.getFirstMatchDate());
    	deadline.add(Calendar.MINUTE, -Constants.LAST_CHANGE_MINUTES);
    	return now.after(deadline);
  	}
}
