package com.lemonet.dreamball.dreamworld.domain;

import java.util.List;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.realworld.domain.Round;

public interface VirtualTeamDAO {
    public void create(VirtualTeam vTeam) throws NotEnoughMoneyException;
    public void update(VirtualTeam vTeam) throws NotEnoughMoneyException;
	public List<VirtualTeam> getTeamsByPlayer(Player player);
	public VirtualTeam getVirtualTeamById(Long id);
	public List<VirtualTeam> getPlayerTeamsInLeague(Player player, VirtualLeague virtualLeague);
	public Long getPoints(VirtualTeam virtualTeam);
	public Long getNumOfPlayerTeams(VirtualLeague virtualLeague, Player player);
	public List<VirtualTeam> getActiveVirtualTeams();
	public void merge(VirtualTeam vTeam);
	public List<VirtualTeam> getTeamsFinishedByPlayer(Player p, Round round);
	public void addPlace(VirtualTeam virtualTeam);
	public Object getRecentTeamsByPlayer(Player p);
	public Object getFinishedTeamsByPlayer(Player p);
	public VirtualTeam getLatestUnemptyMainTeam(Player p);
}
