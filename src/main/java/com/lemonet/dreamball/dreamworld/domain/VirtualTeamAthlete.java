package com.lemonet.dreamball.dreamworld.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties({"virtualTeam"})
@Table(name = "dreamworld__virtualteam__athlete",
		uniqueConstraints = {@UniqueConstraint(columnNames={"virtual_team_id", "virtual_athlete_id"})})
public class VirtualTeamAthlete {
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
	 * @uml.property  name="virtualTeam"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "virtual_team_id", nullable = false)
	private VirtualTeam virtualTeam;

	/**
	 * @uml.property  name="virtualLeagueAthlete"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "virtual_athlete_id", nullable = false)
	private VirtualLeagueAthlete virtualLeagueAthlete;

	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return
	 * @uml.property  name="virtualTeam"
	 */
	public VirtualTeam getVirtualTeam() {
		return virtualTeam;
	}

	/**
	 * @param virtualTeam
	 * @uml.property  name="virtualTeam"
	 */
	public void setVirtualTeam(VirtualTeam virtualTeam) {
		this.virtualTeam = virtualTeam;
	}

	/**
	 * @return
	 * @uml.property  name="virtualLeagueAthlete"
	 */
	public VirtualLeagueAthlete getVirtualLeagueAthlete() {
		return virtualLeagueAthlete;
	}

	/**
	 * @param virtualLeagueAthlete
	 * @uml.property  name="virtualLeagueAthlete"
	 */
	public void setVirtualLeagueAthlete(VirtualLeagueAthlete virtualLeagueAthlete) {
		this.virtualLeagueAthlete = virtualLeagueAthlete;
	}

}
