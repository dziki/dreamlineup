package com.lemonet.dreamball.dreamworld.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "dreamworld__bonus")
public class VirtualLeagueBonus {
	public static Integer PLACE_BONUS = 1;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "virtual_league_id", nullable = false)
	private VirtualLeague virtualLeague;
	
	public Integer type;
	public Integer place;
	public String description;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getPlace() {
		return place;
	}

	public void setPlace(Integer place) {
		this.place = place;
	}

	public String getDescription() {
		return description;
	}

	public void setValue(String description) {
		this.description = description;
	}


	public VirtualLeague getVirtualLeague() {
		return virtualLeague;
	}

	public void setVirtualLeague(VirtualLeague virtualLeague) {
		this.virtualLeague = virtualLeague;
	}
	
}
