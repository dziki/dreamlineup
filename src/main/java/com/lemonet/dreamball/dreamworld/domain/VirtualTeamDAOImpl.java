package com.lemonet.dreamball.dreamworld.domain;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.realworld.domain.Round;

@Repository("virtualTeamDAO")
public class VirtualTeamDAOImpl implements VirtualTeamDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Autowired OperationDAO odao;
	
	/**
	 * @uml.property  name="session"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="com.lemonet.dreamball.dreamworld.domain.VirtualTeam"
	 */

	@Override
	public void create(VirtualTeam vTeam) throws NotEnoughMoneyException {
	}

	@Override
	public void update(VirtualTeam vTeam) {
		Session session = sessionFactory.getCurrentSession();
    	session.update(vTeam);
	}
	
	@Override
	public void merge(VirtualTeam vTeam) {
		Session session = sessionFactory.getCurrentSession();
    	session.merge(vTeam);
	}
	
	@Override
	public List<VirtualTeam> getTeamsByPlayer(Player player) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualTeam.class);
    	criteria.add(Restrictions.eq("player", player));

    	@SuppressWarnings("unchecked")
    	List<VirtualTeam> vTeams = criteria.list();

    	for (VirtualTeam vt : vTeams)
    		if (vt.getNumOfAthletes() == 11)
    			vt.setValid(true);

    	return vTeams;
	}

	@Override
	public VirtualTeam getVirtualTeamById(Long id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from VirtualTeam where id = :id");
		query.setParameter("id", id);
		return (VirtualTeam) query.uniqueResult();
	}
	
	@Override
	public List<VirtualTeam> getPlayerTeamsInLeague(Player player,
			VirtualLeague virtualLeague) {
		
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualTeam.class);
    	criteria.add(Restrictions.eq("player", player));
    	criteria.add(Restrictions.eq("virtualLeague", virtualLeague));

    	@SuppressWarnings("unchecked")
    	List<VirtualTeam> vTeams = criteria.list();
    	
    	for (VirtualTeam vt : vTeams)
    		if (vt.getNumOfAthletes() == 11)
    			vt.setValid(true);

        return vTeams;
	}

	@Override
	public Long getPoints(VirtualTeam virtualTeam) {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select sum(p.points) " +
    			"from VirtualTeam vt " +
    			"left join vt.virtualAthletes as vta " +
    			"left outer join vta.virtualLeagueAthlete.athlete.performances as p " +
    			"where vt = :virtualTeam and " +
    			"p.match.round >= vt.virtualLeague.startingRound and " +
    			"p.match.round <= vt.virtualLeague.startingRound + vt.virtualLeague.duration " +
    			"group by vt.id");
    	query.setParameter("virtualTeam", virtualTeam);
    	return (Long) query.uniqueResult();
	}
	
	@Override
	public void addPlace(VirtualTeam virtualTeam){
		if (!virtualTeam.getVirtualLeague().isFinished())
			return;
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select count(vt) " +
    			"from VirtualTeam vt " +
    			"where (vt.points > :points or (vt.points = :points and vt.teamPrice > :teamPrice))"
    			+ "and vt.virtualLeague = :virtualLeague");
    	query.setParameter("points", virtualTeam.getPoints());
    	query.setParameter("teamPrice", virtualTeam.getTeamPrice());
    	query.setParameter("virtualLeague", virtualTeam.getVirtualLeague());
    	
    	virtualTeam.setPlace(1L + (Long) query.uniqueResult());
	}

	@Override
	public List<VirtualTeam> getActiveVirtualTeams() {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualTeam.class);
    	criteria.addOrder(Order.asc("virtualLeague.id"));

    	@SuppressWarnings("unchecked")
    	List<VirtualTeam> vTeams = criteria.list();

        return vTeams;
	}

	@Override
	public List<VirtualTeam> getTeamsFinishedByPlayer(Player p, Round round) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select vt "
				+ "from VirtualTeam vt "
				+ "left join vt.virtualLeague as vl "
				+ "where vl.status = 1 and "
				+ "vl.startingRound + vl.duration -1 = :rnum and "
				+ "vt.player = :p "
				+ "order by vl.id desc");
    	query.setParameter("rnum", round.getNumber());
    	query.setParameter("p", p);

    	@SuppressWarnings("unchecked")
		List<VirtualTeam> vt = query.list();
   
    	return vt;
	}

	@Override
	public Long getNumOfPlayerTeams(VirtualLeague virtualLeague, Player player){
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select vt " +
    			"from VirtualTeam vt " +
    			"where vt.virtualLeague = :virtualLeague and " +
    			"vt.player = :player ");
    	query.setParameter("virtualLeague", virtualLeague);
    	query.setParameter("player", player);

    	@SuppressWarnings("unchecked")
		List<VirtualTeam> vt = query.list();
       
    	return new Long(vt.size());
	}

	@Override
	public List<VirtualTeam> getRecentTeamsByPlayer(Player p) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualTeam.class,"vt");
    	criteria.add(Restrictions.eq("vt.player", p));
    	criteria.createAlias("vt.virtualLeague", "vl");
    	criteria.add(Restrictions.eq("vl.status", 0L));

    	@SuppressWarnings("unchecked")
    	List<VirtualTeam> vTeams = criteria.list();

    	for (VirtualTeam vt : vTeams)
    		if (vt.getNumOfAthletes() == 11)
    			vt.setValid(true);

    	return vTeams;
    }

	@Override
	public Object getFinishedTeamsByPlayer(Player p) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualTeam.class,"vt");
    	criteria.add(Restrictions.eq("vt.player", p));
    	criteria.createAlias("vt.virtualLeague", "vl");
    	criteria.add(Restrictions.eq("vl.status", 1L));
    	criteria.addOrder(Order.desc("vl.id"));
    	
    	criteria.setMaxResults(5);

    	@SuppressWarnings("unchecked")
    	List<VirtualTeam> vTeams = criteria.list();

    	for (VirtualTeam vt : vTeams)
    		if (vt.getNumOfAthletes() == 11)
    			vt.setValid(true);

    	return vTeams;
	}

	@Override
	public VirtualTeam getLatestUnemptyMainTeam(Player p) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualTeam.class,"vt");
    	criteria.add(Restrictions.eq("vt.player", p));
    	criteria.createAlias("vt.virtualLeague", "vl");
    	criteria.createAlias("vl.bonuses", "b");
    	criteria.createAlias("vt.virtualAthletes", "a");
    	criteria.add(Restrictions.isNotNull("b.id"));
    	criteria.add(Restrictions.isNotNull("a.id"));
    	criteria.addOrder(Order.desc("vt.id"));
    	
    	criteria.setMaxResults(1);

    	return (VirtualTeam)criteria.uniqueResult();
	}
}
