package com.lemonet.dreamball.dreamworld.domain;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lemonet.dreamball.realworld.domain.Athlete;

@Repository("virtualLeagueAthleteDAO")
public class VirtualLeagueAthleteDAOImpl implements VirtualLeagueAthleteDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public VirtualLeagueAthlete getByID(Long id) {
		VirtualLeagueAthlete vlathlete = null;
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualLeagueAthlete.class)
    		.add(Restrictions.eq("id", id));
        return (VirtualLeagueAthlete) criteria.uniqueResult();
	}

	@Override
	public List<VirtualLeagueAthlete> getVirtualLeagueAthlets(
			VirtualLeague virtualLeague) {
		
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualLeagueAthlete.class)
    		.add(Restrictions.eq("virtualLeague", virtualLeague))
    		.setMaxResults(50);

    	@SuppressWarnings("unchecked")
		List<VirtualLeagueAthlete> athlets = criteria.list();
        return athlets;
	}

	@Override
	public void create(VirtualLeagueAthlete vla) {
    	Session session = sessionFactory.getCurrentSession();
		session.save(vla);
	}

	@Override
	public void update(VirtualLeagueAthlete vla) {
    	Session session = sessionFactory.getCurrentSession();
    	session.update(vla);
	}

	@Override
	public void update(VirtualLeagueAthlete vla, Session session) {
    	session.saveOrUpdate(vla);
	}

	@Override
	public void delete(VirtualLeagueAthlete vla) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VirtualLeagueAthlete getVirtualLeagueAthlete(VirtualLeague vl,
			Athlete a) {
		VirtualLeagueAthlete vlathlete = null;
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualLeagueAthlete.class)
    		.add(Restrictions.eq("virtualLeague", vl))
    		.add(Restrictions.eq("athlete", a));
    	return (VirtualLeagueAthlete) criteria.uniqueResult();
	}

	/**
	 * Returns a list of athletes playing in this tournament.
	 * Designed for list of players in Draft Room.
	 */
	@Override
	public List<VirtualLeagueAthlete> getVirtualLeagueAthlets(VirtualLeague vl,
			Long teamId, String pos, Long page) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualLeagueAthlete.class)
    		.createAlias("athlete","a")
    		.add(Restrictions.eq("virtualLeague", vl))
    		.addOrder(Order.desc("price"));

   	
    	if (Arrays.asList("G","D","M","S").contains(pos))
    		criteria.add(Restrictions.eq("a.position", pos));
    	if (teamId != null){
    		criteria.createAlias("a.team", "team");
    		criteria.add(Restrictions.eq("team.id", teamId));
    	}
    	
    	criteria.setMaxResults(50);
    	criteria.setFirstResult((int) (page * 50));

    	@SuppressWarnings("unchecked")
		List<VirtualLeagueAthlete> athlets = criteria.list();

        return athlets;
	}

}