package com.lemonet.dreamball.dreamworld.domain;

import java.util.List;

import org.hibernate.Session;

import com.lemonet.dreamball.realworld.domain.Athlete;

public interface VirtualLeagueAthleteDAO {
	void create(VirtualLeagueAthlete vla);
	void update(VirtualLeagueAthlete vla);
	void delete(VirtualLeagueAthlete vla);

	VirtualLeagueAthlete getByID(Long vTeamAthleteId);
	List<VirtualLeagueAthlete> getVirtualLeagueAthlets(VirtualLeague virtualLeague);
	void update(VirtualLeagueAthlete vla, Session session);
	VirtualLeagueAthlete getVirtualLeagueAthlete(VirtualLeague vl, Athlete a);
	List<VirtualLeagueAthlete> getVirtualLeagueAthlets(VirtualLeague vl,
			Long teamId, String pos, Long page);
}
