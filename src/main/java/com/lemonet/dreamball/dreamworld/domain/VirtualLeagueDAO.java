package com.lemonet.dreamball.dreamworld.domain;

import java.util.List;

import com.lemonet.dreamball.auth.domain.Player;

public interface VirtualLeagueDAO {
    public List<VirtualLeague> getLeagues();
    public void create(VirtualLeague vleague);
    public void update(VirtualLeague vleague);
	public VirtualLeague getLeagueById(Long id);
	public List<VirtualLeague> getLeaguesByPlayer(Player player);
	public List<VirtualLeague> getUnevaluatedLeagues();
	List<VirtualTeam> getStandings(VirtualLeague league);
	public List<VirtualLeague> getOpenLeagues();
	public VirtualLeague getCurrentSponsoredLeague();
	public List<VirtualLeague> getRecentLeagues();

}
