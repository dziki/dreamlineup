package com.lemonet.dreamball.dreamworld.domain;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lemonet.dreamball.realworld.domain.Team;

@Repository("virtualTeamAthleteDAO")
public class VirtualTeamAthleteDAOImpl implements VirtualTeamAthleteDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void create(VirtualTeamAthlete vta) {
		this.update(vta);
	}

	@Override
	public void update(VirtualTeamAthlete vta) {
		Session session = sessionFactory.getCurrentSession();
    	session.saveOrUpdate(vta);
	}

	@Override
	public void delete(VirtualTeamAthlete vta) {
		Session session = sessionFactory.getCurrentSession();
    	session.delete(vta);
	}

	@Override
	public VirtualTeamAthlete getByID(Long id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(VirtualTeamAthlete.class)
    		.add(Restrictions.eq("id", id));
    	return (VirtualTeamAthlete) criteria.uniqueResult();
	}

	@Override
	public Long countPlayersOnPos(VirtualTeam vt, String pos) {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("" +
    			"select count(vta.id) " +
    			"from VirtualTeamAthlete as vta " +
    			"inner join vta.virtualLeagueAthlete as vla " +
    			"inner join vla.athlete as a " +
    			"where vta.virtualTeam = :vt and a.position = :pos");
    	query.setParameter("vt", vt);
    	query.setParameter("pos", pos);

        return (Long) query.uniqueResult();

	}

	@Override
	public Long getRealTeamFrequency(VirtualTeam vt, Team team) {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("" +
    			"select count(vta.id) " +
    			"from VirtualTeamAthlete as vta " +
    			"inner join vta.virtualLeagueAthlete as vla " +
    			"inner join vla.athlete as a " +
    			"where a.team = :team and vta.virtualTeam = :vt");
    	query.setParameter("team", team);
    	query.setParameter("vt", vt);

        return (Long) query.uniqueResult();
	}

}
