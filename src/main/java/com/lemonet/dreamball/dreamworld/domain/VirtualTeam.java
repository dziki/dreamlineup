package com.lemonet.dreamball.dreamworld.domain;

import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.Formula;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.lemonet.dreamball.auth.domain.Player;
@Entity
@JsonIgnoreProperties({"virtualLeague","player"})
@Table(name = "dreamworld__virtualteam")
public class VirtualTeam {
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	public static List<String> FORMATIONS = Arrays.asList("5-3-2", "4-4-2", "4-3-3", "4-2-4", "3-4-3", "3-5-2", "3-3-4");
	
	@ModelAttribute("availableFormations")
	public List<String> getAvailableFormations(){
		return FORMATIONS;
	}

	@ModelAttribute("prize")
	public Double getPrize(){
		if (this.getPlace() == null)
			return null;
		return virtualLeague.getPrize(this.getPlace().intValue() -1);
	}

	/**
	 * @uml.property  name="virtualLeague"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "virtual_league_id", nullable = false)
	private VirtualLeague virtualLeague;

	/**
	 * @uml.property  name="player"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "player_id", nullable = false)
	private Player player;
	
	/**
	 * @uml.property  name="virtualAthletes"
	 */
	@OneToMany(mappedBy="virtualTeam", cascade = CascadeType.ALL, fetch=FetchType.LAZY)  
	private List<VirtualTeamAthlete> virtualAthletes;

	/**
	 * @uml.property  name="teamPrice"
	 */
	@Formula("(select sum(vla.price) from dreamworld__virtualteam__athlete vta " +
			"inner join dreamworld__virtualleague__athlete vla on (vta.virtual_athlete_id = vla.id) " +
			"where vta.virtual_team_id = id)")
	private Long teamPrice;
	
	@Formula("(select sum(p.points) " +
    			"from realworld__performance p "
    			+ "left join realworld__athlete a on (a.id = p.athlete_id)  "
    			+ "left join dreamworld__virtualleague__athlete vla on (vla.athlete_id = a.id) " 
    			+ "left join dreamworld__virtualteam__athlete vta on (vta.virtual_athlete_id = vla.id) " 
    			+ "left join dreamworld__virtualteam vt on (vta.virtual_team_id = vt.id) " 
    			+ "left join realworld__match m on (p.match_id = m.id) " 
    			+ "left join dreamworld__virtualleague vl on (vt.virtual_league_id = vl.id) " 
    			+ "where vt.id = id and m.round >= vl.startinground and m.round <= vl.startinground + vl.duration - 1)")
	private Long points;
	
	@Formula("(select count(*) from dreamworld__virtualteam__athlete vta " +
			"where vta.virtual_team_id = id)")
	private Long numAthletes;
	

	public Long getNumAthletes() {
		return numAthletes;
	}

	public void setNumAthletes(Long numAthletes) {
		this.numAthletes = numAthletes;
	}

	@Transient
	boolean valid;

	@Transient
	Long place;


	public void setPlace(Long place) {
		this.place = place;
	}

	public Long getPoints() {
		if (points != null)
			return points;
		return 0L;
	}
	public void setPoints(Long points) {
		this.points = points;
	}

	/**
	 * @uml.property  name="formation"
	 */
	private String formation = "4-4-2";

	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return
	 * @uml.property  name="virtualLeague"
	 */
	public VirtualLeague getVirtualLeague() {
		return virtualLeague;
	}
	/**
	 * @param virtualLeague
	 * @uml.property  name="virtualLeague"
	 */
	public void setVirtualLeague(VirtualLeague virtualLeague) {
		this.virtualLeague = virtualLeague;
	}
	/**
	 * @return
	 * @uml.property  name="player"
	 */
	public Player getPlayer() {
		return player;
	}
	/**
	 * @param player
	 * @uml.property  name="player"
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}
	public void setVirtualAthletes(List<VirtualTeamAthlete> virtualAthletes) {
		this.virtualAthletes = virtualAthletes;
	}
	@ModelAttribute("virtualAthletes")
	public List<VirtualTeamAthlete> getVirtualAthletes() {
		return virtualAthletes;
	}
	/**
	 * @param teamPrice
	 * @uml.property  name="teamPrice"
	 */
	public void setTeamPrice(Long teamPrice) {
		this.teamPrice = teamPrice;
	}
	public int getTeamPrice() {
		return (teamPrice==null)?0:teamPrice.intValue();
	}

	/**
	 * @return
	 * @uml.property  name="formation"
	 */
	public String getFormation() {
		return formation;
	}

/*	@ModelAttribute("points")
	public Long getPoints() {
		VirtualTeamDAO vtdao = new VirtualTeamDAOImpl();
		return vtdao.getPoints(this);
	}*/
	
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public Integer getNumOfAthletes() {
		return this.getVirtualAthletes().size();
	}
	public void setFormation(String formation) {
		this.formation = formation;
		
	}

	public Long getPlace() {
		return place;
	}

}
