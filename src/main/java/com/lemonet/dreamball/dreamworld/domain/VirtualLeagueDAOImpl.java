package com.lemonet.dreamball.dreamworld.domain;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.realworld.domain.AthleteDAO;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.Round;
import com.lemonet.dreamball.realworld.domain.RoundDAO;

@Repository("virtualLeagueDAO")
public class VirtualLeagueDAOImpl implements VirtualLeagueDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Autowired LeagueDAO ldao;
	@Autowired RoundDAO rdao;
	@Autowired AthleteDAO adao;
	@Autowired VirtualLeagueAthleteDAO vladao;
	@Autowired OperationDAO odao;

	/**
	 * @uml.property  name="session"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="com.lemonet.dreamball.dreamworld.domain.VirtualLeague"
	 */
	public static class FireException extends Exception {
		private static final long serialVersionUID = 1L;

		public FireException(String msg){
			super(msg);
		}
	}


	@Override
    public List<VirtualLeague> getLeagues(){
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from VirtualLeague where 1=1");

		@SuppressWarnings("unchecked")
		List<VirtualLeague> res = query.list();

		return res;
    }

	@Override
	@Transactional
	public List<VirtualLeague> getOpenLeagues() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from VirtualLeague where status = 0");

		@SuppressWarnings("unchecked")
		List<VirtualLeague> res = query.list();
		return res;
	}

	@Override
	@Transactional
	public List<VirtualLeague> getRecentLeagues() {
		Round r = rdao.getFirstUnstartedRound(ldao.getLeagueById(Constants.CURRENT_LEAGUE));
		Integer currentRound = 30;
		if (r != null)
			currentRound = r.getNumber().intValue();
		String bonus = " and not bonus is null ";
		
		Calendar cal = Calendar.getInstance();
		int day_of_week = cal.get(Calendar.DAY_OF_WEEK);
		if ((day_of_week == 2 && cal.get(Calendar.HOUR_OF_DAY) > 18) ||
				day_of_week == 3 ||
				day_of_week == 4){
			bonus = "";
		}
		
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select vl "
    			+ "from VirtualLeague vl "
    			+ "left join vl.bonuses as bonus "
       			+ "left join vl.virtualTeams as vt "
    			+ "order by vl.status asc, vl.id asc "
    			+ "group by vl.id, bonus.id "
    			+ "having vl.season = " + Constants.CURRENT_SEASON
    			+ " and (vl.startingRound >= :currentRound "
    			+ "or (count(vt) > 1 and (vl.startingRound + vl.duration >= :currentRound +1 "
    			+ "or (vl.startingRound + vl.duration >= :currentRound "
    			+ bonus
    			+ ") "
    			+ ")))");
    	query.setParameter("currentRound", currentRound);
    	
		@SuppressWarnings("unchecked")
		List<VirtualLeague> res = query.list();
		return res;
	}

	@Override
	public void create(VirtualLeague vleague) {
		this.update(vleague);
	}
	
	@Override
	public void update(VirtualLeague vleague) {
		Session session = sessionFactory.getCurrentSession();
    	session.saveOrUpdate(vleague);
	}

	@Override
	public VirtualLeague getLeagueById(Long id) {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("from VirtualLeague where id = :id");
    	query.setParameter("id", id);

    	return (VirtualLeague) query.uniqueResult();
	}

	@Override
	public List<VirtualLeague> getLeaguesByPlayer(Player player) {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualLeague.class);
    	criteria.setFetchMode("virtualLeaguePlayers", FetchMode.JOIN)
    			.createAlias("virtualLeaguePlayers", "virtualLeaguePlayer")
    			.add(Restrictions.eq("virtualLeaguePlayer.player", player));

    	@SuppressWarnings("unchecked")
    	List<VirtualLeague> vLeagues = criteria.list();

        return vLeagues;
	}

	@Override
	public List<VirtualLeague> getUnevaluatedLeagues() {
		Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(VirtualLeague.class)
    		.add(Restrictions.eq("state", 0));

    	@SuppressWarnings("unchecked")
    	List<VirtualLeague> vLeagues = criteria.list();

    	return vLeagues;
	}

	@Override
	public List<VirtualTeam> getStandings(VirtualLeague league) {
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("select vt " +
    			"from VirtualTeam vt " +
    			"left outer join vt.virtualAthletes as vta " +
    			"left outer join vta.virtualLeagueAthlete.athlete.performances as p " +
    			"where vt.virtualLeague = :virtualLeague "
    			+ " and (p.match.round >= vt.virtualLeague.startingRound and p.match.round <= vt.virtualLeague.startingRound + vt.virtualLeague.duration - 1) and" +
    			" (p.match.season = " + league.getSeason() + ") " 
    			+ " group by vt.id "
    			+ " having vt.numAthletes = 11"
    			+ " order by case when sum(p.points) is null then 0 else sum(p.points) end desc");
    	query.setParameter("virtualLeague", league);

    	@SuppressWarnings("unchecked")
		List<VirtualTeam> vt = query.list();
   
    	return vt;
	}

	@Override
	public VirtualLeague getCurrentSponsoredLeague() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select vt "
				+ "from VirtualLeague vt "
				+ "left join vt.bonuses as b "
				+ "where status = 0 and "
				+ "b is not null "
				+ "order by vt.id desc");
		query.setMaxResults(1);

		return (VirtualLeague) query.uniqueResult();
	}

}