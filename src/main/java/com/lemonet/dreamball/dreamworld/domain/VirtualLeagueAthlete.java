package com.lemonet.dreamball.dreamworld.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.Formula;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.lemonet.dreamball.realworld.domain.Athlete;

@Entity
@JsonIgnoreProperties({"virtualLeague"})
@Table(name = "dreamworld__virtualleague__athlete")
public class VirtualLeagueAthlete {
	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
	 * @uml.property  name="virtualLeague"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "virtual_league_id", nullable = false)
	private VirtualLeague virtualLeague;

	/**
	 * @uml.property  name="athlete"
	 * @uml.associationEnd  
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "athlete_id", nullable = false)
	private Athlete athlete;
	
	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return
	 * @uml.property  name="virtualLeague"
	 */
	public VirtualLeague getVirtualLeague() {
		return virtualLeague;
	}
	/**
	 * @param virtualLeague
	 * @uml.property  name="virtualLeague"
	 */
	public void setVirtualLeague(VirtualLeague virtualLeague) {
		this.virtualLeague = virtualLeague;
	}
	/**
	 * @return
	 * @uml.property  name="points"
	 */
	public Long getPoints() {
		if (points == null)
			return 0L;
		return points;
	}
	/**
	 * @param points
	 * @uml.property  name="points"
	 */
	public void setPoints(Long points) {
		this.points = points;
	}
	/**
	 * @return
	 * @uml.property  name="price"
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price
	 * @uml.property  name="price"
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @param athlete
	 * @uml.property  name="athlete"
	 */
	public void setAthlete(Athlete athlete) {
		this.athlete = athlete;
	}
	/**
	 * @return
	 * @uml.property  name="athlete"
	 */
	public Athlete getAthlete() {
		return athlete;
	}
	/**
	 * @uml.property  name="points"
	 */
	@Formula("(select sum(p.points) " +
			"from realworld__performance p "
			+ "left join realworld__athlete a on (a.id = p.athlete_id)  "
			+ "left join dreamworld__virtualleague__athlete vla on (vla.athlete_id = a.id) "
			+ "left join realworld__match m on (p.match_id = m.id) " 
			+ "left join dreamworld__virtualleague vl on (vla.virtual_league_id = vl.id) " 
			+ "where vla.id = id and m.round >= vl.startinground and m.round <= vl.startinground + vl.duration - 1)")
	private Long points;

	/**
	 * @uml.property  name="price"
	 */
	@NumberFormat(style = Style.NUMBER, pattern = "#,##0,, ;[Red](#,##0,,);- ;")
	private double price;

	public void addPoints(Long points2) {
		points += points2;
	}
}
