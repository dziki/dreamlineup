package com.lemonet.dreamball.dataprovider;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.lemonet.dreamball.realworld.domain.Athlete;
import com.lemonet.dreamball.realworld.domain.AthleteDAO;
import com.lemonet.dreamball.realworld.domain.AthleteDAOImpl;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.realworld.domain.MatchDAOImpl;
import com.lemonet.dreamball.realworld.domain.Performance;
import com.lemonet.dreamball.realworld.domain.PerformanceDAO;
import com.lemonet.dreamball.realworld.domain.PerformanceDAOImpl;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;
import com.lemonet.dreamball.realworld.domain.TeamDAOImpl;

@Component
public class StatisticHandler extends GenericHandler{
	/**
	 * @uml.property  name="teams"
	 * @uml.associationEnd  qualifier="id:java.lang.Long com.lemonet.dreamball.realworld.domain.Team"
	 */
	List<Performance> performances = new ArrayList<Performance>();
	/**
	 * @uml.property  name="personName"
	 */
	Performance currentPerformance;
	Team currentTeam;
	Athlete currentAthlete;
	Match currentMatch;
	String tmpValue;
	
	@Autowired PerformanceDAO pdao;
	@Autowired TeamDAO tdao;
	@Autowired MatchDAO mdao;
	@Autowired AthleteDAO adao;
	
	String variableName;
	Logger logger = Logger.getLogger("PARSER");

	boolean dontSave = false;

	@Override
	public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("statystyka")){
			currentPerformance = new Performance();
		}
		
		super.startElement(uri, localName, qName, attributes);
	}
	
	private void updateEntities(){
		for (Performance p : performances){
			p.setPosition(p.getAthlete().getPosition());
			logger.trace("PARSER: Adding perf of " + p.getAthlete().getId().toString());
			logger.trace("PARSER: (match " + p.getMatch().getOutsideId().toString() + ")");
			pdao.create(p);
		}
	}
 
	@Override
	public void endElement(String uri, String localName,
		String qName) throws SAXException {
 
		if (qName.equalsIgnoreCase("statystyka")){
			performances.add(currentPerformance);
		}
		if (qName.equalsIgnoreCase("statistics_zawodnikow")){
			updateEntities();
		}
		if (qName.equalsIgnoreCase("id_zawodnika")){
			Long athleteOutsideId = new Long(tmpValue);
			currentAthlete = adao.getAthleteByOutsideId(athleteOutsideId);
			currentPerformance.setAthlete(currentAthlete);
			if (currentAthlete == null)
				logger.warn("PARSER: Player " + athleteOutsideId.toString() + " does not exist!");
		}
		if (qName.equalsIgnoreCase("id_meczu")){
			Long matchOutsideId = new Long(tmpValue);
			currentMatch = mdao.getMatchByOutsideId(matchOutsideId);
			currentPerformance.setMatch(currentMatch);
		}
		if (qName.equalsIgnoreCase("id_druzyny")){
			Long teamId = new Long(tmpValue);
		}
		if (qName.equalsIgnoreCase("gole"))
		{
			Long goals = new Long(tmpValue);
			currentPerformance.setGoals(goals);
		}
		if (qName.equalsIgnoreCase("gole_czerwone"))
		{
			currentPerformance.setOwnGoals(new Long(tmpValue));
		}
		if (qName.equalsIgnoreCase("gole_p"))
		{
//			Long goals = new Long(tmpValue);
//			currentPerformance.setGoals(goals);
		}
		if (qName.equalsIgnoreCase("zolta"))
		{
			currentPerformance.setYellowCard(new Long(tmpValue));
		}
		if (qName.equalsIgnoreCase("czerwona"))
		{
			currentPerformance.setRedCard(new Long(tmpValue));
		}
		if (qName.equalsIgnoreCase("time_in"))
		{
			currentPerformance.setTimeIn(new Long(tmpValue));
		}
		if (qName.equalsIgnoreCase("time_out"))
		{
			currentPerformance.setTimeOut(new Long(tmpValue));
		}
		
		super.endElement(uri, localName, qName);
	}
 
    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        tmpValue = new String(ac, i, j);
    }
};
