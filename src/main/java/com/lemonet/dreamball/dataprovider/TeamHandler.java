package com.lemonet.dreamball.dataprovider;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.LeagueDAOImpl;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;
import com.lemonet.dreamball.realworld.domain.TeamDAOImpl;

@Component
public class TeamHandler extends GenericHandler{
	@Autowired TeamDAO tdao;
	@Autowired LeagueDAO ldao;

	/**
	 * @uml.property  name="teams"
	 * @uml.associationEnd  qualifier="id:java.lang.Long com.lemonet.dreamball.realworld.domain.Team"
	 */
	List<Team> teams = new ArrayList<Team>();
	/**
	 * @uml.property  name="personName"
	 */
	Team currentTeam;
	String tmpValue;

	@Override
	public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("druzyna"))
			currentTeam = new Team();

		
		super.startElement(uri, localName, qName, attributes);
	}
	
	private void updateEntities(){
		League l = ldao.getLeagueById(Constants.CURRENT_LEAGUE);

		for (Team t : teams){
			Team nt = tdao.getTeamByOutsideId(t.getOutsideId());
			if (nt == null){
				t.setLeague(l);
				tdao.create(t);
			}
		}
	}
 
	@Override
	public void endElement(String uri, String localName,
		String qName) throws SAXException {
 
		if (qName.equalsIgnoreCase("druzyna")){
			teams.add(currentTeam);
		}
		if (qName.equalsIgnoreCase("druzyny")){
			updateEntities();
		}
		if (qName.equalsIgnoreCase("id")){
			currentTeam.setOutsideId(new Long(tmpValue));
		}
		if (qName.equalsIgnoreCase("nazwa")){
			currentTeam.setName(tmpValue);
		}
		
		super.endElement(uri, localName, qName);
	}
 
    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        tmpValue = new String(ac, i, j);
    }
};
