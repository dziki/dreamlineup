package com.lemonet.dreamball.dataprovider;

import java.util.LinkedList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class GenericHandler extends DefaultHandler {
	/**
	 * @uml.property  name="elements"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="java.lang.String"
	 */
	LinkedList<String> elements = new LinkedList<String>();

	@Override
	public void startElement(String uri, String localName,String qName, 
            Attributes attributes) throws SAXException {
		elements.push(qName);
	}

	@Override
	public void endElement(String uri, String localName,
		String qName) throws SAXException {
		elements.pop();
	}
}
