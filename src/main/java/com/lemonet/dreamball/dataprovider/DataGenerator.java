package com.lemonet.dreamball.dataprovider;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lemonet.dreamball.realworld.domain.Athlete;
import com.lemonet.dreamball.realworld.domain.AthleteDAO;
import com.lemonet.dreamball.realworld.domain.AthleteDAOImpl;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.LeagueDAOImpl;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.realworld.domain.MatchDAOImpl;
import com.lemonet.dreamball.realworld.domain.Round;
import com.lemonet.dreamball.realworld.domain.RoundDAO;
import com.lemonet.dreamball.realworld.domain.RoundDAOImpl;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;
import com.lemonet.dreamball.realworld.domain.TeamDAOImpl;
import com.lemonet.notification.service.EmailService;

@Component
public class DataGenerator {
	@Autowired MatchDAO mdao;
	@Autowired RoundDAO rdao;
	@Autowired AthleteDAO adao;
	@Autowired LeagueDAO ldao;
	@Autowired TeamDAO tdao;


	public void generateMatches(Round round, List<Team> teams, boolean inverse){
		Match m = new Match();
		
		for (int i = 1; i <= 8; i++){
			m.setId(round.getSeason() * 10000 + round.getNumber() * 10 + i);
			m.setLeague(round.getLeague());
// TODO:			m.setRound((long) round.getNumber());
			m.setSeason((long) round.getSeason());
			
			m.setMatchTime(null);
			Calendar cal = new GregorianCalendar();
			cal.set(2013, 8, round.getNumber().intValue(), 20, 0);

			m.setDate(cal);

			if (!inverse) {
				m.setHomeTeam(teams.get(i - 1));
				m.setAwayTeam(teams.get(16 - i));
			}
			else {
				m.setHomeTeam(teams.get(16 - i));
				m.setAwayTeam(teams.get(i - 1));
			}
			
			mdao.create(m);
			
		}
		
	}
	
	public void generateRounds(League league, Long season) {

		for (int i = 1; i <= 30; i++){
			Round r = new Round();
			r.setNumber(new Long(i));
			r.setSeason(season);
			r.setLeague(league);
			r.setStatus(Round.UNSTARTED);
			rdao.create(r);
		}
	}
	
	public static String dir = "/Users/kidzik/Documents/workspace/dreamlineup/datafeed/toy/";

	public void createPlayers(Team team) {
		
		String line;
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(dir + "team" + team.getId().toString() + ".txt");
		} catch (FileNotFoundException e1) {
			EmailService.emailException(e1);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
		try {
			while ((line = br.readLine()) != null) {
				String[] array = line.split(";");
				Athlete athlete = new Athlete();
				String[] name = array[2].split(" ",2);
				if (name.length == 1){
					athlete.setFirstName("");
					athlete.setLastName(name[0]);
				}
				else {
					athlete.setFirstName(name[0]);
					athlete.setLastName(name[1]);
				}
				
				String position = "G";
				if (array[1].equals("OB"))
					position = "D";
				if (array[1].equals("PO"))
					position = "M";
				if (array[1].equals("NA"))
					position = "S";
				athlete.setPosition(position);
				athlete.setShirtNr(Long.parseLong(array[0]));

				athlete.setId(athlete.getShirtNr() + team.getId()*100 + 10000);

				athlete.setTeam(team);
				adao.create(athlete);
			}
		} catch (NumberFormatException e) {
			EmailService.emailException(e);
		} catch (IOException e) {
			EmailService.emailException(e);
		}	
	}
	
	public void createToyData() {
		//base league
		League league = new League();
		league.setCountry("PL");
		league.setDivision(1);
		league.setName("Extraklasa");
		ldao.create(league);
		
		String line;
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(dir + "teams.txt");
		} catch (FileNotFoundException e1) {
			EmailService.emailException(e1);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
		try {
			while ((line = br.readLine()) != null) {
				String[] array = line.split(";");
				Team team = new Team();
				team.setLeague(league);
				team.setId(Long.parseLong(array[0]));
				team.setName(array[1]);
				tdao.create(team);
				createPlayers(team);
			}
		} catch (NumberFormatException e) {
			EmailService.emailException(e);
		} catch (IOException e) {
			EmailService.emailException(e);
		}
		
		generateRounds(league, 2013L);

	}
}
