package com.lemonet.dreamball.dataprovider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.LeagueDAOImpl;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.realworld.domain.MatchDAOImpl;
import com.lemonet.dreamball.realworld.domain.Round;
import com.lemonet.dreamball.realworld.domain.RoundDAO;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;
import com.lemonet.dreamball.realworld.domain.TeamDAOImpl;
import com.lemonet.notification.service.EmailService;

@Component
public class MatchHandler extends GenericHandler{
	/**
	 * @uml.property  name="teams"
	 * @uml.associationEnd  qualifier="id:java.lang.Long com.lemonet.dreamball.realworld.domain.Team"
	 */
	List<Match> matches = new ArrayList<Match>();
	/**
	 * @uml.property  name="personName"
	 */
	Match currentMatch;
	String tmpValue;
	
	@Autowired RoundDAO rdao;
	@Autowired TeamDAO tdao;
	@Autowired LeagueDAO ldao;
	@Autowired MatchDAO mdao;

	String variableName;
	String currentDate;
	String currentTime;

	Long season = Constants.CURRENT_SEASON;

	@Override
	public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("mecz")){
			currentMatch = new Match();
			currentMatch.setSeason(season);
			currentDate = null;
			currentTime = null;
		}

		
		super.startElement(uri, localName, qName, attributes);
	}
	
	private void updateEntities()
	{
		League l = ldao.getLeagueById(Constants.CURRENT_LEAGUE);
		Logger logger = Logger.getLogger("PARSER");

		for (Match m : matches){
		    m.setLeague(l);
		    
		    Match mcurrent = mdao.findMatch(m.getLeague(),m.getSeason(),m.getHomeTeam(),m.getAwayTeam());
		    logger.trace("PARSER: Match " + m.getOutsideId());
		    if (mcurrent == null){
			    logger.trace("PARSER: Not found");
		    	mdao.create(m);
		    }
		    else {
			    logger.trace("PARSER: Found");
		    	boolean dirty = false;
		    	if (!m.getDate().equals(mcurrent.getDate())){
		    		mcurrent.setDate(m.getDate());
		    		dirty = true;
				    logger.trace("PARSER: Date to change");
		    	}
		    	if (m.getHomeScore() != null){
			    	mcurrent.setHomeScore(m.getHomeScore());
			    	mcurrent.setAwayScore(m.getAwayScore());
			    	mcurrent.setGoalTimesAway(m.getGoalTimesAway());
			    	mcurrent.setGoalTimesHome(m.getGoalTimesHome());
		    		dirty = true;
				    logger.trace("PARSER: Score to change: " + mcurrent.getGoalTimesAway() + " vs " + mcurrent.getGoalTimesHome());
		    	}
		    	if (dirty){
		    		mdao.update(mcurrent);
		    		logger.trace("PARSER: Updated");
		    	}
		    }
		}
	}
 
	Logger logger = Logger.getLogger("PARSER");

	@Override
	public void endElement(String uri, String localName,
		String qName) throws SAXException {
 
		if (qName.equalsIgnoreCase("mecz")){
			Calendar cal = Calendar.getInstance();
			
			SimpleDateFormat sdf;
			String format = "dd/MM/yy";
			

			if (currentTime != null)
			{
				format = format + " HH:mm";
				sdf = new SimpleDateFormat(format);
			    try {
					cal.setTime(sdf.parse(currentDate + " " + currentTime));
				} catch (ParseException e) {
					EmailService.emailException(e);
				}
			}
			else
			{
				sdf = new SimpleDateFormat(format);
			    try {
					cal.setTime(sdf.parse(currentDate));
				} catch (ParseException e) {
					EmailService.emailException(e);
				}
			}
			currentMatch.setDate(cal);;

			matches.add(currentMatch);
    		logger.trace("PARSER: Match added to list");
		}
		if (qName.equalsIgnoreCase("matches")){
			updateEntities();
		}
		if (qName.equalsIgnoreCase("id")){
			currentMatch.setOutsideId(new Long(tmpValue));
		}
		if (qName.equalsIgnoreCase("druzyna1_id"))
		{
			Long teamOutsideId = new Long(tmpValue);
			Team t = tdao.getTeamByOutsideId(teamOutsideId);
			currentMatch.setHomeTeam(t);
		}
		if (qName.equalsIgnoreCase("druzyna2_id"))
		{
			Long teamOutsideId = new Long(tmpValue);
			Team t = tdao.getTeamByOutsideId(teamOutsideId);
			currentMatch.setAwayTeam(t);
		}
		if (qName.equalsIgnoreCase("time"))
		{
			if (tmpValue.length() > 4)
				currentTime = tmpValue;
		}
		if (qName.equalsIgnoreCase("data"))
		{
			currentDate = tmpValue;
		}
		if (qName.equalsIgnoreCase("goals1"))
		{
			currentMatch.setGoalTimesHome(tmpValue.replaceAll("\\s",""));
		}
		if (qName.equalsIgnoreCase("goals2"))
		{
			currentMatch.setGoalTimesAway(tmpValue.replaceAll("\\s",""));
		}
		if (qName.equalsIgnoreCase("result"))
		{
			String[] parts = tmpValue.split(" - ");
			if (parts.length == 2){
				currentMatch.setHomeScore(new Long(parts[0]));
				currentMatch.setAwayScore(new Long(parts[1]));
			}
			else {
				currentMatch.setHomeScore(null);
				currentMatch.setAwayScore(null);
			}
		}
		if (qName.equalsIgnoreCase("round"))
		{
			Round round = rdao.getRound(season, ldao.getLeagueById(Constants.CURRENT_LEAGUE), new Long(tmpValue));
			currentMatch.setRound(new Long(tmpValue));
//			currentMatch.setRoundInstance(round);
		}
		
		super.endElement(uri, localName, qName);
	}
 
    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        tmpValue = new String(ac, i, j);
    }
};
