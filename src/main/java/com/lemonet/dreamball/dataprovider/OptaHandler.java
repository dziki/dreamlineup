package com.lemonet.dreamball.dataprovider;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.lemonet.dreamball.realworld.domain.Athlete;
import com.lemonet.dreamball.realworld.domain.AthleteDAO;
import com.lemonet.dreamball.realworld.domain.AthleteDAOImpl;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.LeagueDAOImpl;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.realworld.domain.MatchDAOImpl;
import com.lemonet.dreamball.realworld.domain.Performance;
import com.lemonet.dreamball.realworld.domain.PerformanceDAO;
import com.lemonet.dreamball.realworld.domain.PerformanceDAOImpl;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;
import com.lemonet.dreamball.realworld.domain.TeamDAOImpl;

public class OptaHandler extends GenericHandler{
        /**
         * @uml.property  name="match"
         * @uml.associationEnd  
         */
        Match match;
        
        /**
         * @uml.property  name="type"
         */
        String type;
        /**
         * @uml.property  name="team"
         * @uml.associationEnd  
         */
        Team team;
        /**
         * @uml.property  name="athlete"
         * @uml.associationEnd  
         */
        Athlete athlete;
        
        /**
         * @uml.property  name="newTeam" multiplicity="(0 -1)" dimension="1"
         */
        boolean newTeam[] = new boolean[2];
        
        /**
         * @uml.property  name="tdao"
         * @uml.associationEnd  multiplicity="(1 1)"
         */
        TeamDAO tdao = new TeamDAOImpl();
        /**
         * @uml.property  name="adao"
         * @uml.associationEnd  multiplicity="(1 1)"
         */
        AthleteDAO adao = new AthleteDAOImpl();
        /**
         * @uml.property  name="pdao"
         * @uml.associationEnd  multiplicity="(1 1)"
         */
        PerformanceDAO pdao = new PerformanceDAOImpl();
        /**
         * @uml.property  name="mdao"
         * @uml.associationEnd  multiplicity="(1 1)"
         */
        MatchDAO mdao = new MatchDAOImpl();
        
        /**
         * @uml.property  name="athlets"
         * @uml.associationEnd  qualifier="id:java.lang.Long com.lemonet.dreamball.realworld.domain.Athlete"
         */
        Map<Long, Athlete> athlets = new HashMap<Long, Athlete>();
        /**
         * @uml.property  name="performances"
         * @uml.associationEnd  qualifier="a:com.lemonet.dreamball.realworld.domain.Athlete com.lemonet.dreamball.realworld.domain.Performance"
         */
        Map<Athlete, Performance> performances = new HashMap<Athlete, Performance>();
        /**
         * @uml.property  name="teams"
         * @uml.associationEnd  qualifier="id:java.lang.Long com.lemonet.dreamball.realworld.domain.Team"
         */
        Map<Long, Team> teams = new HashMap<Long, Team>();
        /**
         * @uml.property  name="personName"
         */
        boolean personName = false;
        
        private Athlete getAthlete(Long id){
                Athlete a = athlets.get(id);
                if (a == null){
                        a = adao.getAthleteById(id);
                }
                if (a == null){
                        a = new Athlete();
                        a.setId(new Long(id));
                        a.setDirty(true);
                }
                
                athlets.put(id, a);
                return a;
        }
        private Team getTeam(Long id){
                Team t = teams.get(id);
                if (t == null){
                        t = tdao.getTeamById(id);
                }
                if (t == null){
                        t = new Team();
                        t.setId(id);
                        t.setDirty(true);
                }
                
                teams.put(id, t);
                return t;
        }
        private Performance getPerformance(Athlete a){
                Performance p = performances.get(a);
                if (p == null){
                        p = pdao.getByAthleteMatch(a,match);
                }
                if (p == null){
                        p = new Performance();
                        p.setAthlete(a);
                        p.setMatch(match);
                }
                performances.put(a,p);
                return p;
        }
        
        @Override
		public void startElement(String uri, String localName,String qName, 
                Attributes attributes) throws SAXException {
                if (qName.equalsIgnoreCase("SoccerDocument")){
                        String fixtureRef = attributes.getValue("uID");
                        Long intFixtureRef = new Long(fixtureRef.substring(1));
                        
                        match = mdao.getMatchById(intFixtureRef);
                        if (match == null){
                                match = new Match();
                                match.setId(intFixtureRef);
                        }
                }
                if (qName.equalsIgnoreCase("Stat")){
                        type = attributes.getValue("Type");
                }
                if (qName.equalsIgnoreCase("PersonName")){
                        personName = true;
                }
                
                if (qName.equalsIgnoreCase("Team")){
                        String teamRef = attributes.getValue("uID");
                        Long intTeamRef = new Long(teamRef.substring(1));
                        team = getTeam(intTeamRef);
                }
                
                if (qName.equalsIgnoreCase("TeamData")){
                        String teamType = attributes.getValue("Side");
                        String teamRef = attributes.getValue("TeamRef");
                        Long intTeamRef = new Long(teamRef.substring(1));

                        team = getTeam(intTeamRef);

                        if (teamType.equalsIgnoreCase("Home"))
                                match.setHomeTeam(team);
                        else
                                match.setAwayTeam(team);
                }
                if (qName.equalsIgnoreCase("Booking")){
                        String cardType = attributes.getValue("Card");
                        String athleteId = attributes.getValue("PlayerRef");
                        Long intAthleteId = new Long(athleteId.substring(1));
                        Athlete a = getAthlete(intAthleteId);
                        Performance p = getPerformance(a);
                        if (cardType.equalsIgnoreCase("Yellow"))
                                p.setYellowCard(new Long(1));
                        if (cardType.equalsIgnoreCase("Red"))
                                p.setYellowCard(new Long(1));
                }
                if (qName.equalsIgnoreCase("Goal")){
                        String athleteId = attributes.getValue("PlayerRef");
                        Long intAthleteId = new Long(athleteId.substring(1));
                        Athlete a = getAthlete(intAthleteId);
                        Performance p = getPerformance(a);

                        String type = attributes.getValue("Type");
                        
                        boolean own = false;
                        if (type.equalsIgnoreCase("Own")){
                                p.addOwnGoal();
                        }
                        else {
                                p.addGoal();
                        }
                        if (!own){
                                if ((match.getHomeTeam() == team))
                                        match.addHomeGoal();
                                else
                                        match.addAwayGoal();
                                }
                        else {
                                if ((match.getHomeTeam() == team))
                                        match.addAwayGoal();
                                else
                                        match.addHomeGoal();
                                }
                }
                if (qName.equalsIgnoreCase("Assist")){
                        String athleteId = attributes.getValue("PlayerRef");
                        Long intAthleteId = new Long(athleteId.substring(1));
                        Athlete a = getAthlete(intAthleteId);
                        Performance p = getPerformance(a);
                        p.addAssist();
                }
                
                if (qName.equalsIgnoreCase("PenaltyShot")){
                        String athleteId = attributes.getValue("PlayerRef");
                        Long intAthleteId = new Long(athleteId.substring(1));
                        Athlete a = getAthlete(intAthleteId);
                        Performance p = getPerformance(a);
                        String outcome = attributes.getValue("Outcome");
                        if (outcome.equalsIgnoreCase("Missed")){
                                p.addMissedPenalty();
                        }
                        if (outcome.equalsIgnoreCase("Saved")){
                                // TODO
                        }
                }

                if (qName.equalsIgnoreCase("Substitution")){
                        String athleteOffId = attributes.getValue("SubOff");
                        Long intAthleteOffId = new Long(athleteOffId.substring(1));
                        String athleteOnId = attributes.getValue("SubOn");
                        Long intAthleteOnId = new Long(athleteOnId.substring(1));

                        String timeStr = attributes.getValue("Time");
                        Long time = new Long(timeStr);

                        Athlete aOff = getAthlete(intAthleteOffId);
                        Performance pOff = getPerformance(aOff);
                        
                        Athlete aOn = getAthlete(intAthleteOnId);
                        Performance pOn = getPerformance(aOn);
                        
                        pOff.setTimeIn(time);
                        pOn.setTimeOut(time);
                }
                if (qName.equalsIgnoreCase("MatchPlayer")){
                        String athleteId = attributes.getValue("PlayerRef");
                        String startSquad = attributes.getValue("Status");
                        Long intAthleteId = new Long(athleteId.substring(1));
                        Athlete a = getAthlete(intAthleteId);
                        Performance p = getPerformance(a);
                        if (startSquad.equalsIgnoreCase("Start"))
                                p.setStartingLinup(new Long(1));
                        if (startSquad.equalsIgnoreCase("Sub"))
                                p.setSecondLinup(new Long(1));
                        String position = attributes.getValue("Position");
                        if (position.equalsIgnoreCase("Striker"))
                                position = "F";
                        else if (position.equalsIgnoreCase("Substitude"))
                                position = null;
                        else
                                position = position.substring(0,1);
                        a.setPosition(position);
                        a.setShirtNr(new Long(attributes.getValue("ShirtNumber")));
                        a.setTeam(team);
                }
                if (qName.equalsIgnoreCase("Player")){
                        String athleteId = attributes.getValue("uID");
                        Long intAthleteId = new Long(athleteId.substring(1));
                        athlete = getAthlete(intAthleteId); 
                }
                
                super.startElement(uri, localName, qName, attributes);
        }
        
        private void updateEntities(){
                LeagueDAO ldao = new LeagueDAOImpl();
                League l = ldao.getLeagues().get(0);
                match.setLeague(l);

                for (Team t : teams.values()){
                        if (t.getDirty()){
                                t.setLeague(l);
                                tdao.create(t);
                        }
                }
                for (Athlete a : athlets.values()){
                        if (a.getDirty())
                                adao.create(a);
                }
                mdao.create(match);
                for (Performance p : performances.values()){
                        p.setPosition(p.getAthlete().getPosition());
//                        p.calculateTime(match.getMatchTime());
                        pdao.create(p);
                }
        }
 
        @Override
		public void endElement(String uri, String localName,
                String qName) throws SAXException {
 
                if (qName.equalsIgnoreCase("SoccerFeed")){
                        updateEntities();
                }
                if (qName.equalsIgnoreCase("Team")){
                        team = null;
                }
                
                super.endElement(uri, localName, qName);
        }
 
        @Override
		public void characters(char ch[], int start, int length) throws SAXException {
                String element = elements.peek();
                String value = new String(ch, start, length);
                Logger logger = Logger.getRootLogger();
                logger.trace(element + " -> " + value);

                if (element.equalsIgnoreCase("Stat")){
                        logger.trace("Stat -> " + type + " -> "+ value);
                        if (type.equalsIgnoreCase("season_id"))
                        {
                                match.setSeason(new Long(value));
                        }
                        if (type.equalsIgnoreCase("matchday"))
                        {
// TODO                                match.setRound(new Long(value));
                        }
                        if (type.equalsIgnoreCase("match_time"))
                        {
                                match.setMatchTime(new Long(value));
                        }
                }
                if (element.equalsIgnoreCase("Date")){
                        int year = (new Long(value.substring(0,4))).intValue();
                        int month = (new Long(value.substring(4,6))).intValue();
                        int day = (new Long(value.substring(6,8))).intValue();

                        Calendar cal = Calendar.getInstance();
                        cal.set(year, month, day);
                        
                        match.setDate(cal);
                }
                if (personName){
                        if (element.equalsIgnoreCase("First")){
                                athlete.setFirstName(value);
                        }
                        if (element.equalsIgnoreCase("Last")){
                                athlete.setLastName(value);
                        }
                }
                if (element.equalsIgnoreCase("Name")){
                        if (team != null){
                                team.setName(value);
                        }
                }

        }
 
};