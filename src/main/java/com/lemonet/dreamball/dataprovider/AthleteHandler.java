package com.lemonet.dreamball.dataprovider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.lemonet.dreamball.realworld.domain.Athlete;
import com.lemonet.dreamball.realworld.domain.AthleteDAO;
import com.lemonet.dreamball.realworld.domain.AthleteDAOImpl;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;
import com.lemonet.dreamball.realworld.domain.TeamDAOImpl;

@Component
public class AthleteHandler extends GenericHandler{
	/**
	 * @uml.property  name="teams"
	 * @uml.associationEnd  qualifier="id:java.lang.Long com.lemonet.dreamball.realworld.domain.Team"
	 */
	List<Athlete> athletes = new ArrayList<Athlete>();
	/**
	 * @uml.property  name="personName"
	 */
	Athlete currentAthlete;
	String tmpValue;
	
	@Autowired TeamDAO tdao;
	@Autowired AthleteDAO adao;
	
	String variableName;


	@Override
	public void startElement(String uri, String localName, String qName, 
                Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("z"))
			currentAthlete = new Athlete();

		
		super.startElement(uri, localName, qName, attributes);
	}
	
	private void updateEntities(){
		HashMap<Long, Set<Long>> hm = new HashMap<Long, Set<Long>>();

		for (Athlete a : athletes){
			Long id = a.getTeam().getId();
			if (!hm.containsKey(a.getTeam().getId()))
				hm.put(id, new HashSet<Long>());
			hm.get(id).add(a.getOutsideId());
		}
		
		for (Long teamId : hm.keySet())
			adao.removeOthersFromTeam(teamId, hm.get(teamId));
		
		for (Athlete a : athletes){
			Logger logger = Logger.getLogger("PARSER");
			logger.trace("PARSER: Adding athlete " + a.getOutsideId() + " " + a.getLastName());
			Athlete oa = adao.getAthleteByOutsideId(a.getOutsideId());
			if (oa == null)
				adao.create(a);
			else {
				oa.copyProperties(a);
				adao.update(oa);
			}
		}
		
	}
 
	@Override
	public void endElement(String uri, String localName,
		String qName) throws SAXException {
 
		if (qName.equalsIgnoreCase("z")){
			athletes.add(currentAthlete);
		}
		if (qName.equalsIgnoreCase("athletes")){
			updateEntities();
		}
		if (qName.equalsIgnoreCase("id")){
			currentAthlete.setOutsideId(new Long(tmpValue));
		}
		if (qName.equalsIgnoreCase("id_druzyny")){
			Long teamOutsideId = new Long(tmpValue);
			
			Team t = tdao.getTeamByOutsideId(teamOutsideId);
			currentAthlete.setTeam(t);
		}
		if (qName.equalsIgnoreCase("n")){
			variableName = tmpValue;
		}
		if (qName.equalsIgnoreCase("w")){
			if (variableName.equalsIgnoreCase("first name"))
				currentAthlete.setFirstName(tmpValue);
			if (variableName.equalsIgnoreCase("last name"))
				currentAthlete.setLastName(tmpValue);
//			if (variableName.equalsIgnoreCase("nationality"))
//				currentAthlete.setNationality(tmpValue);
//			if (variableName.equalsIgnoreCase("date of birth"))
//				currentAthlete.setLastName(tmpValue);
//			if (variableName.equalsIgnoreCase("age"))
//				currentAthlete.setLastName(tmpValue);
//			if (variableName.equalsIgnoreCase("country of birth"))
//			currentAthlete.setLastName(tmpValue);
			if (variableName.equalsIgnoreCase("position")){
				currentAthlete.setPosition("S");
				if (tmpValue.equalsIgnoreCase("attacker"))
					currentAthlete.setPosition("S");
				if (tmpValue.equalsIgnoreCase("midfielder"))
					currentAthlete.setPosition("M");
				if (tmpValue.equalsIgnoreCase("defender"))
					currentAthlete.setPosition("D");
				if (tmpValue.equalsIgnoreCase("goalkeeper"))
					currentAthlete.setPosition("G");
			}
		}
		
		super.endElement(uri, localName, qName);
	}
 
    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        tmpValue = new String(ac, i, j);
    }
};
