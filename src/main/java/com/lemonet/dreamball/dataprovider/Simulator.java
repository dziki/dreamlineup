package com.lemonet.dreamball.dataprovider;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import cern.jet.random.Poisson;
import cern.jet.random.engine.DRand;
import cern.jet.random.engine.RandomEngine;

import com.lemonet.dreamball.realworld.domain.Athlete;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.realworld.domain.MatchDAOImpl;
import com.lemonet.dreamball.realworld.domain.Performance;
import com.lemonet.dreamball.realworld.domain.PerformanceDAO;
import com.lemonet.dreamball.realworld.domain.PerformanceDAOImpl;
import com.lemonet.dreamball.realworld.domain.Round;
import com.lemonet.dreamball.realworld.domain.Team;


public class Simulator {
	RandomEngine engine = new DRand();
	Poisson poisson = new Poisson(1, engine);

	public Long getPoisson(double lambda){
		return new Long(poisson.nextInt(lambda));
	}
	public Long getBinomial(double success){
        long currentTime = System.currentTimeMillis();
        Random r = new Random(currentTime);
		return new Long((r.nextDouble() < success)?1:0);
	}
	
	private void simulateAthletePerformance(Athlete athlete, Match match)
	{
		PerformanceDAO pdao = new PerformanceDAOImpl();
		Performance performance;
		performance = pdao.getByAthleteMatch(athlete, match);
		if (performance == null)
			performance = new Performance();

		performance.setAthlete(athlete);
		performance.setMatch(match);

		performance.setAssists(getPoisson(0.2));
		performance.setCausedPenalties(getPoisson(0.05));
//		performance.setGoalsConceded(getPoisson(0.2));
		performance.setGoals(getPoisson(0.2));
		performance.setLostGoals(getPoisson(0.1));
		performance.setLostPenalties(getPoisson(0.01));
		performance.setMinutesPlayed(90L);
		performance.setMissedPenalties(getPoisson(0.01));
		performance.setTimeIn(0L);
		performance.setTimeOut(90L);
		performance.setOwnGoals(getPoisson(0.01));
		performance.setPenaltiesDefended(getPoisson(0.01));
		performance.setPenaltiesReceived(getPoisson(0.01));
		performance.setPosition(athlete.getPosition());
		performance.setRedCard(getBinomial(0.1));
		performance.setSecondLinup(0L);
		performance.setShotsDefended(getPoisson(0.2));
		performance.setStartingLinup(1L);
		performance.setWithoutLostGoals(getPoisson(0.1));
		performance.setYellowCard(getBinomial(0.3));
		
		pdao.update(performance);
	}
	
	private void simulateMatch(Match match)
	{
		Team t1 = match.getHomeTeam();
		Team t2 = match.getAwayTeam();
		
		for (Athlete p : t1.getAthletes())
			simulateAthletePerformance(p,match);
		for (Athlete p : t2.getAthletes())
			simulateAthletePerformance(p,match);
	}
	
	public void simulateRound(Round round)
	{
		Logger logger = Logger.getRootLogger();
		MatchDAO mdao = new MatchDAOImpl();
		List<Match> matchs = mdao.getMatches(round.getLeague(), round.getSeason(), round.getNumber(), round.getNumber());
		for (Match match : matchs)
		{
			logger.trace("simulate match nr " + match.getId());
			simulateMatch(match);
			match.setMatchTime(90L);
			mdao.update(match);
		}
	}	
	
}
