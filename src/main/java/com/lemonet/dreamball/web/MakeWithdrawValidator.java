package com.lemonet.dreamball.web;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.lemonet.dreamball.web.commands.MakeWithdraw;

public class MakeWithdrawValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return MakeWithdraw.class.isAssignableFrom(clazz);
	}
	@Override
	public void validate(Object obj, Errors errors) {
		MakeWithdraw withdraw = (MakeWithdraw) obj;  
		
		if (withdraw.getAmount() <= 0){
			errors.rejectValue("amount", "amount.negative", "Amount should be positive");
		}
	}
}
