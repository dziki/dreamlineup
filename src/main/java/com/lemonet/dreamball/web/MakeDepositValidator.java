package com.lemonet.dreamball.web;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.lemonet.dreamball.web.commands.MakeDeposit;

public class MakeDepositValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return MakeDeposit.class.isAssignableFrom(clazz);
	}
	@Override
	public void validate(Object obj, Errors errors) {
		MakeDeposit withdraw = (MakeDeposit) obj;  
		
		if (withdraw.getAmount() <= 0){
			errors.rejectValue("amount", "amount.negative", "Amount should be positive");
		}
	}
}
