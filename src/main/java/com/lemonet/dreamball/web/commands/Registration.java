package com.lemonet.dreamball.web.commands;

import com.lemonet.dreamball.auth.domain.Player;

public class Registration extends Player {
	boolean confirmTerms;
	
	public boolean getConfirmTerms() {
		return confirmTerms;
	}
	public void setConfirmTerms(boolean confirmTerms) {
		this.confirmTerms = confirmTerms;
	}
	public Registration(){}
	/**
	 * @param confirmPassword
	 * @uml.property  name="confirmPassword"
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	/**
	 * @return
	 * @uml.property  name="confirmPassword"
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}
	/**
	 * @uml.property  name="confirmPassword"
	 */
	private String confirmPassword;
	public Player getPlayer(){
		Player p = new Player(this.getFirstName(),
					this.getLastName(), 
					this.getBirthDate(), 
					this.getNationality(), 
					this.getEmail(),
					this.getUsername(),
					this.getPassword(), 
					this.getAddress(), 
					this.getHouseNr(), 
					this.getCity(), 
					this.getCountry(),
					this.getZipcode(), 
					this.getDocumentId(), 
					this.getDocumentType()
				);
		return p;
	}
}