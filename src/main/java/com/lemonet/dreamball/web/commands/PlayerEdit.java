package com.lemonet.dreamball.web.commands;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Component;

import com.lemonet.dreamball.auth.domain.Player;

public class PlayerEdit {
	String lastName;
	String firstName;

	@Email
	@NotBlank
	@NotNull
	@Pattern(regexp=".+@.+\\..+")
	@Length(max = 50, min = 2) 
	String email;

	Date birthDate;	
	
	Boolean emailSubscription;
	Boolean resubmit;
		
	public Boolean getEmailSubscription() {
		return emailSubscription;
	}
	public void setEmailSubscription(Boolean emailSubscription) {
		this.emailSubscription = emailSubscription;
	}
	public Boolean getResubmit() {
		return resubmit;
	}
	public void setResubmit(Boolean resubmit) {
		this.resubmit = resubmit;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public PlayerEdit(Player player){
		this.setLastName(player.getLastName());
		this.setFirstName(player.getFirstName());
		this.setEmail(player.getEmail());
		this.setBirthDate(player.getBirthDate());	
		this.setEmailSubscription(player.getEmailSubscription());
		this.setResubmit(player.getResubmit());
	}
	public PlayerEdit(){}
}