package com.lemonet.dreamball.web.commands;


public class ChangePassword {
	public ChangePassword(){}
	/**
	 * @param amount
	 * @uml.property  name="amount"
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return
	 * @uml.property  name="amount"
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @uml.property  name="amount"
	 */
	private String token;
	private String password;
	private String passwordRep;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordRep() {
		return passwordRep;
	}
	public void setPasswordRep(String passwordRep) {
		this.passwordRep = passwordRep;
	}
}