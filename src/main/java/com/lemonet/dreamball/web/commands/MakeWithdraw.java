package com.lemonet.dreamball.web.commands;


public class MakeWithdraw {
	public MakeWithdraw(){}
	/**
	 * @param amount
	 * @uml.property  name="amount"
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	/**
	 * @return
	 * @uml.property  name="amount"
	 */
	public double getAmount() {
		return amount;
	}
	/**
	 * @uml.property  name="amount"
	 */
	private double amount;
}