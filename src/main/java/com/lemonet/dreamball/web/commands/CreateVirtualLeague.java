package com.lemonet.dreamball.web.commands;

import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;

public class CreateVirtualLeague extends VirtualLeague {
	public CreateVirtualLeague(){}
	public VirtualLeague getVirtualLeague(){
		VirtualLeague v = new VirtualLeague();
		v.setBuyIn(this.getBuyIn());
		v.setNumOfPlayers(this.getNumOfPlayers());
		v.setStartingRound(this.getStartingRound());
		v.setDuration(this.getDuration());
		v.setStatus(VirtualLeague.READY);;
		v.setBaseLeague(this.getBaseLeague());
		return v;
	}
}