package com.lemonet.dreamball.web.commands;


public class SendPasswordChangeLink {
	public SendPasswordChangeLink(){}
	/**
	 * @param amount
	 * @uml.property  name="amount"
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return
	 * @uml.property  name="amount"
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @uml.property  name="amount"
	 */
	private String email;
}