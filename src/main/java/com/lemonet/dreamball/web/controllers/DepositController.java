package com.lemonet.dreamball.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.lemonet.dreamball.auth.domain.DreamUser;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.finance.domain.Operation;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.web.commands.MakeDeposit;

@Controller
@RequestMapping("/finance/deposit.html")
@SessionAttributes("deposit")
public class DepositController {
	@Autowired PlayerDAO pdao;
	@Autowired OperationDAO odao;

	@ModelAttribute("deposit")
	public MakeDeposit getGreetingObject() {
		return new MakeDeposit();
	}

	@Secured("ROLE_USER")
	@RequestMapping(method = RequestMethod.GET)
	public String showDepositForm(ModelMap model)  {
		MakeDeposit deposit = new MakeDeposit();
		model.addAttribute(deposit);
		return "finance/deposit";
	}
	
	@Secured("ROLE_USER")
	@Transactional
    @RequestMapping(method = RequestMethod.POST)
    public String onSubmit(@ModelAttribute("deposit") MakeDeposit deposit) throws Exception {
		if (deposit.getAmount() <= 0)
			throw new Exception("Negative deposit");

		DreamUser user = (DreamUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Operation operation = new Operation(deposit.getAmount(),
				Operation.DEPOSIT,
				user.getPlayer());
		
		try {
			odao.save(operation);
		}
		catch (NotEnoughMoneyException e) {
			// deposit is always positive
		}

		return "finance/deposit";
	}
}