package com.lemonet.dreamball.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;

@Controller
public class PlayersController {
	@Autowired PlayerDAO pdao;
	@Autowired VirtualTeamDAO vtdao;

	@RequestMapping("/players/list.html")
    public String index(Model model) throws Exception {
		model.addAttribute("players", pdao.listWithWins(30));  
		return "players";
	}

	@RequestMapping("/players/{id}/stats.html")
    public String index(@PathVariable Long id, Model model) throws Exception {
		Player p = pdao.getPlayerById(id);
		model.addAttribute("player", p);
		
		for (VirtualTeam vt : p.getVirtualTeams()){
			vtdao.addPlace(vt);
		}
		
		return "player/stats";
	}
}