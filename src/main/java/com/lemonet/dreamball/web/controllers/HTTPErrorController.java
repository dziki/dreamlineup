package com.lemonet.dreamball.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/errors")
public class HTTPErrorController {
	@Autowired private MessageSource messageSource;  

	@RequestMapping(value="/500.html")
    public String handle500(Model model) {
        model.addAttribute("head", messageSource.getMessage("error500.head", null, null));
        model.addAttribute("content", messageSource.getMessage("error500.body", null, null));
    	return "static";
    }
    @RequestMapping(value="/404.html")
    public String handle404(Model model) {
        model.addAttribute("head", messageSource.getMessage("error404.head", null, null));
        model.addAttribute("content", messageSource.getMessage("error404.body", null, null));
    	return "static";
    }
    @RequestMapping(value="/403.html")
    public String handle403(Model model) {
        model.addAttribute("head", messageSource.getMessage("error403.head", null, null));
        model.addAttribute("content", messageSource.getMessage("error403.body", null, null));
    	return "static";
    }

}