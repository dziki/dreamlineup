package com.lemonet.dreamball.web.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.lemonet.dreamball.auth.domain.DreamUser;
import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.auth.domain.PlayerDAOImpl.TooYoung;
import com.lemonet.dreamball.auth.domain.Token.TokenExpiredException;
import com.lemonet.dreamball.auth.domain.TokenDAO;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.service.AuthenticationService;
import com.lemonet.dreamball.web.RegistrationValidator;
import com.lemonet.dreamball.web.commands.PlayerEdit;
import com.lemonet.dreamball.web.commands.Registration;
import com.lemonet.notification.service.EmailService;

@Controller
@RequestMapping("/auth")
@SessionAttributes("user")
public class RegistrationFormController {
	@Autowired private MessageSource messageSource;  

	@Autowired private OperationDAO odao;
	@Autowired private TokenDAO tdao;
	@Autowired private PlayerDAO pdao;

	@Autowired private RegistrationValidator registrationValidatior;
	@Autowired private EmailService emailService;
	@Autowired private AuthenticationService authenticationService;


    public void setRegistrationValidatior(
                    RegistrationValidator registrationValidation) {
            this.registrationValidatior = registrationValidation;
    }

	@ModelAttribute("user")
	public Registration getRegistrationObject() {
		return new Registration();
	}

	@RequestMapping(value="/registration.html", method = RequestMethod.GET)
	public String showRegistrationForm(ModelMap model)  {
		Registration registration = new Registration();
		model.addAttribute(registration);
		return "registration";
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, null, new CustomDateEditor(dateFormat, true));
	}
	
    @RequestMapping("/{token}/activate.html")
    public String activate(@PathVariable String token, Model model) throws Exception {
    	boolean error = false;
    	try {
    		authenticationService.activatePlayer(token);
    
    		return "redirect:/auth/activated.html";
    	}
    	catch (EntityNotFoundException e){
    		error = true;
    	}
    	catch (TokenExpiredException e) {
    		error = true;
    	}
    	
    	if (error){
            model.addAttribute("head", messageSource.getMessage("auth.registration_error", null, "Registration error", null));
            model.addAttribute("content", messageSource.getMessage("auth.registration_error_body", null, "Invalid link.", null));
    	}

    	return "static";
    }
	
	@RequestMapping(value="/activated.html", method = RequestMethod.GET)
    public String activated(Model model) throws Exception {
        model.addAttribute("head", messageSource.getMessage("auth.registration_complete", null, null));
        model.addAttribute("content", messageSource.getMessage("auth.registration_complete_body", null, null));

        return "static";
	}
		
	@RequestMapping(value="/success.html", method = RequestMethod.GET)
    public String success(Model model) throws Exception {
		Integer num = 1 + (int)(Math.random() * (10000000 + 1));
		
        model.addAttribute("head", messageSource.getMessage("auth.registration_complete", null, null));
        model.addAttribute("content", messageSource.getMessage("auth.registration_complete_body", null, null)
        		+ "<img src=\"http://panel.portmmo.com/scripts/sale.php?AccountId=default1&ActionCode=register-dlu\" width=\"1\" height=\"1\" >"
        		+ "<div id='m3_tracker_344' style='position: absolute; left: 0px; top: 0px; visibility: hidden;'><img src='http://ad4mmo.pl/openx/www/delivery/ti.php?trackerid=344&amp;cb=" + num.toString() + "' width='0' height='0' alt='' /></div>"
        		+ "\n<!-- Offer Conversion: DreamLineUp -->\n<img src=\"http://tracking.linktogame.com/aff_l?offer_id=372\" width=\"1\" height=\"1\" />\n<!-- // End Offer Conversion -->");

		return "static";
	}

	@Secured("ROLE_USER")
	@RequestMapping(value="/edit.html", method = RequestMethod.GET)
    public String authEdit(HttpServletRequest request, Model model){
		DreamUser user = (DreamUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Player player = user.getPlayer();	

		PlayerEdit playerEdit = new PlayerEdit(player);
		model.addAttribute(playerEdit);

		return "auth/edit";
		
	}

	@Secured("ROLE_USER")
	@RequestMapping(value="/edit.html", method = RequestMethod.POST)
    public String onAuthEdit(HttpServletRequest request, @Valid PlayerEdit playerEdit,
            BindingResult result, Model model) throws Exception {
		String baseUrl = String.format("%s://%s:%d%s",
				request.getScheme(),  request.getServerName(), request.getServerPort(), request.getContextPath());

        DreamUser user = (DreamUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Player p = user.getPlayer();

		registrationValidatior.validateName(playerEdit.getFirstName(), playerEdit.getLastName(), result);
//		registrationValidatior.validateEmail(playerEdit.getEmail(), result);
        if (result.hasErrors()) 
    		return "auth/edit";
        
        p.setBirthDate(playerEdit.getBirthDate());
        p.setEmail(playerEdit.getEmail());
        p.setFirstName(playerEdit.getFirstName());
        p.setLastName(playerEdit.getLastName());
        p.setEmailSubscription(playerEdit.getEmailSubscription());
        p.setResubmit(playerEdit.getResubmit());
        pdao.update(p);
        
        // UPDATE PLAYER
		return "redirect:/index.html";
	}
	
	@RequestMapping(value="/registration.html", method = RequestMethod.POST)
    public String onSubmit(HttpServletRequest request, @Valid Registration registration,
            BindingResult result, Model model) throws Exception {
		String baseUrl = String.format("%s://%s:%d%s",
				request.getScheme(),  request.getServerName(), request.getServerPort(), request.getContextPath());

		registrationValidatior.validate(registration, result);
        if (result.hasErrors()) 
        	return "registration";
    	
    	Player player = registration.getPlayer();
//    	try {
//    		player.setActive(true);
    		authenticationService.create(player,baseUrl);
            return "redirect:/auth/success.html";
/*    	}
		catch (TooYoung e){
			return "registration";
		}
		catch (Exception e){
			return "registration";
		}*/

	
//		return "redirect:/auth/success.html";
	}
}