package com.lemonet.dreamball.web.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.lemonet.notification.service.EmailService;

//@ControllerAdvice
class GlobalDefaultExceptionHandler {
	@Autowired private EmailService emailService;
	
    public static final String DEFAULT_ERROR_VIEW = "static";

    @ExceptionHandler(value = Exception.class)
    public String defaultErrorHandler(HttpServletRequest req, HttpServletResponse res, Exception e) throws Exception {
        // If the exception is annotated with @ResponseStatus rethrow it and let
        // the framework handle it - like the OrderNotFoundException example
        // at the start of this post.
        // AnnotationUtils is a Spring Framework utility class.
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
            throw e;
        if (AccessDeniedException.class.isAssignableFrom(e.getClass()))
            throw e;
        
        // it is not handled thus serious - send it to the admin
        EmailService.emailException(e, req);
        
        return "redirect:/errors/500.html";
    }
}