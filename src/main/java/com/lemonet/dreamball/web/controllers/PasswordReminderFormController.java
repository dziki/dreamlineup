package com.lemonet.dreamball.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.auth.domain.TokenDAO;
import com.lemonet.dreamball.service.AuthenticationService;
import com.lemonet.dreamball.web.commands.SendPasswordChangeLink;
import com.lemonet.notification.service.EmailService;

@Controller
@RequestMapping("/auth/pass")
@SessionAttributes("spclink")
public class PasswordReminderFormController {
	@Autowired PlayerDAO pdao;
	@Autowired TokenDAO tdao;
	@Autowired ReloadableResourceBundleMessageSource messageSource;  
	@Autowired EmailService emailService;
	@Autowired AuthenticationService authService;

	@ModelAttribute("spclink")
	public SendPasswordChangeLink getSendPasswordChangeLink() {
		return new SendPasswordChangeLink();
	}

	@RequestMapping(value="/remind.html", method = RequestMethod.GET)
	public String showPasswordReminderForm(ModelMap model)  {
		SendPasswordChangeLink spclink = new SendPasswordChangeLink();
		model.addAttribute("spclink", spclink);
		return "password_remind";
	}
	
	@RequestMapping(value="/remind/sent.html", method = RequestMethod.GET)
    public String success(Model model) throws Exception {
		return "password_remind_sent";
	}

	@RequestMapping(value="/remind/send.html", method = RequestMethod.POST)
    public String onSubmit(HttpServletRequest request, @ModelAttribute("spclink") SendPasswordChangeLink spclink,
            BindingResult result) throws Exception {
		String baseUrl = String.format("%s://%s:%d%s",
				request.getScheme(),  request.getServerName(), request.getServerPort(), request.getContextPath());
		Player p = pdao.findByEmailOrUsername(spclink.getEmail());
		if (p != null)
			authService.requestPasswordChange(p,baseUrl);
		
		return "redirect:/auth/pass/remind/sent.html";
	}
}