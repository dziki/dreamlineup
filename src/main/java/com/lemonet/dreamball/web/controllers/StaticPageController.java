package com.lemonet.dreamball.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StaticPageController {
	@Autowired  
	ReloadableResourceBundleMessageSource messageSource;  
	 
	@RequestMapping("/terms.html")
    public String terms(Model model) throws Exception {
		return "terms";
	}
	@RequestMapping("/rules.html")
    public String rules(Model model) throws Exception {
		return "rules";
	}
	@RequestMapping("/contact.html")
    public String contact(Model model) throws Exception {
		return "contact";
	}
}