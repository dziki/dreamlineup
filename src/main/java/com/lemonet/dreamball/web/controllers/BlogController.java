package com.lemonet.dreamball.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BlogController {

		@RequestMapping("/blog.html")
	    public String index(Model model) throws Exception {
		return "blog";
	}
}