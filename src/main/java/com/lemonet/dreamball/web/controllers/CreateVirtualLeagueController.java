package com.lemonet.dreamball.web.controllers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.lemonet.dreamball.auth.domain.DreamUser;
import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.Round;
import com.lemonet.dreamball.realworld.domain.RoundDAO;
import com.lemonet.dreamball.service.DreamworldService;

@Controller
@RequestMapping("/leagues")
@SessionAttributes("league")
public class CreateVirtualLeagueController {
	@Autowired RoundDAO rdao;
	@Autowired LeagueDAO ldao;
	@Autowired DreamworldService dreamService;

	@Autowired  
	ReloadableResourceBundleMessageSource messageSource;  

	@ModelAttribute("league")
	public VirtualLeague getVirtualLeagueObject() {
		VirtualLeague vl = new VirtualLeague();
		return vl;
	}

	@Secured("ROLE_USER")
	@RequestMapping(value="/finished.html", method = RequestMethod.GET)
	public String seasonFinished(ModelMap model)  {
		model.addAttribute("head", messageSource.getMessage("leagues.season_finished_head", null, null));
		model.addAttribute("content", messageSource.getMessage("leagues.season_finished_conent", null, null));
	
		return "static";
	}

	@Secured("ROLE_USER")
	@RequestMapping(value="/create.html", method = RequestMethod.GET)
	public String showLeagueForm(ModelMap model)  {
		VirtualLeague league = new VirtualLeague();
		
		Round r = rdao.getFirstUnstartedRound(ldao.getLeagueById(Constants.CURRENT_LEAGUE));
		if (r == null)
			return "redirect:/leagues/finished.html";
		
		Map<String,String> startingRound = new LinkedHashMap<String,String>();
		for (Long i = r.getNumber(); i<= Math.min(r.getNumber() + 2L, r.getLeague().getRounds().size()); i++){
			startingRound.put(i.toString(), i.toString());
		}
		model.addAttribute("startingRoundList", startingRound);

		Map<String,String> duration = new LinkedHashMap<String,String>();
		for (Long i = 1L; i<=r.getLeague().getRounds().size() - r.getNumber() + 1; i++){
			if (i > 2)
				break;
			duration.put(i.toString(), i.toString());
		}
		model.addAttribute("durationList", duration);

		Map<String,String> numOfPlayers = new LinkedHashMap<String,String>();
		numOfPlayers.put("", messageSource.getMessage("lobby.unlimited", null, "No limits", null));
		numOfPlayers.put("2", "2");
		numOfPlayers.put("3", "3");
		numOfPlayers.put("4", "4");
		numOfPlayers.put("5", "5");
		numOfPlayers.put("10", "10");
		numOfPlayers.put("15", "15");
		numOfPlayers.put("20", "20");
		model.addAttribute("numOfPlayersList", numOfPlayers);
		
		VirtualLeague vl = new VirtualLeague();

		Map<String,String> buyIn = new LinkedHashMap<String,String>();
		List<Long> stakes = new ArrayList<Long>();
		stakes.add(1L);
		stakes.add(2L);
		stakes.add(3L);
		stakes.add(4L);
		stakes.add(5L);
		stakes.add(10L);
		stakes.add(20L);
		stakes.add(50L);
//		stakes.add(100L);
		
		for (Long stake : stakes){
			Double d = stake * vl.getRake();
			Double s = new Double(stake);
			buyIn.put(d.toString(), s.toString() + "0");
		}

		model.addAttribute("buyInList", buyIn);
		model.addAttribute("baseLeague", league.getBaseLeague());
		model.addAttribute("season", league.getSeason());

		model.addAttribute(league);
		return "create.league";
	}
	
	@Secured("ROLE_USER")
    @RequestMapping(value="/create.html", method = RequestMethod.POST)
    public String onSubmit(@ModelAttribute("league") VirtualLeague league, Model model) throws Exception {
		DreamUser user = (DreamUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Player player = user.getPlayer();
		
		try {
			dreamService.createLeague(player,league);
		}
		catch (NotEnoughMoneyException e) {
            model.addAttribute("head", messageSource.getMessage("leagues.create_error", null, null));
            model.addAttribute("content", messageSource.getMessage("leagues.create_error_money", null, null));
            return "static";
		}

  
//		return "redirect:/leagues/"+ league.getId() +"/details.html";
		return "redirect:/leagues/"+ league.getId() +"/join.html";
	}

}