package com.lemonet.dreamball.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lemonet.dreamball.auth.domain.DreamUser;
import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.finance.domain.OperationDAO;

@Controller
public class AccountHistoryController {
	@Autowired OperationDAO odao;

	@Secured("ROLE_USER")
	@RequestMapping("/finance/history.html")
    public String index(Model model) throws Exception {
		DreamUser user = (DreamUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Player player = user.getPlayer();
		model.addAttribute("operations", odao.getOperations(player));  
		return "finance/history";
	}
}