package com.lemonet.dreamball.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lemonet.dreamball.auth.domain.DreamUser;
import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthleteDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;

@Controller
public class DraftRoomController {
	@Autowired VirtualLeagueAthleteDAO vladao;
	@Autowired VirtualTeamDAO vtdao;
	@Autowired TeamDAO tdao;

	@Secured("ROLE_USER")
	@Transactional
	@RequestMapping("/draftroom/{vTeamId}.html")
    public String index(@PathVariable String vTeamId, Model model) throws Exception {
		Long id = Long.parseLong(vTeamId);
		VirtualTeam vtp = vtdao.getVirtualTeamById(id);
		if (vtp.getVirtualLeague().isFinished())
			return "redirect:/lobby/team/" + vtp.getId().toString() + "/details.html";
			
		List<Team> teams = tdao.getTeams(vtp.getVirtualLeague().getBaseLeague());

		DreamUser user = (DreamUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Player player = user.getPlayer();

		if (vtp == null)
			throw new Exception("Team does not exist");
		if (!vtp.getPlayer().getId().equals(player.getId()))
			throw new Exception("Team of other player.");

		model.addAttribute("teams", teams);  
		model.addAttribute("virtualteam", vtp);  

		return "draft";
	}
}