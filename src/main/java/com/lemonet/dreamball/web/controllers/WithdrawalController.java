package com.lemonet.dreamball.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.finance.domain.Operation;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.web.commands.MakeWithdraw;

@Controller
@RequestMapping("/finance/withdrawal")
@SessionAttributes("withdraw")
public class WithdrawalController {
	@Autowired PlayerDAO pdao;
	@Autowired OperationDAO odao;

	@ModelAttribute("withdraw")
	public MakeWithdraw getWithdrawalObject() {
		return new MakeWithdraw();
	}

	@Secured("ROLE_USER")
	@RequestMapping(method = RequestMethod.GET)
	public String showDepositForm(ModelMap model)  {
		MakeWithdraw deposit = new MakeWithdraw();
		model.addAttribute(deposit);
		return "finance/withdrawal";
	}
	
	@Secured("ROLE_USER")
	@Transactional
    @RequestMapping(method = RequestMethod.POST)
    public String onSubmit(@ModelAttribute("withdraw") MakeWithdraw withdrawal) throws Exception {
		if (withdrawal.getAmount() <= 0)
			throw new Exception("Negative withdrawal");

		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Player player = pdao.findByEmailOrUsername(user.getUsername());

		Operation operation = new Operation(-withdrawal.getAmount(),Operation.WITHDRAWAL,player);
		
		try {
			odao.save(operation);
		}
		catch (NotEnoughMoneyException e) {
			// TODO
		}

		return "finance/withdrawal";
	}
}