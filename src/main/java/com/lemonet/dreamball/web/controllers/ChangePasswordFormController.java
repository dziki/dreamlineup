package com.lemonet.dreamball.web.controllers;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.auth.domain.Token.TokenExpiredException;
import com.lemonet.dreamball.service.AuthenticationService;
import com.lemonet.dreamball.web.ChangePasswordValidator;
import com.lemonet.dreamball.web.commands.ChangePassword;
import com.lemonet.notification.service.EmailService;

@Controller
@RequestMapping("/auth/pass")
@SessionAttributes("passchange")
public class ChangePasswordFormController {
	@Autowired  
	ReloadableResourceBundleMessageSource messageSource;  

	@Autowired
    private ChangePasswordValidator passwordValidatior;

	@Autowired
    private EmailService emailService;
	
	@Autowired
	PlayerDAO pdao;
	
	@Autowired
	private AuthenticationService authenticationService;

    public void setPasswordValidator(
    		ChangePasswordValidator passwordValidatior) {
            this.passwordValidatior = passwordValidatior;
    }
    
	@ModelAttribute("passchange")
	public ChangePassword getSendPasswordChangeLink() {
		return new ChangePassword();
	}

	@RequestMapping(value="/{token}/change.html", method = RequestMethod.GET)
	public String showPasswordReminderForm(@PathVariable String token, ModelMap model)  {
		ChangePassword passchange = new ChangePassword();
		model.addAttribute("passchange", passchange);
		return "password_change";
	}
	
	@RequestMapping(value="/{token}/changed.html", method = RequestMethod.GET)
    public String success(Model model) throws Exception {
		return "password_change_sent";
	}

	@RequestMapping(value="/{token}/change.html", method = RequestMethod.POST)
    public String onSubmit(HttpServletRequest request, @PathVariable String token, @ModelAttribute("passchange") ChangePassword passchange,
            BindingResult result) throws Exception {
    	try {
    		passwordValidatior.validate(passchange, result);
    		if (result.hasErrors())
    			return "password_change";
    		authenticationService.changePassword(token, passchange.getPassword());
    	}
    	catch (EntityNotFoundException e) {
			return "password_change";
    	}
    	catch (TokenExpiredException e) {
			return "password_change";
    	}
		
		return "redirect:/auth/login.html";
	}
}