package com.lemonet.dreamball.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lemonet.dreamball.auth.domain.DreamUser;
import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.service.DreamworldService;
import com.lemonet.dreamball.service.DreamworldService.LeagueClosedException;
import com.lemonet.dreamball.service.DreamworldService.PlayerLimitReachedError;
import com.lemonet.dreamball.service.DreamworldService.TooManyTeamsException;

@Controller
public class LeagueJoinController {
	@Autowired DreamworldService dreamworldService;
	@Autowired ReloadableResourceBundleMessageSource messageSource;  
	@Autowired VirtualLeagueDAO vldao;  
	
	@Secured({"ROLE_USER"})
	@RequestMapping("/leagues/{leagueId}/join.html")
    public String index(@PathVariable Long leagueId, Model model) throws Exception {
		DreamUser user = (DreamUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Player player = user.getPlayer();
		try {
			VirtualLeague virtualLeague = vldao.getLeagueById(leagueId);
			VirtualTeam vt = dreamworldService.joinLeague(player, virtualLeague);
			return "redirect:/draftroom/" + vt.getId() + ".html";
		}
		catch (PlayerLimitReachedError e){
			model.addAttribute("modalError", "lobby.cant_join");
			model.addAttribute("modalErrorDesc", e.getMessage());
		}
		catch (NotEnoughMoneyException e){
			model.addAttribute("modalError", "lobby.cant_join");
			model.addAttribute("modalErrorDesc", "lobby.no_money");
		}
		catch (LeagueClosedException e){
			model.addAttribute("modalError", "lobby.cant_join");
			model.addAttribute("modalErrorDesc", "lobby.league_closed");
		}
		catch (TooManyTeamsException e){
			model.addAttribute("modalError", "lobby.cant_join");
			model.addAttribute("modalErrorDesc", "lobby.too_many_teams");
		}

		return "redirect:/leagues/" + leagueId + "/details.html";
	}
}