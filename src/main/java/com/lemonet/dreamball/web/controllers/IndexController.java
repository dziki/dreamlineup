package com.lemonet.dreamball.web.controllers;

import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.dataprovider.AthleteHandler;
import com.lemonet.dreamball.dataprovider.DataGenerator;
import com.lemonet.dreamball.dataprovider.MatchHandler;
import com.lemonet.dreamball.dataprovider.StatisticHandler;
import com.lemonet.dreamball.dataprovider.TeamHandler;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.service.LeagueMaintenanceService;

@Controller
public class IndexController {
	@Autowired VirtualLeagueDAO vldao;
	@Autowired VirtualTeamDAO vtdao;
	@Autowired PlayerDAO pdao;
	@Autowired LeagueDAO ldao;

	@Autowired MatchHandler matchHandler;
	@Autowired StatisticHandler statisticHandler;
	@Autowired TeamHandler teamHandler;
	@Autowired AthleteHandler athleteHandler;
	@Autowired DataGenerator dataGenerator;

	@Autowired LeagueMaintenanceService lms;

	@RequestMapping("/scheduled.html")
	public String updateData(Model model) throws Exception {
		lms.updateResults();
		lms.distributePrizes();
//		lms.sendEmails();
		return index(model);
	}	

	@RequestMapping("/index.html")
    public String index(Model model) throws Exception {
		List<VirtualLeague> leagues = vldao.getRecentLeagues();
		List<Player> players = pdao.listWithWins(5);

		model.addAttribute("leagues", leagues);  
		model.addAttribute("players", players);  
		
		return "hello";
	}
	
}
