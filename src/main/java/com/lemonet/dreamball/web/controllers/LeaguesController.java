package com.lemonet.dreamball.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;

@Controller
public class LeaguesController {
	@Autowired VirtualLeagueDAO vldao;

	@RequestMapping("/leagues/list.html")
    public String index(Model model) throws Exception {
		List<VirtualLeague> leagues = vldao.getRecentLeagues();
		System.out.print(leagues.size());
	
		model.addAttribute("leagues", leagues);  
		return "leagues";
	}
}