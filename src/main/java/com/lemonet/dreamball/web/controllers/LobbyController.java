package com.lemonet.dreamball.web.controllers;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueBonus;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.realworld.domain.RoundDAO;

@Controller
public class LobbyController {
	@Autowired
	VirtualLeagueDAO vldao;

	@Autowired
	VirtualTeamDAO vtdao;

	@Autowired
	PlayerDAO pdao;

	@Autowired
	RoundDAO rdao;

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;  

	@RequestMapping("/leagues/sponsored.html")
	@Transactional
    public String index(Model model) throws Exception {
		VirtualLeague vleague = vldao.getCurrentSponsoredLeague();
		String lid = vleague.getId().toString();
		
		return "redirect:/leagues/" + lid + "/details.html";

	}

	@RequestMapping("/leagues/{leagueId}/results.html")
    public String results(@PathVariable String leagueId,
    		Model model) throws Exception {
		
		VirtualLeague vleague = vldao.getLeagueById(new Long(leagueId));
		model.addAttribute("standings",vldao.getStandings(vleague));
		model.addAttribute("vleague", vleague);  
		return "lobby/results";
	}

	@RequestMapping("/lobby/team/{teamId}/details.html")
    public String teamDetails(@PathVariable String teamId,
    		Model model) throws Exception {
		VirtualTeam vteam = vtdao.getVirtualTeamById(new Long(teamId));
		
		if (vteam.getVirtualLeague().getStatus() == 0L)
			return "redirect:/draftroom/" + teamId + ".html";
		
		vtdao.addPlace(vteam);
		
		model.addAttribute("vteam", vteam);  
		return "lobby/teamres";
	}

	@RequestMapping("/leagues/{leagueId}/details.html")
	@Transactional
    public String index(@PathVariable String leagueId,
    		@RequestParam(required=false) String modalErrorDesc,
    		@RequestParam(required=false) String modalError,
    		Model model) throws Exception {
		int id = Integer.parseInt(leagueId);
	
		// get the object
		VirtualLeague vleague = vldao.getLeagueById(new Long(id));
		List<VirtualTeam> vteams;

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		Player player = null;
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			User user = (User)auth.getPrincipal();
			player = pdao.findByEmailOrUsername(user.getUsername());
		}
		
		
		vteams = vtdao.getPlayerTeamsInLeague(player,vleague);

		if (modalErrorDesc != null)
			model.addAttribute("modalErrorDesc",messageSource.getMessage(modalErrorDesc, null, null, null));
		if (modalError != null)
			model.addAttribute("modalError",messageSource.getMessage(modalError,null, null, null));

		model.addAttribute("vleague", vleague);  
		model.addAttribute("playerVirtualTeams",vteams);
		model.addAttribute("standings",vldao.getStandings(vleague));
		model.addAttribute("rounds", rdao.getRounds(vleague));  
		
		
		List<String> prizes = new ArrayList<String>();

		NumberFormat formatter = new DecimalFormat("#0.00");     
		int i = 0;
		for (Double p : vleague.getPrizeDistribution()){
			if (p < 0.01)
				break;
			i++;
			String bonusString = formatter.format(p) + " DP";
			for (VirtualLeagueBonus bonus : vleague.getBonuses())
				if (bonus.getPlace().equals(new Integer(i)))
					bonusString += " + " + bonus.getDescription();
			prizes.add(bonusString);
		}
		
		
		model.addAttribute("prizes",prizes);

		return "lobby";
	}
}