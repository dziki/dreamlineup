package com.lemonet.dreamball.web.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.lemonet.dreamball.auth.domain.DreamUser;
import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthlete;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthleteDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAOImpl.FireException;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamAthlete;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.service.DraftRoomService;
import com.lemonet.dreamball.service.DraftRoomService.FormationException;
import com.lemonet.dreamball.service.DraftRoomService.HireException;

@Controller
@RequestMapping("/json")
@SessionAttributes("user")
@Transactional
public class VirtualAthletesController {
	@Autowired SessionFactory sessionFactory;
	@Autowired ReloadableResourceBundleMessageSource messageSource;  
	@Autowired DraftRoomService draftRoomService;  
	@Autowired VirtualTeamDAO vtdao;
	@Autowired VirtualLeagueAthleteDAO vladao;
	@Autowired VirtualLeagueDAO vldao;

	public Player getLoggedPlayer(){
		DreamUser user = (DreamUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user.getPlayer();
	}

	@Secured("ROLE_USER")
	@RequestMapping(value="/leagues/{leagueId}/athletes.html", method = RequestMethod.GET)
    public @ResponseBody List<VirtualLeagueAthlete> index(@PathVariable long leagueId,
    		@RequestParam(required=false, value="teamId") Long teamId,
    		@RequestParam(required=false, value="page") Long page,
    		@RequestParam(required=false, value="pos") String pos
    ) throws Exception {
		VirtualLeague vl = vldao.getLeagueById(new Long(leagueId));
		return vladao.getVirtualLeagueAthlets(vl, teamId, pos, page);
	}

	@Secured("ROLE_USER")
	@RequestMapping(value="/team/{virtualTeamId}/athlete/{vLeagueAthleteId}/hire.html", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> hire(@PathVariable long virtualTeamId,
    		@PathVariable Long vLeagueAthleteId) throws Exception {
		HashMap<String, Object> d = new HashMap<String, Object>();
		d.put("status", "OK");

		try {
			VirtualTeamAthlete vta = draftRoomService.hireAthlete(vLeagueAthleteId, virtualTeamId);
			d.put("vathlete", vta);
		}
		catch (HireException e) {
			d.put("status", "ERROR");
			d.put("message", messageSource.getMessage(e.getMessage(), null, null, null));
		}

		return d;
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value="/team/{virtualTeamId}/athlete/{vTeamAthleteId}/fire.html", method = RequestMethod.GET)
    public @ResponseBody Map<String, String> fire(@PathVariable long virtualTeamId,
    		@PathVariable(value="vTeamAthleteId") Long vTeamAthleteId) throws Exception {
		HashMap<String, String> d = new HashMap<String, String>();
		
		try {
			draftRoomService.fireAthlete(vTeamAthleteId);
			d.put("status", "OK");
		}
		catch (FireException e) {
			d.put("status", "ERROR");
			d.put("message", messageSource.getMessage(e.getMessage(), null, null, null));
		}

		return d;
	}

	@Secured("ROLE_USER")
	@RequestMapping(value="/team/{virtualTeamId}/athlete/list.html", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, Object> athleteList(@PathVariable long virtualTeamId) throws Exception {
		HashMap<String, Object> d = new HashMap<String, Object>();
		d.put("status", "OK");
		d.put("virtualTeam", vtdao.getVirtualTeamById(virtualTeamId));

		return d;
	}
	
	@Transactional
	@Secured("ROLE_USER")
	@RequestMapping(value="/team/{virtualTeamId}/formation/{formation}.html", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> changeFormation(@PathVariable Long virtualTeamId,
    		@PathVariable String formation) throws Exception {
		HashMap<String, Object> d = new HashMap<String, Object>();
		
		d.put("status", "OK");

		try {
			draftRoomService.setFormation(virtualTeamId, formation);
			d.put("formation", formation);
		}
		catch (FormationException e) {
			e.getMessage();
			d.put("status", "ERROR");
			d.put("message", messageSource.getMessage(e.getMessage(), null, null, null));
		}
		finally{

		}

		return d;
	}
}