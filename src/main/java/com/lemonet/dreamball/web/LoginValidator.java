package com.lemonet.dreamball.web;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.lemonet.dreamball.service.AuthenticationService;
import com.lemonet.dreamball.web.commands.Login;

@Component
public class LoginValidator implements Validator {
	@Autowired
	AuthenticationService authService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Login.class.isAssignableFrom(clazz);
	}
	@Override
	public void validate(Object obj, Errors errors) {
		Login loginData = (Login) obj;  
		
		if (null == authService.loginPlayer(loginData)){
			errors.rejectValue("password", "wrong.password", "Login and password do not match");
		}
	}
} 