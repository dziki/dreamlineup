package com.lemonet.dreamball.web;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.lemonet.dreamball.web.commands.CreateVirtualLeague;

public class CreateVirtualLeagueValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return CreateVirtualLeague.class.isAssignableFrom(clazz);
	}
	@Override
	public void validate(Object obj, Errors errors) {	
	}
} 