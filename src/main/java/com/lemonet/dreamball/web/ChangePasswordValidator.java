package com.lemonet.dreamball.web;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.lemonet.dreamball.web.commands.ChangePassword;

@Component("changePasswordValidator")
public class ChangePasswordValidator {
	public boolean supports(Class<?> clazz) {
		return ChangePassword.class.isAssignableFrom(clazz);
	}
	public void validate(ChangePassword obj, Errors errors) {	
		ChangePassword cpwd = obj;  
		if (!cpwd.getPassword().equals(cpwd.getPasswordRep())) {
			errors.rejectValue("passwordRep", "error.dont.match", "Passwords don't match.");
		}
		if (cpwd.getPassword().length() < 8) {
			errors.rejectValue("password", "error.too.short", "Password is too short.");
		}
	}
} 