package com.lemonet.dreamball.web;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.web.commands.Registration;

@Component("registrationValidator")
public class RegistrationValidator {
	@Autowired ReloadableResourceBundleMessageSource messageSource;  
	@Autowired PlayerDAO pdao;  

	public boolean supports(Class<?> clazz) {
		return Registration.class.isAssignableFrom(clazz);
	}
	public void validate(Registration player, Errors errors) {	
		if (player.getConfirmTerms() == false) {
			errors.rejectValue("confirmTerms", "error.empty.field", messageSource.getMessage("registration.confirmTermsError", null, "Error", null));
		}
 
		if (!player.getPassword().equals(player.getConfirmPassword())) {
			errors.rejectValue("confirmPassword", "wrong.password", messageSource.getMessage("registration.passwordsDontMatch", null, "Passwords do not match.", null));
		}
		this.validateCredentials(player, errors);
		this.validateEmail(player.getEmail(), errors);
	}
	
	public void validateCredentials(Player player, Errors errors){
		if (player.getUsername().length() < 2) {
//			errors.rejectValue("username", "error.empty.field", messageSource.getMessage("registration.username", null, "Please enter a username.", null));
		}
		if (player.getUsername().length() >= 2 && pdao.findByUsernamel(player.getUsername()) != null) {
			errors.rejectValue("username", "error.empty.field", messageSource.getMessage("registration.usernameExists", null, "This username is already registered.", null));
		}
		if (player.getPassword().length() < 8) {
			errors.rejectValue("password", "error.empty.field", messageSource.getMessage("registration.shortPassword", null, "Password must have at least 8 characters.", null));
		} 
	}

	public void validateEmail(String email, Errors errors){
		if (email.length() >= 2) {
			Player p = pdao.findByEmailOrUsername(email);
			if (p == null || p.getId().equals(email))
				return;
			
			errors.rejectValue("email", "error.empty.field", messageSource.getMessage("registration.emailExists", null, "This e-mail is already registered.", null));
		}
	}
	
	public void validateName(String firstName, String lastName, Errors errors) {
		if (firstName == null || firstName.length() < 2) {
			errors.rejectValue("firstName", "error.empty.field", messageSource.getMessage("registration.enterName", null, "Please enter your name.", null));
		}
		if (lastName == null || lastName.length() < 2) {
			errors.rejectValue("lastName", "error.empty.field", messageSource.getMessage("registration.enterName", null, "Please enter your name.", null));
		}
	}
} 