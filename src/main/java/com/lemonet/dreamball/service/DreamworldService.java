package com.lemonet.dreamball.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.dataprovider.AthleteHandler;
import com.lemonet.dreamball.dataprovider.DataGenerator;
import com.lemonet.dreamball.dataprovider.MatchHandler;
import com.lemonet.dreamball.dataprovider.StatisticHandler;
import com.lemonet.dreamball.dataprovider.TeamHandler;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthlete;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthleteDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamAthlete;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.finance.domain.Operation;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.realworld.domain.Athlete;
import com.lemonet.dreamball.realworld.domain.AthleteDAO;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.realworld.domain.Round;
import com.lemonet.dreamball.realworld.domain.RoundDAO;
import com.lemonet.dreamball.service.DraftRoomService.HireException;
import com.lemonet.notification.domain.Email;
import com.lemonet.notification.service.EmailService;

@Service
public class DreamworldService {
	public class PlayerLimitReachedError extends Exception {
		private static final long serialVersionUID = 1L;

		PlayerLimitReachedError(){
			super();
		}
	}

	public class TooManyTeamsException extends Exception {
		private static final long serialVersionUID = 1L;

		TooManyTeamsException(){
			super();
		}
	}

	public class LeagueClosedException extends Exception {
		private static final long serialVersionUID = 1L;

		LeagueClosedException(){
			super();
		}
	}

	@Autowired SessionFactory sessionFactory;
	@Autowired OperationDAO odao;
	@Autowired VirtualTeamDAO vtdao;
	@Autowired VirtualLeagueDAO vldao;
	@Autowired AthleteDAO adao;
	@Autowired VirtualLeagueAthleteDAO vladao;
	@Autowired RoundDAO rdao;
	@Autowired LeagueDAO ldao;
	@Autowired MatchDAO mdao;
	@Autowired PlayerDAO pdao;
	@Autowired EmailService emailService;
	@Autowired DraftRoomService draftRoomService;
	@Autowired DataGenerator dataGenerator;

	@Autowired MatchHandler matchHandler;
	@Autowired StatisticHandler statisticHandler;
	@Autowired TeamHandler teamHandler;
	@Autowired AthleteHandler athleteHandler;
	

	/**
	 * Logic of joining a virtual league
	 */
	@Transactional
	public VirtualTeam joinLeague(Player player, VirtualLeague virtualLeague) throws TooManyTeamsException, PlayerLimitReachedError, NotEnoughMoneyException, LeagueClosedException {
		VirtualTeam vt = new VirtualTeam();
		vt.setPlayer(player);
		vt.setVirtualLeague(virtualLeague);

		
		Session session = sessionFactory.getCurrentSession();

    	Operation operation = new Operation(-virtualLeague.getBuyInPlusRake(),Operation.BUYIN,player);
    	operation.setStatus(0L);

    	try {
    		odao.save(operation);
    	}
    	catch (NotEnoughMoneyException e){
    		throw e;
    	}

    	if (virtualLeague.getNumOfPlayers() != null){
			if (virtualLeague.getCurNumOfPlayers() >= virtualLeague.getNumOfPlayers()){
				throw new PlayerLimitReachedError();
			}
    	}
		
    	if (virtualLeague.isClosed()){
    		throw new LeagueClosedException();
    	}

    	Long numTeams = vtdao.getNumOfPlayerTeams(virtualLeague, player);
    	
    	// reached the limit of team entries
    	// has one team in sponsored tournament
    	// has more than a half teams in this tournament
    	if (numTeams >= Constants.MAX_TEAM_ENTRIES ||
        	(numTeams >= 1 && virtualLeague.getBonuses().size() > 0) ||
        	(numTeams >= 1 && numTeams*2 >= virtualLeague.getCurNumOfPlayers())
        	)
    	{
    		throw new TooManyTeamsException();
    	}
    	
    	session.save(vt);

    	odao.update(operation);

    	return vt;
	}


	/**
	 * Generate player prices based on their past experience
	 * TODO: Should be moved outside DAO
	 * 
	 * @param vleague
	 * @param session
	 */
	public void priceAthlets(VirtualLeague vleague) {
		Random r = new Random();

		List<Athlete> athletes = adao.getLeagueAthletsByPerformance(vleague.getBaseLeague());
/*	LINEAR
 * 
 * 		for (Athlete athlete : athletes) {
			VirtualLeagueAthlete vla = new VirtualLeagueAthlete();
			vla.setAthlete(athlete);

//			get mean points
			Double dpoints = athlete.getMeanPoints();
			if (dpoints == null)
				dpoints = 1.0;
						
			Double d = r.nextGaussian()*100000;
			dpounts *= 300000.0d;
			Long price = new Long(1000000 + dpoints + d.intValue());
			price = (price/1000L)*1000L;
			if (price < 500000L)
				price = 500000L;

			vla.setPrice(price);
			vla.setVirtualLeague(vleague);
			vladao.create(vla);
		}*/
		
		Long rank = 1L;
		//1-250
		
		for (Athlete athlete : athletes) {
				VirtualLeagueAthlete vla = new VirtualLeagueAthlete();
				vla.setAthlete(athlete);

				Double dprice =  10000.0d*(250-rank);
				if (dprice < 0d)
					dprice = 0d;
				Double derror = (r.nextGaussian()*Math.sqrt(dprice*0.07));
				dprice += derror + 250000L;
				if (vla.getAthlete().getPosition().equalsIgnoreCase("M"))
					dprice *= 0.85;
				else if (!vla.getAthlete().getPosition().equalsIgnoreCase("S"))
					dprice *= 0.7;

				vla.setPrice(dprice.intValue());
				vla.setVirtualLeague(vleague);
				vladao.create(vla);
				
				rank += 1;
			}
			
	}


	@Transactional
	public void createLeague(Player player, VirtualLeague vleague) throws NotEnoughMoneyException {
		if (vleague.getBuyIn() > odao.getAccountBalance(player))
			throw new NotEnoughMoneyException();
		
		Round r = rdao.getFirstUnstartedRound(ldao.getLeagueById(Constants.CURRENT_LEAGUE));
		vleague.setBaseLeague(r.getLeague());
		vleague.setSeason(r.getSeason());

		vleague.setOwner(player);
		vleague.setBudget(12500000);
		
		vldao.create(vleague);
		priceAthlets(vleague);
	}
	
	@Transactional
	public void cancelLeagues(){
		Session session = sessionFactory.getCurrentSession();
    	Query query = session.createQuery("" +
    		"select vl " +
    		"from VirtualLeague as vl " +
   			"left join vl.baseLeague.rounds as rd " +
   			"left join vl.virtualTeams as vt " +
   			"where vl.status = 0 " +
    		"and rd.number <= vl.startingRound + vl.duration - 1 " +
   			"and rd.season = vl.season " +
   			"and rd.status = 'S' "
   			+ "group by vl having count(vt) <= 1"
   			);
	}

	/**
	 * Distribute prizes if all rounds are finished
	 */
	@Transactional
	public void distributePrizes(VirtualLeague vl) {
		Session session = sessionFactory.getCurrentSession();
		if (vl.isFinished())
			return; // TODO: Should throw error
		
		Logger logger = Logger.getRootLogger();
		logger.trace("League finished!");

    	List<VirtualTeam> vTeams = vldao.getStandings(vl);
    	int pos = 0;

    	if (vTeams.size() == 0){
    		
    	}
    	else if (vl.hasOnlyOnePlayer()){
    		this.payBack(vl);
    	}
    	else {
			for ( VirtualTeam team : vTeams ) {
				Player p = team.getPlayer();
				Double prize = vl.getPrize(pos);
				if (prize == 0)
					break;
				else {
					Operation operation = new Operation(prize, Operation.PRIZE, p);
					operation.setComments("Prize " + (pos + 1) + " in league " + vl.getId().toString());
					operation.setRelatedClass("VirtualLeague");
					operation.setRelatedId(vl.getId());
			    	session.save(operation);
					
					logger.trace("prize? " + prize + " -> " + p.getEmail() + " (" + team.getPoints() + " pts)");
				}
				pos += 1;
			}
    	}
		vl.setFinished();
    	session.merge(vl);
	}

	public Object payBack(VirtualLeague virtualLeague) {
		for (VirtualTeam vteam : virtualLeague.getVirtualTeams()){
			Operation operation = new Operation(virtualLeague.getBuyInPlusRake(), Operation.PAYBACK, vteam.getPlayer());
			operation.setComments("Rollback league " + virtualLeague.getId().toString());
			operation.setRelatedClass("VirtualLeague");
			operation.setRelatedId(virtualLeague.getId());
			try {
				odao.save(operation);
			}
			catch(Exception e){
				
			}
		}
		return null;
	}

	@Transactional
	public void finishRounds()
	{
		Session session = sessionFactory.getCurrentSession();

		League league = ldao.getLeagueById(Constants.CURRENT_LEAGUE);
		Round round = rdao.getFirstUnfinishedRound(league);

    	Query query = session.createQuery("" +
    			"select m " +
    			"from Match as m " +
    			"where m.homeScore is null " +
    			"and m.round = '" + round.getNumber() +"' "
    			);
    	
    	@SuppressWarnings("unchecked")
		List<Match> matches = query.list();
		if (matches.size() == 0){
			round.setStatus(Round.FINISHED);
			rdao.update(round);
		}
		
/*		Match m = mdao.getFirstMatch(round.getLeague(), round.getSeason(), round.getNumber());

		Calendar c = m.getDate();
		Calendar now = Calendar.getInstance();
		c.add(Calendar.HOUR, -6);
		if (now.after(c)){
			round.setStatus(Round.STARTED);
			rdao.update(round);
		}*/
    }
	
	@Transactional
	public void updateLeagueStatuses(){
		
		
	}

	@Transactional
	public void reimburseMainLeague(){
		for (VirtualTeam vt : odao.getMainLeagueNotReimbursed()){
			Operation o = new Operation();
			o.setType(Operation.MAINLEAGUE_BONUS);
			o.setPlayer(vt.getPlayer());
			o.setRelatedClass("VirtualTeam");
			o.setRelatedId(vt.getId());
			o.setValue(vt.getVirtualLeague().getBuyIn());
			try {
				odao.save(o);
			} catch (NotEnoughMoneyException e) {
				// not possible here
			}
		}
		
	}

	@Transactional
	public void distributePrizes(){
		Session session = sessionFactory.getCurrentSession();
		
		// Find virtual leagues which are finished after given round 
		// TODO: Can be improved by taking less leagues
    	Query query = session.createQuery("" +
    			"select vl " +
    			"from VirtualLeague as vl " +
    			"left join vl.baseLeague.rounds as rd " +
    			"where vl.status = 0 " +
    			"and rd.number >= vl.startingRound " +
    			"and rd.number <= vl.startingRound + vl.duration - 1 " +
    			"and rd.season = vl.season " +
    			"and rd.status = 'F' " +
    			"group by vl having count(rd) = vl.duration");

    	@SuppressWarnings("unchecked")
		List<VirtualLeague> vleagues = query.list();
		
		for (VirtualLeague vl : vleagues){
			this.distributePrizes(vl);
		}
	}
	
	@Transactional
	public void resubscribeToMain(){
		// get latest teams of people who wish to resubscribe
    	List<Player> players = pdao.getPlayersWithResubmit();

		// find the main league
    	VirtualLeague vl = vldao.getCurrentSponsoredLeague();
		
    	int i = 0;
    	
		// subscribe those folks
    	for (Player p : players){
    		VirtualTeam vt = vtdao.getLatestUnemptyMainTeam(p);
    		List<VirtualTeam> vts = vtdao.getPlayerTeamsInLeague(p, vl);
    		VirtualTeam vtNew = null;

    		i = i + 1;
    		if (i > 20)
    			break;
    				
    		if (vt == null)
    			continue;
    		// copy players one by one starting with the most expensive
    		
    		if (vts.size() == 0){
	    		try {
	    			vtNew = joinLeague(p, vl);
	    		}
	    		catch (Exception e){
	    		}
    		}
    		else
    			vtNew = vts.get(0);
    		
    		try {	
    			draftRoomService.copyPlayers(vt,vtNew);
    		}
    		catch (Exception e){
    			
    		}
    	}
		
	}

	@Transactional
	public void loadData() throws SAXException, IOException, ParserConfigurationException {
		String dir = "/home/kidzik/workspace/dreamlineup/soccer-parser/output/";
//		dir = "/var/dreamlineup/datafeed/parsed/";
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		saxParser.parse(dir + "teams.xml", teamHandler);
		dataGenerator.generateRounds(ldao.getLeagueById(Constants.CURRENT_LEAGUE),Constants.CURRENT_SEASON);

		saxParser.parse(dir + "athletes.xml", athleteHandler);
		saxParser.parse(dir + "matches.xml", matchHandler);
//		saxParser.parse(dir + "stats.xml", statisticHandler);
		

		/* PARSE OPTA */
//		SAXParserFactory factory = SAXParserFactory.newInstance();
//		SAXParser saxParser = factory.newSAXParser();

//		String dir = "/home/kidzik/datafeed/";
//		File f = new File(dir);
//		File[] files = f.listFiles();
//			for (File file : files) {
//				logger.trace(dir + file.getName());
//				saxParser.parse(dir + file.getName(), new MatchHandler());
//			}

		/* LOAD soccer way */

//		return "redirect:/index.html";
	}


	@Transactional
	public void loadAthletes() throws ParserConfigurationException, SAXException, IOException {
		String dir = "/home/kidzik/workspace/dreamlineup/soccer-parser/output/";
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		saxParser.parse(dir + "athletes.xml", athleteHandler);
		
	}
	
	
	
}
