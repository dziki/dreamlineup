package com.lemonet.dreamball.service;

import java.io.IOException;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.lemonet.dreamball.auth.domain.DreamUser;
import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.finance.domain.OperationDAO;

@Component("UserDataFilter")
public class UserDataFilter extends OncePerRequestFilter implements Filter{
	@Autowired
	@Qualifier("virtualTeamDAO")
	VirtualTeamDAO vtdao;
	@Autowired
	@Qualifier("operationDAO")
	OperationDAO odao;

	@Override
	protected void doFilterInternal(HttpServletRequest req,
			HttpServletResponse res, FilterChain chain)
			throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        if (roles.contains("ROLE_USER")) {
        	DreamUser du = (DreamUser)authentication.getPrincipal();
        	Player p = du.getPlayer();
            request.getSession().setAttribute("currentPlayer", p);
            request.getSession().setAttribute("virtualTeams", vtdao.getRecentTeamsByPlayer(p));  
            request.getSession().setAttribute("pastVirtualTeams", vtdao.getFinishedTeamsByPlayer(p));  

            request.getSession().setAttribute("accountBalance", odao.getAccountBalance(p));
            request.getSession().setAttribute("inPlayBalance", odao.getInPlayBalance(p));

        }

        chain.doFilter(req, res);		
	}

}