package com.lemonet.dreamball.service;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthlete;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthleteDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAOImpl.FireException;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamAthlete;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamAthleteDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.realworld.domain.Athlete;

@Service
public class DraftRoomService {
	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	VirtualLeagueAthleteDAO vladao;

	@Autowired
	VirtualTeamAthleteDAO vtadao;

	@Autowired
	VirtualTeamDAO vtdao;

	public class HireException extends Exception { 
		private static final long serialVersionUID = 1L;

		public HireException(String msg) { 
			super(msg);
		  } 
		}

	@Transactional
	public void fireAthlete(Long virtualTeamAthleteId) throws FireException{
		VirtualTeamAthlete vta = vtadao.getByID(virtualTeamAthleteId);
		if (vta == null)
			throw new FireException("draft.no_such_athlete");
    	if (vta.getVirtualTeam().getVirtualLeague().isStarted())
    		throw new FireException("draft.league_started_error");
    	vta.getVirtualTeam().getVirtualAthletes().remove(vta);
    	vtadao.delete(vta);
	}

	@Transactional
	public VirtualTeamAthlete hireAthlete(Long virtualLeagueAthleteId, Long virtualTeamId) throws HireException{
		VirtualLeagueAthlete vla = vladao.getByID(virtualLeagueAthleteId);
		VirtualTeam vt = vtdao.getVirtualTeamById(virtualTeamId);

		VirtualTeamAthlete vta = new VirtualTeamAthlete();
		
		if (vt.getVirtualLeague().getBudget() - vt.getTeamPrice() < vla.getPrice()){
			throw new HireException("draft.no_money");
		}
		if (vtadao.getRealTeamFrequency(vt, vla.getAthlete().getTeam()) >= Constants.MAX_TEAM_PLAYERS){
			throw new HireException("draft.too_many_from_one_team");
		}
		Logger logger = Logger.getRootLogger();
		logger.trace("hire? " + vt.getId() + " " + vla.getId());
		
		String pos = vla.getAthlete().getPosition().substring(0,1);
		if (!isPositionAvailable(vt, pos)){
			String posDesc = "draft.you_have_goalkeeper";
			if (pos.equalsIgnoreCase("D"))
				posDesc = "draft.you_have_defenders";
			if (pos.equalsIgnoreCase("M"))
				posDesc = "draft.you_have_midfielders";
			if (pos.equalsIgnoreCase("S"))
				posDesc = "draft.you_have_strikers";
			throw new HireException(posDesc);
		}
		
		vta.setVirtualLeagueAthlete(vla);
		vta.setVirtualTeam(vt);
		try {
			vtadao.create(vta);
		}
		catch (ConstraintViolationException e){
			throw new HireException("draft.player_already_added");
		}
    	if (vt.getVirtualLeague().isStarted())
    		throw new HireException("draft.league_started_error");

		return vta;
	}
	
	
	@Transactional
	public void setFormation(Long virtualTeamId, String formation) throws FormationException {
		VirtualTeam vtp = vtdao.getVirtualTeamById(virtualTeamId);
		if (!isFormationSupported(formation))
			throw new FormationException("draft.formation_unsupported.");
		if (!isFormationAvailable(vtp, formation))
			throw new FormationException("draft.formation_invalid_team");
		vtp.setFormation(formation);
		vtdao.merge(vtp);
	}
	
	public int freeSlots(VirtualTeam vt, String pos, String form) {
		int i = 0;
		pos = pos.substring(0, 1);
		if (pos.equalsIgnoreCase("D"))
			i = 0;
		if (pos.equalsIgnoreCase("M"))
			i = 2;
		if (pos.equalsIgnoreCase("S"))
			i = 4;
		Long nMax = null;
		if (pos.equalsIgnoreCase("G"))
			nMax = new Long(1);
		else
			nMax = new Long(form.substring(i, i+1));
		
		return nMax.intValue() - getUsedSlots(vt, pos);
		
	}
	public int freeSlots(VirtualTeam vt, String pos){
		return freeSlots(vt, pos, vt.getFormation());
	}
	public boolean isPositionAvailable(VirtualTeam vt, String pos) {
		return freeSlots(vt, pos) > 0;
	}
	
	
	private int getUsedSlots(VirtualTeam vt, String pos) {
		return vtadao.countPlayersOnPos(vt,pos).intValue();
	}
	
	public class FormationException extends Exception {
		private static final long serialVersionUID = 1L;

		FormationException(String msg) { 
			super(msg);
		} 
	}
	
	
	
	private boolean isFormationAvailable(VirtualTeam vt, String formation) {
		List<String> positions = Arrays.asList("G","D","M","S");
		
		for (String pos : positions){
			if (freeSlots(vt, pos, formation) < 0)
				return false;
		}
		return true;
	}
	private boolean isFormationSupported(String formation) {
		return VirtualTeam.FORMATIONS.contains(formation);
	}
	
	public void removeAllPlayers(VirtualTeam vt)
	{
		for (VirtualTeamAthlete vta : vt.getVirtualAthletes()){
			try {
				fireAthlete(vta.getId());
			} catch (FireException e) {
				e.printStackTrace();
			}
		}
		
	}

	
	public void copyPlayers(VirtualTeam from, VirtualTeam to)
	{
		//removeAllPlayers(to);
		if (to.getVirtualAthletes().size() > 0)
			return;
		VirtualLeague vl = to.getVirtualLeague();
		
		for (VirtualTeamAthlete vta : from.getVirtualAthletes()){
			// find the athlete in new league
			Athlete a = vta.getVirtualLeagueAthlete().getAthlete();
			VirtualLeagueAthlete vla = vladao.getVirtualLeagueAthlete(vl, a);
			
			// try to hire
			try {
				hireAthlete(vla.getId(), to.getId());
			} catch (HireException e) {
				
			}
		}
	}
	

}
