package com.lemonet.dreamball.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.velocity.VelocityContext;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.dataprovider.MatchHandler;
import com.lemonet.dreamball.dataprovider.StatisticHandler;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeam;
import com.lemonet.dreamball.dreamworld.domain.VirtualTeamDAO;
import com.lemonet.dreamball.engine.Constants;
import com.lemonet.dreamball.realworld.domain.LeagueDAO;
import com.lemonet.dreamball.realworld.domain.Match;
import com.lemonet.dreamball.realworld.domain.MatchDAO;
import com.lemonet.dreamball.realworld.domain.Round;
import com.lemonet.dreamball.realworld.domain.RoundDAO;
import com.lemonet.notification.domain.Email;
import com.lemonet.notification.service.EmailService;

@Service
public class LeagueMaintenanceService {
	@Autowired VirtualLeagueDAO vldao;
	@Autowired VirtualTeamDAO vtdao;
	@Autowired LeagueDAO ldao;
	@Autowired MatchDAO mdao;
	@Autowired RoundDAO rdao;
	@Autowired PlayerDAO pdao;

	@Autowired DataGrabberService dataGrabberService;
	@Autowired DreamworldService dreamService;
	@Autowired EmailService emailService;
	
	@Autowired MatchHandler matchHandler;
	@Autowired StatisticHandler statisticHandler;

	@Transactional
	public void parseResults()
	{
		Logger logger = Logger.getLogger("PARSER");
		String dir = Constants.PARSER_PATH + "output/";
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = null;

		logger.trace("PARSER:" + dir);
		
		try {
			saxParser = factory.newSAXParser();
		} catch (ParserConfigurationException e) {
			logger.trace("PARSER: " + e.toString());
			EmailService.emailException(e);
		} catch (SAXException e) {
			logger.trace("PARSER: " + e.toString());
			EmailService.emailException(e);
		}
		try {
			saxParser.parse(dir + "matches.xml", matchHandler);
//			saxParser.parse(dir + "stats.xml", statisticHandler);
		} catch (SAXException e) {
			logger.trace("PARSER: " + e.toString());
			EmailService.emailException(e);
		} catch (IOException e) {
			logger.trace("PARSER: " + e.toString());
			EmailService.emailException(e);
		}
		logger.trace("PARSER: DATA LOADED");
	}
	
	@Scheduled(cron="0 25 * * * ?")
//	@Transactional
	public void updateResults()
	{
		int i = 0;
		Logger logger = Logger.getLogger("PARSER");
		logger.trace("PARSER: Loading results ");
		List<Match> matches = mdao.getFinishedMatches();
		for (Match m : matches){
			logger.trace("PARSER: Downloading " + m.getOutsideId().toString());
			dataGrabberService.downloadResult(m.getOutsideId());
			this.parseResults();
			i++;
			if (i == 1)
				break;
		}
	}
	
//	@Transactional
	public void updateResult(Long mid)
	{
		Match m = mdao.getMatchById(mid);
		dataGrabberService.downloadResult(m.getOutsideId());
		this.parseResults();
	}
	
	public void updateDates()
	{
		Round r = rdao.getFirstUnstartedRound(ldao.getLeagueById(Constants.CURRENT_LEAGUE));
		List<Match> matches = mdao.getMatches(r.getLeague(), r.getSeason(), r.getNumber(), r.getNumber());
		for (Match m : matches){
			dataGrabberService.downloadResult(m.getOutsideId());
			this.parseResults();
		}
	}
	
//	@Scheduled(cron="0 30 * * * ?")
	public void distributePrizes()
	{
		dreamService.finishRounds();
		dreamService.distributePrizes();
	}
	
	public void sendRoundFinishedEmail(Player p, Round r){
		VelocityContext context = new VelocityContext();
		Map<Long, Double> prizes = new HashMap<Long, Double>();
		Map<Long, Integer> positions = new HashMap<Long, Integer>();

		List<VirtualTeam> vts = vtdao.getTeamsFinishedByPlayer(p, r);
		List<VirtualTeam> vtsReal = new ArrayList<VirtualTeam>();
		Double prizeTotal = new Double(0);

		for (VirtualTeam vt : vts) {
			List<VirtualTeam> standings = vldao.getStandings(vt.getVirtualLeague());

			int pos = standings.indexOf(vt);
			if (pos == -1)
				continue;
			Double prize = vt.getVirtualLeague().getPrize(pos);
			prizeTotal += prize;

			prizes.put(vt.getId(), prize);
			positions.put(vt.getId(), pos + 1);

			vtsReal.add(vt);
		}
		if (vtsReal.size() == 0)
			return;
		
		context.put("prizeTotal", prizeTotal);
		context.put("prizes", prizes);
		context.put("positions", positions);
		context.put("virtualTeams", vtsReal);

		String msg = "";
		try {
			msg = EmailService.fillOutTemplate("velocity/league_finished_pl.jsp", context);
		} catch (Exception e1) {
			EmailService.emailException(e1);
		}
		String to = p.getEmail();
		Email email = new Email(to,"Podsumowanie tygodnia", msg);
		emailService.queue(email);
	}
	
//	@Scheduled(cron="0 35 * * * ?")
	public void sendQueuedEmails() throws Exception
	{
		emailService.sendAll();
	}
	
	public void generateRoundEmails(Long rnum)
	{
		Round r = rdao.getRound(Constants.CURRENT_SEASON, ldao.getLeagueById(Constants.CURRENT_LEAGUE), rnum);
		List<Player> players = pdao.getPlayersFinishedInRound(r);
		for (Player p : players){
			if (p.getEmailSubscription())
				sendRoundFinishedEmail(p,r);
		}
		
	}
}
