package com.lemonet.dreamball.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.lemonet.dreamball.engine.Constants;
import com.lemonet.notification.service.EmailService;

@Service
public class DataGrabberService {
	public void downloadResult(Long matchId)
	{
		Logger logger= Logger.getLogger("PARSER");
		
		String path = Constants.PARSER_PATH;
				
		logger.trace("PARSER: Run tracker.py");
		String cmd = "tracker.py";
		
		String test[] = {"/usr/bin/python",path + cmd,"-d",path,"-m",matchId.toString()};
//		String test[] = {"echo","blabla"};
		Runtime r = Runtime.getRuntime();
		Process p = null;
		try {
			p = r.exec(test);
		} catch (IOException e) {
			EmailService.emailException(e);
		}
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			EmailService.emailException(e);
		}
		int exitCode = p.exitValue();
		
		p.getErrorStream();
		BufferedReader reader;

		if (exitCode == 0)
			reader = new BufferedReader(new InputStreamReader(
			 p.getInputStream()));
		else{
			reader = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));
		}
		String res = "";
		String line = null;
		
		try {
			line = reader.readLine();
			res = line;
			while (line != null) {
				line = reader.readLine();
				res += line;
			}
		} catch (IOException e) {
			EmailService.emailException(e);
		}
		
		if (exitCode != 0){
			EmailService.emailProblem("Results download",res);
			logger.trace("PARSER: Failed to download data." );
		}
		logger.trace("PARSER: " + res);
		
//		PythonInterpreter.initialize(System.getProperties(), System.getProperties(), new String[0]);
/*		PythonInterpreter pi = new PythonInterpreter();
		pi.exec("import sys");
		pi.exec("sys.path.append('/usr/lib/python2.7/')");
		pi.exec("import os");
		pi.exec("os.chdir('" + path + "')");
		pi.execfile(file);*/

	}


}
