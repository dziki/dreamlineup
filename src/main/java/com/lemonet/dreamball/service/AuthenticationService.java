package com.lemonet.dreamball.service;

import javax.persistence.EntityNotFoundException;

import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.auth.domain.PlayerDAOImpl.TooYoung;
import com.lemonet.dreamball.auth.domain.Token;
import com.lemonet.dreamball.auth.domain.Token.TokenExpiredException;
import com.lemonet.dreamball.auth.domain.TokenDAO;
import com.lemonet.dreamball.finance.domain.Operation;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.web.commands.Login;
import com.lemonet.notification.domain.Email;
import com.lemonet.notification.service.EmailService;

@Service
public class AuthenticationService {
	@Autowired  
	ReloadableResourceBundleMessageSource messageSource;  

	@Autowired
	PlayerDAO pdao;
	
	@Autowired
	TokenDAO tdao;

	@Autowired
	OperationDAO odao;
	
	@Autowired
    EmailService emailService;
	
	@Autowired PasswordEncoder passwordEncoder;

	
	public Token findValid(String token) throws TokenExpiredException {
		Token t = tdao.find(token);
		if (t==null)
			throw new EntityNotFoundException();
		if (!t.isValid())
			throw new TokenExpiredException();
		return t;
	}

	@Transactional
	public Player loginPlayer(Login loginData){
    	return pdao.loginPlayer(loginData.getUsername(),loginData.getPassword());
    }
	
	@Transactional
	public void activatePlayer(String token) throws TokenExpiredException, EntityNotFoundException {
		Token t = this.findValid(token);
		Player player = t.getPlayer();
		
		// Don't activate already active player.
		// The only case when it can happen is double click on the link so ignore the problem (no exception)
		if (!player.isActive()){
			player.setActive(true);
			pdao.update(player);
		}
	}
	@Transactional
	public void reSendActivationLink(Player player, String baseUrl) throws Exception {
		sendActivationLink(player, baseUrl);
	}

	
	public void sendActivationLink(Player player, String baseUrl) throws Exception {
		Token t = new Token(player);
		tdao.create(t);
		
		VelocityContext context = new VelocityContext();
		context.put("registrationLink", baseUrl + "/auth/" + t.getToken() + "/activate.html");
		String msg = EmailService.fillOutTemplate("velocity/registration_pl.jsp", context);
		String to = player.getEmail();
		Email email = new Email(to,"Rejestracja", msg);
		emailService.send(email);		
	}

	@Transactional
	public void create(Player player, String baseUrl) throws TooYoung, Exception {
		player.setPassword(passwordEncoder.encode(player.getPassword()));
		pdao.create(player);
		  
		Operation operation = new Operation(new Double(100), Operation.ENTRANCE_BONUS, player);
		
		try {
			odao.save(operation);
		}
		catch (NotEnoughMoneyException e) {
			// ignore: impossible with positive deposit
		}

//		sendActivationLink(player, baseUrl);
	}

	@Transactional
	public void changePassword(String token, String password) throws EntityNotFoundException, TokenExpiredException, Exception {
		// he received an e-mail with password change link so it is enough
		// we can activate his account if we didn't do that before.
		activatePlayer(token);
		
		Token t = this.findValid(token);
		Player p = t.getPlayer();
		p.setPassword(passwordEncoder.encode(password));
		pdao.update(p);
		
		VelocityContext context = new VelocityContext();
		String msg = EmailService.fillOutTemplate("velocity/password_changed_pl.jsp", context);
		String to = p.getEmail();
		Email email = new Email(to,messageSource.getMessage("auth.password_changed", null, "Password changed", null), msg);
		emailService.send(email);
	}

	@Transactional
	public void requestPasswordChange(Player p, String baseUrl) throws Exception {
		Token t = new Token(p);
		tdao.create(t);
		
		VelocityContext context = new VelocityContext();
		context.put("changePassLink", baseUrl + "/auth/pass/" + t.getToken() + "/change.html");
		String msg = EmailService.fillOutTemplate("velocity/change_pass_pl.jsp", context);
		String to = p.getEmail();
		Email email = new Email(to,messageSource.getMessage("auth.password_change", null, "Password change", null), msg);
		emailService.send(email);
	}

	@Transactional
	public void activate(Player player) throws Exception {
		if (!player.isActive()){
			player.setActive(true);
			pdao.update(player);
		}
		VelocityContext context = new VelocityContext();
		String msg = EmailService.fillOutTemplate("velocity/registration_automatic_pl.jsp", context);
		String to = player.getEmail();
		Email email = new Email(to,"Konto aktywowane", msg);
		emailService.send(email);
		
	}
}
