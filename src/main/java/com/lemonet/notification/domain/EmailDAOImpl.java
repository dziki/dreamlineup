package com.lemonet.notification.domain;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("emailDAO")
public class EmailDAOImpl implements EmailDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void update(Email email) {
    	Session session = sessionFactory.getCurrentSession();
    	session.update(email);
	}

	@Override
	public void create(Email email) {
    	Session session = sessionFactory.getCurrentSession();
    	session.save(email);
	}

	@Override
	public List<Email> getAll() {
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Email.class)
    		.add(Restrictions.eq("sent", false));
    	
		@SuppressWarnings("unchecked")
		List<Email> emails = criteria.list();
		
		return emails;
	}
}