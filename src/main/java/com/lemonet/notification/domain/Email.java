package com.lemonet.notification.domain;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.log4j.Logger;

@Entity
@Table(name = "notification__email")
public class Email implements Cloneable {
	static Logger logger = Logger.getLogger(Email.class);

	/**
	 * @uml.property  name="id"
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
	 * @uml.property  name="toEmails"
	 */
	private String toEmail;

	public String getTo() {
		return toEmail;
	}

	public void setTo(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @uml.property  name="subject"
	 */
	private String subject;

	/**
	 * @uml.property  name="body"
	 */
	@Column(columnDefinition="TEXT")
	private String body;
	
	private boolean sent;

	public Email(){
		
	}
	
	public Email(String to, String subject, String body) {
		super();
		this.toEmail = to;
		this.subject = subject;
		this.body = body;
	}

    public MimeMessage draftEmailMessage(Session mailSession) throws AddressException, MessagingException
    {
        MimeMessage emailMessage = new MimeMessage(mailSession);
        /**
         * Set the mail recipients
         * */
        emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(this.toEmail));
        emailMessage.setSubject(subject);
        /**
         * If sending HTML mail
         * */
        emailMessage.setContent(body, "text/html; charset=utf-8");
        /**
         * If sending only text mail
         * */
        //emailMessage.setText(emailBody);// for a text email
        return emailMessage;
    }

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

}
