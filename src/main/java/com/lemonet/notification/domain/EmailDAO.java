package com.lemonet.notification.domain;

import java.util.List;


/**
 * @author  kidzik
 */
public interface EmailDAO {
    public void create(Email email);
    public List<Email> getAll();
	public void update(Email email);
}
