package com.lemonet.notification.service;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.notification.domain.Email;
import com.lemonet.notification.domain.EmailDAO;

@Service
public class EmailService {
	
	@SuppressWarnings("unchecked")
	public static String dumpRequest(HttpServletRequest req){
		String res = "";
		if (req == null)
			return "[NO REQUEST]";
				
		res += "\n" + "URI: " + req.getRequestURI();
		res += "\n" + "Session: " + req.getRequestedSessionId();
		res += "\n" + "Query string: " + req.getQueryString();
		res += "\n" + "Content type: " + req.getContentType();
		res += "\n" + "Method: " + req.getMethod();

		res += "\n" + "Params";
		Enumeration<String> elements = req.getParameterNames();
		while(elements.hasMoreElements()){
			String s = elements.nextElement();
			res += "\n" + s + ": " + req.getParameter(s);
		}
		
		if (req.getUserPrincipal() != null){
			res += "\n" + "USER";
			res += "\n" + "Principal: " + req.getUserPrincipal().getName();
		}
		
		res += "\n" + "Headers";
		elements = req.getHeaderNames();
		while(elements.hasMoreElements()){
			String s = elements.nextElement();
			res += "\n" + s + ": " + req.getHeader(s);
		}
		
		
		return res;
	}
	
    public static void emailException(Exception e, HttpServletRequest req){
		String msg;
		String title = "dreamlineup.com error";

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		e.printStackTrace(ps);
		
		msg = dumpRequest(req);
		msg += "\n";
		msg += baos.toString();
		title = "dreamlineup.com: " + e.getMessage();
		msg = "<html><head></head><body>" + msg.replace("\n", "\n<br />") + "</body></html>";
		
		EmailService.emailProblem(title, msg);
    }
    
    public static void emailException(Exception e){
    	emailException(e,null);
    }
  
	public static String fillOutTemplate(String path, VelocityContext context) throws Exception
	{
		Template template;
		StringWriter writer = new StringWriter();

		Properties p = new Properties();
        p.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        p.setProperty("resource.loader", "class");
        p.setProperty("class.resource.loader.description", "Velocity Classpath Resource Loader");
        p.setProperty("class.resource.loader.class", ClasspathResourceLoader.class.getName());
        p.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
        org.apache.velocity.app.Velocity.init(p);
		template = org.apache.velocity.app.Velocity.getTemplate(path, "UTF-8");		
		template.merge(context, writer);

		return writer.toString();
	}
	
	
	public static void emailProblem(String title, String msg) {
		String to = "lukasz.kidzinski@gmail.com";
		Email email = new Email(to,title, msg);
		try {
			EmailService es = new EmailService();
	    	es.send(email);
		} catch (AddressException e1) {
			// No way to go for the moment...
		} catch (MessagingException e1) {
			// No way to go for the moment.
		}
	}

	Session mailSession;
    @Autowired EmailDAO edao;

    private void setMailServerProperties()
    {
        Properties emailProperties = System.getProperties();
        emailProperties.setProperty("mail.smtp.host", "localhost");
        emailProperties.setProperty("mail.mime.charset", "UTF-8");

        mailSession = Session.getDefaultInstance(emailProperties, null);
    }
    
    public void send(Email email) throws AddressException, MessagingException{
        /**
         * Sender's credentials
         * */
    	setMailServerProperties();
    	
        MimeMessage emailMessage = email.draftEmailMessage(mailSession);
        emailMessage.setFrom(new InternetAddress("auto@dreamlineup.com"));

        Transport.send(emailMessage, emailMessage.getAllRecipients());
    }

    @Transactional
    public void queue(Email email) {
    	edao.create(email);
    }
    
    @Transactional
    public void sendAll() throws AddressException, MessagingException{
    	List<Email> emails = edao.getAll();
    	for (Email e : emails){
    		this.send(e);
    		e.setSent(true);
    		edao.update(e);
    	}
	}
}
