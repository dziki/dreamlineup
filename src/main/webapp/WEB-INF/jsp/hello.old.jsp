<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />

<h2><spring:message code="base.welcome" text="Welcome" /></h2>

<h2><spring:message code="base.yourleagues" text="Your leagues" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="leagues.id" text="id" /></th>
<th><spring:message code="leagues.name" text="name" /></th>
<th><spring:message code="leagues.fee" text="fee" /></th>
<th><spring:message code="leagues.numofplayers" text="num of players" /></th>
<th><spring:message code="leagues.duration" text="duration" /></th>
<th><spring:message code="leagues.closesin" text="closes in" /></th>
<th><spring:message code="leagues.prizepool" text="prize pool" /></th>
</tr>
<c:forEach var="vlp" items="${myparticipations}">
<tr onclick="document.location = '<c:out value="/dreamball/draft/room.html?id=${vlp.id}"/>'">
<td><c:out value="${vlp.virtualLeague.id}"/></td>
<td><c:out value="${vlp.virtualLeague.title}"/></td>
<td><c:out value="${vlp.virtualLeague.buyInPlusRake}"/></td>
<td><c:out value="${vlp.virtualLeague.curNumOfPlayers}"/> / <c:out value="${vlp.virtualLeague.numOfPlayers}"/></td>
<td><c:out value="${vlp.virtualLeague.duration}"/></td>
<td><c:out value="${vlp.virtualLeague.registrationDeadline}"/></td>
<td><c:out value="${vlp.virtualLeague.pricePool}"/></td>
</tr>
</c:forEach>
</table>

<h2><spring:message code="base.activeleagues" text="Active leagues" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="leagues.id" text="id" /></th>
<th><spring:message code="leagues.name" text="name" /></th>
<th><spring:message code="leagues.fee" text="fee" /></th>
<th><spring:message code="leagues.numofplayers" text="num of players" /></th>
<th><spring:message code="leagues.duration" text="duration" /></th>
<th><spring:message code="leagues.closesin" text="closes in" /></th>
<th><spring:message code="leagues.prizepool" text="prize pool" /></th>
</tr>
<c:forEach var="league" items="${leagues}">
<tr onclick="document.location = '<c:out value="/dreamball/leagues/details.html?id=${league.id}"/>'">
<td><c:out value="${league.id}"/></td>
<td><c:out value="${league.title}"/></td>
<td><c:out value="${league.buyIn}"/></td>
<td><c:out value="${league.curNumOfPlayers}"/> / <c:out value="${league.numOfPlayers}"/></td>
<td><c:out value="${league.duration}"/></td>
<td><c:out value="${league.registrationDeadline}"/></td>
<td><c:out value="${league.pricePool}"/></td>
</tr>
</c:forEach>
</table>
<h2><spring:message code="base.bestplayers" text="Best players" /></h2>

<h2><spring:message code="base.rules" text="Rules" /></h2>

<h2><spring:message code="base.videos" text="Videos" /></h2>

<jsp:include page="footer.jsp" />
