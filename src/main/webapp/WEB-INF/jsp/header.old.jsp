<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>dreamlineup.com</title>

<style>
.active-rows tr td {
	color: blue;
	text-decoration: underline;
	cursor: hand;
}
</style>
</head>
<body>

<h1>dreamlineup.com</h1>
<c:if test="${!(currentPlayer==null)}">
<spring:message code="base.player" text="Player" />:
<c:out value="${currentPlayer.firstName}" /><br />
<spring:message code="base.accountbalance" text="Account balance" />:
<c:out value="${currentPlayer.accountBalance}" /><br />
<spring:message code="base.inplay" text="In play" />:
<!--c:out value="${currentPlayer.accountBalance}" /><br /-->

<h2>Player Menu</h2>

<ul>
<c:if test="${!(currentPlayer==null)}">
<li><a href="<c:url value='/finance/history.html' />"><spring:message code="base.history" text="Account history" /></a></li>
<li><a href="<c:url value='/finance/deposit.html' />"><spring:message code="base.deposit" text="Deposit" /></a></li>
<li><a href="<c:url value='/finance/withdrawal.html' />"><spring:message code="base.withdrawal" text="Withdrawal" /></a></li>
</c:if>
</ul>

</c:if>

<h2>Main Menu</h2>
<ul>
<c:if test="${(currentPlayer==null)}">
<li><a href="<c:url value='/auth/login.html' />"><spring:message code="base.login" text="Login" /></a></li>
<li><a href="<c:url value='/auth/registration.html' />"><spring:message code="base.register" text="Register" /></a></li>
</c:if>

<li><a href="<c:url value='/index.html' />"><spring:message code="base.home" text="Home" /></a></li>
<li><a href="<c:url value='/rules.html' />"><spring:message code="base.rules" text="Rules" /></a></li>
<li><a href="<c:url value='/leagues/list.html' />"><spring:message code="base.leagues" text="Leagues" /></a> - <a href="<c:url value='/leagues/add.html' />">Create league</a></li>
<li><a href="<c:url value='/players/list.html' />"><spring:message code="base.players" text="Players" /></a></li>
<li><a href="<c:url value='/blog.html' />"><spring:message code="base.blog" text="Blog" /></a></li>
</ul>
