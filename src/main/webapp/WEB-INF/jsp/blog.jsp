<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />

			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><spring:message code="base.blog" text="Blog" /></h2>
				    <div class="page_box">
					  <div class="normal_page">

<h3>Otwarcie dreamlineup.com!</h3>
<div style="font-size: 11px; padding-bottom: 5px; text-align: right;">1/02/2014</div>
Udowodnij znajomym, że selekcjonujesz lepiej niż Smuda czy Beenhakker. 
Wybierz najlepszy skład kolejki i wygraj nagrody! Spróbuj swoich szans
w <a style="color: #ffffff; text-decoration: underline; fonr-weight: bold;" href="<c:url value="/leagues/sponsored.html"/>">turnieju głównym</a> i weź udział w walce
o kartę EMPiK o wartości 50PLN!
<div style="font-size: 11px; padding: 10px; text-align: right;">dreamlineup.com</div>

</div>
				    </div>

			      </div>
<jsp:include page="right.jsp" />
			</div>

<jsp:include page="footer.jsp" />
