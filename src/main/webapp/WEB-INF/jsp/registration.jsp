<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:useBean id="date" class="java.util.Date" />
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />

			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><spring:message code="registration.signup" /></h2>
				    <div class="page_box">
					  <div class="normal_page">

<form:form method="Post" action="registration.html" commandName="registration">
<table class="form_table">
<!-- tr>
<td><spring:message code="registration.firstName" text="First name" />:<br /><FONT color="red"><formerrors path="firstName" /></FONT></td>
<td><forminput path="firstName" /></td>
</tr>
<tr>
<td><spring:message code="registration.lastName" text="Last name" />:<br /><FONT color="red"><formerrors path="lastName" /></FONT></td>
<td><forminput path="lastName" /></td>
</tr -->
<tr>
<td><spring:message code="registration.username" text="Username" />:<br /><FONT color="red"><form:errors path="username" /></FONT></td>
<td><form:input path="username" /></td>
</tr>
<!-- tr>
<td><spring:message code="registration.birthDate" text="Date of birth" />:<br /><FONT color="red"><form:errors path="birthDate" /></FONT></td>
<td><forminput path="birthDate" id="birthDate" cssStyle="display:none;" /><select id="day" style="width: 70px;">
<% for (int i = 1; i <= 31; i++) {
            %>
<option value="<%= (i)%>"><%= (i)%></option>
<% } %>
</select>

<select id="month" style="width: 100px;">
<option value="1"><spring:message code="month.january" /></option>
<option value="2"><spring:message code="month.february" /></option>
<option value="3"><spring:message code="month.march" /></option>
<option value="4"><spring:message code="month.april" /></option>
<option value="5"><spring:message code="month.may" /></option>
<option value="6"><spring:message code="month.june" /></option>
<option value="7"><spring:message code="month.july" /></option>
<option value="8"><spring:message code="month.august" /></option>
<option value="9"><spring:message code="month.september" /></option>
<option value="10"><spring:message code="month.october" /></option>
<option value="11"><spring:message code="month.november" /></option>
<option value="12"><spring:message code="month.december" /></option>
</select>

<select id="year" style="width: 70px;">
<%
 for (int i = 1900; i <= 2010; i++) {
            %>
<option value="<%= (i)%>"><%= (i)%></option>
<% } %>
</select>
</td>
</tr -->
<tr>
<td><spring:message code="registration.email" text="Email" />:<br /><FONT color="red"><form:errors path="email" /></FONT></td>
<td><form:input path="email" /></td>
</tr>
<tr>
<td><spring:message code="registration.password" text="Password" />:<br /><FONT color="red"><form:errors path="password" /></FONT></td>
<td><form:password path="password" /></td>
</tr>
<tr>
<td><spring:message code="registration.confirmPass" text="Confirm Password" />:<br /><FONT color="red"><form:errors path="confirmPassword" /></FONT></td>
<td><form:password path="confirmPassword" /></td>
</tr>
<tr>
<td></td>
<td><form:checkbox path="confirmTerms" /><br /> <spring:message code="registration.confirmTerms" /> <br /><FONT color="red"><form:errors path="confirmTerms" /></FONT></td>
</tr>
<tr>
				<td colspan='2'><input style="width: 150px;" name="submit" type="submit"
					value="<spring:message code="registration.register" />" />
				</td>
</tr>
</table>
</form:form>
					  </div>
				    </div>

			      </div>

<jsp:include page="registration.right.jsp" />
			</div>

<jsp:include page="footer.jsp" />