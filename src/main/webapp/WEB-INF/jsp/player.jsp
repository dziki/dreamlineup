<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:if test="${!(currentPlayer==null)}">
				    <h3><span>Welcome</span> ${currentPlayer.firstName}</h3>
				    <div class="summary_box clearfix">
					  <div class="sum_in"><p>Account balance</p> <span>5010$</span></div>
					  <div class="sum_in"><p>In play</p> <span>5010$</span></div>
					  <div class="sum_in">
						<ul>
<li><a href="<c:url value='/finance/history.html' />"><spring:message code="base.history" text="Account history" /></a></li>
<li><a href="<c:url value='/finance/deposit.html' />"><spring:message code="base.deposit" text="Deposit" /></a></li>
<li><a href="<c:url value='/finance/withdrawal.html' />"><spring:message code="base.withdrawal" text="Withdrawal" /></a></li>
						</ul>
					  </div>
				    </div>
				    <h4>Your Leagues</h4>
				    <div class="leagues_box clearfix">
<c:forEach var="vlp" items="${myparticipations}">
<div class="lea_in" onclick="document.location = '<c:out value="/dreamball/draft/room.html?id=${vlp.id}"/>'"><p><c:out value="${vlp.virtualLeague.title}"/></p> <span><c:out value="${vlp.virtualLeague.pricePool}"/>$</span></div>
</c:forEach>
				    </div>
</c:if>

