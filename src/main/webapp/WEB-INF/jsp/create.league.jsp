<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />
<style>
table.form_table select {width: 150px;}
</style>
			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><spring:message code="leagues.createLeague" /></h2>
				    <div class="page_box">
					  <div class="normal_page" style="width: 400px;">
					  
<form:form method="POST" commandName="league">
<table class="form_table">
<tr>
<td><spring:message code="leagues.startingRound" text="Starting round" />:<FONT color="red"><form:errors path="startingRound" /></FONT></td>
<td><form:select path="startingRound">
    <form:options items="${startingRoundList}" />
</form:select></td>
</tr>
<tr>
<td><spring:message code="leagues.duration" text="Duration" />:<FONT color="red"><form:errors path="duration" /></FONT></td>
<td><form:select path="duration">
    <form:options items="${durationList}" />
</form:select> <spring:message code="leagues.rounds" text="rounds" /></td>
</tr>
<tr>
<td><spring:message code="leagues.numOfPlayers" text="Number of players" />:<FONT color="red"><form:errors path="numOfPlayers" /></FONT></td>
<td><form:select path="numOfPlayers">
    <form:options items="${numOfPlayersList}" />
</form:select></td>
</tr>
<tr>
<td><spring:message code="leagues.buyIn" text="Buy-in" />:<FONT color="red"><form:errors path="buyIn" /></FONT></td>
<td><form:select path="buyIn">
    <form:options items="${buyInList}" />
</form:select> DP</td>
</tr>
</table>
<input type="submit" value="<spring:message code="leagues.create" />" />
</form:form>

</div>
				    </div>

			      </div>
<jsp:include page="right.jsp" />
			</div>

<jsp:include page="footer.jsp" />
