<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />

			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><spring:message code="auth.login" text="Login" /></h2>
				    <div class="page_box">
					  <div class="normal_page">

<h3><spring:message code="auth.login" text="Login" /></h3>
<br/>

	<c:if test="${not empty error}">
		<div class="errorblock">
			<spring:message code="auth.login_error" /><br />
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>
 
	<form name='f' action="<c:url value='/j_spring_security_check' />"
		method='POST'>
 
		<table class="form_table">
			<tr>
				<td><spring:message code="auth.email" text="Login" />:</td>
				<td><input type='text' name='j_username' value=''>
				</td>
			</tr>
			<tr>
				<td><spring:message code="auth.password" text="Login" />:</td>
				<td><input type='password' name='j_password' />
				</td>
			</tr>
			<tr>
				<td colspan='2'><input style="width: 150px;" name="submit" type="submit"
					value="<spring:message code="auth.login" text="Login" />" />
				</td>
			</tr>
		</table>
		<a style="font-size: 15px; color: #ffffff; text-decoration: underline" href="<c:url value="/auth/registration.html"/>"><spring:message code="auth.register" /></a><br /><br />

		<a style="color: #ffffff; text-decoration: underline" href="<c:url value="/auth/pass/remind.html"/>"><spring:message code="auth.password_recovery" /></a>
 
	</form>
</div>
				    </div>

			      </div>
<jsp:include page="registration.right.jsp" />
			</div>

<jsp:include page="footer.jsp" />

<script><% if (request.getParameter("error") != null && request.getParameter("error").equals("1")) { 
	 out.println("alertBox(i18nstrings['error'],i18nstrings['auth.login_error']);");
 }
%></script>
