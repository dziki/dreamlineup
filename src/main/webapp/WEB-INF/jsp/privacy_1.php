
<?php include('template/header.php'); ?>
<?php include('template/top.php'); ?>

		
			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title">Normal page name: <b>Privacy Policy</b></h2>
				    <div class="page_box">
					  <div class="normal_page">
					<h3>NOTICE: Privacy Policy Effective March 15th, 2010</h3>
<h4>DraftStreet™ Website Privacy Policy</h4>

Thank you for visiting the DraftStreet™ website located at www.draftstreet.com (the “Site”). The Site is an Internet property of Left Tackle LLC (“DraftStreet™,” “we” or “us”). This DraftStreet™ Website Privacy Policy (“Privacy Policy”) covers our treatment of Personal Information (as defined hereinbelow) and other information that we collect when you: (a) access or use the Site; (b) become a DraftStreet™ Member (as defined in the DraftStreet™ Website Terms and Conditions (“Terms and Conditions”)); (c) use the DraftStreet™ Services (as defined in the Terms and Conditions); (d) purchase DraftStreet™ products, or register to purchase DraftStreet™ purchase products, from the DraftStreet™ shop accessible via the Site (“DraftStreet™ Shop”); and/or (e) register for one of our sweepstakes, promotional offers or contests (the “Sweepstakes”). It is important that visitors are fully informed about the use of their Personal Information. We believe in 100% permission-based marketing.<br /><br />

If you are a resident of the State of California and would like to opt-out from the disclosure of your Personal Information to any third party for marketing purposes, please e-mail us at: cs@draftstreet.com. Please be advised that where California State residents opt-out from permitting their Personal Information to be shared, such individuals may still receive selected offers directly from us, in accordance with applicable law.<br /><br />

When you register on our Site, you hereby agree and acknowledge that your Personal Information will be placed into the DraftStreet™ database and that such Personal Information shall be shared with third parties for marketing purposes.
<h4>Personal Information that We Collect from You</h4>

By registering with, or providing Personal Information on, the Site, users allow us and our affiliates to make their Personal Information available to third parties in accordance with the terms of this Privacy Policy, and to otherwise provide marketing offers to such users. While on the Site, submitting an application to become a DraftStreet™ Member, registering for a Sweepstakes, registering to make a purchase from the DraftStreet™ Shop or otherwise utilizing the DraftStreet™ Services, we may collect “Personal Information” from you. For the purposes of this Privacy Policy, Personal Information shall mean individually identifiable information from or about an individual including, but not limited to, the individual’s: (a) full name; (b) e-mail address; (c) billing address; (d) daytime, evening and/or cellular telephone numbers; (e) credit card information; (f) at the individual’s option, his/her PayPal® account information; (g) date of birth; and/or (h) any other information requested on the applicable form. For purposes of this Privacy Policy, items (e) and (f) in the preceding sentence shall be referred to as “Sensitive Information.”<br /><br />

For purposes of this Privacy Policy, “PayPal” shall mean PayPal, Inc. Please be advised that DraftStreet™ is not in any way affiliated with PayPal, and the Site, DraftStreet™ Shop, Sweepstakes and DraftStreet™ Services are neither endorsed, nor sponsored, by PayPal. PayPal ® is a registered trademark of PayPal, Inc.
<h4>How We Use Personal Information</h4>

The Personal Information that you submit to us remains your property, but by submitting such information to us you grant us the right, subject to applicable state and federal law, to use the Personal Information (other than Sensitive Information) for marketing purposes. Without limiting the generality of the foregoing, we reserve the right to share, sell, rent, lease and/or otherwise distribute any and all Personal Information (other than Sensitive Information) with/to any third-party for any and all uses permitted by this Privacy Policy and applicable law. These third parties may include, but shall not be limited to: (a) providers of direct marketing services and applications, including lookup and reference, data enhancement, suppression and validation; (b) e-mail marketers; (c) telemarketers (where and to the extent permitted by applicable law); (d) SMS text-based marketers (where and to the extent permitted by applicable law); and (e) direct marketers.
<br /><br />
We may also use your Personal Information for any purpose related to the Site, DraftStreet™ Shop, Sweepstakes and/or DraftStreet™ Services, and/or marketing and survey purpose, on our own behalf and on behalf of our affiliates and subsidiaries. We may disclose Personal Information to third-party agents and independent contractors that help us create and/or operate promotions and surveys. You agree that we may contact you at any time with updates and/or any other information that we may deem appropriate for you to receive in connection with your continued use of the Site, DraftStreet™ Shop, Sweepstakes and/or DraftStreet™ Services.<br /><br />

We are able to offer the Site, DraftStreet™ Shop, Sweepstakes and DraftStreet™ Services to you, in part, based on your willingness to be reached by our third-party advertisers. Your Personal Information (other than Sensitive Information) will be shared with advertisers on an aggregate and non-aggregate basis. We also use contact information from your Personal Information to send you information about the Site, DraftStreet™ Shop, Sweepstakes and DraftStreet™ Services, our other offerings and to keep you informed of our other products and services that may be of interest to you and to contact you about your billing account status. Please keep in mind, though, that we do not control, and are not responsible for, the practices of our third-party advertisers. If you wish to stop receiving future communications from us and/or third party advertisers, or if you wish to prevent the transfer and/or sale of your Personal Information to third parties (subject to restrictions contained in applicable state and federal law), please see the Removal of Your Information/Opting Out section below.<br /><br />

By submitting your Personal Information at the Site, you agree to receive e-mail marketing from us and our third-party advertisers. In addition, where you submit a telephone number at the Site, if ever, you agree that such act constitutes a purchase, an inquiry and/or an application for purposes of the Amended Telemarketing Sales Rule, 16 CFR §310 et seq. (the “ATSR”). With respect to the ATSR, and notwithstanding that your telephone number may be listed on the Federal Trade Commission’s Do-Not-Call List, we retain the right to contact you via telemarketing in accordance with the terms of the ATSR.<br /><br />

In addition, where you submit a mobile telephone number at the Site, if ever, you agree to receive mobile marketing including, but not limited to, SMS text-message based marketing, from us and our third party advertisers and marketers (where, and to the extent that, such marketing is permitted by applicable law). As such, notwithstanding that your mobile telephone number may be listed on state and/or federal Do-Not-Call registries, we retain the right to contact you via SMS text-message based marketing in accordance with applicable state and federal law. Further, you agree that we reserve the right to share, sell, rent, lease and/or otherwise distribute your mobile telephone and other mobile data with/to any third-party for any and all non-marketing uses permitted by this Privacy Policy and applicable law.<br /><br />

We reserve the right to release current or past Personal Information: (i) in the event that we believe that the Site, DraftStreet™ Shop, Sweepstakes and/or DraftStreet™ Services is/are being or has/have been used in violation of this Privacy Policy, the Terms and Conditions, any Contest Rules (as defined in the Terms and Conditions) or to commit unlawful acts; (ii) if the information is subpoenaed or otherwise requested pursuant to a valid legal proceeding; or (iii) if we are sold or acquired. Moreover, you hereby consent to the disclosure of any record or communication to any third-party when we, in our sole discretion, determine the disclosure to be appropriate including, without limitation, sharing your e-mail address with third-parties for suppression purposes in compliance with the CAN-SPAM Act of 2003, as amended from time to time. Users should also be aware that courts of equity, such as U.S. Bankruptcy Courts, might have the authority under certain circumstances to permit Personal Information to be shared or transferred to third parties without permission.<br /><br />

If you enroll in certain DraftStreet™ Services or enter certain Sweepstakes, we may use your Personal Information to send you a welcome email that may confirm your user name and password. In addition, we may send you electronic newsletters, contact you about the Site, DraftStreet™ Shop, Sweepstakes and/or DraftStreet™ Services and other products, services, information and news that may be of interest to you, and provide you with targeted feedback. In addition, if you identify yourself to us by sending us an email with questions or comments, we may use such information (including Personal Information) to respond to your questions or comments, and we may file your questions or comments for future reference.
<br /><br />
We may also supplement the information that we collect with information from other sources to assist us in evaluating and improving the Site and Services, and to determine your preferences so that we can tailor the Site and Services to your needs. </div>
				    </div>

			      </div>
			      <div class="right_c">
				    <h3><span>Welcome</span> Łukasz</h3>
				    <div class="summary_box clearfix">
					  <div class="sum_in"><p>Account balance</p> <span>5010$</span></div>
					  <div class="sum_in"><p>In play</p> <span>5010$</span></div>
					  <div class="sum_in">
						<ul>
						      <li><a href="#">History</a></li>
						      <li><a href="#">Deposites</a></li>
						      <li><a href="#">Withdraval</a></li>
						      <li><a href="#">Profile</a></li>
						</ul>
					  </div>
				    </div>
				    <h4>Your Leagues</h4>
				    <div class="leagues_box clearfix">
					  <div class="lea_in"><p>masterleague 200</p> <span>422$</span></div>
					  <div class="lea_in"><p>masterleague 200</p> <span>422$</span></div>
					  <div class="lea_in"><p>masterleague 200</p> <span>422$</span></div>
				    </div>
				    <h4>Bonus</h4>
				    <a href="#">
					  <img src="common/images/_temp/banner_bonus.png" alt="" />
				    </a>
			      </div>
			</div>
			
     
<?php include('template/footer.php'); ?>