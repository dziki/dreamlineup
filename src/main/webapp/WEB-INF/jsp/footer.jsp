<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:useBean id="date" class="java.util.Date" />
<footer id="footer">
			      <div class="info_site">
				    <div class="rules">
					  
<spring:message code="footer.full_rules"/> <a href="<c:url value='/rules.html' />"><spring:message code="click-here"/></a><br />
<!-- spring:message code="footer.licence"/><br / -->
<a href="<c:url value='/terms.html' />"><spring:message code="footer.policy"/> </a>
				    </div>
				    <div class="foot_menu">
					  <h5><a href="<c:url value='/'/>"><img id="logo_mini" src="<c:url value='/common/images/logo.png'/>" /></a></h5>
					  <nav id="mini_menu">
						<ul>
					  <li><a href="<c:url value='/index.html' />"><spring:message code="menu.home" /></a></li>
					  <li><a href="<c:url value='/rules.html' />"><spring:message code="menu.rules" /></a></li>
					  <li><a href="<c:url value='/terms.html' />"><spring:message code="menu.terms" /></a></li>
					  <li><a href="<c:url value='/leagues/list.html' />"><spring:message code="menu.leagues" /></a></li>
					  <li><a href="<c:url value='/players/list.html' />"><spring:message code="menu.players" /></a></li>
					  <li><a href="<c:url value='/blog.html' />"><spring:message code="menu.blog" /></a></li>
					  <li><a href="<c:url value='/contact.html' />"><spring:message code="menu.contact" /></a></li>
						</ul>
					  </nav>
				    </div>
			      </div>
			      <div class="copy clearfix">
				    <!--div class="des">
						<p>Design by</p><a href="#"><img src="common/images/kdn_logo.png" alt="" /></a>
				    </div-->
				    <div class="cop">
						<p><spring:message code="footer.copyright" /> 2012-<fmt:formatDate value="${date}" pattern="yyyy" /></p>
				    </div>
			      </div>
			</footer>
		  </div>
	    </div>
      </div>
      

      
      <div class="player_toottip">
	    <div class="pt_top clearfix"></div>
	    <div class="pt_cont clearfix"></div>
	    <div class="pt_foot clearfix"></div>
      </div>
      <div class="modal_window none" id="alert_m">
	    <div class="modal_head">
		  <p id="alert_title"></p>
		  <a href="#" class="close_m"></a>
	    </div>
	    <div id="alert_body" class="modal_list">
	    	
	    </div>
	    	<a class="short_button close_m" href="javascript:;"><p>OK</p></a>

      </div>
            
</body>
</html>
