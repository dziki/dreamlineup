<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../top.jsp" />

			<div class="main_content">
			      <div class="left_c">


				    <h2 class="head_title"><b><spring:message code="players.playerStats" text="team" /></b> <c:out value="${player.username}"/></h2>
				    
				    
				    <div class="page_box">

				    <div class="players_box">
					    <div class="p_box" style="width: 100%;">


<table id="teamStats" class="tablesorter active-rows" style="width: 643px;">
<thead><tr>
<th class="nr"></th>
<th><spring:message code="leagues.league" text="Liga" /></th>
<th><spring:message code="lobby.points" text="Points" /></th>
<th><spring:message code="draft.position" text="Pos" /></th>
<th><spring:message code="finance.prize" text="Prize" /></th>
</tr></thead><tbody>
<c:forEach var="item" items="${player.virtualTeams}" varStatus="status">
<tr>
<td class="nr"><p><span><c:out value="${status.count}"/></span></p></td>
<td><a href="<c:url value="/leagues/${item.virtualLeague.id}/details.html" />"><c:out value="${item.virtualLeague.title}"/></a></td>
<td><c:out value="${item.points}"/></td>
<td><c:out value="${item.place}"/></td>
<td><fmt:formatNumber value='${item.prize}' currencySymbol='DP' type='currency'/></td>
</tr>
</c:forEach>
</tbody>
<!-- tfoot style="border-top: 2px solid #3b333f;">
<tr>
<td></td>
<td></td>
<td></td>
<td><spring:message code="team.total" text="Total" /></td>
<td><fmt:formatNumber value='${vteam.teamPrice/1000000}' type="number" minFractionDigits="2" maxFractionDigits="2"  /> mln</td>
</tr> -->
</tfoot>
</table>
				    </div>
				    </div>
				    </div>
				  </div>


<jsp:include page="../right.jsp" />
			      </div>
<script>
$(document).ready(function() 
	    { 
	        $(".tablesorter").tablesorter( {cssHeader: "vl_header", sortList: [[5,1]] } ); 
	    } 
	); 
</script>
<jsp:include page="../footer.jsp" />
