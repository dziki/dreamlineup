<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />

			<div class="main_content">
			      <div class="left_c">
<h2 class="head_title"><spring:message code="index.activeLeagues" text="Active leagues" /></h2>
					<div class="active_leagues clearfix">
					  <table class=" tablesorter" id="virtualLeagues">
					  <thead>
						<tr>
<th class="corner1"><spring:message code="leagues.id" text="id" /></th>
<th><spring:message code="leagues.name" text="name" /></th>
<th><spring:message code="leagues.buyIn" text="fee" /></th>
<th><spring:message code="leagues.numOfPlayers" text="num of players" /></th>
<th><spring:message code="leagues.duration" text="duration" /></th>
<!-- th><spring:message code="leagues.starts" text="starts" /></th -->
<th><spring:message code="leagues.closesIn" text="closes in" /></th>
<th class="corner2"><spring:message code="leagues.prizePool" text="prize pool" /></th>
						</tr>
					  </thead>
					  <tbody>
<c:forEach var="league" items="${leagues}">
<tr class="row_one <c:if test="${(fn:length(league.bonuses) > 0)}">sponsored_league</c:if>" onclick="document.location = '<c:url value="/leagues/${league.id}/details.html"/>'">
<td style="width: 60px" class="corner1"><span><c:out value="${league.id}"/></span></td>
<td style="width: 80px; padding: 1px;"><c:out value="${league.title}"/></td>
<td style="width: 60px"><fmt:formatNumber value='${league.buyInPlusRake}' currencySymbol='DP' type='currency'/></td>
<td style="width: 80px"><c:out value="${league.curNumOfPlayers}"/> /
<c:if test="${!(league.numOfPlayers == null)}">${league.numOfPlayers}</c:if>
<c:if test="${league.numOfPlayers == null}">&#8734;</c:if></td>
<td style="width: 70px">
<c:out value="${league.duration}"/>
<c:if test="${league.duration == 1}"><spring:message code="leagues.rounds_singular" text="round" /></c:if>
<c:if test="${league.duration > 1}"><spring:message code="leagues.rounds_plural" text="rounds" /></c:if>
</td>
<!-- td> fmt:formatDate value="${league.registrationDeadline}" type="both" timeStyle="short" dateStyle="short" /></td -->
<c:if test="${(league.status == 0)}">
	<c:if test="${(league.numOfPlayers == league.curNumOfPlayers)}">
		<td style="width: 100px"><spring:message code="leagues.full" /></td>
	</c:if>
	<c:if test="${!(league.numOfPlayers == league.curNumOfPlayers)}">
		<td style="width: 100px" class="countDown"><fmt:formatDate value="${league.registrationDeadline}" type="both" timeStyle="short" dateStyle="short" /></td>
	</c:if>
</c:if>
<c:if test="${(league.status == 1)}">
	<td style="width: 100px"><spring:message code="leagues.finished" /></td>
</c:if>
<td style="width: 100px" class="corner2"><span>
<c:if test="${(fn:length(league.bonuses) == 0)}">
<fmt:formatNumber value='${league.prizePool}' currencySymbol='DP' type='currency'/>
</c:if>
<c:if test="${(fn:length(league.bonuses) > 0)}">
<c:out value='${league.bonuses[0].description}' />
</c:if>
</span></td>
</tr>
</c:forEach>
					  </tbody>
					  </table>
<form action="<c:url value="/leagues/create.html"/>"><input style="width: 150px; margin-top: 10px;" name="submit" type="submit" value="<spring:message code="index.create_new" />" /></form>
					</div>
			      </div>
<jsp:include page="right.jsp" />
			</div>
<script>
// add parser through the tablesorter addParser method 
function parseStatus(s) { 
	if (s == i18nstrings['leagues.full'])
		return 24*360;
   	if (s == i18nstrings['leagues.started'])
   		return 2*24*360;
   	if (s == i18nstrings['leagues.finished'])
   		return 3*24*360;
    	
    	var P = s.split(" "); 
    	var d = 0;
    	var i = 0;
    	if (P.length == 2){
    		i = 1;
    		d = parseInt(P[0].split("d")[0]) * 24;
    	}
    	d += parseInt(P[i].split(":")[0]);
    	
    	
        return d;
    };

    dp_parser = function(s) { 
    	s = $.trim(s);
    	if (s.substring(0,2) != "DP")
    		return 100000.0;
    	s = s.replace(/\xA0/g,"");
        var P = s.split(' ').slice(1); 
        var f = P.join("");
        return parseFloat(f);
    };

$.tablesorter.addParser({ 
    // set a unique id 
    id: 'DP', 
    is: function(s) { return false; }, 
    format: dp_parser, 
    type: 'numeric' 
}); 
$.tablesorter.addParser({ 
    // set a unique id 
    id: 'players', 
    is: function(s) { return false; }, 
    format: function(s) { 
        var P = s.split(" "); 
        return parseInt(P[0]);
    }, 
    type: 'numeric' 
}); 

$.tablesorter.addParser({ 
    // set a unique id 
    id: 'status', 
    is: function(s) { return false; }, 
    format: parseStatus, 
    type: 'numeric' 
}); 

$(document).ready(function() 
	    { 
	        $(".tablesorter").tablesorter( {cssHeader: "vl_header", headers: { 
                2: {sorter:'DP' },
                3: {sorter:'players' },
                5: {sorter:'status' },
                6: {sorter:'DP' },
            }, sortList: [[5,0],[1,1]] } ); 
	    } 
	); 
	
//Change the selector if needed
var $table = $('#virtualLeagues'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Get the tbody columns width array
colWidth = $bodyCells.map(function() {
    return $(this).width();
}).get();

// Set the width of thead columns
$table.find('thead tr').children().each(function(i, v) {
    $(v).width(colWidth[i]);
});    
$(window).resize(function() {
    /* Same as before */ 
}).resize(); // Trigger the resize handler once the script runs

</script>

<jsp:include page="footer.jsp" />
