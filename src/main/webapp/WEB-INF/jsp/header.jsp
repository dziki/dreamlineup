<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Fantasy Football</title>

<meta name="description" content="<spring:message code='header.description' javaScriptEscape='true' />" />
<meta name="keywords" content="Fantasy Football" />

<!--style-->

<link rel="stylesheet" href="<c:url value='/common/css/style-1.css'/>" type="text/css" media="screen"/>
<link href="<c:url value='/common/css/jquery-ui.css'/>" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="<c:url value='/common/images/favicon.ico'/>">
<link href="<c:url value='/common/css/lightbox/lightbox.css'/>" rel="stylesheet" />
<!--style end-->

<!--javascripts-->

<script src="<c:url value='/common/js/jquery-1.7.1.min.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/jquery.easing.1.3.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/jquery.cycle.min.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/jquery-ui-1.8.13.min.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/jquery.tablesorter.min.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/brs.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/custom.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/countdown/jquery.countdown.js'/>" type="text/javascript"></script>
<script src="<c:url value='/common/js/lightbox-2.6.min.js'/>" type="text/javascript"></script>
<script src=""></script>
<script>
var SERVER_PREFIX = "<c:url value='/' />";
var DEADLINE_MINUTES = 0;
var LAST_CHANGE_MINUTES = 5;

var i18nstrings = new Array();
var i18nposition = new Array();

i18nstrings['leagues.started'] = "<spring:message code='leagues.started' javaScriptEscape='true' />";
i18nstrings['leagues.finished'] = "<spring:message code='leagues.finished' />";
i18nstrings['leagues.reg_closed'] = "<spring:message code='leagues.reg_closed' javaScriptEscape='true' />";
i18nstrings['leagues.full'] = "<spring:message code='leagues.full' />";

i18nstrings['draft.cant_change_formation'] = "<spring:message code='draft.cant_change_formation' javaScriptEscape='true' />";
i18nstrings['draft.cant_fire'] = "<spring:message code='draft.cant_fire' javaScriptEscape='true' />";
i18nstrings['draft.cant_hire'] = "<spring:message code='draft.cant_hire' javaScriptEscape='true' />";
i18nstrings['draft.add'] = "<spring:message code='draft.add' javaScriptEscape='true' />";
i18nstrings['draft.complete'] = "<spring:message code='draft.complete' javaScriptEscape='true' />";
i18nstrings['draft.incomplete'] = "<spring:message code='draft.incomplete' javaScriptEscape='true' />";
i18nstrings['draft.in_play'] = "<spring:message code='draft.in_play' javaScriptEscape='true' />";

i18nstrings['auth.login_error'] = "<spring:message code='auth.login_error' javaScriptEscape='true' />";
i18nstrings['error'] = "<spring:message code='error' javaScriptEscape='true' />";

i18nposition['G'] = "<spring:message code='position.G' javaScriptEscape='true' />";
i18nposition['D'] = "<spring:message code='position.D' javaScriptEscape='true' />";
i18nposition['S'] = "<spring:message code='position.S' javaScriptEscape='true' />";
i18nposition['M'] = "<spring:message code='position.M' javaScriptEscape='true' />";

</script>

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
      
<!--javascripts end-->   

</head>

<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44998901-1', 'dreamlineup.com');
  ga('send', 'pageview');
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrapper_wide_bg">
      <div id="wrapper_insider_bg">
	    <div id="wrapper">
