<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />
			<div class="main_content">
			      <div class="left_c">
				    <div class="slider_show">
					  <div class="sl_box">
						<a href="<c:if test="${currentPlayer== null}"><c:url value="/auth/registration.html"/></c:if><c:if test="${!(currentPlayer == null)}"><c:url value="/leagues/sponsored.html"/></c:if>"><img src="common/images/slider_1.jpg" /></a>
						<a href="<c:if test="${currentPlayer== null}"><c:url value="/auth/registration.html"/></c:if><c:if test="${!(currentPlayer == null)}"><c:url value="/leagues/sponsored.html"/></c:if>"><img src="common/images/slider_2.jpg" /></a>
						<a href="<c:if test="${currentPlayer== null}"><c:url value="/auth/registration.html"/></c:if><c:if test="${!(currentPlayer == null)}"><c:url value="/leagues/sponsored.html"/></c:if>"><img src="common/images/slider_3.jpg" /></a>
					  </div>
					  <div class="sl_navi">
						<span class="prev_ar"></span>
						<span class="next_ar"></span>
					  </div>
				    </div>
				    <div class="active_leagues clearfix">
					  <div class="al_top clearfix">
						  <h3><spring:message code="index.activeLeagues" /></h3>
						  <a href="<c:url value='/leagues/list.html' />"><spring:message code="see_all" /></a>
					  </div>
					  <table class="scrollable_tbl tablesorter" id="virtualLeagues">
					  <thead>
						<tr>
<th class="corner1"><spring:message code="leagues.id" text="id" /></th>
<th><spring:message code="leagues.name" text="name" /></th>
<th><spring:message code="leagues.buyIn" text="fee" /></th>
<th><spring:message code="leagues.numOfPlayers" text="num of players" /></th>
<th><spring:message code="leagues.duration" text="duration" /></th>
<!-- th><spring:message code="leagues.starts" text="starts" /></th -->
<th><spring:message code="leagues.closesIn" text="closes in" /></th>
<th class="corner2"><spring:message code="leagues.prizePool" text="prize pool" /></th>
						</tr>
					  </thead>
					  <tbody>
<c:forEach var="league" items="${leagues}">
<tr class="row_one <c:if test="${(fn:length(league.bonuses) > 0)}">sponsored_league</c:if>" onclick="document.location = '<c:url value="/leagues/${league.id}/details.html"/>'">
<td style="width: 60px" class="corner1"><span><c:out value="${league.id}"/></span></td>
<td style="width: 80px"><c:out value="${league.title}"/></td>
<td style="width: 60px"><fmt:formatNumber value='${league.buyInPlusRake}' currencySymbol='DP' type='currency'/></td>
<td style="width: 80px"><c:out value="${league.curNumOfPlayers}"/> /
<c:if test="${!(league.numOfPlayers == null)}">${league.numOfPlayers}</c:if>
<c:if test="${league.numOfPlayers == null}">&#8734;</c:if></td>
<td style="width: 70px">
<c:out value="${league.duration}"/>
<c:if test="${league.duration == 1}"><spring:message code="leagues.rounds_singular" text="round" /></c:if>
<c:if test="${league.duration > 1}"><spring:message code="leagues.rounds_plural" text="rounds" /></c:if>
</td>
<!-- td> fmt:formatDate value="${league.registrationDeadline}" type="both" timeStyle="short" dateStyle="short" /></td -->
<c:if test="${(league.status == 0)}">
	<c:if test="${(league.numOfPlayers == league.curNumOfPlayers)}">
		<td style="width: 100px"><spring:message code="leagues.full" /></td>
	</c:if>
	<c:if test="${!(league.numOfPlayers == league.curNumOfPlayers)}">
		<td style="width: 100px" class="countDown"><fmt:formatDate value="${league.registrationDeadline}" type="both" timeStyle="short" dateStyle="short" /></td>
	</c:if>
</c:if>
<c:if test="${(league.status == 1)}">
	<td style="width: 100px"><spring:message code="leagues.finished" /></td>
</c:if>
<td style="width: 100px" class="corner2"><span>
<c:if test="${(fn:length(league.bonuses) == 0)}">
<fmt:formatNumber value='${league.prizePool}' currencySymbol='DP' type='currency'/>
</c:if>
<c:if test="${(fn:length(league.bonuses) > 0)}">
<c:out value='${league.bonuses[0].description}' />
</c:if>
</span></td>
</tr>
</c:forEach>
					  </tbody>
					  </table>
					  
					  <form action="<c:url value="/leagues/create.html"/>"><input style="width: 110px; font-size: 18px; height: 27px; margin-top: 20px;" name="submit" type="submit" value="<spring:message code="index.create_new" />" /></form>
				    </div>
			      </div>
			      <div class="right_c">
				<jsp:include page="auth.jsp" />

					<h4><spring:message code="index.best_players" /></h4>
				   
				    <div class="p_box clearfix">
				    <table class="summary_players">
					  <tr>
						<td> </td>
						<td> </td>
						<td><p class="win_row"><spring:message code="index.cashed" /></p></td>
						<td><p class="win_row"><spring:message code="index.total" /></p></td>
					  </tr>
<c:forEach var="player" items="${players}" varStatus="status">
						      <tr>
							    <td class="nr"><p><c:out value="${status.index+1}" /></p></td>
							    <td class="name"><a href="<c:url value="/players/${player.id}/stats.html"/>"><c:out value="${player.username}"/></a></td>
							    <td class="gr"><p><c:out value="${player.cashed}"/></p></td>
							    <td class="gr"><p>
							    <fmt:formatNumber value='${player.totalWin}' currencySymbol='DP' type='currency'/>
							    </p></td>
						      </tr>
</c:forEach>
						</table>
				    </div>
			      <div><a href="<c:url value="/leagues/sponsored.html"/>"><img style="padding-top: 10px;" src="<c:url value='/common/images/banner_bonus.png'/>" alt="Bonus" /></a></div>
			      </div>
			      
			      <!-- div class="fill_bg clearfix">
				    <div class="left_c2">
					  <h3 class="green_head"><spring:message code="index.rules"/></h3>

					  <p><spring:message code="index.rules_short" /></p>
					  <a href="<c:url value='/rules.html' />" class="read_more"><spring:message code="read_more"/></a>
				    </div>
				    <div class="right_c">
					  <h3 class="green_head"><spring:message code="index.videos"/></h3>
					  <div class="video_show">
						<object width="316" height="196" name="player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="1mdku0jfc0s"><param value="http://c1304512.r12.cf0.rackcdn.com/mediaplayer/player.swf" name="movie"><param value="true" name="allowfullscreen"><param value="always" name="allowscriptaccess"><param value="transparent" name="wmode"><param value="file=http://c1304512.r12.cf0.rackcdn.com/videos/DraftStreetFantasySports.flv&amp;image=http://c1304512.r12.cf0.rackcdn.com/vids/DraftStreetFantasySports.png" name="flashvars"><embed width="316" height="196" flashvars="file=http://c1304512.r12.cf0.rackcdn.com/videos/DraftStreetFantasySports.flv&amp;image=http://c1304512.r12.cf0.rackcdn.com//vids/DraftStreetFantasySports.png" wmode="transparent" allowfullscreen="true" allowscriptaccess="always" src="http://c1304512.r12.cf0.rackcdn.com/mediaplayer/player.swf" name="player2" id="player2" type="application/x-shockwave-flash"></object>
					  </div>
				    </div>
			      </div> -->
			</div>
<script>
// add parser through the tablesorter addParser method 
function parseStatus(s) { 
	if (s == i18nstrings['leagues.full'])
		return 24*360;
   	if (s == i18nstrings['leagues.started'])
   		return 2*24*360;
   	if (s == i18nstrings['leagues.finished'])
   		return 3*24*360;
    	
    	var P = s.split(" "); 
    	var d = 0;
    	var i = 0;
    	if (P.length == 2){
    		i = 1;
    		d = parseInt(P[0].split("d")[0]) * 24;
    	}
    	d += parseInt(P[i].split(":")[0]);
    	
    	
        return d;
    };

    dp_parser = function(s) { 
    	s = $.trim(s);
    	if (s.substring(0,2) != "DP")
    		return 100000.0;
    	s = s.replace(/\xA0/g,"");
        var P = s.split(' ').slice(1); 
        var f = P.join("");
        return parseFloat(f);
    };

$.tablesorter.addParser({ 
    // set a unique id 
    id: 'DP', 
    is: function(s) { return false; }, 
    format: dp_parser, 
    type: 'numeric' 
}); 
$.tablesorter.addParser({ 
    // set a unique id 
    id: 'players', 
    is: function(s) { return false; }, 
    format: function(s) { 
        var P = s.split(" "); 
        return parseInt(P[0]);
    }, 
    type: 'numeric' 
}); 

$.tablesorter.addParser({ 
    // set a unique id 
    id: 'status', 
    is: function(s) { return false; }, 
    format: parseStatus, 
    type: 'numeric' 
}); 

$(document).ready(function() 
	    { 
	        $(".tablesorter").tablesorter( {cssHeader: "vl_header", headers: { 
                2: {sorter:'DP' },
                3: {sorter:'players' },
                5: {sorter:'status' },
                6: {sorter:'DP' },
            }, sortList: [[5,0],[1,1]] } ); 
	    } 
	); 
	
//Change the selector if needed
var $table = $('#virtualLeagues'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Get the tbody columns width array
colWidth = $bodyCells.map(function() {
    return $(this).width();
}).get();

// Set the width of thead columns
$table.find('thead tr').children().each(function(i, v) {
    $(v).width(colWidth[i]);
});    
$(window).resize(function() {
    /* Same as before */ 
}).resize(); // Trigger the resize handler once the script runs

</script>
			
<jsp:include page="footer.jsp" />

