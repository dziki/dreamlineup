<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />

			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><b>Kontakt</b></h2>
				    <div class="page_box">
					  <div class="normal_page">
					  Właścicielem i administratorem serwisu dreamlineup.com jest firma:
					  
<div style="padding: 10px"><strong>DREAM LINEUP LTD</strong><br />
Suite 18354<br />
Lower Ground Floor<br />
145-157 St John Street<br />
London<br />
EC1V 4PW<br />
<br />
E-mail: <a class="contact_email" href="mailto:biuro@dreamlineup.com">biuro@dreamlineup.com</a>
</div>
				    </div>
				    </div>
			      </div>
<jsp:include page="right.jsp" />
			</div>

<jsp:include page="footer.jsp" />
