<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />

			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><spring:message code="players.best" text="Best players" /></h2>
				    <div class="page_box">

				    <div class="players_box">
					    <div class="p_box" style="width: 100%;">

				    <table id="teamStats" class=" active-rows">
					  <thead><tr>
						<th><spring:message code="players.nr" text="nr" /></p></th>
						<th><spring:message code="players.name" text="name" /></p></th>
						<th><p style="width: 80px"><spring:message code="players.wins" text="wins" /></th>
						<!-- th><p class="win_row" style="width: 65px"><spring:message code="players.cashed" text="cashed" /></p></th-->
						<th><p style="width: 55px"><spring:message code="players.total" text="total" /></p></th>
					  </tr></thead><tbody>
<c:forEach var="player" items="${players}" varStatus="status">
						      <tr>
							    <td class="nr"><p><span><c:out value="${status.index+1}" /></span></p></td>
							    <td class="name"><a href="<c:url value="/players/${player.id}/stats.html"/>"><c:out value="${player.username}"/></a></td>
							    <!-- td><p><c:out value="${player.wins}"/></p></td -->
							    <td class="gr"><p><c:out value="${player.cashed}"/></p></td>
							    <td class="gr"><p><fmt:formatNumber value='${player.totalWin}' currencySymbol='DP' type='currency'/></p></td>
						      </tr>
</c:forEach></tbody>
					</table>
				    </div>

				    </div>
				    </div>

			      </div>
<jsp:include page="right.jsp" />
			</div>

<jsp:include page="footer.jsp" />
