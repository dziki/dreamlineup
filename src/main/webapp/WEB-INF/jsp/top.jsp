<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:if test="${(currentPlayer==null)}">
	<a href="<c:url value='/auth/registration.html'/>" class="join_us"></a>
</c:if>
	<div class="top">
			      <div id="motto"><spring:message code="header.motto" /></div><!--img src="<c:url value='/common/images/motto.png'/>" alt="" /-->
			      <div class="account_box">
			      <!-- >a href="?lang=pl"><img style="position: relative; top: -3px;" src="<c:url value='/common/images/flag-pl.gif'/>"></a>
			      <a href="?lang=en"><img style="position: relative; top: -3px;margin-right: 30px;" src="<c:url value='/common/images/flag-en.gif'/>"></a -->
			      
<c:if test="${(currentPlayer==null)}">
				    <a href="<c:url value='/auth/login.html' />" class="login"><p><spring:message code="auth.login" /></p></a>
</c:if>
<c:if test="${!(currentPlayer==null)}">
<!-- 				    <a href="<c:url value='/finance/history.html' />" class="account"><p>account</p><p class="ammount"><fmt:formatNumber value='0' currencySymbol='DP' type='currency'/></p></a> -->
				    <a style="font-size:13px;" href="<c:url value='/j_spring_security_logout' />" class="login"><p><spring:message code="auth.logout" /></p></a>
</c:if>
			      </div>
			</div>
			<header class="header">
			      <h1><a href="<c:url value='/index.html'/>"><img id="logo" src="<c:url value='/common/images/logo.png'/>" alt="Fantasy Football" /></a></h1>
			      <nav id="main_menu">
				    <ul>
					  <li class="active"><a href="<c:url value='/index.html' />"><spring:message code="menu.home" /> <span></span></a></li>
					  <li><a href="<c:url value='/rules.html' />"><spring:message code="menu.rules" /> <span></span></a></li>
					  <li><a href="<c:url value='/leagues/list.html' />"><spring:message code="menu.leagues" /> <span></span></a></li>
					  <li><a href="<c:url value='/players/list.html' />"><spring:message code="menu.players" /> <span></span></a></li>
					  <li><a href="<c:url value='/blog.html' />"><spring:message code="menu.blog" /> <span></span></a></li>
				    </ul>
			      </nav>
			</header>
