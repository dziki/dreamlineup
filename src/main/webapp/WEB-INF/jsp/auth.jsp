<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<sec:authorize access="hasRole('ROLE_USER')">
				    <h3><span><spring:message code="auth.welcome" /></span> ${currentPlayer.firstName}</h3>
				    <div class="summary_box clearfix">
					  <div class="sum_in"><p><spring:message code="auth.balance" /></p> <span><fmt:formatNumber value='${accountBalance}' currencySymbol='DP' type='currency'/></span></div>
					  <div class="sum_in"><p><spring:message code="auth.inplay" /></p> <span><fmt:formatNumber value='${inPlayBalance}' currencySymbol='DP' type='currency'/></span></div>
					  <div class="sum_in">
						<ul>
<li><a href="<c:url value='/finance/history.html' />"><spring:message code="auth.history" text="History" /></a></li>
<li><a href="<c:url value='/auth/edit.html' />"><spring:message code="auth.profile" text="Profile" /></a></li>
<!-- li><a href="<c:url value='/finance/deposit.html' />"><spring:message code="auth.deposit" text="Deposit" /></a></li>
<li><a href="<c:url value='/finance/withdrawal.html' />"><spring:message code="auth.withdrawal" text="Withdrawal" /></a></li -->
<li><a href="<c:url value='/j_spring_security_logout' />"><spring:message code="auth.logout" text="Logout" /></a></li>
						</ul>
					  </div>
				    </div>
				    <h4><spring:message code="auth.yourleagues" /></h4>
				    <div class="leagues_box clearfix">
<c:forEach var="vlp" items="${virtualTeams}">
<div class="lea_in" onclick="document.location = '<c:url value="/leagues/${vlp.virtualLeague.id}/details.html"/>'"><p><c:out value="${vlp.virtualLeague.title}"/></p>

<span><a class="<c:if test="${(vlp.valid)}">lobby_mini_but</c:if><c:if test="${!(vlp.valid)}">lobby_mini_but_invalid</c:if>" href="<c:url value="/draftroom/${vlp.id}.html"/>"><spring:message code="lobby.yourTeam" /></a></span>
<span><fmt:formatNumber value='${vlp.virtualLeague.prizePool}' currencySymbol='DP' type='currency'/></span>
</div>
</c:forEach>
				    </div>
				    
				    <h4><spring:message code="auth.yourresults" /></h4>
				    <div class="leagues_box clearfix">
<c:forEach var="vlp" items="${pastVirtualTeams}">
<div class="lea_in" onclick="document.location = '<c:url value="/leagues/${vlp.virtualLeague.id}/details.html"/>'"><p><c:out value="${vlp.virtualLeague.title}"/></p>

<span><a class="<c:if test="${(vlp.valid)}">lobby_mini_but</c:if><c:if test="${!(vlp.valid)}">lobby_mini_but_invalid</c:if>" href="<c:url value="/draftroom/${vlp.id}.html"/>"><spring:message code="lobby.yourTeam" /></a></span>
<span><fmt:formatNumber value='${vlp.virtualLeague.prizePool}' currencySymbol='DP' type='currency'/></span>
</div>
</c:forEach>
<div style="text-align: center; margin-top: 5px" class="lea_in" onclick="document.location = '<c:url value="/players/${currentPlayer.id}/stats.html"/>'">
<spring:message code="see_all" />
</div>
				    </div>
				    
				    
				    
</sec:authorize>
<sec:authorize access="!hasRole('ROLE_USER')">

				    <h3><span><spring:message code="auth.login" /></span></h3>
				    <div class="summary_box clearfix">
					  <div class="sum_in">
	<form name='f' action="<c:url value='/j_spring_security_check' />"
		method='POST'>
 
		<table class="form_table">
			<tr>
				<td><spring:message code="auth.email" />:</td>
				<td><input style="width: 170px;" type='text' name='j_username' value=''>
				</td>
			</tr>
			<tr>
				<td><spring:message code="auth.password" />:</td>
				<td><input style="width: 170px;" type='password' name='j_password' />
				</td>
			</tr>
			<tr>
				<td colspan='2'><input style="width: 150px;" name="submit" type="submit"
					value="<spring:message code="auth.login" />" />
				</td>
			</tr>
		</table>
		<a style="font-size: 15px; color: #ffffff; text-decoration: underline" href="<c:url value="/auth/registration.html"/>"><spring:message code="auth.register" /></a><br /><br />

		<a style="color: #ffffff; text-decoration: underline" href="<c:url value="/auth/pass/remind.html"/>"><spring:message code="auth.password_recovery" /></a>
 
	</form>

</div>
				    </div>
</sec:authorize>
<h3><span><spring:message code="facebook.find_us" /></span></h3>
<div class="summary_box clearfix">
	<div class="sum_in" style="height: 232px;">
		<div class="fb-like-box" data-href="http://www.facebook.com/dreamlineup" data-width="292" data-height="222" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
	</div>
</div>