<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />

			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><b><spring:message code="lobby.lobby" /></b> - <c:out value="${vleague.title}"/></h2>
				    <div class="page_box">
					  <div class="list450">
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.first_match" />:</p>
							<div class="item_content"><p><fmt:formatDate value="${vleague.firstMatchDate}" type="both" timeStyle="short" dateStyle="short" /></p></div>
						  </div>
						  <c:if test="${vleague.status == 0}">
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.closes_in" />:</p>
							<div class="item_content"><p class="countDown"><fmt:formatDate value="${vleague.registrationDeadline}" type="both" timeStyle="short" dateStyle="short" /></p></div>
						  </div>
						  </c:if>
						  <c:if test="${vleague.status == 1}">
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="leagues.status" />:</p>
							<div class="item_content"><spring:message code="leagues.finished" /></p></div>
						  </div>
						  </c:if>
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.league_id" />:</p>
							<div class="item_content"><p><c:out value="${vleague.id}"/></p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.duration" />:</p>
							<div class="item_content"><p><c:out value="${vleague.duration}"/> <spring:message code="lobby.rounds" />, <spring:message code="lobby.starting_from" /> <c:out value="${vleague.startingRound}"/></p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.players" />:</p>
							<div class="item_content"><p><c:out value="${vleague.curNumOfPlayers}"/> / 
<c:if test="${!(vleague.numOfPlayers == null)}">${vleague.numOfPlayers}</c:if>
<c:if test="${vleague.numOfPlayers == null}">&#8734;</c:if></td>
</p></div>
						  </div>
						  <!--div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.first_match" />:</p>
							<div class="item_content"><p>SNG/weekly major</p></div>
						  </div-->
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.current_prizepool" />:</p>
							<div class="item_content"><p><b><fmt:formatNumber value='${vleague.prizePool}' currencySymbol='DP' type='currency'/></b></p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.buy-in" />:</p>
							<div class="item_content"><p><b><fmt:formatNumber value='${vleague.buyInPlusRake}' currencySymbol='DP' type='currency'/></b></p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label"><spring:message code="lobby.bonuses" />:</p>
							<div class="item_content">
							      <ul>
<c:forEach var="bonus" items="${vleague.bonuses}" varStatus="status">
								    <li><p><c:out value="${bonus.description}" /> za <c:out value="${bonus.place}" />. miejsce</p></li>
</c:forEach>
								    <li><p><spring:message code="lobby.stats_email" /></p></li>
							      </ul>
							</div>
						  </div>
					  </div>
					  <div class="boxes_right200">
						  <div class="br200_area">
<c:forEach var="vlp" items="${playerVirtualTeams}" varStatus="status">
	<a href="<c:url value="/draftroom/${vlp.id}.html"/>" class="<c:if test="${(vlp.valid)}">join</c:if><c:if test="${!(vlp.valid)}">invalid</c:if>"><spring:message code="lobby.yourTeam" /> <c:out value="${status.count}" />
	<c:if test="${!(vlp.valid)}"><spring:message code='draft.incomplete' javaScriptEscape='true' /></c:if>
	</a>
</c:forEach>
<c:if test="${playerVirtualTeams != null}">
							<br />
</c:if>

							<a href="#fixtures_m" class="fixtures modal"><spring:message code="lobby.show_fixtures" /></a>
							<a href="#prices_m" class="prices modal"><spring:message code="lobby.show_prize_distr" /></a>
<c:if test="${vleague.closed == false}">
		<a href="#join_m" class="join modal"><spring:message code="lobby.join_league" /></a>			
</c:if>
<div style="text-align: center; margin-top: 20px; margin-bottom: 5px;">
Wygraj ze znajomym
</div><div style="text-align: center; ">
<div class="fb-share-button" data-href="<c:url value="/leagues/${vleague.id}/details.html" />" data-type="button_count"></div>
</div>
						  </div>
					  </div>
					  <div class="players_box clearfix">
					    <h3 class="page_in_h">
<c:if test="${vleague.status == 0}">
					    <spring:message code="lobby.registered_till_now" />
</c:if>
<c:if test="${vleague.status == 1}">
					    <spring:message code="lobby.players" />
</c:if>
					    </h3>
<c:if test="${vleague.status == 0}">
<div class="p_box floatLeft" style="width:100%; padding-bottom: 2px;">
	<div style="padding: 7px;">
	<c:forEach var="entry" items="${vleague.virtualTeams}" varStatus="status">
		<a href="<c:url value="/players/${entry.player.id}/stats.html" />"><c:out value="${entry.player.username}"/></a>, 
	</c:forEach>      
</div>
</div>
</c:if>
<c:if test="${vleague.status == 1}">
					    <div class="p_box floatLeft">
						<table>

						      <tr>
							    <th class="nr"></th>
							    <th><spring:message code="lobby.player" /></th>
							    <th><spring:message code="lobby.points" /></th>
							    <th><spring:message code="lobby.payout" /></th>
						      </tr>
<c:forEach  begin="0" end="9" var="entry" items="${standings}" varStatus="status">
						      <tr>
							    <td class="nr"><p><span><c:out value="${1 + status.index}"/></span></p></td>
							    <td class="name"><p><a href="<c:url value="/lobby/team/${entry.id}/details.html" />"><c:out value="${entry.player.username}"/></a></p></td>
							    <td class="gr"><p><c:out value="${entry.points}"/></p></td>
							    <td class="gr"><c:if test="${vleague.status == 1}"><p><fmt:formatNumber value='${vleague.prizeDistribution[status.index]}' currencySymbol='DP' type='currency'/></p></c:if></td>
						      </tr>
</c:forEach>
						</table>
						</div>
						<div class="p_box floatRight">
						<table>

						      <tr>
							    <th class="nr"></th>
							    <th><spring:message code="lobby.player" /></th>
							    <th><spring:message code="lobby.points" /></th>
							    <th><spring:message code="lobby.payout" /></th>
						      </tr>
<c:forEach  begin="10" end="19" var="entry" items="${standings}" varStatus="status">
						      <tr>
							    <td class="nr"><p><span><c:out value="${1 + status.index}"/></span></p></td>
							    <td class="name"><p><a href="<c:url value="/lobby/team/${entry.id}/details.html" />"><c:out value="${entry.player.username}"/></a></p></td>
							    <td class="gr"><p><c:out value="${entry.points}"/></p></td>
							    <td class="gr"><c:if test="${vleague.status == 1}"><p><fmt:formatNumber value='${vleague.prizeDistribution[status.index]}' currencySymbol='DP' type='currency'/></p></c:if></td>
						      </tr>
</c:forEach>
						</table>
					    </div>
					    <div class="clearfix"></div>
					    <div style="padding: 8px; padding-left: 265px;">
					    <a href="<c:url value="/leagues/${vleague.id}/results.html"/>" class="fixtures"><spring:message code="lobby.all_results" /></a>
					    </div>
</c:if>
					  </div>
				    </div>
				  </div>


<jsp:include page="right.jsp" />
			      </div>

<jsp:include page="footer.jsp" />

      <div class="modal_window none" id="fixtures_m">
	    <div class="modal_head">
		  <p><spring:message code="lobby.fixtures" /></p>
		  <a href="#" class="close_m"></a>
	    </div>
<c:forEach var="round" items="${rounds}">
	<h5 class="md_h"><spring:message code="lobby.round" /> <c:out value="${round.number}"/></h5>
	    <div class="modal_list">
		  <table>
	<c:forEach var="match" items="${round.matches}">
			<tr><td style="text-align: right; font-weight: bold; width: 210px;"><c:out value="${match.homeTeam.name}"/></td> <td style="padding: 2px;">vs</td> <td style="font-weight: bold;"><c:out value="${match.awayTeam.name}"/></td></tr>
	</c:forEach>
		  </table>
	    </div>
</c:forEach>
      </div>

      <div class="modal_window none" id="prices_m">
	    <div class="modal_head">
		  <p><spring:message code="lobby.prizes" /></p>
		  <a href="#" class="close_m"></a>
	    </div>
	    <div class="modal_list">
		  <ul>
			<li><p><spring:message code="lobby.currentNumOfPlayers" />: </p><b><c:out value="${vleague.curNumOfPlayers}"/></b></li>
		  </ul>
	    </div>
	    <h5 class="md_h"><spring:message code="lobby.prize_distribution" /></h5>
	    <div class="modal_list">
		  <ul>
<c:forEach var="prize" items="${prizes}" varStatus="status">
			<li><p><c:out value="${1 + status.index}"/></p><b><c:out value="${prize}"/></b></li>
</c:forEach>
		  </ul>
	    </div>
      </div>
      
      <div class="modal_window none" id="join_m">
	    <div class="modal_head">
		  <p><spring:message code="are_you_sure" /></p>
		  <a href="#" class="close_m"></a>
	    </div>
	    <div class="modal_lisperformancet">
	    	<spring:message code="lobby.join_cost" /> <c:out value="${vleague.buyIn}"/> DP.
	    </div>
	    	<a class="short_button" href="<c:url value='/leagues/${vleague.id}/join.html'/>"><p><spring:message code="leagues.join" /></p></a>
      </div>

<script>
$(document).ready(function(){
$("#f<c:out value="${virtualteam.formation}"/>").click();

  if ("${modalError}".length){
	alertBox("${modalError}","${modalErrorDesc}");
  }
});
</script>
