<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../top.jsp" />

			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><spring:message code="auth.history" text="Account history" /></h2>

				    <div class="page_box">

				    <div class="players_box">
					    <div class="p_box" style="width: 100%;">


<table id="teamStats" style="width: 100%;">
<thead>
<tr>
<th ><p class="win_row"><spring:message code="history.date" text="Date" /></p></th>
<th ><p class="win_row"><spring:message code="history.amount" text="Amount" /></p></th>
<th ><p class="win_row"><spring:message code="history.type" text="Value" /></p></th>
<!-- th><p class="win_row"><spring:message code="history.status" text="Status" /></p></th -->
</tr><thead><tbody>
<c:forEach var="operation" items="${operations}">
<tr>
<td><p><fmt:formatDate value="${operation.date}" type="both" 
      pattern="MM-dd-yyyy" /></p></td>
<td><p><b><fmt:formatNumber value='${operation.value}' currencySymbol='' type='currency'/> DP</p></b></td>
<td><p><c:out value="${operation.typeOpDesc}"/></p></td>
</tr>
</c:forEach>
</tbody></table>
</div>

				    </div>
				    </div>

			      </div>
<jsp:include page="../right.jsp" />
			</div>





<jsp:include page="../footer.jsp" />
