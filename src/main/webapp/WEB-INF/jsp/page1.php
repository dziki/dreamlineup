<?php include('template/header.php'); ?>
<?php include('template/top.php'); ?>
			<div class="main_content">
			      <div class="normal_header clearfix">
					  <h2 class="head_title"><b>Drafting for the 10 man 15$ weekly special</b></h2>
					  <a href="#" class="back_to_lobby">back to the lobby</a>
			      </div>
			      <div class="full_box clearfix">
				    <div class="full_box_left">
					  
					  <div class="pick_players clearfix">
						<div class="pp_header clearfix">
						      <ul>
							    <li><a class="active_tab" href="#pick_field">pick from the field</a></li>
							    <li><a href="#pick_list">pick from the list</a></li>
						      </ul>
						</div>
						<div class="pp_content">
						      <div id="pick_field" class="clearfix p_co">
							    <div class="field_nav">
								  <p>select formation</p>
								  <ul>
									<li class="active_f" id="f1442">1-4-4-2</li>
									<li id="f1352">3-5-2-1</li>
									<li id="f1334">3-3-4-1</li>
								  </ul>
							    </div>
							    <div class="field clearfix">
								  <div class="formation f1442">
									<a href="#" class="pl1" rel="Goalkeeper" title="Casillas">1</a>
									<a href="#" class="pl2" rel="Defenders"title="Albiol" >2</a>
									<a href="#" class="pl3" rel="Defenders" title="Piqué">3</a>
									<a href="#" class="pl4" rel="Defenders" title="Ramos">4</a>
									<a href="#" class="pl5" rel="Defenders" title="Alba">5</a>
									<a href="#" class="pl6" rel="Midfielders" title="Iniesta">6</a>
									<a href="#" class="pl7" rel="Midfielders" title="Xavi">7</a>
									<a href="#" class="pl8" rel="Midfielders" title="Xabi Alonso">8</a>
									<a href="#" class="pl9" rel="Midfielders" title="Silva">9</a>
									<a href="#" class="pl10" rel="Forwards" title="Torres">10</a>
									<a href="#" class="pl11" rel="Forwards" title="Fabregas">11</a>
								  </div>
								  <div class="formation f1352 none">
									<a href="#" class="pl1" rel="Goalkeeper" title="Casillas">1</a>
									<a href="#" class="pl2" rel="Defenders"title="Albiol" >2</a>
									<a href="#" class="pl3" rel="Defenders" title="Piqué">3</a>
									<a href="#" class="pl4" rel="Defenders" title="Ramos">4</a>
									<a href="#" class="pl5" rel="Midfielders" title="Alba">5</a>
									<a href="#" class="pl6" rel="Midfielders" title="Iniesta">6</a>
									<a href="#" class="pl7" rel="Midfielders" title="Xavi">7</a>
									<a href="#" class="pl8" rel="Midfielders" title="Xabi Alonso">8</a>
									<a href="#" class="pl9" rel="Midfielders" title="Silva">9</a>
									<a href="#" class="pl10" rel="Forwards" title="Torres">10</a>
									<a href="#" class="pl11" rel="Forwards" title="Fabregas">11</a>
								  </div>
								  
							    </div>
						      </div>
						      <div id="pick_list" class="clearfix none p_co">
							    <a href="#" class="player_item clearfix">
								  <span class="pi_img">
									<img src="common/images/player_face.jpg" alt="" />
								  </span>
								  <span class="pi_details">
									<h4>F.Beria</h4>
									<p>Manchester UTD</p>
								  </span>
								  <span class="pi_cost">
									1,1 mln $
								  </span>
							    </a>
							    <a href="#" class="player_item clearfix">
								  <span class="pi_img">
									<img src="common/images/player_face.jpg" alt="" />
								  </span>
								  <span class="pi_details">
									<h4>F.Beria</h4>
									<p>Manchester UTD</p>
								  </span>
								  <span class="pi_cost">
									1,1 mln $
								  </span>
							    </a>
							    <a href="#" class="player_item clearfix">
								  <span class="pi_img">
									<img src="common/images/player_face.jpg" alt="" />
								  </span>
								  <span class="pi_details">
									<h4>F.Beria</h4>
									<p>Manchester UTD</p>
								  </span>
								  <span class="pi_cost">
									1,1 mln $
								  </span>
							    </a>
							    <a href="#" class="player_item clearfix">
								  <span class="pi_img">
									<img src="common/images/player_face.jpg" alt="" />
								  </span>
								  <span class="pi_details">
									<h4>F.Beria</h4>
									<p>Manchester UTD</p>
								  </span>
								  <span class="pi_cost">
									1,1 mln $
								  </span>
							    </a>
							    <a href="#" class="player_item clearfix">
								  <span class="pi_img">
									<img src="common/images/player_face.jpg" alt="" />
								  </span>
								  <span class="pi_details">
									<h4>F.Beria</h4>
									<p>Manchester UTD</p>
								  </span>
								  <span class="pi_cost">
									1,1 mln $
								  </span>
							    </a>
							    <a href="#" class="player_item clearfix">
								  <span class="pi_img">
									<img src="common/images/player_face.jpg" alt="" />
								  </span>
								  <span class="pi_details">
									<h4>F.Beria</h4>
									<p>Manchester UTD</p>
								  </span>
								  <span class="pi_cost">
									1,1 mln $
								  </span>
							    </a>
							    <a href="#" class="player_item clearfix">
								  <span class="pi_img">
									<img src="common/images/player_face.jpg" alt="" />
								  </span>
								  <span class="pi_details">
									<h4>F.Beria</h4>
									<p>Manchester UTD</p>
								  </span>
								  <span class="pi_cost">
									1,1 mln $
								  </span>
							    </a>
						      </div>
						</div>
						
					  </div>
					  
					  <div class="choosed_player clearfix">
						<div class="chp_header clearfix">
						      <ul>
							    <li><a class="active_tab" href="#p_about">selected player</a></li>
							    <li><a href="#p_stats">player full stats</a></li>
						      </ul>
						</div>
						<div class="chp_content" id="p_about">
						      <div class="player_det">
							    <div class="pd_name">
								  <h5>Mario Cecto</h5>
								  <p>Manchester UTD</p>
							    </div>
							    <table>
								  <tr>
									<td class="plabel">Nationality</td>
									<td>Nigeria</td>
								  </tr>
								  <tr>
									<td class="plabel">Date of birth</td>
									<td>29 August 1982</td>
								  </tr>
								  <tr>
									<td class="plabel">Age</td>
									<td>29</td>
								  </tr>
								  <tr>
									<td class="plabel">Country of birth</td>
									<td>Nigeria</td>
								  </tr>
								  <tr>
									<td class="plabel">Place of birth</td>
									<td>Kaduna</td>
								  </tr>
								  <tr>
									<td class="plabel">Position</td>
									<td>Forward</td>
								  </tr>
							    </table>
							    
						      </div>
						      <div class="player_img">
							    <img src="common/images/big_face.jpg" alt="" />
						      </div>
						      <div class="player_hire clearfix">
							    <div class="hire_box">
								  <span>price</span>
								  <p class="price_g">1 mln $</p>
								  <a href="#" class="hire_add">add</a>
							    </div>
							    <div class="hire_info_box">
								  <table>
									<tr>
									      <td> <p>Points in this season: </p></td>
									      <td class="points">110</td>
									</tr>
									<tr>
									      <td> <p>Points in last season: </p></td>
									      <td class="points">66</td>
									</tr>
								  </table>
							    </div>
						      </div>
						</div>
						<div class="chp_content none" id="p_stats">
						      statsy
						</div>
						
					  </div>
					  
				    </div>
				    <div class="full_box_right">
					  <div class="players_header clearfix">
						<a href="#" class="all_t">all teams</a>
						<ul>
						      <li><a class="active" href="#">all players</a></li>
						      <li><span>|</span></li>
						      <li><a href="#">goalkeepers</a></li>
						      <li><span>|</span></li>
						      <li><a href="#">defenders</a></li>
						      <li><span>|</span></li>
						      <li><a href="#">midifields</a></li>
						      <li><span>|</span></li>
						      <li><a href="#">forwards</a></li>
						</ul>
					  </div>
					   <table class="all_player_list">
						<tr>
						      <th width="110" class="corner1"><span>name</span></th>
						      <th width="30" >position</th>
						      <th width="100">club</th>
						      <th width="50">points</th>
						      <th>price</th>
						      <th class="corner2"><span class="indent">hire</span></th>
						</tr>
						<tr class="border">
						      <td colspan="6"></td>
						</tr>
						
					   </table>
					   <div class="table_mask">
					   <table class="all_player_list">
						
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_one">
						      <td width="110" class="corner1"><span>M.Basa</span></td>
						      <td width="45">DEF</td>
						      <td width="100">Paris St Germain</td>
						      <td width="30">100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						<tr class="row_two">
						      <td class="corner1"><span>M.Basanowsky</span></td>
						      <td>DEF</td>
						      <td>Lille</td>
						      <td>100</td>
						      <td>1,1 mln $</td>
						      <td class="corner2"><span><a href="#" class="mini_add">add</a></span></td>
						</tr>
						
						
					  </table>
					   </div>
					  <div class="summary_box_big clearfix">
						<div class="box_one">
						      <span>total cost</span>
						      <p class="price_g">195 200 $</p>
						</div>
						<div class="box_one">
						      <span>availble to spend</span>
						      <p class="price_g">804 200 $</p>
						</div>
						<a href="#" class="save_gr">save </a>
					  </div>
				    </div>
			      </div>
			      
			</div>
<?php include('template/footer.php'); ?>


