<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />
<style>
	.normal_page ol {padding-left: 30px;}
</style>
			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><b>Zasady</b></h2>
				    <div class="page_box">
					  <div class="normal_page">
					  <h2>Regulamin konkursu</h2>

<h3>Definicje</h3>
<ol>
<li>Organizator - firma Dream Lineup LTD organizator konkursu dreamlineup.com.
<li>Użytkownik - osoba korzystająca z serwisu.
<li>Konto - unikalny identyfikator użytkownika biorącego udział w konkursie oraz wszystkie dane niezbędne do identyfikacji zwycięzcy konkursu.
<li>Liga - Polska Ekstraklasa.
<li>Kolejka - Runda polskiej Ekstraklasy.
<li>Turniej - jedna instancja gry pomiędzy użytkownikami.
<li>Wirtualna drużyna - zbiór zawodników wybranych przez użytkownika do udziału w turnieju.
<li>Dream Points (DP) - punkty wymagane do rejestracji w wirtualnych ligach.
</ol>
<h3>Założenia ogólne</h3>
<ol>
<li>Każdy użytkownik ma prawo założenia jednego konta. Każde konto w rozgrywce może korzystać tylko z jednego adresu IP komputera. Z każdego adresu IP można założyć i korzystać wyłącznie z jednego konta.
<li>W rozgrywce mogą uczestniczyć wyłącznie osoby pełnoletnie.
<li>Gra jest skierowana do osób zamieszkałych na terenie Polski.
<li>Udział w rozgrywce jest bezpłatny, DreamLineup w żadnym przypadku nie zasili konta gracza środkami finansowymi. Przelewy środków pomiędzy graczami są wykluczone. Najlepsi gracze w turnieju głównym otrzymają nagrody rzeczowe.
<li>Dream Points nie mają wartości pieniężnej. 
<li>Handel lub wymiana Dream Points pomiędzy użytkownikami jest niedozwolona.
<li>Składy drużyn użytkownika pozostają niewidoczne dla innych użytkowników do momentu rozpoczęcia rozgrywki.
</ol>
<h3>Zasady gry</h3>
<ol>
<li>Użytkownik może wziąć udział w turnieju jeśli posiada DP wystarczające na wpisowe do Turnieju.
<li>Użytkownik wybiera drużynę spośród zawodników Ekstraklasy.
<li>Po zakończeniu Kolejki zawodnicy są oceniani wg obowiązującej tabeli punktów.
<li>Po zakończeniu ostatniej Kolejki w ramach danego Turnieju, Organizator publikuje ranking Użytkowników biorących udział w Turnieju, wg zdobytych punktów.
<li>W przypadku dwóch uczestników z taką sama liczbą punktów wygrywa tańsza drużyna.
</ol>
<h3>Turnieje</h3>
<ol>
<li>W każdej kolejce Ekstraklasy Organizator zakłada turniej główny z nagrodami rzeczowymi.
<li>Użytkownicy muszą zagrać co najmniej raz w turnieju głównym  aby móc tworzyć własne turnieje.
<li>Użytkownik tworzący nowy turniej musi w nim uczestniczyć.
<li>Użytkownik ma prawo założenia turnieju obejmującego wybrane kolejki ekstraklasy.
<li>W przypadku dwóch lub więcej identycznych zwycięskich drużyn nagroda jest dzielona.
<li>Organizator zastrzega sobie prawo do dyskwalifikacji Użytkownika w przypadku gdy działa on niezgodnie z regulaminem.
<li>W przypadku dyskwalifikacji Użytkownika nagroda przechodzi na użytkownika z następnym najlepszym wynikiem.
<li>Składy drużyn można edytować dowolna liczbę razy do momentu rozpoczęcia rozgrywki.
<li>Organizator zastrzega sobie prawo usunięcia turniejów bez podania przyczyny.
</ol>
<h3>Odbiór nagród</h3>
<ol>
<li>Warunkiem otrzymania nagrody jest podanie prawdziwych danych osobowych.
<li>Warunkiem otrzymania nagrody jest podanie prawidłowych danych do wysyłki.
<li>Organizator zastrzega sobie prawo do weryfikacji prawidłowości danych podanych przez użytkownika.
<li>Nagrody zostaną wysłane w ciągu 14 dni od zakończenia rozgrywek.
</ol>
<h3>Klasyfikacjia użytkowników</h3>
<ol>
<li>Kazdy uzytkownik dostaje na początek 100 DP.
<li>Na stronach serwis, Organizator publikuje ranking Użytkowników z największą liczbą punktów.
<li>Ranking ten nie jest uwzględniany 
</ol>
<h3>Uwarunkowanie techniczne</h3>
<ol>
<li>Organizator nie odpowiada za problemy techniczne spowodowane brakiem dostępu do internetu itd.
<li>Organizatora dokłada wszelkich starań by osiągnięcia zawodników Ligi były zgodne ze stanem faktycznym. Werdykt Organizatora dotyczący poprawności danych jest ostateczny.
<li>Mecze przełożone, które nie zmieszczą się w czasie turnieju nie będą liczyć się do punktacji.
<li>W sprawach nieuwzględnionych w tym regulaminie werdykt Organizatora jest ostateczny.
</ol>

<div style="font-size: 12px; text-align: right; padding: 5px; padding-top: 10px;">Data ostatniej modyfikacji: 29 stycznia 2014</div>


				    </div>
				    </div>
			      </div>
<jsp:include page="right.jsp" />
			</div>

<jsp:include page="footer.jsp" />
