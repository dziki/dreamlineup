<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
<h1>Admin dashboard</h1>
<h2>Menu</h2>
<!-- span style="font-weight: bold; color: red">WEBSITE WORKING IN TESTING MODE! - rounds are simulated</span><br / -->

<c:forEach var="league" items="${leagues}">
<!-- h4><c:out value="${league.name}"/></h4>
<table style="margin: 15px;">
<tr><td>Current season:</td><td><cout value="{league.firstUnfinishedRound.season}"/>/<cout value="{league.firstUnfinishedRound.season + 1}"/></td></tr>
<tr><td>Current round:</td><td><cout value="{league.firstUnfinishedRound.number}"/></td></tr>
<tr><td colspan="2"><a href="<c:url value='/admin/${league.id}/simulate.html' />">Simulate next round!</a></td></tr>
</table -->

</c:forEach>


<h3>Virtual data</h3>
<ul>
<li><a href="<c:url value='/admin/vleague/list.html' />"><spring:message code="admin.leagues" text="Leagues" /></a></li>
<!-- li><a href="<c:url value='/admin/vteam/list.html' />"><spring:message code="admin.virtualteams" text="Virtual Teams" /></a -->


<!--li><a href="<c:url value='/admin/playerlist.html' />"><spring:message code="admin.users" text="Users" /></a>

<li><a href="<c:url value='/admin/vleaguelist.html' />"><spring:message code="admin.virtualleagues" text="Virtual Leagues" /></a>
<li><a href="<c:url value='/admin/vteamlist.html' />"><spring:message code="admin.virtualteams" text="Virtual Teams" /></a>

<li><a href="<c:url value='/admin/operationlist.html' />"><spring:message code="admin.financial" text="Operations" /></a-->

</ul>
<h3>Real data</h3>
<ul>
<li><a href="<c:url value='/admin/player/list.html' />"><spring:message code="admin.users" text="Users" /></a></li>
<li><a href="<c:url value='/admin/athlete/list.html' />"><spring:message code="admin.athletes" text="Athletes" /></a>
<li><a href="<c:url value='/admin/performance/list.html' />"><spring:message code="admin.performance" text="Performance" /></a>
<!-- >li><a href="<c:url value='/admin/team/list.html' />"><spring:message code="admin.teams" text="Teams" /></a -->
</ul>
<h3>Dla Łukasza(!):</h3>
<ul>
<li><a href="<c:url value='/admin/diagnostics/test-email.html' />"><spring:message code="admin.testemail" text="Test e-mail" /></a></li>
<li><a href="<c:url value='/admin/diagnostics/test-error.html' />"><spring:message code="admin.testerror" text="Test error" /></a></li>
<li><a href="<c:url value='/admin/diagnostics/update-matches.html' />"><spring:message code="admin.updatematches" text="Update match dates" /></a></li>
<li><a href="<c:url value='/admin/diagnostics/download-results.html' />"><spring:message code="admin.downloadres" text="Download results" /></a></li>
<li><a href="<c:url value='/admin/diagnostics/generate-emails.html' />"><spring:message code="admin.generateemails" text="Generate e-mails" /></a></li>
<li><a href="<c:url value='/admin/diagnostics/send-emails.html' />"><spring:message code="admin.sendemails" text="Send e-mails" /></a></li>
<li><a href="<c:url value='/admin/diagnostics/copy-teams.html' />"><spring:message code="admin.copyteams" text="Copy teams" /></a></li>
<li><a href="<c:url value='/admin/diagnostics/load-data.html' />"><spring:message code="admin.loaddata" text="Load data" /></a></li>
<li><a href="<c:url value='/admin/diagnostics/load-athletes.html' />"><spring:message code="admin.loadathletes" text="Load athletes" /></a></li>
</ul>


</html>
</body>
