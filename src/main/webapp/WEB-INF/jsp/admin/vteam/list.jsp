<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
<a href="<c:url value='/admin/index.html' />"><spring:message code="admin.index" text="Index" /></a>

<h2><spring:message code="base.instances" text="Instances" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="admin.id" text="id" /></th>
<th><spring:message code="admin.player" text="player" /></th>
<th><spring:message code="admin.virtual_league" text="virtual league" /></th>
<th><spring:message code="admin.points" text="points" /></th>
</tr>
<c:forEach var="item" items="${objects}">
<tr onclick="document.location = '<c:url value="/admin/player/details.html?class=${class}&id=${item.id}" />'">
<td><c:out value="${item.id}"/></td>
<td><c:out value="${item.player.fullName}"/></td>
<td><c:out value="${item.virtualLeague.title}"/></td>
<td><c:out value="${item.points}"/></td>
</tr>
</c:forEach>
</table>

</body>
</html>
