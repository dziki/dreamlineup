<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
<a href="<c:url value='/admin/index.html' />"><spring:message code="admin.index" text="Index" /></a>

<h1><spring:message code="base.virtual_team" text="Virtual team" /></h1>
<h2><c:out value="${vteam.player.fullName}"/></h2>

<h2><spring:message code="base.athletes" text="Athletes" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="admin.id" text="id" /></th>
<th><spring:message code="admin.name" text="name" /></th>
<th><spring:message code="admin.team" text="team" /></th>
<th><spring:message code="admin.position" text="pos" /></th>
<th><spring:message code="admin.price" text="price" /></th>
<th><spring:message code="admin.points" text="points" /></th>
</tr>
<c:forEach var="item" items="${vteam.virtualAthletes}" varStatus="status">
<!-- tr onclick="document.location = '<c:url value="/admin/athlete/${item.virtualLeagueAthlete.athlete.id}/details.html" />'" -->
<tr>
<td><c:out value="${status.count}."/></td>
<td><c:out value="${item.virtualLeagueAthlete.athlete.firstName} ${item.virtualLeagueAthlete.athlete.lastName}"/></td>
<td><c:out value="${item.virtualLeagueAthlete.athlete.team.name}"/></td>
<td><c:out value="${item.virtualLeagueAthlete.athlete.position}"/></td>
<td><fmt:formatNumber value='${item.virtualLeagueAthlete.price/1000000}' currencySymbol='DP' type='currency'/> mln</td>
<td><c:out value="${item.virtualLeagueAthlete.points}"/></td>
</tr>
</c:forEach>
</table>

</body>
</html>
