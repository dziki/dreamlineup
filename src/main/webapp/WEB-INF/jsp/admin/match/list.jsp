<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
<a href="<c:url value='/admin/index.html' />"><spring:message code="admin.index" text="Index" /></a>

<h2><spring:message code="base.instances" text="Instances" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="admin.date" text="date" /></th>
<th><spring:message code="admin.home_team" text="home team" /></th>
<th><spring:message code="admin.away_team" text="away team" /></th>
<th><spring:message code="admin.result" text="result" /></th>
</tr>
<c:forEach var="match" items="${objects}">
<tr>
<td><fmt:formatDate value="${match.date.time}" type="both" timeStyle="short" dateStyle="short" /></td>
<td><a href="<c:url value="/admin/team/${match.homeTeam.id}/athlete/list.html"/>"><c:out value="${match.homeTeam.name}"/></a></td>
<td><a href="<c:url value="/admin/team/${match.awayTeam.id}/athlete/list.html"/>"><c:out value="${match.awayTeam.name}"/></a></td>
<td><c:out value="${match.result}"/></td>
</tr>
</c:forEach>
</table>

</body>
</html>
