<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
<a href="<c:url value='/admin/index.html' />"><spring:message code="admin.index" text="Index" /></a>

<h1><spring:message code="base.virtual_team" text="Virtual league" /></h1>
<h2><c:out value="${vleague.title}"/></h2>

<h2><spring:message code="base.teams" text="Teams" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="admin.id" text="id" /></th>
<th><spring:message code="admin.player" text="player" /></th>
<th><spring:message code="admin.points" text="points" /></th>
</tr>
<c:forEach var="item" items="${vleague.virtualTeams}" varStatus="status">
<tr onclick="document.location = '<c:url value="/admin/vteam/${item.id}/details.html" />'" -->
<td><c:out value="${status.count}."/></td>
<td><c:out value="${item.player.firstName} ${item.player.lastName}"/></td>
<td><c:out value="${item.points}"/></td>
</tr>
</c:forEach>
</table>

</body>
</html>
