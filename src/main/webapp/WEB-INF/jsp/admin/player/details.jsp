<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
<a href="<c:url value='/admin/index.html' />"><spring:message code="admin.index" text="Index" /></a>
<h1><c:out value="${player.firstName} ${player.lastName}"/></h1>

<h2><spring:message code="base.player_details" text="Player details" /></h2>
<table>
<tr>
	<td><spring:message code="registration.birthdate" text="Birthday" /></td>
	<td><fmt:formatDate value="${player.birthDate}" type="date" dateStyle="short" /></td>
</tr>
<tr>
	<td><spring:message code="registration.email" text="E-mail" /></td>
	<td><c:out value="${player.email}"/></td>
</tr>
</table>
<h2><spring:message code="base.finance" text="Finance" /></h2>
<table class="padding2">
<tr>
<th style="width: 100px;"><p class="win_row"><spring:message code="history.date" text="Date" /></p></th>
<th style="width: 90px;"><p class="win_row"><spring:message code="history.amount" text="Amount" /></p></th>
<th style="width: 150px;"><p class="win_row"><spring:message code="history.type" text="Value" /></p></th>
<!-- th><p class="win_row"><spring:message code="history.status" text="Status" /></p></th -->
</tr>
<c:forEach var="operation" items="${operations}">
<tr>
<td><p><fmt:formatDate value="${operation.date}" type="both" 
      pattern="MM-dd-yyyy" /></p></td>
<td><p><b><fmt:formatNumber value='${operation.value}' currencySymbol='' type='currency'/> DP</p></b></td>
<td><p><c:out value="${operation.typeOpDesc}"/></p></td>
</tr>
</c:forEach>
</table>


<h2><spring:message code="base.instances" text="Virtual teams" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="admin.id" text="id" /></th>
<th><spring:message code="admin.virtual_league" text="virtual league" /></th>
<th><spring:message code="admin.points" text="points" /></th>
</tr>
<c:forEach var="item" items="${virtualTeams}">
<tr onclick="document.location = '<c:url value="/admin/vteam/${item.id}/details.html" />'">
<td><c:out value="${item.id}"/></td>
<td><c:out value="${item.virtualLeague.title}"/></td>
<td><c:out value="${item.points}"/></td>
</tr>
</c:forEach>
</table>

</body>
</html>
