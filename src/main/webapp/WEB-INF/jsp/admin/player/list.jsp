<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
<a href="<c:url value='/admin/index.html' />"><spring:message code="admin.index" text="Index" /></a>

<h2><spring:message code="base.players" text="Players" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="admin.id" text="id" /></th>
<th><spring:message code="admin.name" text="name" /></th>
</tr>
<c:forEach var="item" items="${objects}">
<tr onclick="document.location = '<c:url value="/admin/player/${item.id}/details.html" />'">
<td><c:out value="${item.id}"/></td>
<td><c:out value="${item.fullName}"/></td>
</tr>
</c:forEach>
</table>

</body>
</html>
