<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
<a href="<c:url value='/admin/index.html' />"><spring:message code="admin.index" text="Index" /></a>

<h2><spring:message code="base.instances" text="Instances" /></h2>
<table class="active-rows">
<tr>
<th><spring:message code="admin.id" text="id" /></th>
<th><spring:message code="admin.name" text="name" /></th>
<th>team</th>


<th><c:out value="{position}"/></th>
<th><c:out value="{goals}"/></th>
<th><c:out value="{redcard}"/></th>
<th><c:out value="{yellowcard}"/></th>
<th><c:out value="{timeIn}"/></th>
<th><c:out value="{timeOut}"/></th>
<th><c:out value="{goalsConceded}"/></th>
<th><c:out value="{cleanSheet}"/></th>

<th><c:out value="{points}"/></th>

</tr>
<c:forEach var="item" items="${objects}">
<tr>
<td><c:out value="${item.athlete.firstName}"/></td>
<td><c:out value="${item.athlete.lastName}"/></td>
<td><c:out value="${item.athlete.team.name}"/></td>

<td><c:out value="${item.position}"/></td>
<td><c:out value="${item.goals}"/></td>
<td><c:out value="${item.redCard}"/></td>
<td><c:out value="${item.yellowCard}"/></td>
<td><c:out value="${item.timeIn}"/></td>
<td><c:out value="${item.timeOut}"/></td>
<td><c:out value="${item.goalsConceded}"/></td>
<td><c:out value="${item.cleanSheet}"/></td>

<td><c:out value="${item.points}"/></td>
</tr>
</c:forEach>
</table>

</body>
</html>
