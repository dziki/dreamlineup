<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../top.jsp" />

			<div class="main_content">
			      <div class="left_c">
<a href="<c:url value="/leagues/${vteam.virtualLeague.id}/details.html"/>" class="back_to_lobby"><spring:message code="draft.backlobby" /></a>

				    <h2 class="head_title"><b><spring:message code="team.playerTeam" text="team" /></b> 
				    <a href="<c:url value="/players/${vteam.player.id}/stats.html"/>"><c:out value="${vteam.player.username}"/></a></h2>
				    
				    
				    <div class="page_box">

<span style="font-size: 15px;"><spring:message code="team.thisTeam" text="This team got the " /> <strong><c:out value="${vteam.place}"/>.</strong> <spring:message code="team.placeInTournament" text="place in the tournament" /> <a style="font-weight: bold; color: #ffffff;" href="<c:url value='/leagues/${vteam.virtualLeague.id}/details.html'/>">"<c:out value="${vteam.virtualLeague.title}"/>"</a>.</span>
&nbsp; <div style="float:right;" class="fb-share-button" data-href="<c:url value="/lobby/team/${vteam.id}/details.html" />" data-type="button"></div>

				    <div class="players_box">
					    <div class="p_box" style="width: 100%;">


<table id="teamStats" class="tablesorter active-rows" style="width: 643px;">
<thead><tr>
<th class="nr"></th>
<th><spring:message code="team.lastName" text="Name" /></th>
<th><spring:message code="team.team" text="Team" /></th>
<th><spring:message code="draft.position" text="Pos" /></th>
<th><spring:message code="team.price" text="Price" /></th>
<th><spring:message code="lobby.points" text="Points" /></th>
</tr></thead><tbody>
<c:forEach var="item" items="${vteam.virtualAthletes}" varStatus="status">
<tr>
<td class="nr"><p><span><c:out value="${status.count}"/></span></p></td>
<td><c:out value="${item.virtualLeagueAthlete.athlete.firstName} ${item.virtualLeagueAthlete.athlete.lastName}"/></td>
<td><c:out value="${item.virtualLeagueAthlete.athlete.team.name}"/></td>
<td><spring:message code='position.${item.virtualLeagueAthlete.athlete.position}' /></td>
<td><fmt:formatNumber value='${item.virtualLeagueAthlete.price/1000000}' type="number" minFractionDigits="2" maxFractionDigits="2"  /> mln</td>
<td><c:out value="${item.virtualLeagueAthlete.points}"/></td>
</tr>
</c:forEach>
</tbody>
<tfoot style="border-top: 2px solid #3b333f;">
<tr>
<td></td>
<td></td>
<td></td>
<td><spring:message code="team.total" text="Total" /></td>
<td><fmt:formatNumber value='${vteam.teamPrice/1000000}' type="number" minFractionDigits="2" maxFractionDigits="2"  /> mln</td>
<td><c:out value="${vteam.points}"/></td>
</tr>
</tfoot>
</table>
				    </div>
				    </div>
				    </div>
				  </div>


<jsp:include page="../right.jsp" />
			      </div>
<script>
$(document).ready(function() 
	    { 
	        $(".tablesorter").tablesorter( {cssHeader: "vl_header", sortList: [[5,1]] } ); 
	    } 
	); 
</script>
<jsp:include page="../footer.jsp" />
