<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../top.jsp" />

			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><b><spring:message code="lobby.lobby" /></b> - <c:out value="${vleague.title}"/> - <spring:message code="lobby.results" /></h2>
				    <div class="page_box">
<c:if test="${vleague.status == 1}">
				    <div class="players_box">
					    <div class="p_box floatLeft">
						<table>

						      <tr>
							    <th class="nr"></th>
							    <th><spring:message code="lobby.player" /></th>
							    <th><spring:message code="lobby.points" /></th>
							    <th><spring:message code="lobby.payout" /></th>
						      </tr>
<c:forEach  begin="0" end="${fn:length(standings)/2}" var="entry" items="${standings}" varStatus="status">
						      <tr>
							    <td class="nr"><p><span><c:out value="${1 + status.index}"/></span></p></td>
							    <td class="name"><p><a href="<c:url value="/lobby/team/${entry.id}/details.html" />"><c:out value="${entry.player.username}"/></a></p></td>
							    <td class="gr"><p><c:out value="${entry.points}"/></p></td>
							    <td class="gr"><c:if test="${vleague.status == 1}"><p><fmt:formatNumber value='${vleague.prizeDistribution[status.index]}' currencySymbol='DP' type='currency'/></p></c:if></td>
						      </tr>
</c:forEach>
						</table>
						</div>
						<div class="p_box floatRight">
						<table>

						      <tr>
							    <th class="nr"></th>
							    <th><spring:message code="lobby.player" /></th>
							    <th><spring:message code="lobby.points" /></th>
							    <th><spring:message code="lobby.payout" /></th>
						      </tr>
<c:forEach  begin="${1 + fn:length(standings)/2}" var="entry" items="${standings}" varStatus="status">
						      <tr>
							    <td class="nr"><p><span><c:out value="${1 + status.index}"/></span></p></td>
							    <td class="name"><p><a href="<c:url value="/lobby/team/${entry.id}/details.html" />"><c:out value="${entry.player.username}"/></a></p></td>
							    <td class="gr"><p><c:out value="${entry.points}"/></p></td>
							    <td class="gr"><c:if test="${vleague.status == 1}"><p><fmt:formatNumber value='${vleague.prizeDistribution[status.index]}' currencySymbol='DP' type='currency'/></p></c:if></td>
						      </tr>
</c:forEach>
						</table>
					    </div>
					    <div class="clearfix"></div>
					  </div>
</c:if>
				    </div>
				  </div>


<jsp:include page="../right.jsp" />
			      </div>

<jsp:include page="../footer.jsp" />
