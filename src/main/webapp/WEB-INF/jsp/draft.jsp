<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />
			<div class="main_content">
			      <div class="normal_header clearfix">
					  <h2 class="head_title"><spring:message code="draft.for" /> <b>${virtualteam.virtualLeague.title}</b></h2>
					  
					  <a href="<c:url value="/leagues/${virtualteam.virtualLeague.id}/details.html"/>" class="back_to_lobby"><spring:message code="draft.backlobby" /></a>
					  <div class="team_status status_incomplete"><spring:message code='draft.incomplete' javaScriptEscape='true' /></div>
			      </div>
			      <div class="full_box clearfix">
				    <div class="full_box_left">
					  
					  <div class="pick_players clearfix">
						<div class="pp_header clearfix">
						      <ul>
							    <!-- li><a href="#pick_field"><spring:message code="draft.field" /></a></li -->
							    <li><a class="active_tab" href="#pick_list"><spring:message code="draft.list" /></a></li>
						      </ul>
						</div>
						<div class="pp_content">
							    <div class="field_nav">
								  <p><spring:message code="draft.formation" /></p>
								  <ul>
<c:forEach var="formation" items="${virtualteam.availableFormations}">
<li id="f<c:out value="${formation}"/>"><c:out value="${formation}"/></li>
</c:forEach>
								  </ul>
<c:if test="${!(formationError==null)}">
<spring:message code="draft.formationexception" text="Formation error" />: <c:out value="${formationError}"/><br />
</c:if>
							    </div>
						      <div id="pick_field" class="clearfix none p_co">
							    <div class="field clearfix">
								  <div class="formation">
									<a href="#" class="pl1" rel="Goalkeeper" title="Casillas">1</a>
									<a href="#" class="pl2" rel="Defenders"title="Albiol" >2</a>
									<a href="#" class="pl3" rel="Defenders" title="PiquÃ©">3</a>
									<a href="#" class="pl4" rel="Defenders" title="Ramos">4</a>
									<a href="#" class="pl5" rel="Defenders" title="Alba">5</a>
									<a href="#" class="pl6" rel="Midfielders" title="Iniesta">6</a>
									<a href="#" class="pl7" rel="Midfielders" title="Xavi">7</a>
									<a href="#" class="pl8" rel="Midfielders" title="Xabi Alonso">8</a>
									<a href="#" class="pl9" rel="Midfielders" title="Silva">9</a>
									<a href="#" class="pl10" rel="Forwards" title="Torres">10</a>
									<a href="#" class="pl11" rel="Forwards" title="Fabregas">11</a>
								  </div>
								  
							    </div>
						      </div>
						      <div id="pick_list" class="clearfix p_co">
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl1"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl2"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl3"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl4"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl5"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl6"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl7"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl8"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl9"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl10"></a>						      
<a class="player_item fireAthlete clearfix" href="javascript:;" id="pl11"></a>						      
<!--forEach var="vathlete" items="{virtualteam.virtualAthletes}">
							    <a href="javascript:;" id="fireAthlete${vathlete.id}" class="player_item fireAthlete clearfix">
								  <span class="pi_img">
									<img src="<c:url value='/common/images/player_face.jpg'/>" alt="" />
								  </span>
								  <span class="pi_details">
									<h4><c:out value="${vathlete.virtualLeagueAthlete.athlete.lastName}"/> <c:out value="${vathlete.virtualLeagueAthlete.athlete.firstName}"/></h4>
									<p><c:out value="${vathlete.virtualLeagueAthlete.athlete.team.name}"/></p>
								  </span>
								  <span class="pi_cost">
									<fmt:formatNumber value='${vathlete.virtualLeagueAthlete.price}' currencySymbol='DP' type='currency'/>
								  </span>
							    </a>
</forEach-->
						      </div>
						</div>
						
					  </div>
					  
					  <!--div class="choosed_player clearfix">
						<div class="chp_header clearfix">
						      <ul>
							    <li><a class="active_tab" href="#p_about"><spring:message code="draft.selectedPlayer" /></a></li>
							    <li><a href="#p_stats"><spring:message code="draft.playerFullStats" /></a></li>
						      </ul>
						</div>
						<div class="chp_content" id="p_about">
						      <div class="player_det">
							    <div class="pd_name">
								  <h5>Mario Cecto</h5>
								  <p>Manchester UTD</p>
							    </div>
							    <table>
								  <tr>
									<td class="plabel"><spring:message code="draft.nationality" /></td>
									<td>Nigeria</td>
								  </tr>
								  <tr>
									<td class="plabel"><spring:message code="draft.birthdate" /></td>
									<td>29 August 1982</td>
								  </tr>
								  <tr>
									<td class="plabel"><spring:message code="draft.age" /></td>
									<td>29</td>
								  </tr>
								  <tr>
									<td class="plabel"><spring:message code="draft.country" /></td>
									<td>Nigeria</td>
								  </tr>
								  <tr>
									<td class="plabel"><spring:message code="draft.place" /></td>
									<td>Kaduna</td>
								  </tr>
								  <tr>
									<td class="plabel"><spring:message code="draft.position" /></td>
									<td>Forward</td>
								  </tr>
							    </table>
							    
						      </div>
						      <div class="player_img">
							    <img src="<c:url value='/common/images/big_face.jpg'/>" alt="" />
						      </div>
						      <div class="player_hire clearfix">
							    <div class="hire_box">
								  <span>price</span>
								  <p class="price_g">1 mln $</p>
								  <a href="#" class="hire_add"><spring:message code="draft.add" /></a>
							    </div>
							    <div class="hire_info_box">
								  <table>
									<tr>
									      <td> <p><spring:message code="draft.pointsThisSeason" />: </p></td>
									      <td class="points">110</td>
									</tr>
									<tr>
									      <td> <p><spring:message code="draft.pointsLastSeason" />: </p></td>
									      <td class="points">66</td>
									</tr>
								  </table>
							    </div>
						      </div>
						</div>
						<div class="chp_content none" id="p_stats">
						      statsy
						</div>
						
					  </div-->
					  
				    </div>
				    <div class="full_box_right">
					  <div class="players_header clearfix">
						<a href="#" class="all_t"><spring:message code="draft.allTeams" /></a>
						<ul>
						      <li><a class="active selectPosition" id="posFilter" href="#"><spring:message code="draft.allPlayers" /></a></li>
						      <li><span>|</span></li>
						      <li><a class="selectPosition" id="posFilterG" href="#G"><spring:message code="draft.goalkeepers" /></a></li>
						      <li><span>|</span></li>
						      <li><a class="selectPosition" id="posFilterD" href="#D"><spring:message code="draft.defenders" /></a></li>
						      <li><span>|</span></li>
						      <li><a class="selectPosition" id="posFilterM" href="#M"><spring:message code="draft.midfields" /></a></li>
						      <li><span>|</span></li>
						      <li><a class="selectPosition" id="posFilterS" href="#S"><spring:message code="draft.strikers" /></a></li>
						</ul>
					  </div>
					   <table class="all_player_list">
						<tr>
						      <th width="140" class="corner1"><span><spring:message code="draft.name" /></span></th>
						      <th width="12" ><spring:message code="draft.pos" /></th>
						      <th width="150"><spring:message code="draft.club" /></th>
						      <th width="65"><spring:message code="draft.price" /></th>
						      <th class="corner2"><span class="indent"><spring:message code="draft.hire" /></span></th>
						</tr>
						<tr class="border">
						      <td colspan="6"></td>
						</tr>
						
					   </table>
					   <div class="table_mask">
					   <table class="all_player_list" id="athlete_list">
<!-- forEach var="vathlete" items="${athlets}">
						<tr class="row_one hireAthlete" id="hireAthlete${vathlete.id}">
						      <td width="140" class="corner1"><span><c:out value="${vathlete.athlete.lastName}"/> A.</span></td>
						      <td width="12"><c:out value="${vathlete.athlete.position}"/></td>
						      <td width="150"><c:out value="${vathlete.athlete.team.name}"/></td>
						      <!--td><c:out value="${vathlete.price}"/>$</td-->
						      <td width="65">0 PLN</td>
						      <td class="corner2"><span><a class="mini_add" href="#"><spring:message code="draft.add" /></a></span></td>
						</tr>
<forEach-->
						
						
					  </table>
					   </div>
					  <div class="summary_box_big clearfix">
						<div class="box_one">
						      <span><spring:message code="draft.total" /></span>
						      <p class="price_g" id="moneySpent"><c:out value="${virtualteam.teamPrice}"/> PLN</p>
						</div>
						<div class="box_one">
						      <span><spring:message code="draft.available" /></span>
						      <p class="price_g" id="moneyAvailable"><c:out value="${virtualteam.virtualLeague.budget - virtualteam.teamPrice}"/> PLN</p>
						</div>
						<a href="<c:url value="/leagues/${virtualteam.virtualLeague.id}/details.html"/>" class="save_gr"><spring:message code="draft.back" /> </a>
					  </div>
				    </div>
			      </div>
			      
			</div>
</table>

<c:if test="${!(hireError==null)}">
<spring:message code="draft.hireexception" text="Hire error" />: <c:out value="${hireError}"/><br />
</c:if>

<jsp:include page="footer.jsp" />

<script>
var teamId = ${virtualteam.id};
var leagueId = ${virtualteam.virtualLeague.id};
var vlbudget = ${virtualteam.virtualLeague.budget};

var leagueStarted = <c:if test="${virtualteam.virtualLeague.started}">1</c:if><c:if test="${!virtualteam.virtualLeague.started}">0</c:if>;



$(document).ready(function(){
	  $.getJSON(SERVER_PREFIX + 'json/team/' + teamId + '/athlete/list.html', function(data) {
		  if (data["status"] == "ERROR")
			  alertBox('You can\'t hire this player',data["message"]);
		  if (data["status"] == "OK") {
		  	  team = data["virtualTeam"];
			  $("#teamFilter").click();
			  $("#f" + team["formation"]).click();
		  	  organizeTeam(team);
		  }
	  });
});
</script>

      <div class="teams_toottip">
	    <div class="pt_top clearfix"><a href="#" class="close_tip"></a></div>
	    <div class="pt_cont clearfix">
		  
		  <ul>
			<li class="active_li"><a class="selectTeam" id="teamFilter" href="#"><spring:message code="draft.allTeams" /></a></li>
<c:forEach var="team" items="${teams}">
			<li><a class="selectTeam" id="teamFilter${team.id}" href="#${team.id}">${team.name}</a></li>
</c:forEach>
		  </ul>
	    </div>
	    <div class="pt_foot clearfix"></div>
      </div>
      
<a href="javascript:;" id="teamAthleteTmpl" class="player_item fireAthlete clearfix" style="display:none">
<span class="pi_img">
<img src="<c:url value='/common/images/athletes/nophoto.gif'/>" width="37" height="37" alt="" />
</span>
<span class="pi_details">
<h4>{{FULL_NAME}}</h4>
<p>{{TEAM_NAME}}</p>
</span>
<span class="pi_cost">
{{COST}} PLN
</span>
</a>
      
