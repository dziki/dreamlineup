<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />
<style>
.tutorial img { vertical-align: middle; }
.tutorial a { border: 1px solid #dedede; display: inline-block; margin-left: 7px; margin-right: 7px;}
.tutorial { width: 100%; text-align: center;}
</style>
			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><b>Zasady</b></h2>
				    <div class="page_box">
					  <div class="normal_page">
					  <h2>Zasady gry</h2>
						Twoim zadaniem jest wybranie jak najlepszej drużyny piłkarskiej której cena mieści się w zaproponowanym
						wirtualnym budżecie. Po zakończeniu kolejki zawodnicy otrzymują punkty za statystyki zaprezentowane w 
						tabeli poniżej. Drużyny ustawione są wg sumy punktów zawodników.<br />
						<br />
					  <h2>6 kroków do zwycięstwa</h2>
						<div style="padding: 5px; padding-bottom: 12px;" class="tutorial">
						<a style="margin-left: 2px;" data-lightbox="tutorial" href="<c:url value="common/images/tutorial/medium/1.png"/>" title="Kliknij 'Dołącz teraz' by założyć konto."><img src="<c:url value="common/images/tutorial/thumbs/1.png"/>"/></a>
						<a data-lightbox="tutorial" href="<c:url value="common/images/tutorial/medium/2.png"/>" title="Uzupełnij swoje dane."><img src="<c:url value="common/images/tutorial/thumbs/2.png"/>"/></a>
						<a data-lightbox="tutorial" href="<c:url value="common/images/tutorial/medium/2a.png"/>" title="Odbierz mail z linkiem aktywacyjnym."><img src="<c:url value="common/images/tutorial/thumbs/2a.png"/>"/></a>
						<a data-lightbox="tutorial" href="<c:url value="common/images/tutorial/medium/3.png"/>" title="Zaloguj się i kliknij w turniej główny."><img src="<c:url value="common/images/tutorial/thumbs/3.png"/>"/></a>
						<a data-lightbox="tutorial" href="<c:url value="common/images/tutorial/medium/4.png"/>" title="Dołącz do ligi."><img src="<c:url value="common/images/tutorial/thumbs/4.png"/>"/></a>
						<a data-lightbox="tutorial" href="<c:url value="common/images/tutorial/medium/6.png"/>" title="Wybierz zawodników z listy po prawej stronie."><img src="<c:url value="common/images/tutorial/thumbs/6.png"/>"/></a>
						</div>

					  <h2>Tabela punktów</h2>
					  Zawodnicy zakupieni do drużyny otrzymują punkty za następujące osiągnięcia.
						<table class="points-table">
							<tr><th>Statystyka</th><th>Liczba punktów</th></tr>
							<tr>
								<td>Zawodnik grał 1-60 minut</td>
								<td>+1 pkt</td>
							</tr>
							<tr>
								<td>Zawodnik grał dłużej niż 60 minut</td>
								<td>+2 pkt</td>
							</tr>
							<tr>
								<td>Napastnik strzelił gola</td>
								<td>+4 pkt</td>
							</tr>
							<tr>
								<td>Pomocnik strzelił gola</td>
								<td>+5 pkt</td>
							</tr>
							<tr>
								<td>Obrońca strzelił gola</td>
								<td>+6 pkt</td>
							</tr>
							<tr>
								<td>Czyste konto bramkarza</td>
								<td>+4 pkt</td>
							</tr>
							<tr>
								<td>Czyste konto obrońcy</td>
								<td>+4 pkt</td>
							</tr>
							<tr>
								<td>Czyste konto pomocnika</td>
								<td>+1 pkt</td>
							</tr>
							<tr>
								<td>Każde 2 stracone gole bramkarza lub obrońcy</td>
								<td>-1 pkt</td>
							</tr>
							<tr>
								<td>Samobójcza bramka</td>
								<td>-2 pkt</td>
							</tr>
							<tr>
								<td>Żółta kartka</td>
								<td>-1 pkt</td>
							</tr>
							<tr>
								<td>Czerwona kartka</td>
								<td>-3 pkt</td>
							</tr>
						</table>
				    </div>
				    </div>
			      </div>
<jsp:include page="right.jsp" />
			</div>

<jsp:include page="footer.jsp" />
