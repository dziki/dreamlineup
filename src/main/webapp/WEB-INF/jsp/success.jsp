<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<jsp:include page="header.jsp" />

<h2><spring:message code="base.welcome" text="Welcome" /> <core:out value="${name}"/></h2><br>
<a href="login.html"><spring:message code="base.back" text="Back" /></a>

<jsp:include page="footer.jsp" />
