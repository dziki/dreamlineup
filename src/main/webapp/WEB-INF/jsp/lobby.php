
<?php include('template/header.php'); ?>
<?php include('template/top.php'); ?>

		
			<div class="main_content">
			      <div class="left_c">
				    <h2 class="head_title"><b>Round 5</b> - 10$ - john1984</h2>
				    <div class="page_box">
					  <div class="list450">
						  <div class="list_item clearfix">
							<p class="label">Registration ends:</p>
							<div class="item_content"><p>May 16rd, 16:00 WET (In 3 days, 4 hours, 36 minutes)</p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label">Duration:</p>
							<div class="item_content"><p>16 May 17:00 - 22 May 22:30</p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label">Players:</p>
							<div class="item_content"><p>6/15</p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label">Type:</p>
							<div class="item_content"><p>SNG/weekly major</p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label">Buy-in:</p>
							<div class="item_content"><p><b>$10</b></p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label">Current prizepool:</p>
							<div class="item_content"><p><b>$615</b></p></div>
						  </div>
						  <div class="list_item clearfix">
							<p class="label">Bonuses:</p>
							<div class="item_content">
							      <ul>
								    <li><p>100 credits</p></li>
								    <li><p>ticket to weekly major</p></li>
								    <li><p>really cool avatar of your choice</p></li>
							      </ul>
							</div>
						  </div>
					  </div>
					  <div class="boxes_right200">
						  <div class="br200_area">
							<a href="#fixtures_m" class="fixtures modal">Show fixtures</a>
							<a href="#prices_m" class="prices modal">Show prices distribution</a>
						  </div>
					  </div>
					  <div class="players_box clearfix">
					    <h3 class="page_in_h">Players</h3>
					    <div class="p_box floatLeft">
						<table>
						      <tr>
							    <th class="nr"></th>
							    <th>Player</th>
							    <th>Points</th>
							    <th>Payout</th>
						      </tr>
						      <tr>
							    <td class="nr"><p>1</p></td>
							    <td class="name"><p>Ricardo 342</p></td>
							    <td class="gr"><p>189</p></td>
							    <td class="gr"><p>$210</p></td>
						      </tr>
						      <tr>
							    <td class="nr"><p>2</p></td>
							    <td class="name"><p>Mały20389</p></td>
							    <td class="gr"><p>19</p></td>
							    <td class="gr"><p>$110</p></td>
						      </tr>
						      <tr>
							    <td class="nr"><p>3</p></td>
							    <td class="name"><p>Zlatana Orana</p></td>
							    <td class="gr"><p>189</p></td>
							    <td class="gr"><p>$230</p></td>
						      </tr>
						      <tr>
							    <td class="nr"><p>4</p></td>
							    <td class="name"><p>Rivaldinho</p></td>
							    <td class="gr"><p>19</p></td>
							    <td class="gr"><p>$1210</p></td>
						      </tr>
						      <tr>
							    <td class="nr"><p>5</p></td>
							    <td class="name"><p>John Oshea</p></td>
							    <td class="gr"><p>189</p></td>
							    <td class="gr"><p>$210</p></td>
						      </tr>
						</table>
					    </div>
					    <div class="p_box floatRight">
						<table>
						      <tr>
							    <th class="nr"></th>
							    <th>Player</th>
							    <th>Points</th>
							    <th>Payout</th>
						      </tr>
						      <tr>
							    <td class="nr"><p>6</p></td>
							    <td class="name"><a href="#">Rezendo1</a></td>
							    <td class="gr"><p>189</p></td>
							    <td class="gr"><p>$1110</p></td>
						      </tr>
						      <tr>
							    <td class="nr"><p>7</p></td>
							     <td class="name"><p>Matuszczok</p></td>
							    <td class="gr"><p>189</p></td>
							    <td class="gr"><p>$210</p></td>
						      </tr>
						      <tr>
							    <td class="nr"><p>8</p></td>
							    <td class="name"><p>Kazandro</p></td>
							    <td class="gr"><p>29</p></td>
							    <td class="gr"><p>$210</p></td>
						      </tr>
						      <tr>
							    <td class="nr"><p>9</p></td>
							    <td class="name"><p>Coco loco</p></td>
							    <td class="gr"><p>189</p></td>
							    <td class="gr"><p>$210</p></td>
						      </tr>
						      <tr>
							    <td class="nr_dbl"><p>10</p></td>
							    <td class="name"><p>Gustavson</p></td>
							    <td class="gr"><p>189</p></td>
							    <td class="gr"><p>$2310</p></td>
						      </tr>
						</table>
					    </div>
					  </div>
				    </div>
			      </div>
			      <div class="right_c">
				    <h3><span>Welcome</span> Łukasz</h3>
				    <div class="summary_box clearfix">
					  <div class="sum_in"><p>Account balance</p> <span>5010$</span></div>
					  <div class="sum_in"><p>In play</p> <span>5010$</span></div>
					  <div class="sum_in">
						<ul>
						      <li><a href="#">History</a></li>
						      <li><a href="#">Deposites</a></li>
						      <li><a href="#">Withdraval</a></li>
						      <li><a href="#">Profile</a></li>
						</ul>
					  </div>
				    </div>
				    <h4>Your Leagues</h4>
				    <div class="leagues_box clearfix">
					  <div class="lea_in"><p>masterleague 200</p> <span>422$</span></div>
					  <div class="lea_in"><p>masterleague 200</p> <span>422$</span></div>
					  <div class="lea_in"><p>masterleague 200</p> <span>422$</span></div>
				    </div>
				    <h4>Bonus</h4>
				    <a href="#">
					  <img src="common/images/_temp/banner_bonus.png" alt="" />
				    </a>
			      </div>
			</div>
			
     
<?php include('template/footer.php'); ?>