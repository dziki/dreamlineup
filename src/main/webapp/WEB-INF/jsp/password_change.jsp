<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />
<style>
#passchange td {
	padding: 5px;
}
</style>
                        <div class="main_content">
                              <div class="left_c">
                                    <h2 class="head_title"><b><spring:message code="auth.password_recovery" /></b></h2>
                                    <div class="page_box">
                                          <div class="normal_page">
<form:form method="Post" action="change.html" commandName="passchange">
<table id="passchange">
<tr><td><spring:message code="auth.password" />:<br /><FONT color="red"><form:errors path="password" /></FONT></td><td><form:password path="password" /></td></tr>
<tr><td><spring:message code="auth.repeat_password" />:<br /><FONT color="red"><form:errors path="passwordRep" /></FONT></td><td><form:password path="passwordRep" /></td></tr>
</table>
<input style="width: 150px;" name="submit" type="submit"
					value="<spring:message code="auth.change" />" />

</form:form>
</div>
                                    </div>

                              </div>
<jsp:include page="right.jsp" />
                        </div>

<jsp:include page="footer.jsp" />