<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp" />
<jsp:include page="top.jsp" />

                        <div class="main_content">
                              <div class="left_c">
                                    <h2 class="head_title"><b><spring:message code="auth.password_recovery" /></b></h2>
                                    <div class="page_box">
                                          <div class="normal_page">
<form:form method="Post" action="remind/send.html" commandName="spclink">
<spring:message code="auth.email" />: &nbsp; <form:input path="email" /><br />
<br />
<input style="width: 150px;" name="submit" type="submit"
					value="<spring:message code="auth.recover" />" />

</form:form>
</div>
                                    </div>

                              </div>
<jsp:include page="right.jsp" />
                        </div>

<jsp:include page="footer.jsp" />