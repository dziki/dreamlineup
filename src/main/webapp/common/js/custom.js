var lastPos = '';
var lastTeamId = '';
var lastPage = 0;
var moreResults = false;
var team = null;
var bWait = false;

function formatPounds(nStr)
{
	x1 = parseInt(nStr) + ''; 
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ' ' + '$2');
	}
	return x1
}

function formatCurrency(nStr)
{
	  nStr = nStr.toFixed(2);
	nStr += '';
	x = nStr.split('.');
	x1 = formatPounds(x[0]);
	x2 = x.length > 1 ? ',' + x[1] : '';
	return x1 + x2;
}

function organizeTeam(team){
	var athletes = team["virtualAthletes"];
	var formation = team["formation"];
	var posMap = {};
	posMap["G"] = 1;
	posMap["D"] = 2;
	posMap["M"] = posMap["D"] + parseInt(formation.substr(0,1));
	posMap["S"] = posMap["M"] + parseInt(formation.substr(2,1));

	for (var i = 1; i<=11; i++){
		$("#pl" + i).html('');
		$("#pl" + i).css("margin-bottom","5px")
	}

	$.each(posMap, function(k, v) {
		var pos = parseInt(v)-1;
		if (pos)
			$("#pl" + pos).css("margin-bottom","15px")
	});
	
	team["virtualAthletes"] = {};

	$.each(athletes, function(i, el) {
		addAthlete(team, el);
	});
}

function getTeamPrice(team){
	price = 0;
	$.each(team["virtualAthletes"], function(i, el) {
		price = price + parseFloat(el["virtualLeagueAthlete"]["price"]);
	});
	return price;
}

function updateStatus(team){
	if (leagueStarted){
		$(".team_status").removeClass("status_incomplete");
		$(".team_status").addClass("status_complete");
		$(".team_status").html(i18nstrings['draft.in_play']);
	}
	else if (Object.keys(team["virtualAthletes"]).length < 11){
		$(".team_status").removeClass("status_complete");
		$(".team_status").addClass("status_incomplete");
		$(".team_status").html(i18nstrings['draft.incomplete']);
	}
	else {
		$(".team_status").removeClass("status_incomplete");
		$(".team_status").addClass("status_complete");
		$(".team_status").html(i18nstrings['draft.complete']);
	}
}

function updateMoney(team){
	var price = parseInt(getTeamPrice(team));
	$("#moneySpent").html(toMlnRepresentation(price) + " mln PLN");
	$("#moneyAvailable").html(toMlnRepresentation(vlbudget - price) +" mln PLN");
}

function addAthlete(team, vltAthlete){
	var formation = team["formation"];
	var posMap = {};
	posMap["G"] = 1;
	posMap["D"] = 2;
	posMap["M"] = posMap["D"] + parseInt(formation.substr(0,1));
	posMap["S"] = posMap["M"] + parseInt(formation.substr(2,1));

	var athleteNr = vltAthlete["virtualLeagueAthlete"]["id"];
	$("#hireAthlete" + athleteNr).css('display','none');
	
	var plPos = vltAthlete["virtualLeagueAthlete"]["athlete"]["position"];
	for (var p = posMap[plPos]; p<=11;p++){
		if (!team["virtualAthletes"][p]){
			team["virtualAthletes"][p] = vltAthlete;
			showTeamAthlete(vltAthlete,p);
			break;
		}
	}
	updateMoney(team);
	updateStatus(team);
}

function removeAthlete(team,athleteNr){
	var id = team["virtualAthletes"][athleteNr]["virtualLeagueAthlete"]["id"];
	delete team["virtualAthletes"][athleteNr];
	$("#pl"+athleteNr).html('');
	updateMoney(team);
	updateStatus(team);
	$("#hireAthlete" + id).attr('style',null);
}

function showTeamAthlete(vTeamAthlete, pos){
  var el = $("#teamAthleteTmpl").clone();
  var val = el.html();
  var vathlete = vTeamAthlete["virtualLeagueAthlete"];
  el.attr('id','fireAthlete' + vTeamAthlete["id"]);
  el.css('display','block');
  
  var name = vathlete["athlete"]["firstName"] + " " + vathlete["athlete"]["lastName"];
  val = val.replace('{{FULL_NAME}}', name);
  val = val.replace('{{OUT_ID}}', vathlete["athlete"]["outsideId"]);
  val = val.replace('{{TEAM_NAME}}', vathlete["athlete"]["team"]["name"]);
  val = val.replace('{{COST}}', toMlnRepresentation(vathlete["price"]) + " mln");
	  
//  el.html(val);
  $("#pl" + pos).html(val);
//  $("#pick_list").append(el);
}


function alertBox(title,body){
	  $("<div class='modal_bg' />").insertBefore(".modal_window:first");
	  $("#alert_title").html(title);
	  $("#alert_body").html(body);
	  $("#alert_m").css('top',( $(window).height() - $("#alert_m").height()) / 2  + $(window).scrollTop()).show();
}

function toMlnRepresentation (v){
	return (v/1000000).toFixed(2).replace(".",",");
}

 $(document).ready(function() {
	  $(".row_one").mouseenter(function(event){
		  $(this).addClass("row_one-hover");
	  });
	  $(".row_one").mouseleave(function(event){
		  $(this).removeClass("row_one-hover");
	  });
	  $(".lea_in").mouseenter(function(event){
		  $(this).addClass("lea_in-hover");
	  });
	  $(".lea_in").mouseleave(function(event){
		  $(this).removeClass("lea_in-hover");
	  });
	  
	  
	 
  $("#date").datepicker();
  
  $("#register_form").validate();
  
  $("body").on("click", ".submit_form", function(event){
	  $("#register_form input").attr('focus');
  });
  
  $(".sl_box").cycle({ 
		fx:     'fade', 
		next:   '.next_ar', 
		prev:   '.prev_ar',
		pager:  '.sl_navi' 
	});

  
  
  function filterPlayers(team,position){
	  return filterPlayersPage(team,position,0);
  }
  function filterPlayersPage(team,position,page){
	  bWait = true;
	  $.getJSON(SERVER_PREFIX + 'json/leagues/' + leagueId + '/athletes.html?teamId=' + team + '&pos=' + position + '&page=' + page, function(data) {
		  bWait = false;
		  var items = [];

		  moreResults = (data.length > 0);
		  
		  $.each(data, function(i, el) {
			  var addAthleteButton = '<td class="corner2"><span><a class="mini_add" href="#">' + i18nstrings['draft.add'] + '</a></span></td>';
			  if (leagueStarted)
				  addAthleteButton = '<td class="corner2">&nbsp;</td>';
		    items.push('<tr class="row_one hireAthlete" id="hireAthlete' + el["id"] + '">' +
		      '<td width="140" class="corner1"><span>'+ el["athlete"]["lastName"] + ' ' + el["athlete"]["firstName"] +'</span></td>' +
		      '<td width="12">'+ i18nposition[el["athlete"]["position"]] + '</td>' +
		      '<td width="150">'+ el["athlete"]["team"]["name"] + '</td>' +
		      '<td width="65">'+ toMlnRepresentation(el["price"]) + ' mln</td>' +
		      addAthleteButton +
		'</tr>');
		  });

		  if (page == 0)
			  $('table#athlete_list').html(items.join(''));
		  else
			  $('table#athlete_list').append(items.join(''));

		  lastTeamId = team;
		  lastPos = position;
		  lastPage = page;
		});
	  
	  return false;
  }
  
  $("body").on("click", "tr.hireAthlete", function(event){
	  if (!leagueStarted){
	  var athleteId = $(this).attr('id').substr("hireAthlete".length);
	  $.getJSON(SERVER_PREFIX + 'json/team/' + teamId + '/athlete/' + athleteId +'/hire.html', function(data) {
		  if (data["status"] == "ERROR")
			  alertBox(i18nstrings['draft.cant_hire'],data["message"]);
		  if (data["status"] == "OK") {
			  addAthlete(team,data["vathlete"]);
		  }
	  });
	  }
  });
  $("body").on("click", "a.fireAthlete", function(event){
	  if (!leagueStarted){
	  var athleteNr = parseInt($(this).attr('id').substr("pl".length));
	  var athlete = team["virtualAthletes"][athleteNr];
	  if (!athlete) return;
	  var athleteId = athlete["id"];
	  var itemId = $(this).attr('id');
	  $.getJSON(SERVER_PREFIX + 'json/team/' + teamId + '/athlete/' + athleteId +'/fire.html', function(data) {
		  if (data["status"] == "ERROR")
			  alertBox(i18nstrings['draft.cant_fire'],data["message"]);
		  if (data["status"] == "OK"){
			  removeAthlete(team,athleteNr);
		  }
	  });
  	}
  });

  $("body").on("click", "a.selectTeam", function(event){
	  var teamId = $(this).attr('href').substr(1);
	  $(".selectTeam").parent().removeClass("active_li");
	  $("#teamFilter"+teamId).parent().addClass("active_li");
	  $(".all_t").html($("#teamFilter"+teamId).html());
	  $(".teams_toottip").hide();
	  $('.modal_bg2').hide();
	  filterPlayers(teamId,lastPos);
  });
  $("body").on("click", "a.selectPosition", function(event){
	  var pos = $(this).attr('href').substr(1);
	  $(".selectPosition").removeClass("active");
	  $("#posFilter"+pos).addClass("active");
	  filterPlayers(lastTeamId,pos);
  });

  
  $("body").on("click", "a.modal", function(event){
	  var open = $(this).attr('href');
	  $("<div class='modal_bg' />").insertBefore(".modal_window:first");
	  $(open).show();
	  var h = ( $(window).height() - $(open).height()) / 2;
	  if (h < 30) h = 30;
	  
	  h += $(window).scrollTop();
	  
	  $(open).css('top',h);
	  return false;
  });
  
  $("body").on("click", "div.modal_bg", function(event){
	  $(this).remove();
	  $('.modal_window').hide();
  });
  
  $("body").on("click", "div.modal_bg2", function(event){
	  $(this).remove();
	  $(".player_toottip").hide();
	  $(".teams_toottip").hide();
	   
  });
  
    $("body").on("click", ".close_tip", function(event){
	  $(".teams_toottip").hide();
	  $('.modal_bg2').hide();
	   
  });
  
  
  
  
  $("body").on("click", "a.close_m", function(event){
	  $('.modal_window').hide();
	  $('div.modal_bg').remove();
  });
  
  
  
  $(".pp_header").on("click", "li a", function(event){
    $('.pp_header li a').removeClass('active_tab');
    $(this).addClass('active_tab');
    $('.p_co').hide();
    var tab = $(this).attr('href');
    $(tab).show();
    return false;
	  
  });
  
  $(".chp_header").on("click", "li a", function(event){
    $('.chp_header li a').removeClass('active_tab');
    $(this).addClass('active_tab');
    $('.chp_content').hide();
    var tab = $(this).attr('href');
    $(tab).show();
    return false;
	  
  });
  
  
  function updateFormation(data) {
	  if (data["status"] == "ERROR")
		  alertBox(i18nstrings['draft.cant_change_formation'],data["message"]);
	  if (data["status"] == "OK"){
			$('.field_nav li ').removeClass('active_f');
		    $("#f" + data["formation"]).addClass('active_f');
		    $('.formation').removeClass().addClass('formation');
		    $('.formation').addClass("f" + data["formation"]);
		    team["formation"] = data["formation"];
		    organizeTeam(team);
	  }
  }
  
  
  $(".field_nav").on("click", "li", function(event){
	  var formation = $(this).attr("id").substr(1);
	  $.getJSON(SERVER_PREFIX + 'json/team/' + teamId + '/formation/' + formation +'.html', updateFormation);

	  
//    var tab = $(this).attr('id');
//    $('.'+tab).show();
    return false;
	  
  });
  
  
  
  $(".formation").on("click", "a", function(event){
	  $("<div class='modal_bg2' />").insertBefore(".player_toottip");
	  $(".player_toottip").show();
	  offset = $(this).offset();
	  var pname = $(this).attr('title');
	  var pgroup = $(this).attr('rel');
	  var topx = offset.top;
	  var leftx = offset.left;
	  $(".player_toottip").css('left',leftx);
	  $(".player_toottip").css('top',topx);
	  $(".player_toottip .pt_cont").html('');
	  $(".player_toottip .pt_cont").append("<h3>"+pgroup+"</h3>");
	  $(".player_toottip .pt_cont").append("<ul>");
	  
	  $(this).parent().children('a[rel="'+pgroup+'"]').each(function(index) {
	  var names = $(this).attr('title');
	  $(".player_toottip .pt_cont ul").append("<li><a href='#'>"+names+"</a></li>");
	  });
	  
	  return false;
  });
  
  
  $(".players_header").on("click", ".all_t", function(event){
	  $("<div class='modal_bg2' />").insertBefore(".teams_toottip");
	  offset = $(".all_t").offset();
	  var pname = $(this).attr('title');
	  var pgroup = $(this).attr('rel');
	  var topx = offset.top;
	  var leftx = offset.left + $(".all_t").width();
	  $(".teams_toottip").css('left',leftx);
	  $(".teams_toottip").css('top',topx);
	  $(".teams_toottip").show();
	  
	  return false;
  });
  
  function parseDate(input) {
	  var parts = input.split(' ');
	  var date = parts[0];
	  var time = parts[1];
	  parts = date.split('-');
	  var parts2 = time.split(':');
	  return new Date("20" + parts[0], parts[1]-1, parts[2], parts2[0], parts2[1]); // months are 0-based
  }
  
  var nowDate = new Date(); 
    
  $('.countDown').each(function(cell){
	  var d = parseDate($( this ).html());
//	  alert(d);
	  
	  if (nowDate.getTime() > d.getTime() - LAST_CHANGE_MINUTES*60*1000)
		  $( this ).text(i18nstrings['leagues.started']);
	  else if (nowDate.getTime() > d.getTime() - DEADLINE_MINUTES*60*1000)
		  $( this ).text(i18nstrings['leagues.reg_closed']);
	  else
		  $( this ).countdown({until: d}); 
	  newYear = d;
  });
  
  //when scroll
  $(".table_mask").scroll(function(){
      if ($(".table_mask").scrollTop() > $(".table_mask").get(0).scrollHeight - 1500){
    	  if (moreResults && !bWait)
    		  filterPlayersPage(lastTeamId,lastPos,lastPage + 1);
      } else {
      }

  });
});

