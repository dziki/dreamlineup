#parse( "velocity/head.vm" )
<h3 style="font-size: 19px;">UWAGA: Drużyna nieaktywna</h3>
<div style="font-size: 14px;">
Jedna z Twoich drużyn nie spełnia kryteriów ligi.
Aby była dopuszczona do rozgrywek musisz ją uaktualnić najpóźniej na 5 minut przed rozpoczęciem ligi.
W przeciwnym razie Twoja drużyna nie weźmie udziału w lidze i stracisz szansę na wygraną.<br />
<br />
<a style="color: #465902;" href="<c:out value="${invalidTeamDraft}"/>">Kliknij tutaj</a>, aby zobaczyć szczegóły.<br />
<br />
Jeśli powyższy link nie działa, przeklej moniższy adres do przeglądarki:<br />
<c:out value="${invalidTeamDraft}"/>
</div>
#parse( "velocity/foot.vm" )
