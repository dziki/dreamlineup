#parse( "velocity/head.vm" )

<h2 style="font-size: 19px;">Podsumowanie tygodnia</h2>
Dziękujemy za udział w grze w poprzedniej kolejce Ekstraklasy i zapraszamy do ponownej walki o nagrody na <a href="http://dreamlineup.com/">dreamlineup.com</a><br />

#if( $prizeTotal > 0.01 )
   <strong>Wygrałeś w sumie ${prizeTotal} DP</strong><br />
#end

<h3 style="font-size: 16px;">Szczegółowe statystyki</h3>
#foreach( $vt in $virtualTeams )
<a href="http://dreamlineup.com/lobby/team/${vt.id}/details.html">Drużyna ${velocityCount}</a> zdobyła ${vt.points} punktów i zajęła miejsce ${positions.get($vt.id)} 
w turnieju <a href="http://dreamlineup.com/leagues/${vt.virtualLeague.id}/details.html">"${vt.virtualLeague.title}"</a>. 
#if( ${prizes.get($vt.id)} > 0.01)
Wygrałeś ${prizes.get($vt.id)} DP.
#end
<br />
#end

#parse( "velocity/foot.vm" )
