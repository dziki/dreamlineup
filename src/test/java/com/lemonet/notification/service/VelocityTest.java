package com.lemonet.notification.service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.velocity.VelocityContext;
import org.junit.Test;

import com.lemonet.notification.domain.Email;

public class VelocityTest {
	private EmailService emailService = new EmailService();
	
	@Test
	public void testVelocityEmail() throws Exception{
		VelocityContext context = new VelocityContext();
		context.put("registrationLink","REGISTRATION LINK");
		String msg = EmailService.fillOutTemplate("velocity/registration_pl.jsp", context);
		String to = "dziki.dziurkacz@gmail.com";
		Email email = new Email(to,"Rejestracja", msg);
		emailService.send(email);
	}
}
