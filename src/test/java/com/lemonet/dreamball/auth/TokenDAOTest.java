package com.lemonet.dreamball.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.auth.domain.PlayerDAOImpl.TooYoung;
import com.lemonet.dreamball.auth.domain.Token;
import com.lemonet.dreamball.auth.domain.Token.TokenExpiredException;
import com.lemonet.dreamball.auth.domain.TokenDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/root-context.xml"})
public class TokenDAOTest {
	
	@Autowired TokenDAO tdao;
	@Autowired PlayerDAO pdao;
	@Autowired SessionFactory sessionFactory;

    @Before
    public void setUp() {

    }
    
    @Test
    public void findToken() throws TooYoung, TokenExpiredException{
    	Player p = new Player();
    	p.setFirstName("Test1");
    	p.setLastName("Test2");
    	p.setEmail("test@test.pl3");
    	p.setActive(false);
    	pdao.create(p);
    	
    	Token token = new Token(p);
    	tdao.create(token);
    	
    	Token rev = tdao.find(token.getToken());
    	assertNull(tdao.find("THERE_IS_NO_SUCH_TOKEN"));
    	assertEquals(rev.getPlayer().getEmail(),"test@test.pl3");
    }

}
