package com.lemonet.dreamball.auth;

import java.util.Calendar;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.auth.domain.PlayerDAOImpl;
import com.lemonet.dreamball.auth.domain.PlayerDAOImpl.TooYoung;
import com.lemonet.dreamball.auth.domain.TokenDAO;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/root-context.xml"})
public class PlayerDAOTest {
    @Autowired PlayerDAO pdao;
    @Autowired TokenDAO tdao;

    @Before
    public void setUp() {
    }
    
    @Test
    public void create() throws TooYoung{
        Player p;
    	// CREATE
    	p = new Player();
    	p.setFirstName("Test1");
    	p.setLastName("Test2");
    	p.setEmail("test@test.pl35");
    	p.setPassword("TestTest123");
    	Calendar birthDate = Calendar.getInstance();
    	birthDate.add(Calendar.YEAR, -20);
    	p.setBirthDate(birthDate.getTime());
    	p.setUsername("Wojtek");
    	p.setActive(false);
    	pdao.create(p);

    	assertNotNull(pdao.findByEmailOrUsername("test@test.pl35"));
    	assertNull(pdao.findByEmailOrUsername("test@test.pl25"));

    	// UPDATE
    	p.setFirstName("Test25");
    	p.setLastName("Test2");
    	p.setEmail("test@test.pl25");
    	p.setActive(true);
    	pdao.update(p);

    	// READ
    	assertNull(pdao.findByEmailOrUsername("test@test.pl35"));
    	Player p1 = pdao.findByEmailOrUsername("test@test.pl25");
    	assertNotNull(p1);
    	
    	Player p2 = pdao.getPlayerById(p1.getId());
    	assertNotNull(p2);
    	assertEquals(p1.getEmail(),p2.getEmail());
    	
	    p2 = pdao.loginPlayer(p.getEmail(), p.getPassword());
    	assertNotNull(p2);
    	assertEquals(p1.getEmail(),p2.getEmail());
    	
    	assertTrue(pdao.list().size() > 0);
    	
    	// TODO: We actually want to register only players with valid pass etc.
    }

    @Test
    public void activate() throws TooYoung{
    	
    }

}
