package com.lemonet.dreamball.finance;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.auth.domain.PlayerDAOImpl.TooYoung;
import com.lemonet.dreamball.finance.domain.Operation;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/root-context.xml"})
public class OperationDAOTest {
	@Autowired OperationDAO odao;
	@Autowired PlayerDAO pdao;

	@SuppressWarnings("deprecation")
	@Before
    public void setUp() throws TooYoung {
        
    	// CREATE
    	Player p = new Player();
    	p.setFirstName("Test1");
    	p.setLastName("Test2");
    	p.setEmail("test@test.pl-operationDao");
    	p.setPassword("test@test.pl-operationDao");
    	p.setActive(false);
    	Calendar cal = Calendar.getInstance();
    	cal.set(1975, 12, 2);
    	p.setBirthDate(cal.getTime());
    	pdao.create(p);
    }
	    
	@Test
    public void create() {
		Player p = pdao.findByEmailOrUsername("test@test.pl-operationDao");
		Operation o = new Operation(0d, Operation.DEPOSIT, p);
		
		try {
			odao.save(o);
		} catch (NotEnoughMoneyException e) {
			Assert.fail("NotEnoughMoneyException");
		}
		
		o.setValue(10d);
		try {
			odao.update(o);
		} catch (NotEnoughMoneyException e) {
			Assert.fail("NotEnoughMoneyException");
		}
		
		assertTrue(Math.abs( odao.getAccountBalance(p) - 10d ) < 0.01d);
		
		o = new Operation(-9d, Operation.BUYIN, p);
		try {
			odao.save(o);
		} catch (NotEnoughMoneyException e) {
			Assert.fail("NotEnoughMoneyException");
		}
		assertTrue(Math.abs( odao.getAccountBalance(p) - 1d ) < 0.01d);
		
		// in-play balance is computed as a sum of buyins in unfinished leagues
		// thus in this case = 0
		assertTrue(Math.abs( odao.getInPlayBalance(p) ) < 0.01d);
		
    }
	    
}
