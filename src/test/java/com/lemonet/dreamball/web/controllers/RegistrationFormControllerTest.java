package com.lemonet.dreamball.web.controllers;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.Token;
import com.lemonet.dreamball.auth.domain.TokenDAO;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.service.AuthenticationService;
import com.lemonet.dreamball.web.RegistrationValidator;
import com.lemonet.notification.service.EmailService;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.mockito.runners.MockitoJUnitRunner;  

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/root-context.xml"})
public class RegistrationFormControllerTest {
	@Mock private RegistrationValidator registrationValidatior;
	@Mock private MessageSource messageSource;
	@Mock private EmailService emailService;
	@Mock OperationDAO odao;
	@Mock TokenDAO tdao;
	@Mock AuthenticationService authenticationService;

    @InjectMocks private RegistrationFormController registrationFormController;

	private MockMvc mockMvc;
    
    @Before
    public void setUp() {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
 
        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(registrationFormController).build();
        
        when(messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenReturn("ok");
        when(messageSource.getMessage(anyString(), any(Object[].class), anyString(), any(Locale.class))).thenReturn("ok");
        when(messageSource.getMessage(eq("auth.registration_error"), any(Object[].class), anyString(), any(Locale.class))).thenReturn("error");
    }        

    @Test
    public void registration() throws Exception{
    	ResultActions result;
    	result = mockMvc.perform(get("/auth/registration.html"));
        result.andExpect(status().isOk());
        
        assertNotNull(authenticationService);
        
    	result = mockMvc.perform(post("/auth/registration.html")
    			.param("firstName", "TestFirstName")
    			.param("lastName", "TestLastName")
    			.param("day", "12")
    			.param("month", "2")
    			.param("year", "1986")
    			.param("email", "dziki.dziurkacz@gmail.com")
    			.param("password", "Qwer!234")
    			.param("confirmPassword", "Qwer!234")
    			.param("confirmTerms", "1"));
        result.andExpect(status().isMovedTemporarily());
        result.andExpect(view().name("redirect:/auth/success.html"));
    }

    @Test
    public void success() throws Exception{
    	ResultActions result;
    	result = mockMvc.perform(get("/auth/success.html"))
    			.andExpect(status().isOk())
    			.andExpect(view().name("static"))
    			.andExpect(model().attributeExists("content"))
    			.andExpect(model().attributeExists("head"));
    }

    @Test
    public void activate() throws Exception{
    	Player p = new Player();
    	p.setFirstName("Test1");
    	p.setLastName("Test2");
    	p.setEmail("test@test.pl3");
    	Token token = new Token(p);
    	tdao.create(token);
    	
    	ResultActions result;
    	result = mockMvc.perform(get("/auth/" + token.getToken() + "/activate.html"))
    			.andExpect(status().isOk())
    			.andExpect(view().name("static"))
    			.andExpect(model().attribute("head", "ok"));
    }

}
