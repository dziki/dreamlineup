package com.lemonet.dreamball.dreamworld;

import java.util.Calendar;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.lemonet.dreamball.auth.domain.Player;
import com.lemonet.dreamball.auth.domain.PlayerDAO;
import com.lemonet.dreamball.auth.domain.PlayerDAOImpl.TooYoung;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeague;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthlete;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueAthleteDAO;
import com.lemonet.dreamball.dreamworld.domain.VirtualLeagueDAO;
import com.lemonet.dreamball.finance.domain.Operation.NotEnoughMoneyException;
import com.lemonet.dreamball.finance.domain.OperationDAO;
import com.lemonet.dreamball.realworld.domain.Athlete;
import com.lemonet.dreamball.realworld.domain.AthleteDAO;
import com.lemonet.dreamball.realworld.domain.League;
import com.lemonet.dreamball.realworld.domain.Team;
import com.lemonet.dreamball.realworld.domain.TeamDAO;
import com.lemonet.dreamball.service.DreamworldService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/root-context.xml"})
public class VirtualLeagueDAOTest {
	@Autowired OperationDAO odao;
	@Autowired PlayerDAO pdao;
	@Autowired VirtualLeagueAthleteDAO vladao;
	@Autowired AthleteDAO adao;
	@Autowired VirtualLeagueDAO vldao;
	@Autowired TeamDAO tdao;
	
	@Autowired DreamworldService dreamworldServivce;

	@Before
    public void setUp() throws TooYoung {
    }
	
	@Test
	public void create() throws NotEnoughMoneyException {
		Player p = pdao.getPlayerById(new Long(1));
		VirtualLeague vl = new VirtualLeague();
		vl.setBudget(15000000);
		vl.setBuyIn(10);
		vl.setNumOfPlayers(10L);
		vl.setDuration(1);
		vl.setStatus(VirtualLeague.READY);
		vl.setOwner(p);
		vl.setSeason(2013L);
		vl.setStartingRound(22);

		dreamworldServivce.createLeague(p, vl);
	}

}
