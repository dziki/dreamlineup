import urllib2
import pycurl
import cStringIO
import codecs

def download_page_loop(url,post=None):
    url = str(url)
    buf = cStringIO.StringIO()
    c = pycurl.Curl()
    c.setopt(pycurl.USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0')
    c.setopt(c.URL, url)
    c.setopt(c.WRITEFUNCTION, buf.write)
    c.setopt(c.COOKIEFILE, 'cookie.txt')
    c.setopt(pycurl.COOKIEJAR, 'cookie.txt')
    if post:
        c.setopt(c.POSTFIELDS, post)
    c.perform()
    dane = buf.getvalue()
    buf.close()
    return dane

# download a page if it is not in cache
def download_page(url,cache_name=None,post=None):
    strona = None
    save = False

    # if possible cache_name given then try to load it from cache
    if cache_name:
        try:
            f = codecs.open('cache/' + cache_name, 'r', 'utf-8')
            strona = f.read()
            f.close()
        except:
            pass

    # if the page is not found in cache then try to download it
    while not strona:
        strona = download_page_loop(url, post=post)

        # if possible cache_name is given then save the page
        if strona and cache_name:
            f = codecs.open('cache/' + cache_name, 'w')
            f.write(strona)
            f.close()
    
    return strona

