# -*- coding: UTF-8 -*-
import re
import urllib2
import pycurl
import cStringIO
from bs4 import BeautifulSoup


def download_page_loop(url,post=None):
    url = str(url)
    buf = cStringIO.StringIO()
    c = pycurl.Curl()
    c.setopt(pycurl.USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0')
    c.setopt(c.URL, url)
    c.setopt(c.WRITEFUNCTION, buf.write)
    c.setopt(c.COOKIEFILE, 'cookie.txt')
    c.setopt(pycurl.COOKIEJAR, 'cookie.txt')
    if post:
        c.setopt(c.POSTFIELDS, post)
    c.perform()
    dane = buf.getvalue()
    buf.close()
    return dane

def download_page(url,cache_name=None,post=None):
    strona = ""
    while len(strona) < 10:
        strona = download_page_loop(url,post=post)
        
    f = open('cache' + cache_name, 'w')
    if cache_name:
        f.write(strona)
    
    return strona

class Team():
    id = ''
    nazwa=''
druzyny_result = []

class Athlete():
    id = ''
    id_druzyny = ''
    dane_personalne = []
athletes_result = []

class Match():
    id = ''
    druzyna1_id = ''
    druzyna2_id = ''
    data = ''
    result = ''
matches_results = []


class StatisticsAthletea():
    id_meczu = ''
    id_zawodnika = ''
    id_druzyny = ''
    shirtnumber = ''
    gole = 0
    gole_czerwone = 0
    gole_p = 0
    substituted = 0
    zolta = 0
    czerwona = 0

statistics_results = []



# POBIERANIE DRUZYN
strona = download_page('http://us.soccerway.com/national/poland/ekstraklasa/20132014/regular-season/r21498/tables/').replace('\n\n','')
tabele = re.search(re.compile('<form action="/teams/comparison/" method="get" accept-charset="utf-8">(.*?)</form>',re.DOTALL),strona).group(1)
tabele_soup = BeautifulSoup(tabele)


druzyny = tabele_soup.findAll('td', {'class':'text team large-link'})
for druzyna in druzyny:
    d = Team()
    d.id =  druzyna.find('a').get('href').split('/')[-2]
    d.nazwa = druzyna.find('a').get('title')
    druzyny_result.append(d)
    print "Parsuje dane druzyny id",d.id

    athletes = download_page("http://us.soccerway.com"+druzyna.find('a').get('href'))
    compiled = re.search(re.compile('<div class="squad-container">(.*?)<div style="margin-bottom: 2em;"></div>',re.DOTALL),athletes)
    if compiled == None:
        continue
    athletes = compiled.group(1)
    athletes = BeautifulSoup(athletes)
    for zawodnik in athletes.findAll('td',{'style':'width:50px;'}):
        # id zawodnika
        print "Parsuje dane zawodnika id",zawodnik.find('a').get('href').split('/')[-2]
        zawodnik_wpis = Athlete()
        zawodnik_wpis.id=zawodnik.find('a').get('href').split('/')[-2]
        zawodnik_wpis.id_druzyny = d.id
        #tracking coacha
        if zawodnik.find('a').get('href').split('/')[-4] == 'coaches':
            continue

        strona_zawodnika = download_page('http://us.soccerway.com'+zawodnik.find('a').get('href'))
        if re.search(re.compile('<h2>Passport</h2>(.*?)<h2>Career</h2>',re.DOTALL),strona_zawodnika):
            zawodnik = re.search(re.compile('<h2>Passport</h2>(.*?)<h2>Career</h2>',re.DOTALL),strona_zawodnika).group(1)
            zawodnik = BeautifulSoup(zawodnik)
            zawodnik = zawodnik.find('dl')
            labels = zawodnik.findAll('dt')
            values =  zawodnik.findAll('dd')
            for nr, label in enumerate(labels):
                zawodnik_wpis.dane_personalne.append([label.text,values[nr].text])
            athletes_result.append(zawodnik_wpis)




# TRACKING MECZY:



print "Parsuje dane meczy"
matches_strona = download_page('http://us.soccerway.com/national/poland/ekstraklasa/20132014/regular-season/r21498/matches/')



matches = re.search(re.compile('<table class="matches   ">(.*?)</table>',re.DOTALL),matches_strona).group(1)
matches = BeautifulSoup(matches)
for mecz in matches.find('tbody').findAll('tr'):
    mecz_wpis = Match()
    mecz_wpis.id = mecz.find('a',{'title':'More info'}).get('href').split('/')[-2]
    print 'Parsuje mecz o id',mecz_wpis.id


    mecz_wpis.druzyna1_id = mecz.find('td',{'class':'team team-a '}).find('a').get('href').split('/')[-2]
    mecz_wpis.druzyna2_id = mecz.find('td',{'class':'team team-b '}).find('a').get('href').split('/')[-2]
    mecz_wpis.data = mecz.find('td',{ 'class':'date no-repetition'}).text
    
    if mecz.find('td',{'class':'score-time score'}):
        mecz_wpis.result= mecz.find('td',{'class':'score-time score'}).text.replace('  ','').replace('\n','')

        # pobieranie szczegolow na temat meczu: statistics zawodnikow.
        strona_meczu_szczegolowa = download_page("http://us.soccerway.com" + mecz.find('a',{'title':'More info'}).get('href'))
        strona_meczu_szczegolowa = BeautifulSoup(strona_meczu_szczegolowa)


        print "pobieram results"
        results_meczu = strona_meczu_szczegolowa.find('div',{'class':'block  clearfix block_match_lineups-wrapper'})
        for nr_teamu, table in enumerate(results_meczu.findAll('table')):
            print "parsuje zawodnikow druzyny"

            for zawodniktr in table.findAll('tr'):

                # Jezeli coach:
                if zawodniktr.find('strong') or zawodniktr.find('th'):
                    continue
                zawodnik = StatisticsAthletea()
                zawodnik.id_meczu = mecz.find('a',{'title':'More info'}).get('href').split('/')[-2]
                if not zawodniktr.find('a'):
                    continue
                zawodnik.id_zawodnika = zawodniktr.find('a').get('href').split('/')[-2]
                if nr_teamu == 0:
                    zawodnik.id_druzyny = strona_meczu_szczegolowa.findAll('h3', {'class':'thick'})[0].find('a').get('href').split('/')[-2]
                else:
                    zawodnik.id_druzyny = strona_meczu_szczegolowa.findAll('h3', {'class':'thick'})[2].find('a').get('href').split('/')[-2]
                zawodnik.shirtnumber = zawodniktr.find('td',{'class':'shirtnumber'}).text

                if zawodniktr.find('img',src='http://s1.swimg.net/gsmf/453/img/events/SO.png'):
                    zawodnik.substituted = 1

                for zolta in zawodniktr.findAll('img',src='http://s1.swimg.net/gsmf/453/img/events/YC.png'):
                    zawodnik.zolta += 1

                for zoltaczerwona in zawodniktr.findAll('img',src='http://s1.swimg.net/gsmf/453/img/events/Y2C.png'):
                    zawodnik.zolta += 1
                    zawodnik.czerwona += 1

                for czerwona in zawodniktr.findAll('img',src='http://s1.swimg.net/gsmf/453/img/events/RC.png'):
                    zawodnik.czerwona += 1
                    

                for gol in zawodniktr.findAll('img',src='http://s1.swimg.net/gsmf/453/img/events/G.png'):
                    zawodnik.gole += 1
                for golp in zawodniktr.findAll('img',src='http://s1.swimg.net/gsmf/453/img/events/PG.png'):
                    zawodnik.p += 1
                for golcz in zawodniktr.findAll('img',src='http://s1.swimg.net/gsmf/453/img/events/OG.png'):
                    zawodnik.gole_czerwone += 1

                statistics_results.append(zawodnik)
        matches_results.append(mecz_wpis)

                # Debug zawodnika:
                # print 'id zawodnika', zawodnik.id_zawodnika, 'id meczu', zawodnik.id_meczu, 'id druzyny',zawodnik.id_druzyny, 'zoltych', zawodnik.zolta, 'czerwonych', zawodnik.czerwona, 'gole', zawodnik.gole, 'gole czerw', zawodnik.gole_czerwone, 'gole P', zawodnik.gole_p




    print '\n'







print "Zapisuje do dane.xml"
xml = open("dane.xml", "w")
xml.write('<?xml version="1.0" encoding="UTF-8"?>\n<pobrano>\n')

xml.write("<druzyny>\n")
for druzyna in druzyny_result:
    xml.write('<druzyna><id>%s</id><nazwa>%s</nazwa></druzyna>\n' % (druzyna.id,druzyna.nazwa.encode('UTF-8')))
xml.write('</druzyny>\n')


xml.write("<athletes>\n")
for zawodnik in athletes_result:
    xml.write('<z><id>%s</id><id_druzyny>%s</id_druzyny><dane>' % (zawodnik.id,zawodnik.id_druzyny))
    for dana in zawodnik.dane_personalne:
        xml.write('<d><n>%s</n><w>%s</w></d>' % (dana[0].encode('UTF-8'),dana[1].encode('UTF-8')))
    xml.write('</dane></z>\n')
xml.write('</athletes>\n')

xml.write('<matches>\n')
for mecz in matches_results:
    xml.write('<mecz><id>%s</id><druzyna1_id>%s</druzyna1_id><druzyna2_id>%s</druzyna2_id><data>%s</data><result>%s</result></mecz>\n' % (mecz.id,mecz.druzyna1_id,mecz.druzyna2_id,mecz.data,mecz.result))
xml.write('</matches>\n')

xml.write('<statistics_zawodnikow>\n')
for s in statistics_results:
    xml.write('<statystyka><id_meczu>%s</id_meczu><id_zawodnika>%s</id_zawodnika><id_druzyny>%s</id_druzyny><shirtnumber>%s</shirtnumber><gole>%s</gole><gole_czerwone>%s</gole_czerwone><gole_p>%s</gole_p><substituted>%s</substituted><zolta>%s</zolta><czerwona>%s</czerwona></statystyka>' % (s.id_meczu,s.id_zawodnika,s.id_druzyny,s.shirtnumber,s.gole,s.gole_czerwone,s.gole_p,s.substituted,s.zolta,s.czerwona))
xml.write('</statistics_zawodnikow>\n')
xml.write('</pobrano>')

xml.close()
print "Zapisano do XML."
