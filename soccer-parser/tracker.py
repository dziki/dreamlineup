# -*- coding: UTF-8 -*-
from export import *
from parsers import *
import sys, getopt, os

def main(argv):
    matchid = None
    directory = None
    matchids = None
    try:
        opts, args = getopt.getopt(argv,"hm:d:",["matchid=","directory="])
    except getopt.GetoptError:
        print 'tracker.py -m <matchid> -d <directory>'
        sys.exit(2)
        
    for opt, arg in opts:
        if opt == '-h':
            print 'tracker.py -m <matchid> -d <directory>'
            sys.exit()
        elif opt in ("-m", "--matchid"):
            matchid = arg
        elif opt in ("-d", "--directory"):
            directory = arg
        print 'Match id is', matchid

#    matchids = None
    if matchid:
        matchids = [matchid,]

    if not (directory is None):
        os.chdir(directory)
    
    if matchid is None:
        druzyny_result, athletes_result = parse_teams()
        export_teams(druzyny_result)
        export_athletes(athletes_result)

    matches_results, statistics_results = parse_matches(matchids)
    export_matches(matches_results)
    export_stats(statistics_results.values())
   
if __name__ == "__main__":
    main(sys.argv[1:])
