from bs4 import BeautifulSoup
import re
import urllib
import cStringIO
import os.path
from trackerutils import *
from models import *
import json

statistics_results = {}

def parse_players(athletes,team):
    athletes_result = []
    for zawodnik in athletes.findAll('td',{'style':'width:50px;'}):
        # id zawodnika
        print "Parsuje dane zawodnika id",zawodnik.find('a').get('href').split('/')[-2]
        zawodnik_wpis = Athlete()
        zawodnik_wpis.id=zawodnik.find('a').get('href').split('/')[-2]
        zawodnik_wpis.id_druzyny = team.id
        #tracking coacha
        if zawodnik.find('a').get('href').split('/')[-4] == 'coaches':
            continue

        strona_zawodnika = download_page('http://us.soccerway.com'+zawodnik.find('a').get('href'),"player-"+zawodnik_wpis.id +".html")
        if re.search(re.compile('<h2>Passport</h2>(.*?)<h2>Career</h2>',re.DOTALL),strona_zawodnika):
            zawodnik = re.search(re.compile('<h2>Passport</h2>(.*?)<h2>Career</h2>',re.DOTALL),strona_zawodnika).group(1)
            zawodnik = BeautifulSoup(zawodnik)
            img = zawodnik.find('img')
            
            zawodnik = zawodnik.find('dl')
            labels = zawodnik.findAll('dt')
            values = zawodnik.findAll('dd')
            zawodnik_wpis.dane_personalne = []
            for nr, label in enumerate(labels):
                zawodnik_wpis.dane_personalne.append([label.text,values[nr].text])
            athletes_result.append(zawodnik_wpis)

            filename = 'img/athlete-' + zawodnik_wpis.id + '.png'
            if not os.path.isfile(filename):
                f = urllib.urlopen(img['src'])    
                fh = open(filename, 'wb')
                fh.write(f.read())
                fh.close()
            
    return athletes_result

def parse_teams():
    druzyny_result = []
    athletes_result = []
    strona = download_page('http://us.soccerway.com/national/poland/ekstraklasa/20142015/regular-season/r25643/tables/','main.html').replace('\n\n','')
    tabele = re.search(re.compile('<form action="/teams/comparison/" method="get" accept-charset="utf-8">(.*?)</form>',re.DOTALL),strona).group(1)
    tabele_soup = BeautifulSoup(tabele)


    druzyny = tabele_soup.findAll('td', {'class':'text team large-link'})
    for druzyna in druzyny:
        d = Team()
        d.id =  druzyna.find('a').get('href').split('/')[-2]
        d.nazwa = druzyna.find('a').get('title')
        druzyny_result.append(d)
        print "Parsuje dane druzyny id",d.id

        athletes = download_page("http://us.soccerway.com"+druzyna.find('a').get('href'),"team-"+d.id+".html")
        compiled = re.search(re.compile('<div class="squad-container">(.*?)<div style="margin-bottom: 2em;"></div>',re.DOTALL),athletes)
        if compiled == None:
            continue
        athletes = compiled.group(1)
        athletes = BeautifulSoup(athletes)
        athletes_result.extend(parse_players(athletes,d))
                
    return (druzyny_result,athletes_result)

def sum_to_number(s):
    if s.find('+')!=-1:
        A = s.split('+')
        s = int(A[0]) + int(A[1])
    else:
        s = int(s)
    return s
    
def parse_stats(stats, mecz, nr_teamu, sub, mecz_wpis):
    for zawodniktr in stats.findAll('tr'):
        # Jezeli coach:
        if zawodniktr.find('strong') or zawodniktr.find('th'):
            continue
        zawodnik = StatisticsAthletea()
        zawodnik.id_meczu = mecz.find('a',{'title':'More info'}).get('href').split('/')[-2]
        if not zawodniktr.find('a'):
            continue
        zawodnik.id_zawodnika = zawodniktr.find('a').get('href').split('/')[-2]
        zawodnik.id_druzyny = nr_teamu
        zawodnik.shirtnumber = zawodniktr.find('td',{'class':'shirtnumber'}).text

        zawodnik.time_out = 90

        if not sub:
            zawodnik.time_in = 0
        
        if sub:
            subfor = zawodniktr.find('p',{'class':'substitute-out'})
            if subfor == None:
                continue
            
            time_in = str(subfor).split(" ")[-1][:-5]

            time_in = sum_to_number(time_in)
            zawodnik.time_in = time_in
            outid = subfor.find("a").get('href').split('/')[-2]

            statistics_results[outid].time_out = zawodnik.time_in

        if zawodniktr.find('img',src=re.compile('SO.png')):
            zawodnik.substituted = 1

        for zolta in zawodniktr.findAll('img',src=re.compile('YC.png')):
            zawodnik.zolta += 1

        for zoltaczerwona in zawodniktr.findAll('img',src=re.compile('Y2C.png')):
            zawodnik.zolta += 1
            zawodnik.czerwona += 1
            time = str(zoltaczerwona.parent).split(" ")[-1].split("'")[0]
            time = sum_to_number(time)
            zawodnik.time_out = time

        for czerwona in zawodniktr.findAll('img',src=re.compile('RC.png')):
            zawodnik.czerwona += 1
            time = str(czerwona.parent).split(" ")[-1].split("'")[0]
            time = sum_to_number(time)
            zawodnik.time_out = time

        # goal
        for gol in zawodniktr.findAll('img',src=re.compile('/G.png')):
            zawodnik.gole += 1

            time = str(gol.parent).split(" ")[-1].split("'")[0]
            time = sum_to_number(time)
            if nr_teamu == mecz_wpis.druzyna1_id:
                l = mecz_wpis.goals1
            else:
                l = mecz_wpis.goals2
            l.append(str(time))

        # penalty
        for golp in zawodniktr.findAll('img',src=re.compile('PG.png')):
            zawodnik.gole += 1
            zawodnik.gole_p += 1

            time = str(golp.parent).split(" ")[-1].split("'")[0]
            time = sum_to_number(time)
            if nr_teamu == mecz_wpis.druzyna1_id:
                l = mecz_wpis.goals1
            else:
                l = mecz_wpis.goals2
            l.append(str(time))
    
        # own goal
        for golcz in zawodniktr.findAll('img',src=re.compile('OG.png')):
            zawodnik.gole_czerwone += 1

            time = str(golcz.parent).split(" ")[-1].split("'")[0]
            time = sum_to_number(time)
            if nr_teamu == mecz_wpis.druzyna1_id:
                l = mecz_wpis.goals2
            else:
                l = mecz_wpis.goals1
            l.append(str(time))

        statistics_results[zawodnik.id_zawodnika] = zawodnik

def parse_finished_match(mecz, matchids = None):
    td = mecz.find('td',{'class':'score-time'})
    mecz_link = td.find('a')
    
    mecz_wpis = Match()
    mecz_wpis.id = mecz_link.get('href').split('/')[-2]
    
    # Only a match with given ID
    if not matchids or not (mecz_wpis.id in matchids):
        return (None,None)

    print 'Match %s found' % mecz_wpis.id


    mecz_wpis.druzyna1_id = mecz.find('td',{'class':'team team-a '}).find('a').get('href').split('/')[-2]
    mecz_wpis.druzyna2_id = mecz.find('td',{'class':'team team-b '}).find('a').get('href').split('/')[-2]
    mecz_wpis.data = mecz.find('td',{ 'class':'date no-repetition'}).text

    # pobieranie szczegolow na temat meczu: statistics zawodnikow.
    link = "http://us.soccerway.com" + mecz_link.get('href')
    strona_meczu_szczegolowa = download_page(link,"match-"+mecz_wpis.id+".html")
    strona_meczu_szczegolowa = BeautifulSoup(strona_meczu_szczegolowa)

    labels = strona_meczu_szczegolowa.findAll('dt')
    values = strona_meczu_szczegolowa.findAll('dd')

    for nr, label in enumerate(labels):
        if label.text == "Kick-off":
            mecz_wpis.time = values[nr].text.strip()
        if label.text == "Game week":
            mecz_wpis.gameweek = values[nr].text.strip()

    if mecz.find('td',{'class':'score-time score'}):
        print "Downloading results"
        mecz_wpis.result= mecz.find('td',{'class':'score-time score'}).text.replace('  ','').replace('\n','')
        results_meczu = strona_meczu_szczegolowa.find('div',{'class':'content-column'})

        if results_meczu:
            for nr_teamu, table in enumerate(results_meczu.findAll('table',{'class':'playerstats'})):
                team_id = None
                if nr_teamu % 2 == 0:
                    team_id = strona_meczu_szczegolowa.findAll('h3', {'class':'thick'})[0].find('a').get('href').split('/')[-2]
                else:
                    team_id = strona_meczu_szczegolowa.findAll('h3', {'class':'thick'})[2].find('a').get('href').split('/')[-2]
                parse_stats(table, mecz, team_id, nr_teamu // 2, mecz_wpis)
        
    return (mecz_wpis, [])
    
def parse_matches_page(page, matchids = None):
    matches_results = []
    
    print "Parsing match list."
    if matchids:
        print "Searching for " + str(matchids)
#    matches_strona = #download_page('http://us.soccerway.com/national/poland/ekstraklasa/20132014/regular-season/r21498/matches/',"matches.html")

    url = "http://int.soccerway.com/a/block_competition_matches?block_id=page_competition_1_block_competition_matches_6&"
    url = url + urllib.urlencode({'callback_params': '{"page":' + page.__str__() + ',"bookmaker_urls":{"5":"http://ad.doubleclick.net/clk;275749130;101895884;r?http://www.paddypower.com/bet?AFF_ID=10078407","13":"http://www.bet365.com/home/?affiliate=365_182137","43":"http://serve.williamhill.com/promoRedirect?member=soccercom&campaign=DEFAULT&channel=odds&zone=1478396557&lp=1478396550","117":"http://new.betway.com/en/2013/lp/75/sports-betting-soccerway/?a=845521847398582"},"block_service_id":"competition_matches_block_competitionmatches","round_id":25643,"outgroup":false,"view":2}'})
    url = url + "&action=changePage&"
    url = url + urllib.urlencode({'params': '{"page":' + (page-1).__str__() + '}'})

    matches_strona = download_page(url)
    d = json.loads(matches_strona)
    matches_strona = d['commands'][0]['parameters']['content']

#download_page('http://us.soccerway.com/national/poland/ekstraklasa/20132014/regular-season/r21498/matches/',"matches.html")

    matches = re.search(re.compile('<table class="matches   ">(.*?)</table>',re.DOTALL),matches_strona).group(0)
    matches = BeautifulSoup(matches)
    for mecz in matches.find('tbody').findAll('tr'):
        
        mecz_wpis, stats = parse_finished_match(mecz, matchids = matchids)
        if (mecz_wpis != None):
            matches_results.append(mecz_wpis)

    return (matches_results,statistics_results)

def parse_matches(matchids = None):
    matches_results = []
    
    k = 0
    R = range(0,5)
    R.extend(range(-1,-5,-1))
    for i in R:
        mar, sar = parse_matches_page(i, matchids = matchids)
        matches_results.extend(mar)

        if mar:
            k += 1
        if matchids and k == len(matchids):
            break
        
    return (matches_results,statistics_results)
