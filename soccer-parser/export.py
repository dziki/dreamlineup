import codecs
import sys

def export_teams(druzyny_result):
    xml = open("output/teams.xml", "w")
    xml.write('<?xml version="1.0" encoding="UTF-8"?>\n')

    xml.write("<druzyny>\n")
    for druzyna in druzyny_result:
        nazwa = unicode(druzyna.nazwa)
        towrite = '\t<druzyna>\n\t\t<id>%s</id>\n\t\t<nazwa>%s</nazwa>\n\t</druzyna>\n' % (druzyna.id,nazwa)
        xml.write(towrite.encode('utf-8'))
    xml.write('</druzyny>\n')
    xml.close()

def export_athletes(athletes_result):
    xml = codecs.open("output/athletes.xml", "w", "utf-8")
    xml.write('<?xml version="1.0" encoding="UTF-8"?>\n')

    xml.write("<athletes>\n")
    for zawodnik in athletes_result:
        xml.write('\t<z>\n\t\t<id>%s</id>\n\t\t<id_druzyny>%s</id_druzyny>\n\t\t<dane>' % (zawodnik.id,zawodnik.id_druzyny))
        for dana in zawodnik.dane_personalne:
#            xml.write('\n\t\t\t<d><n>%s</n><w>%s</w></d>' % (dana[0].encode('UTF-8'),dana[1].encode('UTF-8')))
            xml.write('\n\t\t\t<d><n>%s</n><w>%s</w></d>' % (dana[0],dana[1]))
        xml.write('\n\t\t</dane>\n\t</z>\n')
    xml.write('</athletes>\n')
    xml.close()

def export_matches(matches_results):
    xml = codecs.open("output/matches.xml", "w")
    xml.write('<?xml version="1.0" encoding="UTF-8"?>\n')

    xml.write('<matches>\n')
    for mecz in matches_results:
        xml.write('\t<mecz>\n\t\t<id>%s</id>\n\t\t<druzyna1_id>%s</druzyna1_id>\n\t\t<druzyna2_id>%s</druzyna2_id>\n\t\t<data>%s</data>\n\t\t<time>%s</time>\n\t\t<round>%s</round>\n\t\t<result>%s</result>\n\t\t<goals1>%s</goals1>\n\t\t<goals2>%s</goals2>\n\t</mecz>\n' % (mecz.id,mecz.druzyna1_id,mecz.druzyna2_id,mecz.data,mecz.time,mecz.gameweek,mecz.result,",".join(mecz.goals1),",".join(mecz.goals2)))
    xml.write('</matches>\n')

def export_stats(statistics_results):
    xml = codecs.open("output/stats.xml", "w")
    xml.write('<?xml version="1.0" encoding="UTF-8"?>\n')

    xml.write('<statistics_zawodnikow>\n')
    for s in statistics_results:
        xml.write('\t<statystyka>\n\t\t<id_meczu>%s</id_meczu>\n\t\t<id_zawodnika>%s</id_zawodnika>\n\t\t<id_druzyny>%s</id_druzyny>\n\t\t<shirtnumber>%s</shirtnumber>\n\t\t<gole>%s</gole>\n\t\t<gole_czerwone>%s</gole_czerwone>\n\t\t<gole_p>%s</gole_p>\n\t\t<substituted>%s</substituted>\n\t\t<zolta>%s</zolta>\n\t\t<czerwona>%s</czerwona>\n\t\t<time_in>%s</time_in>\n\t\t<time_out>%s</time_out>\n\t</statystyka>\n' % (s.id_meczu,s.id_zawodnika,s.id_druzyny,s.shirtnumber,s.gole,s.gole_czerwone,s.gole_p,s.substituted,s.zolta,s.czerwona,s.time_in,s.time_out))
    xml.write('</statistics_zawodnikow>\n')

    xml.close()
