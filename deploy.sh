# CONFIGURE
if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit 0
fi

if [ "$1" == "production" ];
  then
    USER=dlu
    BASE=dlu
    PASS=alfa44
    SUBDIR=ROOT
    echo "PRODUCTION"
  else
    USER=dludev
    BASE=dludev
    PASS=alfa44
    SUBDIR=dev
    echo "DEBUG"
fi

# DOWNLOAD THE LATEST VERSION
git pull

# PUT DATABASE PASSWORDS
cat /var/dreamlineup/src/main/resources/hibernate.cfg.template.xml | sed -e "s/[$]USER[$]/${USER}/" |
sed -e "s/[$]BASE[$]/${BASE}/" | sed -e "s/[$]PASS[$]/${PASS}/" > /var/dreamlineup/src/main/resources/hibernate.cfg.xml

# BUILD
mvn clean
mvn compile war:war

# STOP TOMCAT
/etc/init.d/tomcat7 stop

# COPY THE NEW VERSION
cp target/dreamlineup-com.lemonet.dreamball.war /var/lib/tomcat7/webapps/${SUBDIR}.war
rm /var/lib/tomcat7/webapps/${SUBDIR} -r

echo "Deploying under /var/lib/tomcat/webapps/${SUBDIR}"

# START TOMCAT
/etc/init.d/tomcat7 start